/********
 * SimpleQuick -- visualization of Quicksort
 *
 * Compile: javac Quick.java
 *
 * Usage: java Quick n
 * where n is the size of the array to be sorted (values of n <= 75 are
 * best for displaying the sort results)
 *
 * @version	3 Feb 2014
 *********/

public class SimpleQuick {

    public static void main(String[] args) {
        // Input size of array from command line:
        int n = Integer.parseInt(args[0]);
        
        // Create a random array of size n:
        int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(1000);
        }
        sort(a);
    }

    // Quicksort -- see textbook, pp 289-291
    public static void sort(int[] a) {
        // First SHUFFLE the array! Yes, scramble it up!
        // Quicksort works best on a random array; already
        // sorted or nearly-sorted arrays produce WORST case
        // behavior.
        StdRandom.shuffle(a);
        sort(a,0,a.length-1);
    }

    // Sort a between positions lo, ... hi
    public static void sort(int[] a, int lo, int hi) {
        if (hi <= lo) return; // array size 0 or 1--nothing to do

        int j = partition(a,lo,hi); // split array around index j
        sort(a,lo,j-1);  // sort values left of j
        sort(a,j+1,hi);  // sort values right of j
    }

    // Partition an array (between positions lo and hi) so that
    // all value on the left are less than or equal to the 
    // value at the partition point and all values on the right
    // are greater than or equal to the value at the partition point.
    //
    // Return the partition point (also called pivot point)
    public static int partition(int[] a, int lo, int hi) {
        int i = lo, j = hi+1; // i, j move towards each other
        int v = a[lo];        // partition around a[lo]
        while (true) {
            while (a[++i] < v)
                if (i==hi) break;
            while (v < a[--j])
                if (j==lo) break;
            if (i >= j) break;
            exch(a,i,j);
	    show(a);
        }
        exch(a,lo,j);
        show(a);
        return j;
    }
    
    // exchange a[i] and a[j] (adapted from program "Merge.java")
    private static void exch(int[] a, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }
    
    /**
     * show -- display a graph of vertical bars, one per array value.
     *
     * @param a an array of integers
     */
    public static void show(int a[]) {
        StdDraw.clear(StdDraw.WHITE);
        StdDraw.setYscale(0,1000);
        StdDraw.setXscale(0,2*a.length); // double width (allow space btwn bars)
        StdDraw.setPenRadius(.01); // can be changed according to taste!
        StdDraw.show(0);
        
        for (int i = 0; i < a.length; i++) {
            StdDraw.line(2*i,0,2*i,a[i]);
        }
        StdDraw.show(100);
    }
}
