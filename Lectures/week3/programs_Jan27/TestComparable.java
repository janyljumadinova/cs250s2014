/**
 * TestComparable -- illustrates use of the "Comparable" interface
 * for predefined Java classes String, int, double, char. Given three
 * objects of one of these types, calls a method that prints the objects
 * in sorted order. The method parameters are all of the type
 * Comparable, making it general.
 *
 * Compiling: javac TestComparable.java
 *   This will result in a warning about unchecked or unsafe
 *   operations because the "Comparable" class is more safely used with
 *   a generic type indicator "<...>"
 *
 * Usage: java TestComparable
 *
 * 
 * @version 17 Sept. 2013
 */
public class TestComparable {

    /*****************
     * "main" simply inputs values of several types and calls
     * the "order" method for each collection of three objects.
    *****************/
    public static void main(String[] args) {
        String s1, s2, s3;
        int i1, i2, i3;
        char c1, c2, c3;
        double d1, d2, d3;

        StdOut.print("Enter three words: ");
        s1 = StdIn.readString();
        s2 = StdIn.readString();
        s3 = StdIn.readString();

        StdOut.print("Enter three integers: ");
        i1 = StdIn.readInt();
        i2 = StdIn.readInt();
        i3 = StdIn.readInt();

        StdOut.print("Enter three doubles: ");
        d1 = StdIn.readDouble();
        d2 = StdIn.readDouble();
        d3 = StdIn.readDouble();

        StdIn.readLine(); // needed to skip over newline char
        StdOut.print("Enter three characters: ");
        c1 = StdIn.readChar();
        c2 = StdIn.readChar();
        c3 = StdIn.readChar();

        order(s1,s2,s3);
        order(i1,i2,i3);
        order(d1,d2,d3);
        order(c1,c2,c3);
    }


    /*****************
     * order: one method can be used for many different kinds of
     * data, as long as all of them implement the "Comparable"
     * interface:
    *****************/
    public static void order(Comparable a, Comparable b, Comparable c) {
      Comparable temp; // used for swapping

      if (a.compareTo(b) > 0) {
        temp = a; a = b; b = temp;
      }
      if (a.compareTo(c) > 0) {
        temp = a; a = c; c = temp;
      }
      if (b.compareTo(c) > 0) {
        temp = b; b = c; c = temp;
      }
      StdOut.println(a+" "+b+" "+c);
    }
}
