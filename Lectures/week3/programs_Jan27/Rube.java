import java.util.Arrays;

public class Rube implements Comparable<Rube> {
    private String[] components;

    public Rube(String[] components) {
        this.components = Arrays.copyOf(components,components.length);
    }
    
    public int compareTo(Rube other) {
        int i = 0;
        while (i < components.length && i < other.components.length) {
            int result = components[i].compareTo(other.components[i]);
            if (result == 0) // keep looking
                i++; 
            else 
                return result;
        }
        // arrays were equal all the way out to the end of the smaller one
        return ((Integer)(components.length)).compareTo(other.components.length);
    }
    
    public String toString() {
        String result = "";
        for (int i = 0; i < components.length; i++)
            result += components[i] + " ";
        return result;
    }
}