/**
 * TestComparable2 -- illustrates use of the "Comparable" interface
 * for user-defined Java classes. Given three objects of the "Rube"
 * class, print them out in order. Here, "order" is defined by the
 * "compareTo" method in the Rube class.
 *
 * Compiling: javac TestComparable2.java
 *   This will result in a warning about unchecked or unsafe
 *   operations because the "Comparable" class is more safely used with
 *   a generic type indicator "<...>"
 *
 * Usage: java TestComparable2
 *
 * 
 * @version 17 Sept. 2013
 */
public class TestComparable2 {

    /*****************
     * "main" simply inputs values of several types and calls
     * the "order" method for each collection of three objects.
    *****************/
    public static void main(String[] args) {
        Rube inv1, inv2, inv3;

        String[] c = In.readStrings("inv1.txt");      
        inv1 = new Rube(c);
        StdOut.println("First invention uses "+inv1);
        
        c = In.readStrings("inv2.txt");
        inv2 = new Rube(c);
        StdOut.println("Second invention uses "+inv2);
        
         c = In.readStrings("inv3.txt");
        inv3 = new Rube(c);
        StdOut.println("Third invention uses "+inv3);
        
        StdOut.println("After calling order:");
        order(inv1, inv2, inv3);
    }


    /*****************
     * order: one method can be used for many different kinds of
     * data, as long as all of them implement the "Comparable"
     * interface:
    *****************/
    public static void order(Comparable a, Comparable b, 
                             Comparable c) {
      Comparable temp; // used for swapping

      if (a.compareTo(b) > 0) {
        temp = a; a = b; b = temp;
      }
      if (a.compareTo(c) > 0) {
        temp = a; a = c; c = temp;
      }
      if (b.compareTo(c) > 0) {
        temp = b; b = c; c = temp;
      }
      StdOut.println(a);
      StdOut.println(b);
      StdOut.println(c);
    }
}
