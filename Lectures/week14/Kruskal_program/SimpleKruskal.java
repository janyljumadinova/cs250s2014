/**
 * Simplified version of Kruskal's Algorithm for finding the minimum
 * spanning tree of an edge-weighted graph. A more sophisticated and
 * efficient algorithm is given in the textbook as KruskalMST.java.
 * The algorithm in the book finds a minimum weighted spanning FOREST
 * of a possibly-disconnected graph. This program works only for
 * connected graphs since it depends on the theorem that a spanning
 * tree for a connected graph with V vertices has V-1 edges.
 *
 * Usage: java SimpleKruskal filename
 *
 * where filename is the name of a file containing an edge-weighted
 * graph.
 *
 * @version April 12, 2014
 */
import java.util.Arrays;

public class SimpleKruskal {
   /**
    * Main just reads in the filename from the command line and
    * calls the minimum spanning tree method.
    */
   public static void main(String[] args) {
      EdgeWeightedGraph g = new EdgeWeightedGraph(new In(args[0]));
      StdOut.println("Graph:\n" + g);
      printMST(g);
   }

   /**
    * Prints out the edges of a minimum spanning tree for 
    * weighted graph g. If g is not connected, prints an
    * error message.
    *
    * @param g an edge-weighted graph
    */
   public static void printMST(EdgeWeightedGraph g) {
      // The algorithm below works only for connected graphs,
      // so call a method to see if g is connected:
      if (!connected(g)) {
         StdOut.println("Graph is not connected; no minimum spanning tree");
         return;
      }

      // Place all edges into a priority queue:
      MinPQ<Edge> pq = new MinPQ<Edge>();
      for (int v = 0; v < g.V(); v++) {
         for (Edge e : g.adj(v)) {
            pq.insert(e);
         }
      }

      // Place all vertices into a union-find data structure:
      UF forest = new UF(g.V());

      // Count number of edges added to spanning tree so far:
      int edgeCount = 0;
      while (edgeCount < g.V()-1) {
         Edge e = pq.delMin();
         int v = e.either();
         int w = e.other(v);
         if (forest.connected(v,w))
            continue;
         StdOut.println("Using edge "+e);
         forest.union(v,w);
         edgeCount++;
      }
   }
                  
   /**
    * Uses depth-first search to see if g is connected.
    *
    * @param g a graph
    */
   public static boolean connected(EdgeWeightedGraph g) {
      boolean marked[] = new boolean[g.V()];
      Arrays.fill(marked,false);
      dfs(g,0,marked);
      int count = 0;
      for (int i = 0; i < g.V(); i++)
         if (marked[i])
            count++;
      return (count == g.V());
   }

   /**
    * Depth first search, modifed for EdgeWeightedGraphs.
    *
    * @param g an edge-weighted graph
    * @param v vertex from which the dfs proceeds
    * @param marked boolean array to keep track of visited vertices
    */
   public static void dfs(EdgeWeightedGraph g, int v, boolean[] marked) {
      marked[v] = true;
      for (Edge e : g.adj(v)) {
         int w = e.other(v);
         if (!marked[w])
            dfs(g,w,marked);
      }
   }
}
