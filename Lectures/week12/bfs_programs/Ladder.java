/**
 * Word ladders are a type of puzzle in which the object is to
 * convert one word into another one by changing a single letter
 * at each step. Every intermediate step must be a real word.
 *
 * Ladder reads in a (far-from-complete) list of three-letter words
 * and takes two three-letter words from the command line. It
 * then prints out the sequence of steps if one exists.
 *
 * @version	April 3, 2014
 */

import java.util.ArrayList;

public class Ladder {
    public static void main(String[] args) {
       // Read in the dictionary of three-letter words:
        String[] words = In.readStrings("three-letter-words.txt");

       // Get starting and ending words from command line:
        String start = args[0];
        String stop = args[1];

       // Create a graph based on the word list;
       // determine which nodes correspond to the start and stop words:
        Graph g = new Graph(words.length);
        int v=-1, w=-1;

        for (int i = 0; i < g.V(); i++) {
            if (words[i].equals(start)) v = i; // is this the start word?
            if (words[i].equals(stop)) w = i;  // is this the stop word?

           // Add an edge if two words differ by one letter:
            for (int j = i+1; j < g.V(); j++) {
                if (dist(words[i],words[j]) == 1)
                    g.addEdge(i,j);
            }
        }             

       // Make sure both words appear:
        if (v == -1) {
            StdOut.println(start + " is not in the word list");
            return;
        }
        if (w == -1) {
            StdOut.println(stop + " is not in the word list");
            return;
        }

       // Find a path from start to stop:
        ArrayList<Integer> path = ShortestPath.shortestPath(g,v,w);
        if (path == null)
           StdOut.println("No path from " + start + " to " + stop);
        else {
           for (int i = path.size()-1; i >= 0; i--) {
              StdOut.println(words[path.get(i)]);
           }
        }
    }

    // Returns number of positions where a differs from b.
    // Assumes a and b are strings of length 3.
    public static int dist(String a, String b) {
        int d = 0;
        for (int i = 0; i < 3; i++)
            if (a.charAt(i) != b.charAt(i)) 
                d++;
        return d;
     }
}
