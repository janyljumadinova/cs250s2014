/**
 * Simple implementation of a breadth-first search; for a more
 * detailed implementation see textbook.
 *
 * @version	April 4, 2014
 */

import java.util.Arrays;

public class SimpleBFS {

    // Main just reads in a graph and calls bfs:
    public static void main(String[] args) {
        Graph g = new Graph(new In(args[0]));
        StdOut.println("Graph: "+g);
        bfs(g,0);
    }

    // Breadth-first search of graph g from vertex v:
    public static void bfs(Graph g, int v) {
       // Nodes adjacent to those already visited, queued up:
        Queue<Integer> queue = new Queue<Integer>();
        queue.enqueue(v);

       // Keeps track of nodes already visited or waiting in the queue:
        boolean[] marked = new boolean[g.V()];
        Arrays.fill(marked,false);

        marked[v] = true;
        while (!queue.isEmpty()) {
            int w = queue.dequeue();
            StdOut.println("Visiting node " + w);

            // Add neighbors of w to the queue:
            for (int x : g.adj(w)) {
                if (!marked[x]) {
                    queue.enqueue(x);
                    marked[x] = true;
                }
            }
        }
    }
}
