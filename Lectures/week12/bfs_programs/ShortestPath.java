/**
 * ShortestPath -- demonstration of breadth first search.
 *      Given a graph g and two vertices start and stop,
 *      find a path in g from start to stop that has the minimum
 *      number of edges.
 *
 *  Usage:   java ShortestPath filename start stop
 *
 * @version	April 3, 2014
 */

import java.util.Arrays;
import java.util.ArrayList;

public class ShortestPath {

    public static void main(String[] args) {
       // Read in the graph and the start and stop vertices:
        Graph g = new Graph(new In(args[0]));
        int start = Integer.parseInt(args[1]);
        int stop = Integer.parseInt(args[2]);

       // Obtain the shortest path:
        ArrayList<Integer> path = shortestPath(g,start,stop);

       // Print the answer:
        if (path == null)
           StdOut.println("No path from " + start + " to " + stop);
        else {
            // Print out the arraylist in reverse:
            StdOut.println("Shortest path from " + start + " to " + stop + ":");
            for (int i = path.size()-1; i >= 0; i--) {
               StdOut.println(path.get(i));
            }
        }
    }

    /**
     * Return an arraylist containing a shortest path (in reverse order)
     * in graph g between vertices v and w.
     *
     * @param	g	the graph to be used
     * @param	v	the starting vertex for the path
     * @param	w	the ending vertex for the path
     * @return		an arraylist containing the reverse path
     */
    public static ArrayList<Integer> shortestPath(Graph g, int v, int w) {
        boolean[] marked = new boolean[g.V()];
        Arrays.fill(marked,false);

        int[] parent = new int[g.V()]; // BFS path information
        Arrays.fill(parent,-1);

        Queue<Integer> queue = new Queue<Integer>();

        queue.enqueue(v);
        marked[v] = true;
        while (!queue.isEmpty()) {
            int x = queue.dequeue();
            if (x == w) break;
            for (int y : g.adj(x)) {
                if (!marked[y]) {
                    parent[y] = x;
                    queue.enqueue(y);
                    marked[y] = true;
                }
            }
        }

        // Build the path from w back to v by following parent pointers:
        ArrayList<Integer> path = new ArrayList<Integer>();
        int last = w;

        while (parent[last] != -1) {
            path.add(last);
            last = parent[last];
        }

        // Did we end up at v?
        if (last != v)
            return null;

        // Yes--finish path and return it
        path.add(v);
        return path;
    }
}
