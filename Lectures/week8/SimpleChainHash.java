/**
 * This is a greatly simplified version of the "separate chaining"
 * hash symbol table. It is good for only about 50,000 distinct keys.
 * Much of this code is taken directly from the more elaborate
 * implementation by Sedgewick and Wayne, "SeparateChainingHashST.java"
 *
 * This version supports only a fixed size list of chains; it does
 * not include methods for "size", "isEmpty", "keys", etc.
 *
 * @version 	22 Oct. 2012
 */
import java.util.ArrayList;

public class SimpleChainHash<Key,Value> {
    private SequentialSearchST<Key,Value>[] st;
    private final int M = 131071;

    public SimpleChainHash() {
        st = (SequentialSearchST<Key,Value>[])new SequentialSearchST[M];
        
        for (int i = 0; i < M; i++)
            st[i] = new SequentialSearchST<Key,Value>();
    }

    public int hash(Key key) {
        return (key.hashCode() & 0x7ffffff) % M;
    }

    public void put(Key key, Value val) {
        int i = hash(key);
        st[i].put(key,val);
    }

    public Value get(Key key) {
        int i = hash(key);
        return st[i].get(key);
    }

    public void delete(Key key) {
        int i = hash(key);
        st[i].delete(key);
    }
}
