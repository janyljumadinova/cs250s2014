/**
 * Test program for the SimpleLinearProbing symbol table.
 *
 * Reads in text and creates a symbol table of words and their
 * frequencies; then tests the table with a number of common and
 * not so common words.
 *
 * @version	22 October 2012
 */

public class TestSimpleLinearProbing {
    public static void main(String[] args) {
        SimpleLinearProbing<String,Integer> wordfreq = 
                       new SimpleLinearProbing<String,Integer>();
        String query[] = {"and","of","not","or","the","in","for","but",
          "except","if","go","name","have","has","had","will","be","over",
          "above","below","love","hate","self","miserable","syzygy",
          "fahrvergnugen","beetle","zelkova","write","mysterious","admirable",
          "poindexter","carton","cartoon","paris","london","timbuktu"};


        while (!StdIn.isEmpty()) {
            String word = StdIn.readString();
            Integer f = wordfreq.get(word);
            if (f == null)
                 wordfreq.put(word,1);
            else
                 wordfreq.put(word,f+1);
        }

       Stopwatch timer = new Stopwatch();
       for (int j = 0; j < 10000; j++) {
        for (int i = 0; i < query.length; i++) {
            String q = query[i];
            //System.out.println(q+" appears "+wordfreq.get(q)+" times.");
            wordfreq.get(q);
        }
       }
       System.out.println("Search: " +timer.elapsedTime());
           
    }
}
