/**
 * This is a greatly simplified version of the "linear probing"
 * hash symbol table. It is good for only about 50,000 distinct keys.
 * Much of this code is taken directly from the more elaborate
 * implementation by Sedgewick and Wayne, "LinearProbingHashST.java"
 *
 * This version supports only a fixed size list of chains; it does
 * not include methods for "size", "isEmpty", "keys", etc.
 *
 * @version 	24 Oct. 2012
 */

public class SimpleLinearProbing<Key, Value> {

    private int M = 131071;  // size of linear probing table
    private Key[] keys;      // the keys
    private Value[] vals;    // the values


    // create linear proving hash table
    public SimpleLinearProbing() {
        keys = (Key[]) new Object[M];
        vals = (Value[]) new Object[M];
    }

    // hash function for keys - returns value between 0 and M-1
    private int hash(Key key) {
        return (key.hashCode() & 0x7fffffff) % M;
    }


    // insert the key-value pair into the symbol table
    public void put(Key key, Value val) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (keys[i].equals(key)) { 
                vals[i] = val; 
                return; 
            }
        }
        keys[i] = key;
        vals[i] = val;
    }

    // return the value associated with the given key, null if no such value
    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % M) 
            if (keys[i].equals(key))
                return vals[i];
        return null;
    }

    // delete the key (and associated value) from the symbol table
    public void delete(Key key) {
        if (get(key)==null) return;

        // find position i of key
        int i = hash(key);
        while (!key.equals(keys[i])) {
            i = (i + 1) % M;
        }

        // delete key and associated value
        keys[i] = null;
        vals[i] = null;

        // rehash all keys in same cluster
        i = (i + 1) % M;
        while (keys[i] != null) {
            // delete keys[i] an vals[i] and reinsert
            Key keyToRehash = keys[i];
            Value valToRehash = vals[i];
            keys[i] = null;
            vals[i] = null;
            put(keyToRehash, valToRehash);
            i = (i + 1) % M;
        }
    }
}
