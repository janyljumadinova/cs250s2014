/**
 * A "naive" implementation of topological sorting based on finding
 * vertices of indegree 0 and deleting them.
 * 
 * For an improved algorithm, see textbook's implementation based on
 * "reverse postorder of DFS forest".
 *
 * Usage:   java NaiveTopSort graphfilename
 *
 * @version April 5, 2014
 */

import java.util.Arrays;

public class NaiveTopSort {

    public static void main(String[] args) {
        Digraph g = new Digraph(new In(args[0]));
        topsort(g);
    }

    /**
     * Print vertices of DAG g in topological sort order.
     *
     * @param g the DAG
     */
    public static void topsort(Digraph g) {
        int[] indegree = new int[g.V()];       // #incoming edges for each v
        boolean[] marked = new boolean[g.V()]; // which vertices have been used
        Arrays.fill(indegree,0);
        Arrays.fill(marked,false);

        // Calculate indegree for each vertex:
        for (int v = 0; v < g.V(); v++) {
            for (int w : g.adj(v)) {
                indegree[w]++;
            }
        }

        // Repeatedly find and "remove" vertices of indegree 0;
        // "removing" means "mark vertex and delete outgoing edges"
        for (int i = 0; i < g.V(); i++) {
            int v = findZero(indegree,marked);
            if (v < 0) {
               StdOut.println("\nError--not a DAG. Cycle found.");
               return;
            }
            marked[v] = true;
            StdOut.print(v+" ");

            // Now reduce indegrees of all adjacent vertices:
            for (int w : g.adj(v)) {
                indegree[w]--;
            }
        }
        StdOut.println();
    }

    /**
     * Search array indegree for first unmarked vertex with indegree
     * zero and return its location
     *
     * @param indegree indegrees of vertices in current graph
     * @param marked  indicates already-visited vertices
     * @return   location of first unmarked zero
     */
    public static int findZero(int indegree[], boolean marked[]) {
        for (int i = 0; i < indegree.length; i++) {
            if (!marked[i] && indegree[i] == 0)
                return i;
        }
        return -1; // should not happen if graph is a DAG
    }
}
