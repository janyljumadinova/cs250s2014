/**
 * Demonstrates topological sort using depth first search and
 * reverse postorder traversal
 *
 * Usage:   java DFSTopSort graphfilename
 *
 * @version April 5, 2014
 */
import java.util.Arrays;

public class DFSTopSort {
    private static boolean marked[];
    private static Stack<Integer> stack;
    private static boolean onstack[]; // needed to check for cycles

    public static void main(String[] args) {
        Digraph g = new Digraph(new In(args[0]));
        marked = new boolean[g.V()];
        Arrays.fill(marked,false);
        stack = new Stack<Integer>();
        onstack = new boolean[g.V()];
        Arrays.fill(onstack,false);

        for (int v = 0; v < g.V(); v++) {
            if (!marked[v])
                dfs(g,v);
        }
        for (int v : stack) {
           StdOut.print(v+" ");
        }
        StdOut.println();
    }

    /**
     * depth first search of digraph g from vertex v; also stores
     * visited vertices in a stack for topological sorting
     *
     * @param g the digraph to be searched
     * @param v the starting vertex
     */
    public static void dfs(Digraph g, int v) {
        marked[v] = true;
        onstack[v] = true;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                dfs(g,w);
            }
            else if (onstack[w]) {
                StdOut.println("Error--cycle in graph");
                stack = new Stack<Integer>();
                return;
            }
        }
        stack.push(v);
        onstack[v] = false;
     }
}
