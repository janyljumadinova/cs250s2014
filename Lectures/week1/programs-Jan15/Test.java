/**
 * A little program testing the HashSet functionality.
 * @author Max Clive
 */

import java.util.Set;
import java.util.HashSet;

public class Test {
    public static void main(String[] args) {
        Set<String> hs = new HashSet<String>();
        String test = "Asdf";
        System.out.printf("Does the set contain my string? %b%n", (hs.contains(test)));
        System.out.printf("Adding the test string to the set.%n"); hs.add(test);
        System.out.printf("Does the set now contain my string? %b%n", (hs.contains(test)));
        System.out.printf("Does the set contain a string I didn't add? %b%n", (hs.contains("P")));
        System.out.printf("Adding a totally new string to the set.%n"); hs.add("P");
        System.out.printf("Does the set contain strings that are similar%n" +
                            "(via equals, as opposed to ===)? %b%n", (hs.contains(new String("Asdf"))));
    }
}


