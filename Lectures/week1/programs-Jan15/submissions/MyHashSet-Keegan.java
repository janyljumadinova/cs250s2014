//Keegan Shudy
//Using hash set example


import java.util.Set;
import java.util.HashSet;


public class MyHashSet {


	public static void main(String [] s){
		String find = s[0];
		HashSet<String> hs = new HashSet<String>();
		hs.add("this");
		hs.add("is");
		hs.add("a");
		hs.add("test");
		
		if(hs.contains(s[0])) 
		  	System.out.println(s[0] + " is in the hashset");
		
	}
}
