//Hashset.java
//Ryan Cambier

//7) Sometimes I have trouble understanding how methods and objects are accessed between separate programs, for example the getName() and getSpecies() methods that exist in Animal.java but are used in Queue1.java. Sometimes I attempt to do this but I get an error that cannot find the method, sorry for the vague explanation but it seems like a reference misunderstanding.


import java.util.*;

public class Hashset {

public static void main (String[] args)
{

HashSet set1 = new HashSet();

set1.add("Hello World");
set1.add("Maine, the way life should be!");
set1.add("Where is brooks?");


if (set1.contains("Hello World"))
System.out.println("Yes");
else
System.out.println("No");

if (set1.contains("Maine, the way life should be!"))
System.out.println("Yes");
else
System.out.println("No");

if (set1.contains("Where is brooks?"))
System.out.println("Yes");
else
System.out.println("No");



}


}
