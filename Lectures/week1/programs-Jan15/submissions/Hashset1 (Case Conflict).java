/**
 * HashSet1.java
 *
 * An example of using a HashSet to store strings,
 * along with some simple tests of which strings 
 * are contained in the HashSet.
 *
 * Author: Nathaniel Blake
 */

import java.util.*;

public class HashSet1 {
	public static void main(String[] args) {
		HashSet<String> hashSet = new HashSet<String>(5);

		hashSet.add("this is a test");
		hashSet.add("geronimo");

		if (hashSet.contains("this is a test"))
			System.out.println("yes");
		else
			System.out.println("no");

		if (hashSet.contains("geronimo"))
			System.out.println("yes");
		else
			System.out.println("no");

		if (hashSet.contains("gator"))
			System.out.println("yes");
		else
				System.out.println("no");
	}
}
