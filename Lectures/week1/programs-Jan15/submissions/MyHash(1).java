import java.util.*;

public class MyHash
{
	public static void main(String[] args)
	{
		HashSet<String> hash = new HashSet<String>();
		hash.add("String 1");
		hash.add("String 2");
		hash.add("String 3");
		
		if(hash.contains("String 1"))
			System.out.println("yes");
		else
			System.out.println("no");
		
		if(hash.contains("geronimo"))
			System.out.println("yes");
		else
			System.out.println("no");
			
	}
}
