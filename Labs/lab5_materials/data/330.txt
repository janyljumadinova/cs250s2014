<DOC>
<DOCID>REU010-0303.940721</DOCID>
<TDTID>TDT000837</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/21/94 04:27</DATE>
<TITLE> JAPAN ART THIEF DEALT IN PICASSOS, CHAGALLS</TITLE>
<HEADLINE> JAPAN ART THIEF DEALT IN PICASSOS, CHAGALLS</HEADLINE>
<SUBJECT> BC-JAPAN-THEFT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TOKYO (Reuter) </DATELINE>
<P>  A Japanese art dealer turned thief took advantage of his well-trained eye and went on a two-year rampage to steal works by Picasso, Chagall and other artists from homes in southern Japan, police said on Thursday. </P>
<P> Eiji Fujino, 62, sneaked into the homes of the wealthy and stole 50 pieces of art amounting to an alleged $1.25 million over two years, selling them to unspecified buyers, local police said. </P>
<P> Fujino was not fooled by fakes, police said, who added that they were trying to find out which galleries bought the stolen pieces. </P>
<P> Fujino has been charged with theft and burglary. </P>
</TEXT>
</DOC>
