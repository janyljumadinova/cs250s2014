<DOC>
<DOCID>REU003-0121.940805</DOCID>
<TDTID>TDT001511</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/05/94 12:42</DATE>
<TITLE> COLOMBIA PRESIDENT-ELECT TAKEN ILL AT CEREMONY</TITLE>
<HEADLINE> COLOMBIA PRESIDENT-ELECT TAKEN ILL AT CEREMONY</HEADLINE>
<SUBJECT> BC-COLOMBIA-SAMPER </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BOGOTA (Reuter) </DATELINE>
<P>  Colombian President-elect Ernesto Samper, who is to be sworn in Sunday, took ill Friday, almost fainting at a public ceremony, RCN radio reported. </P>
<P> The radio said Samper began to sweat, turned pale and was on the point of fainting when aides took him to a private room at the Superior Judicial Council in Bogota. His private doctor was called to attend him, RCN said. </P>
<P> Samper's office was not immediately available for comment. </P>
<P> Samper, 44, was the victim in March 1989 of an assassination attempt which almost cost him his life. He spent weeks in hospital recovering and his body still carries four of the 11 bullets fired into him. </P>
</TEXT>
</DOC>
