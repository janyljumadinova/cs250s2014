<DOC>
<DOCID>REU001-0198.940802</DOCID>
<TDTID>TDT001340</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/02/94 01:38</DATE>
<TITLE> NORTH KOREA WANTS DIALOGUE WITH SOUTH - CNN</TITLE>
<HEADLINE> NORTH KOREA WANTS DIALOGUE WITH SOUTH - CNN</HEADLINE>
<SUBJECT> BC-KOREA-NORTH </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SEOUL, South Korea (Reuter) </DATELINE>
<P>  North Korea wants to hold talks with South Korea despite the death of its ``Great Leader'' Kim Il-sung, but Seoul should avoid ``hysteria'' against the North, Cable News Network reported from Pyongyang Tuesday. </P>
<P> In a television report from the North Korean capital, CNN's Beijing correspondent Mike Chinoy said North Koreans were bitter at South Korea because of Seoul's Cold War propaganda offensive against the North. </P>
<P> ``But North Korean officials tell CNN they want talks with South Korea, that Pyongyang's position now is exactly the same as when Kim Il-sung was alive,'' he added. </P>
<P></P>
<P> ``What North Koreans say they do want from Seoul is restraint, cooling down what they describe as the hysteria in South Korea.'' </P>
<P> North Korea has denounced the South for holding Kim Il-sung responsible for the 1950-53 Korean War and banning South Koreans from mourning the death of the ``Great Leader.'' </P>
<P> One day after Kim's funeral, the Seoul government, in a bid to stop radical students from organizing memorial rallies in the South, released Soviet documents which it said proved North Korea was responsible for starting the war. </P>
<P> South Korean President Kim Young-sam received the documents from his Russian counterpart, Boris Yeltsin, during his visit to Moscow in June. </P>
<P> South Korean officials have repeatedly said Seoul still hopes to hold summit talks with North Korea once Pyongyang produces a stable leadership. </P>
<P> Shortly before the death of Kim Il-sung, the two Koreas agreed to hold an unprecedented summit, originally due July 25, but the North indefinitely postponed the meeting because of its leader's death. </P>
<P> North Korea's elite has pledged allegiance to Kim Jong-il, the eldest son and political heir of Kim Il-sung, but there has still been no formal announcement that he has succeeded to the key posts Kim Il-sung held as national president and Communist Party chief. </P>
</TEXT>
</DOC>
