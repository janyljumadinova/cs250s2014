<DOC>
<DOCID>REU010-0048.940820</DOCID>
<TDTID>TDT002248</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/20/94 15:30</DATE>
<TITLE> ARAFAT VISITS TUNIS</TITLE>
<HEADLINE> ARAFAT VISITS TUNIS</HEADLINE>
<SUBJECT> BC-MIDEAST-ARAFAT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TUNIS, Tunisia (Reuter) </DATELINE>
<P>  PLO leader Yasser Arafat arrived in the Tunisian capital Tunis Saturday for a short visit, PLO officials said. </P>
<P> Arafat, who moved his headquarters from Tunis to the Palestinian self-rule areas of Gaza and Jericho, was expected to hold talks with PLO leaders who remained in Tunisia. </P>
<P> The officials said he arrived from Geneva where he had talks with the president of the United Arab Emirates (UAE) Sheikh Zaid Bin Sultan al-Nahayan. </P>
<P> He was also due to meet Tunisian officials. </P>
</TEXT>
</DOC>
