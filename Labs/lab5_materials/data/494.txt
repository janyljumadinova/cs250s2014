<DOC>
<DOCID>REU014-0293.940730</DOCID>
<TDTID>TDT001242</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/30/94 09:35</DATE>
<TITLE> SPOKESMAN SAYS BEEF REPORT CLEARS IRISH LEADER</TITLE>
<HEADLINE> SPOKESMAN SAYS BEEF REPORT CLEARS IRISH LEADER</HEADLINE>
<SUBJECT> BC-IRELAND-BEEF </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>DUBLIN, Ireland (Reuter) </DATELINE>
<P>  A long-awaited report on alleged favoritism toward a big beef company by the Irish government has vindicated Prime Minister Albert Reynolds, his spokesman said Saturday. </P>
<P> The scandal over government links with Irish beef baron Larry Goodman, whose empire is under bankruptcy protection over export credit provisions for meat shipments to Iraq and other countries, has been hanging over the government for three years. </P>
<P> It brought down the previous government led by Charles Haughey in which Reynolds was industry and commerce minister. </P>
<P> The results of a two-year judicial investigation into the accusations were released to the government late Friday and have not yet been released to the public and the media. </P>
<P> But Reynolds's spokesman Sean Duignan, quoted in the Irish Times newspaper, said the report had found no evidence to suggest that Reynolds, then Industry and Commerce Minister, was personally close to Goodman. </P>
<P> The investigation, led by the High Court President, Justice Liam Hamilton, was the longest and most expensive inquiry in Irish history. </P>
</TEXT>
</DOC>
