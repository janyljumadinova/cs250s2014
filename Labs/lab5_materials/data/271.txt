<DOC>
<DOCID>REU009-0034.940717</DOCID>
<TDTID>TDT000668</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/17/94 18:12</DATE>
<TITLE> FRANCE WARNS RPF NOT TO ENTER RWANDAN SAFE HAVEN</TITLE>
<HEADLINE> FRANCE WARNS RPF NOT TO ENTER RWANDAN SAFE HAVEN</HEADLINE>
<SUBJECT> BC-RWANDA-FRANCE </SUBJECT>
<AUTHOR>     By Bernard Edinger </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PARIS (Reuter) </DATELINE>
<P>  France said Sunday it would no longer tolerate armed incursions by the Rwandan Popular Front (RPF) into its safe haven zone for refugees in western Rwanda. </P>
<P> The warning came after a spate of clashes between French troops and the RPF in the past 24 hours and a threat by the RPF rebels, now in control of most of Rwanda, to invade the zone. </P>
<P> ``France recalls it has a mandate from the United Nations to ensure the protection of the civilian population and no infiltration will be tolerated by any armed elements,'' a foreign ministry statement said. </P>
<P> The statement came after an emergency meeting of top French officials on the Rwandan crisis. </P>
<P> It did not say what France would do if its warning was ignored but officials earlier said privately Paris' ultimate weapon could be the use of fighter-bombers it has deployed in Zairean bases. </P>
<P> The statement said Paris condemned mortar shellings around Zaire's Goma airport which resulted in the deaths Sunday, directly or as a result of trampling, of about 60 people, most of them children. </P>
<P> The statement quoted U.N. officers as saying there was little doubt the shelling came from RPF positions. </P>
<P> The shelling caused the United Nations to suspend its humanitarian aid flights to Goma, the rear base for French troops operating in Rwanda, cutting off aid vital to the survival of millions of refugees, the statement added. </P>
<P> Paris has informed the U.N. Security Council of the situation and of the fact that U.N. demands for an end to military action in the region are being violated, it added. </P>
<P> The statement followed a threat by a spokesman for the RPF in Kigali to invade the French-guarded safe haven zone unless French forces hand over the Hutu ringleaders of the slaughter in Rwanda of some 500,000 people, mostly Tutsis. </P>
<P> ``Moving in (to the zone) is our ambition unless the French hand over the criminals,'' Major Wilson Rutiyisire, military spokesman of the Tutsi-dominated RPF, told reporters in Kigali. </P>
<P> ``If the French arrest them and hand them over, there is no need for us to move in. But we have a duty to follow up these criminals, a safe zone notwithstanding. It is our right to bring the criminals to justice,'' Rutiyisire added. </P>
<P> Leaders of the Hutu government, set up after Rwandan President Juvenal Habyarimana was killed in a rocket attack on his plane April 6, are sheltering in Cyangugu, a major town in the safe haven to which they fled Thursday. </P>
</TEXT>
</DOC>
