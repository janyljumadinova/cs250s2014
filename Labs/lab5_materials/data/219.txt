<DOC>
<DOCID>REU007-0243.940714</DOCID>
<TDTID>TDT000530</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/14/94 02:50</DATE>
<TITLE> MACAU DRAGON DANCE DECLARED GUINNESS RECORD</TITLE>
<HEADLINE> MACAU DRAGON DANCE DECLARED GUINNESS RECORD</HEADLINE>
<SUBJECT> BC-MACAU-DRAGON </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>HONG KONG (Reuter) </DATELINE>
<P>  The Guinness Book of Records has certified a one-mile traditional Chinese dragon dance performed in Macau in April as the world's longest, the Portuguese news agency Lusa said Thursday. </P>
<P> Macau, perched on China's southern coast, also holds another Guinness record -- as the world's most densely populated territory, with 20,419 people per square mile. </P>
<P> The dragon dance, commissioned to celebrate the opening of the Macau-Taipa island bridge and attract good luck, involved 2,180 people, Lusa said. </P>
</TEXT>
</DOC>
