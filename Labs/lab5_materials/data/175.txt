<DOC>
<DOCID>REU006-0108.940711</DOCID>
<TDTID>TDT000407</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/11/94 18:27</DATE>
<TITLE> NAVY LIEUTENANT CHALLENGES U.S. MILITARY GAYS POLICY</TITLE>
<HEADLINE> NAVY LIEUTENANT CHALLENGES U.S. MILITARY GAYS POLICY</HEADLINE>
<SUBJECT> BC-USA-MILITARY-GAYS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  Challenging the U.S. military's new policy on gays, lawyers for Navy Lieutenant Tracy Thorne told a Navy board Monday that his public declaration that he is gay does not prove he commits prohibited homosexual acts. </P>
<P> ``There is no proof of prohibited conduct,'' one of the lawyers, Patrick Lee, said. </P>
<P> The lawyer urged the board considering whether to discharge Thorne, a pilot, to keep him in the Navy. </P>
<P> But Navy lawyer, Lieutenant Peter Dutton, urged the board to discharge Thorne, saying that under the new policy Thorne's public declaration of homosexuality by itself shows a propensity to commit homosexual acts unless he can prove otherwise. </P>
<P> Thorne's lawyer said he will prove that the Navy lieutenant's private life has nothing to do with his conduct as an officer. </P>
<P> The new policy does not prohibit homosexuality but requires the discharge of anyone who commits homosexual acts, including a declaration of homosexuality, unless he or she can prove they do not have sex with others of the same gender. </P>
<P> Thorne first declared his homosexuality in an ABC television interview on the late-night news program ``Nightline'' in 1992. </P>
<P> Thorne's boss, Navy Commander Craig Luigart, called the lieutenant an exemplary officer, although he may have made bad judgment calls. </P>
<P> ``He has been an exemplary officer,'' Luigart testified at the hearing Monday. ``He is a hard-charging lieutenant ... He has done a very good job.'' </P>
</TEXT>
</DOC>
