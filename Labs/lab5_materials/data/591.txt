<DOC>
<DOCID>REU002-0236.940803</DOCID>
<TDTID>TDT001450</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 22:25</DATE>
<TITLE> CLINTON URGES END TO PARTISANSHIP ON HEALTH REFORM</TITLE>
<HEADLINE> CLINTON URGES END TO PARTISANSHIP ON HEALTH REFORM</HEADLINE>
<SUBJECT> BC-HEALTH-CONGRESS 2NDLD </SUBJECT>
<AUTHOR>     By Donna Smith </AUTHOR>
<TEXT>
<NOTES>(Eds: recasts with Clinton news conference, new throughout)</NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  President Clinton asked Americans Wednesday to urge Congress to put aside party politics and pass health reform that covers everyone as Republicans criticized a new Democratic plan. </P>
<P> ``I want to urge the American people to tell their senators and congressmen to put aside partisanship and think of the American people and their fundamental interest and needs,'' Clinton said. ``We have an historic opportunity, we dare not pass it up.'' </P>
<P> He used a televised prime time news conference to praise a new Democratic plan in the Senate and House saying they would achieve his goal of covering the 39 million uninsured Americans and would be less bureaucratic and provide more choice than his original health reform proposal. </P>
<P> ``Much of it has changed for the better,'' Clinton said. ''But one rock-solid principle remains: private insurance, guaranteed, for everyone.'' </P>
<P> He said he would sign into law a compromise plan by Senate Majority Leader George Mitchell that seeks to cover 95 percent of Americans by the turn of the century through market reforms, incentives for business and subsidies for low-income people. A standby mandate for employers to pay half workers' insurance costs cost could kick in if the goal is not reached. </P>
<P> ``I believe it will achieve universal coverage for all Americans,'' Clinton said of the Mitchell bill. ``I believe it does meet the objective I set out in the State of the Union address and I would sign it.'' </P>
<P> Senate Republican Leader Bob Dole of Kansas said at a news conference that lawmakers needed time to digest the 14 pound , 1,400-page Mitchell bill. He said the bill was ``a sort of government run healthcare system'' and that Republicans were willing to work with Democrats on areas of reform where they share common ground. </P>
<P> ``Why don't we go out and do those things,'' Dole said a a news conference in response to Clinton's comments. ``He says he's got to have everything. It may mean we'll do nothing. Why don't we take care of the 20 or 30 provisions that Democrats and Republicans agree on.'' </P>
<P> While the White House has been pointed to the Mitchell bill as more streamlined than Clinton's original proposal, Republicans have been painting it as a plan that will raise taxes and create more bureaucracy. </P>
<P> ``It's a one way ticket to more taxes, more deficits and more bureaucracies,'' said Texas Republican Phil Gramm. </P>
<P> They threatened to slow deliberations in the Senate until they have studied the new bill. </P>
<P> Bob Packwood likened the plan to an ``ugly'' June bug. </P>
<P> The compromise buoyed some moderate lawmakers in the House of Representatives who believe the bill to be brought to the floor next week by Richard Gephardt of Missouri, the second ranking House Democrat, is too liberal. </P>
<P> ``It's a clear improvement,'' said Representative Jim Cooper, a Tennessee Democrat. ``It forces House members to be more realistic and to start thinking about 95 percent coverage instead of 100 percent.'' </P>
<P> The House bill would force employers to pay at least 80 percent of their workers' insurance costs and would create a new Medicare program to cover the poor, unemployed and small businesses. The bill, which is scheduled to be taken up by the full House next week, appears to be short of the 218 votes needed for passage. </P>
<P> Cooper, who has been working with a small group of moderate Democrats and Republicans to forge a bipartisan plan, said his group expects to unveil its ideas Monday. </P>
</TEXT>
</DOC>
