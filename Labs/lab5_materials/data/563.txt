<DOC>
<DOCID>REU002-0025.940803</DOCID>
<TDTID>TDT001377</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 04:23</DATE>
<TITLE> TAIWAN SAYS CHINA SENDS THREE UNFRIENDLY LETTERS</TITLE>
<HEADLINE> TAIWAN SAYS CHINA SENDS THREE UNFRIENDLY LETTERS</HEADLINE>
<SUBJECT> BC-TAIWAN-CHINA </SUBJECT>
<AUTHOR>     By James Peng </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TAIPEI, Taiwan (Reuter) </DATELINE>
<P>  Taiwan said Wednesday it had received a third unfriendly letter from China in four days, but said it would not be discouraged in current talks to end disputes between the two rival Chinese governments. </P>
<P> Taipei received the latest letter from Beijing on Tuesday. The letter complained its military had opened fire on Chinese fishing boats when they entered waters around the Taiwan-held island of Quemoy, Mainland Affairs Council vice chairman Su Chi told a news conference. </P>
<P> ``It's rather unusual for a guest to send such unfriendly letters,'' Su said, referring to the presence of officials from China's Association for Relations Across the Taiwan Straits (ARATS), who completed four days of technical-level talks with Taiwan negotiators on Tuesday. </P>
<P> ``But we will not be discouraged. We will continue to talk,'' Su said. China wants to include the issue in informal discussions on travel safety. Taiwan is opposed. </P>
<P> ``It's completely irrelevant,'' Su said. ``This is a defense issue.'' He gave no details of the other letters. </P>
<P> But opposition protests when the Chinese negotiators arrived last week prompted an angry note from Beijing and a frosty response from Taipei that cooled the atmosphere at the talks. </P>
<P> Both sides have reported progress on disputes over repatriation of hijackers and illegal immigrants, and fishery issues. </P>
<P> It was unclear whether any agreement would be reached in four days of higher level meetings starting on Thursday. </P>
<P> ``If we do not hope for an agreement, why bother to talk?'' Su said. ``We hope to reach agreement, and every agreement will serve as a building block to help establish mutual trust and understanding between the two sides.'' </P>
<P> The previous four rounds ended without result after a first breakthrough meeting in April 1993 between officials of Taiwan's quasi-official Straits Exchange Foundation (SEF) that handles links in the absence of official ties and ARATS. </P>
<P> The talks are aimed at eventual reunification but Taiwan's growing political opposition advocates abandoning China and establishing an independent Taiwan. </P>
<P> The opposition Democratic Progressive Party said it would protest at the airport when Tang arrives later Wednesday. </P>
</TEXT>
</DOC>
