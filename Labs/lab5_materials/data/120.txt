<DOC>
<DOCID>REU004-0252.940708</DOCID>
<TDTID>TDT000277</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 06:34</DATE>
<TITLE> FIRECRACKER CREATES SCARE AT G7 SUMMIT</TITLE>
<HEADLINE> FIRECRACKER CREATES SCARE AT G7 SUMMIT</HEADLINE>
<SUBJECT> BC-GROUP-FIRECRACKER </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NAPLES, Italy (Reuter) </DATELINE>
<P>  Police on guard at the Group of Seven summit sent a helicopter with bomb disposal experts Friday to check out an explosion which turned out to have been caused by a firecracker. </P>
<P> Police in Naples said the explosion occurred in gardens a few miles west of the 18th century Royal Palace where leaders of the world's seven richest nations will hold their talks. </P>
<P> No one was injured and there was no damage, a police spokesman said. He said disposal experts who went to the scene by helicopter found broken glass, indicating the firecracker may have been set off in a bottle to maker the bang louder. </P>
<P> Police were keeping an open mind on whether the incident was staged by anti-summit activists or practical jokers. </P>
<P> Several thousand police and soldiers have been drafted into Naples to guard the three-day gathering, which opens later on Friday with a formal dinner in the 12th century Castel Dell'Ovo fortress that juts into the Bay of Naples. </P>
<P> Naples, southern Italy's biggest city, is the traditional home of Italy's fireworks industry. </P>
<P> Friday's dinner will be followed by a fireworks display for the seven heads of government. </P>
</TEXT>
</DOC>
