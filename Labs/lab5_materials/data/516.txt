<DOC>
<DOCID>REU001-0043.940801</DOCID>
<TDTID>TDT001303</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/01/94 07:20</DATE>
<TITLE> PHILIPPINE ARMY ON ALERT AFTER PRIEST RESCUED</TITLE>
<HEADLINE> PHILIPPINE ARMY ON ALERT AFTER PRIEST RESCUED</HEADLINE>
<SUBJECT> BC-PHILIPPINES-PRIEST 1STLD </SUBJECT>
<AUTHOR>     By Roseller Enriquez </AUTHOR>
<TEXT>
<NOTES>    (Eds: Adds military alert, businessman kidnapped)</NOTES>
<DATELINE>ZAMBOANGA, Philippines (Reuter) </DATELINE>
<P>  A kidnapped American priest rescued by Filipino Muslim guerrillas said Monday he thought he would die when a firefight erupted between the guerrillas and his Islamic fundamentalist captors. </P>
<P> The military ordered a full alert on Jolo island and tight security around Christian churches and Muslim mosques after Islamic gunmen seized Father Clarence Bertelsman Sunday while he was saying mass inside Jolo police headquarters. </P>
<P> Security has also been tightened around the airport and other facilities in Jolo, an island off the southwest tip of Mindanao that is a Moslem guerrilla stronghold. </P>
<P> The abduction of Bertelsman inside the island's main police camp has angered President Fidel Ramos and an investigation is under way into how 15 gunmen got in, officials said. </P>
<P> Bertelsman was wounded in the buttocks and left arm when guerrillas of the mainstream Moro National Liberation Front (MNLF) fired on the van carrying him and his captors after it tried to ram a checkpoint. </P>
<P> ``The thought (of being killed) crossed my mind. It was always a possibility. Many people get killed even just crossing the street,'' the 70-year-old priest from Belleville, Illinois, said from his hospital bed in Zamboanga City. </P>
<P> The military said two of the kidnappers were killed while two of his rescuers were wounded in the 15-minute gunbattle. The MNLF said it killed four kidnappers, described by the military as members of the Abu Sayyaf fundamentalist group. </P>
<P> Abu Sayyaf, blamed for a spate of kidnappings and bombings in the south in the past two years, is already holding a Filipino priest hostage in nearby Basilan island. </P>
<P> Bertelsman was abducted about the same time that four unknown gunmen seized Filipino-Chinese businessman So Kim Ching from his piggery in the southern city of Davao on Mindanao. So is a regional president of the Philippine Olympic Committee. </P>
<P> The gunmen forced So to drive away with them in his own car before abandoning it in another part of the city. </P>
<P> The boldness of the Jolo operation provoked outrage among politicians already indignant about the recent kidnapping of two children of a congressman inside his house in Manila. </P>
<P> ``After the kidnapping of the two children, this one really takes the cake,'' Senator Ernesto Maceda said. ``No wonder people continue to be afraid in their homes.'' </P>
<P> It was not clear whether the Bertelsman kidnap was an attempt by Abu Sayyaf fighers to put pressure on the military to end an attack on their strongholds. </P>
<P> Bertelsman had just finished his sermon when eight men with guns drawn walked into the chapel and told him the local governor wanted to see him. </P>
<P> ``I knew it was kidnapping because the governor would not call for me during a mass,'' he said. ``I said it's gonna be 50 to 60 days in the guimba,'' the Moslem word for mountain. </P>
<P> Bertelsman, who has spent most of his 44 years as a priest in the Philippines, was unfazed by the incident. </P>
<P> ``I am going back to Jolo,'' he said, adding his message to fellow priests was: ``Yeah, keep going, keep going. Don't pay attention to threats. We have our ministry. We have our message to give.'' </P>
<P> The military said the priest's wounds were minor but he would need surgery to remove a bullet from his buttocks. </P>
</TEXT>
</DOC>
