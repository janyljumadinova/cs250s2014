<DOC>
<DOCID>REU001-0141.940801</DOCID>
<TDTID>TDT001322</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/01/94 17:17</DATE>
<TITLE> U.S. AND CANADA REACH UNDERSTANDING ON WHEAT ROW</TITLE>
<HEADLINE> U.S. AND CANADA REACH UNDERSTANDING ON WHEAT ROW</HEADLINE>
<SUBJECT> BC-TRADE-WHEAT-KANTOR </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  After three days of tense talks, the United States and Canada agreed to limit imports of Canadian wheat into the United States for one year beginning August 1, U.S. Trade Representative Mickey Kantor said Monday. </P>
<P> The United States and Canada also agreed to establish a Joint Commission on Grains to examine over the next 12 months all aspects of each country's marketing and support systems for all grains, Kantor said. </P>
<P> The final agreement is still subject to confirmation by both governments Tuesday, Kantor said. </P>
<P> Canada has also agreed not to retaliate against the United States on this dispute for the next 12 months in what is called a ``peace clause'' agreement. </P>
<P> The agreement is the result of frantic last-minute negotiations between Ottawa and Washington following the U.S. threat to impose sanctions on imports of Canadian wheat which have surged to record highs in the last 12 months. </P>
<P> The agreement also establishes a process to address future grain trade disputes through the Joint Commission. </P>
</TEXT>
</DOC>
