<DOC>
<DOCID>REU006-0084.940811</DOCID>
<TDTID>TDT001776</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/11/94 22:47</DATE>
<TITLE> PERU FIRST LADY SAYS SHE WAS 'SILENCED'</TITLE>
<HEADLINE> PERU FIRST LADY SAYS SHE WAS 'SILENCED'</HEADLINE>
<SUBJECT> BC-PERU-SUSANA (PICTURE) </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LIMA (Reuter) </DATELINE>
<P>  Peru's First Lady Susana Higuchi said in an interview published Thursday that she was ``silenced'' after President Alberto Fujimori suspended constitutional rule for eight months in 1992. </P>
<P> In an interview with Caretas magazine, Higuchi also said key presidential adviser Vladimiro Montesinos exercised ``a great deal of power'' and his presence in the government was ``bad for Peru.'' </P>
<P> Higuchi, who abandoned Government Palace last week after protesting a law which bans her from running for office, said she learned of Fujimori's decision to suspend constitutional rule in April 1992 ``through the television.'' </P>
<P> Asked why she thought Fujimori failed to tell her he was closing Congress and the courts, she said: ``Because I would have started a revolution ... It looked (to the public) as if I didn't react, but the fact is I was silenced,'' she added. </P>
<P> Asked how she would describe Montesinos, widely believed to be the architect of Fujimori's so-called ``self-coup,'' she said: ``(I would describe him) as someone who has a great deal of power.'' ``Montesinos is not good for Peru,'' Higuchi said, and added that he should be considered dangerous. </P>
<P> Montesinos has been decribed as the de facto head of Peru's intelligence service, which Higuchi agreed with a reporter's description that it had been converted ``into a sort of secret police.'' </P>
<P> ``(The National Intelligence Service) has followed many of the people who work with me, taken them to the police and been hostile to them,'' the First Lady, who a week after she left the Palace was still staying with a former personal assistant. </P>
<P> Higuchi, who has said she may consider running for president in order to challenge the constitutionality of an electoral law that bans her from seeking office, reiterated that she did not want to be first lady for a second term. </P>
<P> Higuchi, who has insisted that her week-long separation with her husband is over ``differences of ideas'' rather than ''marital discord,'' said Fujimori's character changed after becoming president. Higuchi said her husband's rule had been ''positive'' but she differed with a policy of hand-outs to the poor that ``sometimes even includes political propaganda.'' </P>
<P> That was an apparent reference to Fujimori's custom of handing out calendars with his picture during visits to Lima shantytowns and rural areas. </P>
</TEXT>
</DOC>
