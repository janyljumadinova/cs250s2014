<DOC>
<DOCID>REU008-0029.940816</DOCID>
<TDTID>TDT001976</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/16/94 07:08</DATE>
<TITLE> HAMAS GUERRILLA WOUNDED BY ISRAELI POLICE DIES</TITLE>
<HEADLINE> HAMAS GUERRILLA WOUNDED BY ISRAELI POLICE DIES</HEADLINE>
<SUBJECT> BC-MIDEAST-ISRAEL-GUERRILLA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>JERUSALEM (Reuter) </DATELINE>
<P>  A Hamas guerrilla wounded by Israeli police in a shootout in Jerusalem on Friday died Tuesday, a police spokesman said. </P>
<P> Two other members of the militant Muslim group were shot to death in the clash. An Israeli civilian was killed in a Hamas revenge attack in Gaza on Sunday. </P>
<P> Police did not disclose the name of the guerrilla who died Tuesday. </P>
</TEXT>
</DOC>
