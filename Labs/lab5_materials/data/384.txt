<DOC>
<DOCID>REU012-0111.940725</DOCID>
<TDTID>TDT000998</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/25/94 08:23</DATE>
<TITLE> SINGAPORE CHARGES BURMESE MAN AFTER HEROIN HAUL</TITLE>
<HEADLINE> SINGAPORE CHARGES BURMESE MAN AFTER HEROIN HAUL</HEADLINE>
<SUBJECT> BC-DRUGS-SINGAPORE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SINGAPORE (Reuter) </DATELINE>
<P>  A 25-year-old Burmese man has been charged with illegal transportion of drugs after pure heroin worth $3 million was allegedly found in his baggage at Singapore's Changi Airport Saturday night. </P>
<P> It was the first heroin seizure at the airport this year and one of its biggest ever. </P>
<P> Nearly 10 pounds of heroin was found hidden in a bag carried by Aung Myat on a flight from Thailand, state television said Monday. </P>
<P> Under Singapore's tough anti-drug laws, the death sentence is mandatory for anyone found guilty of trafficking in more than half an ounce of heroin, one ounce of morphine or 18 ounces of cannabis. </P>
<P> Singapore has hanged 59 people for drug trafficking since it introduced the anti-drug laws in 1975. </P>
</TEXT>
</DOC>
