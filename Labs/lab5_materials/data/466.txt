<DOC>
<DOCID>REU013-0437.940728</DOCID>
<TDTID>TDT001177</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/28/94 20:26</DATE>
<TITLE> CANADIANS CHEATED IN BID TO AVOID SPEEDING TICKETS</TITLE>
<HEADLINE> CANADIANS CHEATED IN BID TO AVOID SPEEDING TICKETS</HEADLINE>
<SUBJECT> BC-CANADA-SCAM </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TORONTO (Reuter) </DATELINE>
<P>  Ontario drivers preparing for the introduction of photo radar in the Canadian province are being misled into buying bogus anti-radar gadgets and paying for the advice ``Don't speed!'', the Better Business Bureau said Thursday. </P>
<P> ``People are being deliberately misled,'' said Paul Tuz, president of the BBB of Toronto. ``We've had over 400 calls inquiring or complaining about these gimmicks.'' </P>
<P> Tuz said some companies that advertise in flyers and newspapers are charging up to U.S.101.31 for plate protectors the companies claim distort or hide the license plate when photographed at a specific angle. </P>
<P> He said photo radar devices, already used in the western province of Alberta, will be introduced in Ontario in September. The mechanical watchdogs snap pictures of the license plates of speeding motorists. Tickets are then mailed to the vehicle's owner. </P>
<P> Other devices, often sold at flea-markets and auto shows, block the camera's view of the licence plate or cloud it with smokey glass. </P>
<P> ``The bureau recently investigated one company that went as far as to promise a guaranteed method of avoiding photo radar tickets,'' said Tuz. ``They charge US$21.71, but their information is so good I'll share it for free - Don't speed!'' </P>
<P> In Canada, buying or selling these gadgets is not illegal, but using them could result in a $362 U.S. fine, said Tuz. </P>
</TEXT>
</DOC>
