<DOC>
<DOCID>REU013-0067.940727</DOCID>
<TDTID>TDT001083</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/27/94 04:47</DATE>
<TITLE> IN INDONESIA, GIVE BLOOD AND GET OUT OF JAIL</TITLE>
<HEADLINE> IN INDONESIA, GIVE BLOOD AND GET OUT OF JAIL</HEADLINE>
<SUBJECT> BC-INDONESIA-BLOOD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>JAKARTA (Reuter) </DATELINE>
<P>  Prisoners at an Indonesian jail are being offered the chance to win early release by donating organs or blood. </P>
<P> Marsono, chief warden at Kedungpane jail at Semarang in central Java, told Antara news agency that regular visits to the blood donor center could knock up to six months off an inmate's sentence each year. </P>
<P> The agency Wednesday quoted Marsono as saying the donation of an organ would bring an immediate six-month cut in sentence. </P>
<P> Marsono, who described the program as voluntary, said the incentive was not available to prisoners serving life terms. He did say when it was started. </P>
</TEXT>
</DOC>
