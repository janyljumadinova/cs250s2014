<DOC>
<DOCID>REU013-0243.940727</DOCID>
<TDTID>TDT001127</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/27/94 21:47</DATE>
<TITLE> UN RESOLUTION ON HAITI INVASION MOVING SLOWLY</TITLE>
<HEADLINE> UN RESOLUTION ON HAITI INVASION MOVING SLOWLY</HEADLINE>
<SUBJECT> BC-HAITI-UN </SUBJECT>
<AUTHOR>     By Evelyn Leopold </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>UNITED NATIONS (Reuter) </DATELINE>
<P>  The Clinton administration's efforts to get Security Council authorization for a U.S.-led invasion of Haiti on Wednesday ran into more difficulties than American officials anticipated, with little chance of adoption by Friday. </P>
<P> Council President Jamsheed Marker on Wednesday pointed out that no text had been formally introduced but said he hoped a resolution could be adopted by Sunday. Marker earlier had been pushing for Friday passage. </P>
<P> ``At this point in time, there is no resolution, everybody is working on it,'' he said. The United States informally distributed a text to members Tuesday but has revised it several times since then. </P>
<P> At issue is a resolution seeking approval for a multinational invasion force of more than 15,000 soldiers under American command. Its actions would be monitored by a small group of U.N. military observers and the United Nations is also to field peacekeepers after the invaders leave. </P>
<P> Differences with Washington appear to be less about the concept of an invasion but the role of the United Nations in moving in peacekeepers afterwards. </P>
<P> The U.S. text has not yet received approval from the so-called Friends of Haiti group, which serves as an advisory body at the United Nations. The members include France, Canada, Argentina and Venezuela. </P>
<P> In addition, Latin American nations expect to meet on the resolution early Wednesday. </P>
<P> While only Argentina and Brazil are council members, Brazil has served as a spokesman for the group and indicated it would abide by its decisions. Other members would also hesitate if states in the region voiced strong opposition. </P>
<P> Nevertheless, a U.S. official expressed confidence the resolution would be approved, saying: ``It is a question of when it will be adopted, not whether it will be adopted.'' </P>
<P> Some council members object to U.S. proposals for a 6,000-strong U.N. force after the invaders leave, saying it was doubtful enough peacekeepers could be found. </P>
<P> Therefore the operation would amount to putting blue helmets on at least 3,000 American soldiers. </P>
<P> Another crucial point is the timing of U.N. deployment after the United States topples Haiti's military leaders. </P>
<P> The United States would like the Security Council to make this decision based on evaluations by the U.S. military command in Haiti that a ``stable and secure environment'' has been achieved. Many council members want Secretary-General Boutros Boutros-Ghali's approval first. </P>
<P> The U.S. draft authorizes a multinational force to use ''all necessary means'' to restore a legitimate government in Haiti. It does not mention exiled President Jean-Bertrand Aristide by name, and several Latin American states want his name included. </P>
<P> The draft mentions no deadline or waiting period before an invasion could take place, and diplomats said it was doubtful one would be included in any of the revisions. </P>
</TEXT>
</DOC>
