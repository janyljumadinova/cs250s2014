<DOC>
<DOCID>REU007-0204.940814</DOCID>
<TDTID>TDT001902</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/14/94 19:39</DATE>
<TITLE> BLOOD TRAIL LEADS TO ESCAPED PRISONERS</TITLE>
<HEADLINE> BLOOD TRAIL LEADS TO ESCAPED PRISONERS</HEADLINE>
<SUBJECT> BC-USA-JAIL 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: Updates with prisoners captured)</NOTES>
<DATELINE>PUNTA GORDA, Fla (Reuter) </DATELINE>
<P>  Police captured five escaped prisoners Sunday by following the bloody trail left by one of the men, who suffered gashes while scaling a razor-wire fence. </P>
<P> All five were arrested at a motel near the Charlotte County Jail in Punta Gorda on Florida's southern Gulf Coast, sheriff's spokesman Chuck Ellis said. </P>
<P> The prisoners escaped late Saturday by breaking out of a window in a dormitory-type trailer and climbing over a wall and a wire-topped fence, Ellis said. </P>
<P> One man was cut by the wire, and left a trail of blood drops that led police and tracking dogs to the motel room where they were hiding. </P>
<P> The men who fled had been housed in the trailer because they were classified as the jail's least serious offenders, Ellis said. </P>
<P> Sheriff Richard Worch had been asking county officials to proceed with new jail construction and replace the temporary dormitory trailer. </P>
<P> Six prisoners escaped from a state prison 60 miles away in Immokalee the previous weekend and one of them was still at large. </P>
</TEXT>
</DOC>
