<DOC>
<DOCID>REU004-0217.940707</DOCID>
<TDTID>TDT000262</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/07/94 22:53</DATE>
<TITLE> 17 DEAD IN GEORGIA AS CLINTON DECLARES DISASTER AREA</TITLE>
<HEADLINE> 17 DEAD IN GEORGIA AS CLINTON DECLARES DISASTER AREA</HEADLINE>
<SUBJECT> BC-USA-WEATHER 2NDLD </SUBJECT>
<AUTHOR>     By David Morgan </AUTHOR>
<TEXT>
<NOTES>(Eds: updates with U.S. disaster declaration; adds details)</NOTES>
<DATELINE>ATLANTA (Reuter) </DATELINE>
<P>  At least 17 people were confirmed dead after widespread flooding in Georgia Thursday as President Clinton declared the state's waterlogged towns and farmlands a federal disaster area. </P>
<P> A state of emergency remained in effect for sections of Georgia, Alabama and the Florida Panhandle after four days of heavy rain and high winds forced tens of thousands to flee from their homes, drowned crops and swamped entire communities across the region. </P>
<P> Georgia officials said emergency supplies of water were being trucked to an estimated 500,000 people who were without drinking water from the southern outskirts of Atlanta to the southwestern corner of the state. </P>
<P> More than a quarter million acres of crops were flooded, leading a southeastern farm trade group to predict that weather damage would reduce the U.S. peanut crop by 10 percent this year. </P>
<P> The devastation also disrupted rail service through the southeastern United States, shutting down Amtrak passenger travel along the Florida Panhandle corridor from Miami to Los Angeles and causing delays of up to three days for freight bound between Florida and northern cities, including Chicago. </P>
<P> ``I don't think any of us who have been on this trip today will ever forget what we have seen,'' Georgia Governor Zell Miller told reporters after returning from a helicopter tour of devastated areas. </P>
<P> ``This state has suffered a lot of disasters over its long history. But I don't think there has ever been one, certainly in recent times, that has been more far-reaching and more comprehensive and more costly.'' </P>
<P> Preliminary estimates of damage to roads, bridges and crops topped $250 million, while officials warned that the figure will rise dramatically as floodwaters recede and state damage assessment crews begin surveying the landscape. </P>
<P> Clinton's declaration will enable residents in 31 Georgia counties, where flooding and high winds have triggered a state of emergency, to receive federal grants, low-interest loans and temporary housing. </P>
<P> The president telephoned the disaster announcement to the Georgia governor while on Air Force One, which was en route from Warsaw to Naples, Italy. </P>
<P> Alabama and Florida also have called on the federal government for assistance. </P>
<P> All of the fatalities occurred in Georgia, including nine in southwestern Sumter County, where residents watched helplessly as a woman with a child called in vain for help from her flood-swept car before both disappeared into the raging current of a flooded creek. </P>
<P> Divers and boat crews continued to comb the area, which was deluged by more than 21 inches of rain in just over 24 hours, for another five people reported to be missing. </P>
<P> Flooding breached a levee along the Ocmulgee River, threatening the downtown area of Macon, Georgia, 90 miles southeast of Atlanta, where nearly 150,000 people were without drinking water. </P>
<P> Officials also evacuated as many as 14,000 people in the southern Georgia city of Albany, where the Flint River was rising by a foot an hour and not expected to crest until Saturday. </P>
<P> The downtown streets of Montezuma, Georgia, along the Flint River between Albany and Macon, were already completely engulfed by floodwaters. </P>
<P> The river, which runs from Atlanta in the north to Lake Seminole on the Florida border, was expected to crest at 24 feet above its flood stage. </P>
<P> No deaths or injuries were reported in Alabama, but a state of emergency was in effect for eight southeastern states. </P>
<P> Nearly 20,000 people were evacuated from the Alabama cities of Elba and Geneva as the Pea River threatened to overflow. </P>
<P> The devastation was wrought by the remnants of Tropical Storm Alberto, which swept ashore in northern Florida on Sunday with 60 mph winds. </P>
<P> The storm became a slow-moving tropical depression as it moved inland. It reached as far north as Atlanta before turning back south into Alabama and then heading north again toward northern Georgia and the southern foothills of the Appalachian Mountains. </P>
</TEXT>
</DOC>
