<DOC>
<DOCID>REU007-0210.940713</DOCID>
<TDTID>TDT000522</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/13/94 21:45</DATE>
<TITLE> FORMER AUSTRALIAN STATE PREMIER GUILTY OF FRAUD</TITLE>
<HEADLINE> FORMER AUSTRALIAN STATE PREMIER GUILTY OF FRAUD</HEADLINE>
<SUBJECT> BC-AUSTRALIA-BURKE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PERTH, Australia (Reuter) </DATELINE>
<P>  Former Western Australian premier Brian Burke was found guilty of four charges of false pretenses, and will be sentenced Friday. </P>
<P> Burke, who served as Australian ambassador to Ireland between 1988 and 1991, was convicted Wednesday night. He was released on bail of $18,250. </P>
<P> Burke, 47, a Labor Party premier between 1983 and 1988, had pleaded not guilty to obtaining more than $12,400 on four separate occasions from a parliamentary account which provides money for politicians to make approved trips out of state. </P>
<P> Each charge carries a maximum penalty of three years imprisonment. </P>
<P> During the trial, the prosecution alleged Burke claimed travel expenses for four official overseas trips between 1984 and 1986 when he was premier, knowing they had already been paid for by the state. The money was paid into his personal account. </P>
<P> Burke's counsel, Geoffrey Miller, claimed the application form for expenses had been prepared by public servants and Burke signed the four expense claim forms without realizing what they meant. </P>
<P> Prosecutor John McKechnie said the case was a simple one of greed. </P>
<P> Burke led Labor to victory at Western Australian elections in 1983, ending a decade of conservative rule in the state. </P>
<P> He resigned in early 1988 and later that year was appointed Australian ambassador to Ireland and the Holy See as controversy erupted about Western Australia's finances. </P>
<P> In early 1991, he was forced to return to Australia to face allegations of financial irregularities while he was premier. </P>
</TEXT>
</DOC>
