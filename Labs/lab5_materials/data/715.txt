<DOC>
<DOCID>REU005-0434.940811</DOCID>
<TDTID>TDT001751</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/11/94 15:58</DATE>
<TITLE> CHARITY WON'T AUCTION O.J. SIMPSON TV WARDROBE</TITLE>
<HEADLINE> CHARITY WON'T AUCTION O.J. SIMPSON TV WARDROBE</HEADLINE>
<SUBJECT> BC-USA-SIMPSON-AUCTION </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  Clothes worn by football great O.J. Simpson, accused of murdering his ex-wife and a friend, have been withdrawn from a charity auction for fear such a sale would be considered in bad taste, organizers said Thursday. </P>
<P> The auction of celebrity clothing will go ahead as planned on September 18, the day before Simpson's trial is scheduled to begin, but it will no longer contain Simpson's wardrobe from the NBC television pilot ``Frogmen,'' a charity official said. </P>
<P> Patricia Winters, president of the Valley Mayors' Fund for the Homeless, said the clothing, two shirts, a hat, shoes and pants, would be sold privately. </P>
<P> ``We think it's a fabulous donation because it's worth money that will go to the homeless. But we're not going to exploit these things at auction,'' she said. </P>
<P> Simpson, who parlayed his football career into broadcasting, acting and advertising, has pleaded not guilty to murdering Nicole Brown Simpson, 35, and her friend Ronald Goldman, 25. </P>
<P> They were stabbed and slashed to death outside Nicole's Brentwood townhouse on the night of June 12. </P>
<P> Simpson's Heisman trophy, awarded to him as the best college football player, and his number 32 shirt were stolen recently from display cases at the University of Southern California. </P>
<P> Fearing the same fate for Simpson's wardrobe, charity officials have hidden the clothing. </P>
<P> ``If they're going to steal the Heisman trophy and his shirt, it wouldn't be beyond the realm of possibility that somebody would steal this stuff,'' Winters said. </P>
<P> Simpson, underwent lymph node surgery Thursday at a hospital before being returned to his prison cell. </P>
</TEXT>
</DOC>
