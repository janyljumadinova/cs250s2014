<DOC>
<DOCID>REU007-0124.940814</DOCID>
<TDTID>TDT001889</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/14/94 08:40</DATE>
<TITLE> POLICE SHOOT EGYPTIAN WHO SHELTERED MILITANTS</TITLE>
<HEADLINE> POLICE SHOOT EGYPTIAN WHO SHELTERED MILITANTS</HEADLINE>
<SUBJECT> BC-EGYPT-VIOLENCE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ASSIUT, Egypt (Reuter) </DATELINE>
<P>  Police in southern Egypt have shot and killed a man suspected of sheltering Muslim militants, security sources said Sunday. </P>
<P> They said police Saturday followed Ali Sayyid Hassan Hussein to an orchard in Shamia village near Assiut, 200 miles south of Cairo. Hussein was shot but other people with him escaped, they said. </P>
<P> The sources said Hussein was one of over 200 people who surrendered to police after authorities offered an amnesty and the promise of government jobs to repentant militants. </P>
<P> They said he was released after handing himself over, but then began harboring other militants on the run from police. </P>
<P> Around 400 people have been killed in militant-related violence in Egypt since March 1992, with most of the killings concentrated in the region around Assiut, stronghold of the Gama'a al-Islamiya (Islamic Group). </P>
<P> In March, the militant group was killing several policemen every week in Assiut, but the violence has subsided following a tough government clampdown. </P>
</TEXT>
</DOC>
