<DOC>
<DOCID>REU010-0377.940721</DOCID>
<TDTID>TDT000847</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/21/94 11:59</DATE>
<TITLE> INDIA'S ``BANDIT QUEEN'' SAID TO HAVE WED</TITLE>
<HEADLINE> INDIA'S ``BANDIT QUEEN'' SAID TO HAVE WED</HEADLINE>
<SUBJECT> BC-INDIA-BANDIT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NEW DELHI, India (Reuter) </DATELINE>
<P>  India's legendary ``bandit queen'' Phoolan Devi has married since she was released from jail in February, the United News of India said Thursday. </P>
<P> The news agency quoted Phoolan as saying she and Umed Singh were married in March after developing a mutual attraction during his frequent visits to her in jail. </P>
<P> India's Supreme Court ordered Phoolan released on parole after she had spent 11 years in jail without trial on 44 charges of murder, kidnapping and robbery. She was 23 when she was jailed after surrendering to police in February, 1983. </P>
<P> Investigators alleged Phoolan joined a gang of bandits and fought her way to the top in order to avenge a mass rape in her low-caste Hindu village, of which she was also a victim. She was blamed for the deaths of 18 people in a village near the Uttar Pradesh town of Kanpur -- all high caste and believed to have been directly involved in her rape. </P>
<P> Phoolan, whose story has inspired books and films, has denied killing any of the 18 people. UNI quoted her husband as saying she was undergoing treatment by a Tibetan doctor for a tumor. </P>
<P> ``I want to give her respectability in society,'' Umed Singh said. ``I don't want people to ever point fingers at her and refer to her past again.'' </P>
</TEXT>
</DOC>
