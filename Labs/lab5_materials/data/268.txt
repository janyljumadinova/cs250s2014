<DOC>
<DOCID>REU008-0449.940717</DOCID>
<TDTID>TDT000663</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/17/94 12:55</DATE>
<TITLE> U.S., NATO FACE DEEPER BOSNIA INVOLVEMENT - PERRY</TITLE>
<HEADLINE> U.S., NATO FACE DEEPER BOSNIA INVOLVEMENT - PERRY</HEADLINE>
<SUBJECT> BC-YUGOSLAVIA-PERRY </SUBJECT>
<AUTHOR>     By Charles Aldinger </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BUCHAREST, Romania (Reuter) </DATELINE>
<P>  U.S. Defense Secretary William Perry said Sunday NATO and the United States faced deeper military involvement in Bosnia whether or not the region's warring parties decided to accept an international peace plan. </P>
<P> If the factions in Bosnia reject the plan this week, it could lead to rapid expansion of NATO air protection of ``safe havens,'' a lifting of the arms embargo and a spread of the civil war to other nations, Perry warned. </P>
<P> Even partition of the country under the plan is accepted, it will mean sending thousands of U.S. and other peacekeeping troops to Bosnia, he told reporters who flew with him to Romania from Washington at the start of a nine-nation European tour. </P>
<P> ``The fundamental conclusion is that we're at a fork in the road here,'' said Perry. ``I think if we go down either one of those two forks, there would be an expanded role for NATO -- and the United States would be an important part of that.'' </P>
<P> Serbs, Muslims and Croats have been warned to respond to the peace plan drawn up by Western powers and Russia by mid-week. </P>
<P> The self-styled Bosnian Serb assembly meets in Pale Monday to debate the plan, which would divide the former Yugoslav republic roughly in half between the Serbs and the Muslim-Croat federation. </P>
<P> The Muslim-Croat joint parliament also meets Monday in nearby Sarajevo to consider the same question, a day before the deadline set by the big powers' ``contact group'' for the Bosnian rivals to come up with a definitive response. </P>
<P> Senior U.S. defense officials travelling with Perry said that as many as 15,000 U.S. peacekeepers could be sent to Bosnia from Germany, about half of the total force expected to oversee a peace plan. </P>
<P> Perry cautioned that no final figure had been reached but said the U.S. contribution was unlikely to reach an earlier estimated total of 25,000. </P>
<P> Perry was on the first leg of his trip to the Balkans and Southern Europe to encourage the transition to democracy in former Soviet bloc states and discuss prospects for peace in Bosnia. </P>
<P> He will also visit Bosnia's capital of Sarajevo and Bulgaria, Albania, Macedonia, Croatia, Greece, Turkey and Italy. </P>
<P> Perry's visit to Sarajevo is planned for Friday and his is due to meet government officials and Lieutenant-General Michael Rose, commander of U.N. protection forces (UNPROFOR) in Bosnia. </P>
<P> He told reporters Sunday that threats by France and Britain to pull their peacekeeping forces out of the UNPROFOR contingent if the use of NATO air power is expanded over civilian safe havens would cause major problems in Bosnia. </P>
</TEXT>
</DOC>
