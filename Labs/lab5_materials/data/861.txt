<DOC>
<DOCID>REU009-0016.940818</DOCID>
<TDTID>TDT002105</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/18/94 10:39</DATE>
<TITLE> SRI LANKA'S PEOPLE'S ALLIANCE ASKED TO FORM GOVERNMENT</TITLE>
<HEADLINE> SRI LANKA'S PEOPLE'S ALLIANCE ASKED TO FORM GOVERNMENT</HEADLINE>
<SUBJECT> BC-SRILANKA 1STLD </SUBJECT>
<AUTHOR>     By Rahul Sharma </AUTHOR>
<TEXT>
<NOTES> (Eds: adds details and quotes)</NOTES>
<DATELINE>COLOMBO, Sri Lanka (Reuter) </DATELINE>
<P>  Opposition leader Chandrika Kumaratunga, who has promised unconditional talks aimed at ending Sri Lanka's 11-year civil war, was set Thursday to follow in her parents' footsteps by becoming prime minister. </P>
<P> President Dingiri Banda Wijetunga invited her People's Alliance (PA) to form a government after it defeated the ruling United National Party in Tuesday's general elections, winning 105 seats in the 225-seat parliament to the UNP's 94. </P>
<P> A presidential spokesman said the new government would be sworn in Friday, ending 17 years of rule by the UNP. The new parliament will be convened on August 25, he said. </P>
<P> Prime Minister Ranil Wickremasinghe told reporters Thursday his party's failure to pass laws to deal with corruption and improve people's purchasing power may have cost it the election. </P>
<P> ``People may have wanted a change. We have been there for 17 years,'' he admitted, adding that the new government will be expected to bring peace in the north. </P>
<P> Kumaratunga, a 49-year-old mother of two who is untried on the national political scene, claimed power Wednesday. She said she had the support of the Sri Lanka Muslim Congress, which won seven seats, and an independent party. </P>
<P> She led the campaign because her mother Sirima Bandaranaike, the world's first woman prime minister and the leader of the PA, is recovering from foot surgery. Her father. Solomon Bandaranaike, led the Sri Lankan Freedom Party to power in 1956 and was assassinated in 1959. </P>
<P> Kumaratunga said before the election one of her priorities would be to end the war with the Liberation Tigers of Tamil Eelam, who are fighting for a separate state for minority Tamils in the north and east. </P>
<P> ``We will go to Jaffna and talk to the Tigers unconditionally. I am confident we can find a definite political solution,'' she said then. </P>
<P> More than 30,000 people have been killed in the ethnic war that broke out in 1983, army and civilian sources say. </P>
<P> The UNP at first sought an alliance with smaller groups to stay in power but was turned down. Political sources said Wickremasinghe had urged the president gracefully to concede defeat but some senior UNP members opposed giving up power. </P>
<P> The combination of Wickremasinghe and diplomatic pressure persuaded Wijetunga, a UNP appointee, to tell Kumaratunga to form a new government, the sources told Reuters. </P>
</TEXT>
</DOC>
