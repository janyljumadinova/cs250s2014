<DOC>
<DOCID>REU008-0341.940817</DOCID>
<TDTID>TDT002066</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 17:09</DATE>
<TITLE> FARRAKHAN DROPS MEN-ONLY RULE FOR SPEECH</TITLE>
<HEADLINE> FARRAKHAN DROPS MEN-ONLY RULE FOR SPEECH</HEADLINE>
<SUBJECT> BC-USA-FARRAKHAN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PHILADELPHIA (Reuter) </DATELINE>
<P>  Nation of Islam Minister Louis Farrakhan has dropped his all-male audience rule for his fiery speeches so he could use a city-owned hall for an address next month, officials saig a local fair-practices ordinance that bans discrimination by sex, had turned down Farrakhan's request for the hall. </P>
<P> The September 24 speech will be Farrakhan's fourth in Philadelphia since becoming head of the Nation of Islam in the early 1980s. He has in the past drawn standing-room only crowds. </P>
<P> The previous speeches were open to men and women. But Farrakhan said in March an all-male audience was needed this time to further the Nation of Islam's anti-violen and men. </P>
</TEXT>
</DOC>
