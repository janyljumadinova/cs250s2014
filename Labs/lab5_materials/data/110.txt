<DOC>
<DOCID>REU004-0087.940707</DOCID>
<TDTID>TDT000248</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/07/94 13:29</DATE>
<TITLE> ZAIRE OPPOSITION ISSUES STRIKE CALL</TITLE>
<HEADLINE> ZAIRE OPPOSITION ISSUES STRIKE CALL</HEADLINE>
<SUBJECT> BC-ZAIRE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Updates with opposition strike call, pvs ABIDJAN)</NOTES>
<DATELINE>KINSHASA, Zaire (Reuter) </DATELINE>
<P>  Zaire's hard-line opposition called Thursday for a nationwide strike against the new government of Prime Minister Kengo wa Dondo. </P>
<P> ``The country will be made ungovernable,'' Joseph Olenghakoy, an aide to rival prime minister and opposition leader Etienne Tshisekedi, told Reuters after calling the strike for Friday. </P>
<P> Tshisekedi says his dismissal as prime minister by President Mobutu Sese Seko was unconstitutional and has called many strikes in urban centers. A strike in May was widely observed in Kinshasa and Tshisekedi's home province of Kasai but a nationwide strike on short notice could prove a test of his authority. </P>
<P> Kengo's new government mixed supporters and opponents of Mobutu and reflected a balance of tribes and regions. </P>
<P> Lunda Bululu, former prime minister and legal adviser to Mobutu, became foreign minister, Admiral Mavua Madima defense minister and former central bank governor Pay Pay Wa Syakassighe minister of finance. </P>
<P> ``It's a broad-based government, he's been able to bring in some of the radical opposition...it certainly reflects the political axes and, I believe, the geographical axes,'' a diplomat said. </P>
</TEXT>
</DOC>
