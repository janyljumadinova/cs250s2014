<DOC>
<DOCID>REU001-0337.940802</DOCID>
<TDTID>TDT001359</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/02/94 14:57</DATE>
<TITLE> PET PORKERS SET TO BECOME LEGAL IN LOS ANGELES</TITLE>
<HEADLINE> PET PORKERS SET TO BECOME LEGAL IN LOS ANGELES</HEADLINE>
<SUBJECT> BC-USA-PIGS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  Vietnamese potbellied pigs are about to join camels, llamas, alpacas and alligators as pets that can be legally kept in Los Angeles. </P>
<P> The city's Animal Regulation Commission has decided that the pet porkers are ``cute'' and are not to be confused with their swine brethren. </P>
<P> ``These are not really animals that are covered by the swine regulations. These animals do love people and do develop cute personalities,'' a report presented to the commission said. </P>
<P> The commission's recommendation, made Monday, was forwarded to the City Council and was expected to be approved at it's next meeting. </P>
<P> The miniature pigs, which grow to a height of 20 inches and weigh in at 100 pounds may be cute, but they will not be cheap. Annual license fees for exotic pets such as the pigs are $130 a year. </P>
<P> The commission recommended that ownership be restricted to two pigs per household and that male pigs must have their tusks removed before they reach the age of two. </P>
<P> And they must be kept on a leash when taken for a walk. </P>
</TEXT>
</DOC>
