<DOC>
<DOCID>REU007-0224.940815</DOCID>
<TDTID>TDT001909</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/15/94 00:09</DATE>
<TITLE> NEWCOMER LEADS SRI LANKA OPPOSITION'S VICTORY BID</TITLE>
<HEADLINE> NEWCOMER LEADS SRI LANKA OPPOSITION'S VICTORY BID</HEADLINE>
<SUBJECT> BC-SRILANKA-KUMARANATUNGA (WOMAN-IN-NEWS, PICTURE) </SUBJECT>
<AUTHOR>     By Minoli de Soysa </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>COLOMBO, Sri Lanka (Reuter) </DATELINE>
<P>  Although new to politics and untested nationally, Chandrika Kumaranatunga has captivated many Sri Lankans. </P>
<P> The deputy leader of the opposition People's Alliance (PA), Kumaranatunga has criss-crossed the country before Tuesday's parliamentary elections, sometimes addressing more than 30 meetings a day. </P>
<P> For a recent meeting in the central hill town of Teldeniya, the articulate Kumaranatunga, who smiles easily and exudes confidence, arrived by helicopter to loud cheers from a huge crowd. It is a scene that has been repeated many times in the campaign. </P>
<P> She has had death threats, and has been accompanied by a security convoy and surrounded by bodyguards whenever on stage. </P>
<P> She has led the campaign because her mother, former prime minister Sirima Bandaranaike, leader of the PA and the head of the main opposition Sri Lanka Freedom Party (SLFP), is recovering from foot surgery. </P>
<P> Kumaranatunga, chief minister of the Western Provincial Council and a 49-year-old mother of two, is untried on the national political stage. </P>
<P> In May, in a landscape lacking dynamic leaders, she stepped in to take the PA, which groups the SLFP and left-wing allies, to a stunning victory in a southern regional poll. </P>
<P> ``The question is whether she can effectively lead her party and the country, since she has not been tested on a national level,'' said a Western diplomat. </P>
<P> ``But she seems to have captured the imagination of a lot of people. They say she's young, she's smart and she's not associated with the policies of the past,'' he said. </P>
<P> The business community is afraid the SLFP may revert to its socialist policies of the 1970-77 period, which resulted in shortages and food lines, despite Kumaranatunga's repeated assurances her party would continue current liberal policies. </P>
<P> ``She is able to take decisions, to understand issues, she is quick and perceptive,'' said a businessman who has worked closely with her. </P>
<P> She wants a PA win to reverse the high cost of living, widespread human rights abuses and waste and corruption the current government has been blamed for. </P>
<P> ``The main issue is the cost of living. More than 80 percent of the people have seen their real incomes decrease in the last 17 years,'' she said in a recent interview. </P>
<P> ``The second major issue is the lack of democracy. People are also fed up with waste and corruption within the government,'' she said. </P>
<P> Kumaranatunga has been involved in politics since her mother's rule from 1970 to 1977. She later helped her husband, Vijaya, who broke away from the SLFP to form his own party. </P>
<P> In 1988, Vijaya was killed by left-wing rebels who had launched a violent struggle to overthrow the government. </P>
<P> Kumaranatunga went abroad soon after with their two children, but then decided to return and rejoin the SLFP. </P>
</TEXT>
</DOC>
