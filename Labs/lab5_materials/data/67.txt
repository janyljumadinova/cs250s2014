<DOC>
<DOCID>REU002-0419.940705</DOCID>
<TDTID>TDT000159</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/05/94 06:29</DATE>
<TITLE> N KOREA GROUP HEADS TO GENEVA FOR TALKS WITH U.S.</TITLE>
<HEADLINE> N KOREA GROUP HEADS TO GENEVA FOR TALKS WITH U.S.</HEADLINE>
<SUBJECT> BC-KOREA-USA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TOKYO (Reuter) </DATELINE>
<P>  A group of North Korean negotiators left Pyongyang for Geneva Tuesday to hold high-level talks with the United States, the official Korean Central News Agency (KCNA) said in a report monitored in Tokyo. </P>
<P> The delegation, led by First Vice Foreign Minister Kang Sok-ju, will take part in a third round of talks with its U.S. counterparts in Geneva Friday, the report said. </P>
<P> Washington agreed last month to hold fresh talks between the two countries aimed at easing tensions on the Korean peninsula after former President Jimmy Carter visited Pyongyang and won a North Korean pledge, later confirmed in a letter to Washington, to freeze its nuclear program. </P>
<P> Washington and Pyongyang do not have diplomatic ties. </P>
<P> At the talks, North Korea also wants to discuss how to improve ties with the United States, rebuild the north's shattered economy and obtain light-water nuclear reactors favored by arms control advocates. </P>
<P> The United States has promised that in addition to dealing with concerns about Pyongyang's suspected nuclear weapons program, it will be prepared to discuss a broad agenda of political and economic benefits for North Korea if Pyongyang meets international demands to freeze and eventually roll back its nuclear program. </P>
<P> The U.S. list of issues to be discussed at the talks includes resolving the dispute over North Korea's nuclear program, attempting to learn how much spent fuel Pyongyang may have diverted for nuclear weapons use, regional security, North Korea's conventional forces and missile sales to countries such as Syria. </P>
<P> North Korea warned last Thursday that the talks were in danger of not taking place, or failing if they do, because of American military moves. </P>
<P> A Radio Pyongyang broadcast, monitored in Tokyo by Radiopress, singled out a U.S. congressional vote to increase military spending in South Korea and the dispatch of two U.S. minesweepers to the area as examples of ``continued military moves that are not favorable for a dialogue.'' </P>
<P> The U.S. Defense Department quickly denied it had made any offensive military moves toward North Korea. </P>
<P> ``The very minor things that we're doing at this point are strictly defensive and should in no way be taken as a provocative action,'' said Pentagon spokeswoman Kathleen DeLaski. </P>
</TEXT>
</DOC>
