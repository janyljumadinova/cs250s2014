<DOC>
<DOCID>REU001-0165.940801</DOCID>
<TDTID>TDT001334</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/01/94 19:16</DATE>
<TITLE> VORONTSOV HEADS U.N. COUNCIL, THEN OFF TO WASHINGTON</TITLE>
<HEADLINE> VORONTSOV HEADS U.N. COUNCIL, THEN OFF TO WASHINGTON</HEADLINE>
<SUBJECT> BC-UN-VORONTSOV </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>UNITED NATIONS (Reuter) </DATELINE>
<P>  Russian U.N. envoy Yuli Vorontsov took over as president of the Security Council Monday before becoming Moscow's ambassador to Washington next month. </P>
<P> He will be succeeded in his U.N. post by Sergei Lavrov, a Russian deputy foreign minister. </P>
<P> Before coming to the United Nations in May 1990, Vorontsov had served since 1986 as first deputy foreign minister of the Soviet Union. </P>
<P> The 64-year-old native of Leningrad (now St Petersburg) joined the Soviet diplomatic service in 1952 and from 1966 to 1977 he was a counselor, and later a minister-counselor, at the Soviet Embassy in Washington. </P>
<P> He has headed Soviet embassies in Afghanistan (1988-89), France (1983-86) and India (1977-83). </P>
<P> Vorontsov became Russia's U.N. representative on December 24, 1991, when President Boris Yeltsin informed the United Nations that the Soviet Union's membership in the Security Council and other U.N. bodies was being continued by the Russian Federation. </P>
<P> The Security Council presidency rotates monthly according to the English alphabetical order of its 15 member delegations. Vorontsov replaced outgoing president Jamsheed Marker of Pakistan. </P>
<P> The council is in the unusual position of having only 14 functioning members since the representative of Rwanda's recently ousted government, who has not yet been replaced, has not been taking part in meetings since the latter part of July. </P>
</TEXT>
</DOC>
