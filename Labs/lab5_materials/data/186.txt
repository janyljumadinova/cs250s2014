<DOC>
<DOCID>REU006-0262.940712</DOCID>
<TDTID>TDT000442</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/12/94 13:04</DATE>
<TITLE> R.J. REYNOLDS DEAD AT 60</TITLE>
<HEADLINE> R.J. REYNOLDS DEAD AT 60</HEADLINE>
<SUBJECT> BC-USA-REYNOLDS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  R.J. Reynolds, grandson of the founder of the R.J. Reynolds tobacco company, has died of emphysema and congestive heart failure at the age of 60 at his home in North Carolina, his brother announced in Los Angeles Tuesday. </P>
</TEXT>
</DOC>
