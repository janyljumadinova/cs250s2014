<DOC>
<DOCID>REU008-0185.940715</DOCID>
<TDTID>TDT000621</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 17:23</DATE>
<TITLE> JUSTICE DEPT. SUPPORTS ATT-MCCAW DEAL</TITLE>
<HEADLINE> JUSTICE DEPT. SUPPORTS ATT-MCCAW DEAL</HEADLINE>
<SUBJECT> BC-USA-TELEPHONES 1STLD </SUBJECT>
<AUTHOR>     By James Vicini </AUTHOR>
<TEXT>
<NOTES> (Eds: Adds details, background)</NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  The Justice Department said Friday it has reached a settlement clearing the way for the $12.6 billion merger of AT&T Corp and McCaw Cellular Communications Inc, in what would create a telecommunications powerhouse. </P>
<P> The department agreed to allow the deal as long as McCaw provides competing long-distance carriers with equal access to its cellular systems and AT&T adopts procedures ensuring that its cellular equipment customers will not be put at a disadvantage. </P>
<P> The merger of the world's largest telephone and cellular phone companies was announced last August. AT&T has said it expects to complete the deal by the end of September. </P>
<P> The Justice Department also said it would support AT&T's request for an exemption from the 1982 landmark judicial order that led to the breakup of AT&T and created the seven U.S. regional Bell telephone companies. </P>
<P> U.S. District Judge Harold Greene, who has long presided over the AT&T case, must rule on whether to adopt the Justice Department's position and approve the deal. He has scheduled a hearing for Thursday and is expected to rule by September. </P>
<P> The Justice Department's approval means the deal has cleared a big hurdle. </P>
<P> ``Today we take a major step toward bringing the benefits of competition to millions of consumers of cellular telephone service, one of the fastest growing segments of the telecommunications industry,'' Assistant Attorney General Anne Bingaman said in announcing the settlement. </P>
<P> The proposed settlement establishes procedures to prevent AT&T and McCaw from misusing confidential information obtained from AT&T's equipment customers or McCaw's equipment suppliers. It also ensures that AT&T's customers that compete with McCaw will continue to have access to necessary products or services supplied by AT&T and that AT&T will not interfere with its customers' ability to change equipment suppliers. </P>
<P> If AT&T fails to meet these obligations, the Justice Department could require it to buy cellular equipment back from its cellular service provider customers, such as the regional Bell firms. </P>
<P> Lastly, the settlement requires certain separations of personnel between AT&T and McCaw, including marketing and development staff. </P>
<P> The Justice Department last month reached a similar agreement allowing British Telecommunications Plc to proceed with its plan to purchase a 20 percent stake in MCI Communications Corp. </P>
<P> British Telecom is investing $4.3 billion in the second-largest U.S. provider of long-distance telephone services as part of a global alliance. </P>
</TEXT>
</DOC>
