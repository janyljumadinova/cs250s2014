<DOC>
<DOCID>REU011-0391.940825</DOCID>
<TDTID>TDT002426</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/25/94 06:08</DATE>
<TITLE> CREWS SAVED FROM SHIP FIRES OFF ENGLAND, SCOTLAND</TITLE>
<HEADLINE> CREWS SAVED FROM SHIP FIRES OFF ENGLAND, SCOTLAND</HEADLINE>
<SUBJECT> BC-BRITAIN-FERRY 3RDLD (PICTURE) </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>RAMSGATE, England (Reuter) </DATELINE>
<P>  Air Force and coast guard rescuers said they launched two separate operations Thursday when fires broke out aboard a cross-channel ferry and a Lithuanian fish factory ship near two British ports. </P>
<P> More than 100 people were taken off the blazing Sally Star when its engine room caught fire on an overnight trip from Dunkirk, France to Ramsgate in southeastern England. </P>
<P> Lifeboats took 30 crewmembers off a burning Lithuanian-registered fish factory ship near Lerwick in the Shetland islands. </P>
<P> Firefighters put out the blaze on the Sally Star, which had been kept from drifting in a busy sea lane by tugboats. ``The fire is now out and the ship is now going back to Dunkirk for an investigation,'' a spokeswoman for the Sally Star line said. </P>
<P> She said one crew member slightly injured his back while helping to put out the fire, but no one else was hurt. </P>
<P> A helicopter could be seen hovering over the ferry as crew and freight drivers, chatting cheerfully and wearing bright orange life jackets, arrived at Ramsgate port aboard lifeboats. </P>
<P> The spokeswoman said the ferry normally carried cars and passengers, but on this trip had been carrying freight and trucks. She said 104 crew and 17 freight drivers had been aboard. </P>
<P> ``As the two lifeboats were launched, I could see flames coming out the top of the ship around the bridge area. As the lifeboats got to her, the flames seemed to be under control,'' eyewitness Colin Howe told British Broadcasting Corporation radio. </P>
<P> In a separate operation, helicopters and lifeboats were sent to the Lithuanian-registered Seda when it reported a fire in its engine room. Officials said about 30 of the 73-member crew had been taken off the ship and firefighters were putting out the blaze. </P>
</TEXT>
</DOC>
