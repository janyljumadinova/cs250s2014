<DOC>
<DOCID>REU005-0054.940709</DOCID>
<TDTID>TDT000319</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/09/94 05:58</DATE>
<TITLE> RED MERCURY SEEN BEHIND SOUTH AFRICAN KILLINGS</TITLE>
<HEADLINE> RED MERCURY SEEN BEHIND SOUTH AFRICAN KILLINGS</HEADLINE>
<SUBJECT> BC-SAFRICA-MURDERS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CAPE TOWN, South Africa (Reuter) </DATELINE>
<P>  Red mercury, potentially the key ingredient of a miniature nuclear bomb, could have been at the heart of a string of murders in South Africa believed to involve Israel's secret service, a newspaper said Saturday. </P>
<P> The Weekend Argus said police probing at least four mysterious deaths since 1991 believed that one victim, British immigrant Alan Kidger, was trying to smuggle red mercury to a Middle Eastern client, possibly Iraq. </P>
<P> The newspaper said red mercury could be the key to production of a four-pound nuclear bomb strong enough to flatten the center of a big city. </P>
<P> Another newspaper reported this week that Kidger had been trying to arrange the shipment of chemicals from Zambia to Britain and then to the Middle East. </P>
<P> Police believe that Kidger, a paint technologist whose body was found in the trunk of a car in November 1991, was killed by Israel's Mossad secret service in a signal to others trying to help arm Israel's Middle Eastern rivals. </P>
<P> Police also blame Mossad for last year's murder of chemical engineer Wynand van Wyk in a Cape Town hotel. </P>
<P> Police told Reuters Friday they had expanded their inquiry into Kidger's murder to reexamine other killings and apparent suicides since 1991. </P>
<P> A spokesman said they had reopened the investigation into the suicide of chemical engineer John Scott, who was found gassed in his car outside his home, where his wife and children were stabbed to death. </P>
<P> Police said Friday they were still investigating another apparent suicide of onetime arms dealer Don Lang, an acquaintance of Kidger, who was found gassed in his Durban apartment two weeks ago. </P>
<P> Lang told an associate on the day Kidger's murder was revealed that he suspected Israelis had carried it out. </P>
<P> The Weekend Argus said police had also reopened the inquiry into the March 9 death of Johannesburg-based chemist Trevor Carter, who was found shot in the head. </P>
<P> Mike Louw, the director of South Africa's National Intelligence Service, told legislators in the agency's first public testimony last month that the country was awash with secret agents trying to pick up nuclear secrets. </P>
<P> South Africa built six nuclear weapons during the 1970s and the 1980s, but former president F.W. de Klerk ordered them dismantled as soon as he came to power in 1989. </P>
<P> Louw said agents were trying to find South Africa's nuclear experts either to hire them or to ensure that they did not go to work for countries with nuclear ambitions. </P>
</TEXT>
</DOC>
