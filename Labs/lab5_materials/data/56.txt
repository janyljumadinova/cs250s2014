<DOC>
<DOCID>REU002-0283.940704</DOCID>
<TDTID>TDT000123</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/04/94 13:41</DATE>
<TITLE> GREEK GUERRILLAS KILLED TURKISH DIPLOMAT - MINISTER</TITLE>
<HEADLINE> GREEK GUERRILLAS KILLED TURKISH DIPLOMAT - MINISTER</HEADLINE>
<SUBJECT> BC-GREECE-SHOOTING 4THLD (PICTURE) </SUBJECT>
<AUTHOR>     By Stephen Weeks </AUTHOR>
<TEXT>
<NOTES>(Eds: adds Greek guerrillas blamed for killing)</NOTES>
<DATELINE>ATHENS, Greece (Reuter) </DATELINE>
<P>  Leftist Greek guerrillas shot and killed a senior Turkish diplomat in an ambush outside his seaside Athens home Monday, Public Order Minister Stelios Papathemelis told reporters. </P>
<P> He said a three-man hit-squad opened fire on Turkish embassy Counsellor Omer Haluk Sipahioglu with the same .45 caliber semi-automatic pistol that the November 17 guerrilla group has used in a string of killings, including that of a CIA station chief. </P>
<P> ``It's November 17,'' Papathemelis said bluntly after initial ballistics tests on cartridge cases found at the scene. </P>
<P> The gunmen laid in wait for 46-year-old Sipahioglu outside his home in the seaside suburb of Palaio Faliro. </P>
<P> They pumped six bullets into his chest and abdomen after he climbed into his car to drive to work at the central Athens embassy. They then escaped in heavy traffic. </P>
<P> Sipahioglu was sitting alone in the car when a front side-window was blown out in a hail of bullets. </P>
<P> He survived the attack but died in a hospital several hours later. A hospital spokesman said Sipahioglu underwent surgery but had lost too much blood to be saved. </P>
<P> The diplomat spoke briefly, telling doctors simply: ``I'm dying.'' </P>
<P> Greek leaders sent messages of regret to Turkey and to Sipahioglu's family. </P>
<P> Prime Minister Andreas Papandreou expressed his condolences over the ``abhorrent murder'' in a note to Turkish Prime Minister Tansu Ciller. </P>
<P> ``Please express to the family of the victim the deep sorrow of the Greek government and people, and our assurances that we will make every effort to catch the culprits,'' he said. </P>
<P> November 17 has not claimed responsibility for Monday's shooting. But the group usually makes its claims a day or so after an attack in a long letter to an Athens newspaper or radio station. </P>
<P> The mysterious guerrillas have been in operation in Athens since December 1975, when they killed CIA station chief Richard Welch. Police have never infiltrated the group or made any arrests, leading to endless speculation about who its members are. </P>
<P> November 17 has killed 20 Greeks, Americans and Turks -- ranging from Greek police and politicians to U.S. diplomats and military personnel. They are notorious for precision hits with guns, time-bombs and rocket-propelled grenades. </P>
<P> Papathemelis said the pistol used on Sipahioglu had also killed Welch, former National Bank governor Mihalis Vranopoulos and Pavlos Bakoyiannis, a politician and the son-in-law of a former conservative prime minister. </P>
</TEXT>
</DOC>
