<DOC>
<DOCID>REU002-0005.940703</DOCID>
<TDTID>TDT000090</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/03/94 08:02</DATE>
<TITLE> N YEMENI ROCKETS KILL 17 IN ADEN - WITNESSES</TITLE>
<HEADLINE> N YEMENI ROCKETS KILL 17 IN ADEN - WITNESSES</HEADLINE>
<SUBJECT> BC-YEMEN-SOUTH-ROCKETS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ADEN (Reuter) </DATELINE>
<P>  Northern Yemeni rockets fired at the besieged southern city of Aden on Sunday killed 17 people in two separate locations, witnesses and hospital sources said. </P>
<P> Four rockets fell near a building belonging to the United Nations in the central Khormaksar area of Aden, killing nine people who were digging wells nearby, witnesses told Reuters. </P>
<P> Eight civilians including four children were killed by another rocket which hit a building in the same area, hospital sources said. </P>
</TEXT>
</DOC>
