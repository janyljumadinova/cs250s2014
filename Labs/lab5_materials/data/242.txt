<DOC>
<DOCID>REU008-0082.940715</DOCID>
<TDTID>TDT000593</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 11:35</DATE>
<TITLE> STRIKES BRING NIGERIAN CITIES NEAR STANDSTILL</TITLE>
<HEADLINE> STRIKES BRING NIGERIAN CITIES NEAR STANDSTILL</HEADLINE>
<SUBJECT> BC-NIGERIA </SUBJECT>
<AUTHOR>     By Tunde Obadina </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LAGOS (Reuter) </DATELINE>
<P>  Lagos and other Nigerian cities were close to a standstill Friday after nearly two weeks of pro-democracy strikes led by oil unions demanding an end to military rule. </P>
<P> Many banks, offices and shops in Lagos, Nigeria's commercial capital, remained shut as workers stayed at home either to back the strike or because of a lack of public transportion. </P>
<P> Many schools were closed due partly to a strike by teachers. </P>
<P> ``It seems Lagos is grinding to a halt,'' said a Western diplomat, who added that his embassy was down to less than a week's supply of fuel. </P>
<P> But the oil workers' strike appears so far not to have cut the country's oil output and exports, its economic lifeblood. Junior oil minister Umaru Baba said on Friday: ``Crude oil output is going on.'' </P>
<P> The strike to demand the release of Moshood Abiola, believed to have won last year's presidential election, has disrupted domestic fuel supplies and led to long lines at the few filling stations still selling gasoline. </P>
<P> Commonwealth Secretary General Emeka Anyaoku, a former Nigerian foreign minister, said on Friday he had met both military ruler General Sani Abacha and Abiola, the chief protagonists in Nigeria's year-long political impasse. </P>
<P> Anyaoku said his talks with Abiola were very useful. Their meeting was in Nigeria's inland capital Abuja where Abiola is standing trial for treason for proclaiming himself president. </P>
<P> It was not clear if Anyaoku had made specific proposals to the two sides to solve Nigeria's crisis. </P>
<P> The strike to force Abacha's administration to free Abiola is taking its toll on water and electricity supplies. </P>
<P> The senior staff union SESCAN said on Friday its 600,000 members would join the strike beginning Monday. </P>
<P> SESCAN affiliates include unions in the financial, food and civil service sectors. Some are already on strike. </P>
<P> The umbrella Nigeria Labor Congress representing 3.5 million junior workers has threatened to join the strike Tuesday. </P>
</TEXT>
</DOC>
