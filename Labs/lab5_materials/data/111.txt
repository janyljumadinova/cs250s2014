<DOC>
<DOCID>REU004-0114.940707</DOCID>
<TDTID>TDT000249</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/07/94 15:05</DATE>
<TITLE> ALPINE SKIING-FRENCHWOMAN MERLE RETIRES</TITLE>
<HEADLINE> ALPINE SKIING-FRENCHWOMAN MERLE RETIRES</HEADLINE>
<SUBJECT> BC-SKIING-MERLE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>GRENOBLE, France (Reuter) </DATELINE>
<P>  Giant slalom world champion Carole Merle of France said Thursday she was retiring from Alpine skiing. </P>
<P> Merle, 30, who had 22 World Cup wins in her career, said she planned to run a hotel. </P>
<P> Merle dominated super giant slalom by winning four World Cup titles in the event. She took the super-G silver medal on home soil at the Albertville Olympics in 1992 and went on to win the giant slalom at the world championships last year in Morioka, Japan. </P>
<P> At the Olympics earlier this year in Lillehammer, Norway, she failed to win any medals. </P>
</TEXT>
</DOC>
