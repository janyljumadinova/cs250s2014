<DOC>
<DOCID>REU002-0372.940704</DOCID>
<TDTID>TDT000144</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/04/94 21:11</DATE>
<TITLE> SIMPSON JUDGE TO RULE ON TOSSING BLOODY EVIDENCE</TITLE>
<HEADLINE> SIMPSON JUDGE TO RULE ON TOSSING BLOODY EVIDENCE</HEADLINE>
<SUBJECT> BC-USA-SIMPSON 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  Prosecutors and defense attorneys geared up Monday for what could be a crucial turning point in the O.J. Simpson murder case -- a court session to decide whether bloody evidence against the football legend will be thrown out. </P>
<P> Judge Kathleen Kennedy-Powell had put off until Tuesday hearing arguments on the defense team's request to suppress evidence it said police gathered illegally at Simpson mansion's the day after his ex-wife and a friend were killed. </P>
<P> At stake are items believed critical to the prosecution's case -- a blood-stained glove, blood stains outside and inside Simpson's home and blood found in his Ford Bronco. If they are declared inadmissible, the case against Simpson would be severely weakened. </P>
<P> The courtroom showdown will take place on the third day of a nationally televised preliminary hearing in which the judge will ultimately decide whether the Hall of Fame running back must stand trial on double murder charges. </P>
<P> Robert Shapiro, Simpson's lead attorney, contends police arrived at his Brentwood mansion early on the morning of June 13, scaled the fence and then searched inside and outside -- all before a search warrant was issued six hours later. </P>
<P> In a motion filed last week, Shapiro argued that at least 34 pieces of evidence seized at the estate -- including fiber and hair samples -- are tainted because detectives violated Simpson's constitutional rights. Simpson was not there at the time, having left on a flight to Chicago several hours earlier. </P>
<P> Police Chief Willie Williams has defended his officers, saying they conducted their search in ``an outstanding manner.'' </P>
<P> Legal experts were divided on Shapiro's chances for success, but agreed he had thrown the prosecution off stride. ``What's really going on here is a very sharp psychological game,'' said Charles Lindner, another Los Angeles lawyer. </P>
<P> Simpson, 46, who parlayed football stardom into a career as a play-by-play announcer and actor, is charged with the June 12 murders of Nicole Brown Simpson, 35, and Ronald Goldman, 25. He has pleaded innocent and has been held without bail since his arrest June 17. </P>
<P> The case has triggered a media frenzy and has kept America transfixed for the past three weeks. One in four U.S. households tuned in to live television coverage last week of Simpson's preliminary hearing. </P>
<P> In the latest twist, a sports memorabilia distributor has begun marketing a 10-pack of souvenir trading cards to capitalize on the sensational case. </P>
<P> Priced at up to $10 a pack, the cards -- which feature Simpson's mugshot, his ex-wife's driver's license and Goldman's modeling photos -- were being snapped up at a collectors' show in Anaheim, California, where they were introduced over the weekend. </P>
<P> Adding suspense to the case are a number of unsolved mysteries. </P>
<P> Both sides are anxiously awaiting the judge's decision on whether to unseal a bulky manila envelope she brandished in court last week, saying it contained secret evidence turned over by the defense. </P>
<P> According to some news reports, it is a knife, possibly even one identical to the 15-inch Stiletto knife that witnesses said they sold to Simpson and prosecutors allege he used in the slayings. </P>
<P> In another development, cutlery shop co-owner Richard Wattenberg, whose brother testified last week about selling the knife to Simpson, said anonymous callers angered by his testimony had threatened to bomb the store. Police were reported to have beefed up patrols in the area. </P>
<P> Another knife -- this one said to be of the kitchen variety and smeared with red stains -- was being examined by police after a woman found it Saturday near Simpson's mansion. Police sources say they doubt it is the missing murder weapon. </P>
<P> Working through the long Independence Day weekend, prosecutors Marcia Clark and William Hodgman scoured the murder scene outside Nicole Brown Simpson's luxury townhouse. </P>
<P> Famed attorney F. Lee Bailey, a member of Simpson's all-star defense team, met with him for the first time Sunday at the Men's Central Jail. </P>
<P> Investigators again concentrated their efforts on Simpson's mansion, where authorities say they have already found a trail of blood. </P>
</TEXT>
</DOC>
