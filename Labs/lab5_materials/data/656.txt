<DOC>
<DOCID>REU004-0098.940808</DOCID>
<TDTID>TDT001617</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/08/94 08:58</DATE>
<TITLE> AFGHAN LEADER SEEKS U.N. MONITORING OF BORDER</TITLE>
<HEADLINE> AFGHAN LEADER SEEKS U.N. MONITORING OF BORDER</HEADLINE>
<SUBJECT> BC-AFGHAN-OBSERVERS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ISLAMABAD, Pakistan (Reuter) </DATELINE>
<P>  An envoy for Afghan President Burhanuddin Rabbani called Monday for international monitoring of Afghanistan's borders to cut off military supplies to the country's warring factions. </P>
<P> ``We are asking the United Nations and the OIC (Organization of the Islamic Conference) to monitor all the borders to stop the entry of arms,'' Masood Khalili told a news conference at the Afghan Embassy in Islamabad. </P>
<P> ``There should be a mechanism to stop logistical or military help from entering Afghanistan,'' he said. The United Nations and the OIC, he added, could decide just how this could be done. </P>
<P> Landlocked Afghanistan adjoins China, Tajikistan, Uzbekistan, Turkmenistan, Iran and Pakistan. </P>
<P> U.N. Special Representative Sotirios Mousouris said he was not aware of any formal Afghan request for border monitoring. </P>
<P> Rabbani's guerrilla faction is fighting with opponents led by Prime Minister Gulbuddin Hekmatyar and northern warlord General Abdul Rashid Dostum who want to oust him. </P>
<P> Khalili said Rabbani, leader of the Jamiat-i-Islami party, told a conference in the western city of Herat last month that he wanted to hand over power to a transitional government and would not seek to renew his term after October 23. </P>
<P> That is the date set by the Herat conference for a traditional Loya Jirga, or grand assembly, which would choose a new government for the strife-torn country. </P>
<P> ``Rabbani said he won't be a candidate for power and no-one from Jamiat-i-Islami will be a candidate,'' Khalili said. </P>
<P> He said efforts were under way to form a member committee representing parties, provinces, judges, exiles and nomads. </P>
<P> ``This committee has to call the Jirga and talk to different parties about how to replace the present government in Kabul and decide the destiny of the Rabbani government,'' he said. </P>
<P> Rabbani's foes denounced the Herat conference, but leaders of three neutral factions based in the northwestern Pakistani town of Peshawar, went to Kabul for talks at the weekend. </P>
<P> Khalili said there was no intention to exclude opposition groups and a delegation from the Herat conference would visit Hekmatyar to try to convince him to join the peace effort. </P>
<P> More than 11,500 people have been killed in factional battles for power in Afghanistan since mujahideen guerrillas took over from a communist government in April, 1992. </P>
</TEXT>
</DOC>
