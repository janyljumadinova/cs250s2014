<DOC>
<DOCID>REU010-0382.940822</DOCID>
<TDTID>TDT002320</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/22/94 19:15</DATE>
<TITLE> DETROIT MANHUNT UNCOVERS BODY OF ESCAPED CONVICT</TITLE>
<HEADLINE> DETROIT MANHUNT UNCOVERS BODY OF ESCAPED CONVICT</HEADLINE>
<SUBJECT> BC-USA-PRISON-ESCAPE 1STLD </SUBJECT>
<AUTHOR>     By David Lawder </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>DETROIT (Reuter) </DATELINE>
<P>  The body of an escaped convict turned up in an alley Monday as police continued a major manhunt for five fugitives after a daring escape from a Detroit prison. </P>
<P> Police found the body of Kevin Hatcher, 28, in an alley on Detroit's west side, about eight miles away from the Ryan Regional Correctional Facility, from which he and nine other convicts escaped Sunday morning. </P>
<P> Police said Hatcher's body showed no signs of injury. But hatcher may have died from a drug overdose, said police Sergeant Christopher Buck. </P>
<P> Four fugitives were arrested within hours of the jailbreak. Hatcher's death leaves five escapees -- including three murderers -- still on the loose. </P>
<P> Detroit Police Chief Isiah McKinnon said police were investigating whether the inmates had help from prison guards. ''We are actively pursuing that strong possibility, and I emphasize, that strong possibility,'' McKinnon told a news conference Monday. </P>
<P> McKinnon also said charges may be brought against suspected accomplices in the escape as early as Tuesday, but declined to offer further details. </P>
<P> Meanwhile, the union representing prison guards said three guards were suspended in connection with the incident. </P>
<P> Detroit police, the FBI, State Police and other law enforcement agencies were marshaling hundreds of officers in the search and were following as many as 30 solid leads, McKinnon said. </P>
<P> Still at large are Tyrone Garland, 32, who was serving 20 to 60 years for second-degree murder; Mark McCloud, 26, serving life for first-degree murder and kidnapping; Willie Merriweather, 40,; serving life for second-degree murder;, Lavell Jones, 31, serving 50 to 150 years for armed robbery, unarmed robbery, larceny and felony firearm convictions; and Andre Gamble, 29, serving 13 to 20 years for armed robbery. </P>
<P> The inmates escaped by cutting holes in two chain-link and razor-wire fences at the state-run prison on Detroit's northeast side. The four-minute breakout started after two men outside the facility tossed a shotgun, a box of shells and wire cutters over the fences into the prison yard. </P>
<P> One of the prisoners fired shots at a guard in a patrol vehicle, allowing the group to flee to at least two waiting getaway cars. </P>
</TEXT>
</DOC>
