<DOC>
<DOCID>REU005-0175.940810</DOCID>
<TDTID>TDT001704</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/10/94 13:53</DATE>
<TITLE> S.C. GOVERNOR RACE HEADED FOR RUNOFFS</TITLE>
<HEADLINE> S.C. GOVERNOR RACE HEADED FOR RUNOFFS</HEADLINE>
<SUBJECT> BC-USA-POLITICS-SCAROLINA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>COLUMBIA, SC (Reuter) </DATELINE>
<P>  South Carolina's primary election left the leading Democratic and Republican candidates for governor facing runoffs in two weeks, a nearly final vote count showed Wednesday. </P>
<P> There were also primary races in Georgia and Colorado where Republican challengers for governor were picked. </P>
<P> When South Carolina pollsters finished counting votes from 99 percent of the state's precincts, Democrat Nick Theodore, the lieutenant governor, had missed winning a first-round majority by just 1,000 votes. His 49 percent outpaced Charleston Mayor Joe Riley, who won 38 percent. </P>
<P> In the Republican primary, former state Rep. David Beasley, who switched parties nearly three years ago, had 47 percent of the votes and will face U.S. Rep. Arthur Ravenel, who had 32 percent, in the August 23 runoff. </P>
<P> The candidates needed 50 percent plus one vote to win the nomination without a runoff to replace Republican Governor Carroll Campbell, barred by state law from a third term. </P>
<P> The race turned on issues such as removing the Confederate flag from atop the statehouse here, abortion rights and whether the descendants of slaves should receive reparations. </P>
<P> The South Carolina Elections Commission said there were roughly 500,650 votes cast for state chief executive. An official count will be made public next week. </P>
<P> The state has 1.4 million registered voters, and the turnout was considered very strong, the commission said. </P>
<P> Those knocked out of the crowded field for governor were Democrats Travis Medlock, the state attorney general, who had 9 percent, and businessman Bill Holler, with 4 percent. Republican Tommy Hartnett, a former U.S. Representative, had 21 percent and immediately endorsed Ravenel. </P>
<P> GOP voters were also asked non-binding questions on two hotly contested matters: the Confederate flag and term limits. Majorities answered in favor of term limits for elected officials and said the banner should continue to fly. </P>
<P> Civil Rights groups vigorously oppose the flag, charging it is symbolic of the pro-slavery stance during the Civil War. They have threatened economic boycotts if it is not removed. </P>
<P> In Georgia, millionaire businessman Guy Millner defeated John Knox to become the Republican challenger to Democratic incumbent Governor Zell Miller in November. Millner polled 58 percent of the vote to Knox's 42 percent with 99 percent of the state's precincts reporting. </P>
<P> In Colorado's GOP primary, millionaire oilman Bruce Benson claimed victory in a three-way race for the right to take on Democratic Governor Roy Romer. With 73 percent of the precincts counted, he was leading with 61 percent of the vote against 23 percent for state Senator Mike Bird and 16 percent for Dick Sargent, a two-time candidate for state treasurer. </P>
<P> Romer, a former chairman of the National Governors' Association seeking a third four-year term as Colorado's chief executive, immediately challenged Benson to a series of debates. </P>
</TEXT>
</DOC>
