<DOC>
<DOCID>REU007-0260.940714</DOCID>
<TDTID>TDT000533</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/14/94 04:31</DATE>
<TITLE> GERMANS TROOPS IN PARIS AS BASTILLE PARADE OPENS</TITLE>
<HEADLINE> GERMANS TROOPS IN PARIS AS BASTILLE PARADE OPENS</HEADLINE>
<SUBJECT> BC-FRANCE-GERMANY-PARADE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PARIS (Reuter) </DATELINE>
<P>  President Francois Mitterrand opened France's traditional Bastille Day military parade Thursday amid controversy over the presence of German troops rolling through Paris for the first time since World War Two. </P>
<P> In the presence of German Chancellor Helmut Kohl, Mitterrand drove standing in an army command car down the tree-lined Champs-Elysees avenue in central Paris, flanked by mounted Republican Guards and applauded by large crowds. </P>
<P> Some 7,000 soldiers, 280 horses and 356 military vehicles were taking part in the annual parade. But all eyes were on the 200 German soldiers joining the ceremonies in a powerful gesture of reconciliation with an old enemy. </P>
<P> The soldiers were part of the nascent European Army corps grouping French, Belgian, Spanish and Luxembourg troops, due to become operational in October 1995. </P>
<P> Security was extremely tight and police seemed omnipresent in the parade area, witnesses said. </P>
<P> The parade comes in a week in which Germany's supreme court cleared the way for its troops to play a role in U.N. missions, and President Clinton urged Germany to take a pivotal role in world affairs. </P>
<P> Mitterrand invited the soldiers as a symbol of Franco-German reconciliation and to compensate Germany for its exclusion from last month's D-Day anniversary commemorations. </P>
<P> Most French people support the Germans' participation, but Mitterrand's invitation has ruffled feathers among many who lived through the Nazi occupation. </P>
<P> On the eve of the parade, Interior Minister Charles Pasqua, a hardline Gaullist and ex-Resistance fighter, clashed with Mitterrand, a Socialist who was also in the Resistance. </P>
<P> ``I think the idea of associating the soldiers of Eurocorps with a parade in Paris is not a bad idea but the timing is not good because we are celebrating this year the 50th anniversary of the liberation,'' Pasqua told TF1 television. </P>
<P> During the German occupation of Paris from June 1940 to August 1944, German troops staged marches every day through the Champs-Elysees, to demonstrate the Reich's power and discipline. </P>
<P> In typically blunt style, Pasqua said: ``In July 1944 Paris was paying a pretty heavy tribute for its liberation. The soldiers who will be parading won't be the same but I understand this can upset people.'' </P>
</TEXT>
</DOC>
