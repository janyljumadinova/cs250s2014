<DOC>
<DOCID>REU002-0380.940705</DOCID>
<TDTID>TDT000148</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/05/94 01:15</DATE>
<TITLE> HAITI ACCUSES U.S. OF ORGANIZING EXODUS</TITLE>
<HEADLINE> HAITI ACCUSES U.S. OF ORGANIZING EXODUS</HEADLINE>
<SUBJECT> BC-HAITI-ACCUSATIONS </SUBJECT>
<AUTHOR>     By Andrew Downie </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PORT-AU-PRINCE, Haiti (Reuter) </DATELINE>
<P>  Haiti's army-installed government accused the United States Monday of organizing and financing the mass exodus of refugees from the Caribbean nation as a pretext for a military invasion. </P>
<P> In a communique read out on state-controlled television the de facto regime of Emile Jonassaint said the United States and ''national sectors financed and encouraged the boat people operation ... as a pretext for intervention in Haiti.'' </P>
<P> More than 5,000 Haitians have fled their homeland in the last 10 days trying to take advantage of President Clinton's more lenient refugee policy. </P>
<P> The new U.S. procedure grants all Haitians interdicted at sea political asylum hearings either on ships docked near Jamaica or at special centers at Guantanamo Bay Naval Base in Cuba. </P>
<P> Under the previous policy, all boat people were returned to Port-au-Prince and told to apply for asylum at the three in-country processing centers set up by the U.S. government. </P>
<P> Haiti's military authorities also accused the United States of kidnapping Haitian fisherman and presenting them to the media as boat people, again, as a pretext for intervention. </P>
<P> Clinton has refused to rule out military action to remove the leaders of the September 1991 coup that toppled President Jean-Bertrand Aristide. Despite the imposition of tough economic sanctions against them, the army chiefs so far show no signs of giving up power. </P>
<P> The statement said the Jonassaint government was forming a special commission, comprised of the ministers of information, justice, interior and defense as well as high-ranking military officials, to investigate the boat people phenomenon. </P>
<P> Shortly after taking office on May 11, Jonassaint announced that he would enforce a law that made it illegal to leave Haiti without the proper papers and since then there have been at least three confirmed incidents of violence against refugees preparing to depart. </P>
<P> More than 100 refugees were believed to have drowned Monday when the boat they were in capsized, survivors told reporters. </P>
<P> The survivors said they thought that between 100 and 150 people died when their overloaded craft overturned after leaving an area near the western town of St Marc. A similar number are thought to have survived the sinking, two survivors told reporters who visited the scene. </P>
<P> In another incident, on the south coast two weeks ago, more than 30 people were reported to have drowned when the military or its allies fired in the air to try to stop scores of people from leaving on a small sailing boat. </P>
<P> More than 40,000 Haitians, most of them crammed onto flimsy, overloaded boats, have fled their homeland since Aristide was overthrown. </P>
</TEXT>
</DOC>
