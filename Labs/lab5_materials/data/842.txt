<DOC>
<DOCID>REU008-0352.940817</DOCID>
<TDTID>TDT002072</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 18:14</DATE>
<TITLE> MEDIATOR STAYS HOPEFUL ABOUT STRIKE SETTLEMENT</TITLE>
<HEADLINE> MEDIATOR STAYS HOPEFUL ABOUT STRIKE SETTLEMENT</HEADLINE>
<SUBJECT> BC-BASEBALL </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NEW YORK (Reuter) </DATELINE>
<P>  The sixth day of the Major League baseball strike came and went Wednesday without talks between negotiators for the owners and players, but Federal mediator John Martin was still hopeful. </P>
<P> ``When the timing is right, they will come together,'' said Martin, a commissioner of the Federal Mediation and Conciliation Service. </P>
<P> Mediators were called in last Friday to help resolve the baseball strike, taken by the players, who are determined to keep owners from instituting a salary cap system aimed at controlling the economics of the sport. </P>
<P> Player Relations Committee President Richard Ravitch and Players Association Executive Director Donald Fehr met separately with mediators Saturday to outline their positions. </P>
<P> ``We feel that we have developed a relationship of trust with both Ravitch and Fehr. Maybe they don't trust each other but I hope they trust us,'' Martin told reporters at the Intercontinental Hotel where talks have been held. </P>
<P> Martin, who mediated the 11-day National Hockey League strike in 1992, said a four-person mediation team has already begun advising the two sides. </P>
<P> ``The two sides are reassessing their positions and will meet in a timely fashion,'' Martin said. ``I hope the members of the Player's Labor Committee and the Management Committee are working behind the scenes.'' </P>
<P> Although no date has been set for the next formal negotiating session, Martin tried to stay upbeat. </P>
<P> ``One of the surprising things is that Fehr and Ravitch have a very good line of communications,'' he said. </P>
<P> But Martin conceded there was only so much the mediators could do. </P>
<P> ``The federal mediator's job is to help both sides come to an agreement. We are not an arbitration process and can not order a settlement,'' he said. </P>
<P> ``It's too tough to call how long it (the strike) will go at this time. We come up with ideas to help the process. We want to get it done as soon as possible.'' </P>
</TEXT>
</DOC>
