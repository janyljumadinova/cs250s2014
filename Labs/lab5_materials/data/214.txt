<DOC>
<DOCID>REU007-0139.940713</DOCID>
<TDTID>TDT000515</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/13/94 17:43</DATE>
<TITLE> HAITI STEPPING UP REPRESSION, U.S. SAYS</TITLE>
<HEADLINE> HAITI STEPPING UP REPRESSION, U.S. SAYS</HEADLINE>
<SUBJECT> BC-HAITI-USA 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  Haiti's rulers are ratcheting up repression, and their latest tactic is to dump the bodies of murdered political opponents in the streets to terrorize the people, a senior U.S. official said Wednesday. </P>
<P> Nancy Ely-Raphel, a top official in the State Department's human rights section, said that in the past two weeks several corpses of young men with their hands tied behind their backs had been found in the streets of the capital Port-au-Prince. </P>
<P> She was speaking to journalists as reports of a massacre of 12 young men in a nearby town swept Port-au-Prince. The U.S. embassy sent a team to investigate the alleged killings. </P>
<P> Ely-Raphel said extra-judicial killings had been common before, with 133 cases reported between February and May alone. But she added: ``We believe we may now be seeing a further ratcheting up of repression. </P>
<P> ``The latest developments include increased dumping of bodies in public areas of Port-au-Prince in an attempt to terrorise the populace.'' </P>
<P> Following the June 30 appearance of the corpses of five young men in the streets of the capital, all shot and tied up, one or more bodies had been found each day throughout the Fourth of July weekend, she said. </P>
<P> Allegations of other abuses, including mutilations and politically motivated rapes, had also increased dramatically with complaints received by international human rights monitors standing at 1,143 in June alone. This compared with 1,350 from February through May. </P>
<P> The Haitian government this week ordered the almost 100 monitors to leave the country in a move condemned by the United Nations and United States. </P>
<P> Ely-Raphel said the U.S. embassy would continue to monitor the human rights situation and had already shifted consular officials, no longer issuing visas to Haitians, to this work. </P>
<P> ``Those who think that they can intimidate the international community as well as the people of Haiti, or think we who monitor human rights abuses will get tired, give up and look away are wrong,'' she declared. </P>
<P> ``What they're doing is they're slowly turning Haiti into hell, because this de facto regime is losing control and even losing control of the violence it's unleashed.'' </P>
<P> The expulsion of the monitors has intensified speculation over a possible U.S. invasion of the Caribbean island to oust the military rulers and reinstall elected president Jean-Bertrand Aristide, exiled in the United States. </P>
<P> But both Ely-Raphel and State Department spokeswoman Christine Shelly gave the impression they believed the diplomatic track had some way to travel. </P>
<P> ``We have been effective so far in using international pressure, diplomatic pressure in addition to the (U.N.) embargo, and I would suggest that we continue to pursue that,'' Ely-Raphel said. </P>
<P> Shelly said: ``Our principal goal is to bring enough pressure on the de facto government and the military leaders for them to realise that they have no other alternative except to step down.'' </P>
<P> Meanwhile, Senate Republican Leader Bob Dole, who opposes U.S. military action, proposed in an amendment to a foreign aid bill that a bi-partisan congressional commission probe conditions in Haiti and report back to Congress in 45 days. </P>
<P> ``The last thing we should do is shoot first and ask questions later -- questions that could lead to a peaceful resolution,'' Dole, of Kansas, said in a Senate speech. A vote on his amendment is expected Thursday. </P>
</TEXT>
</DOC>
