<DOC>
<DOCID>REU002-0235.940704</DOCID>
<TDTID>TDT000116</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/04/94 09:16</DATE>
<TITLE> FRENCH WILL HALT REBELS, KIGALI FALLS</TITLE>
<HEADLINE> FRENCH WILL HALT REBELS, KIGALI FALLS</HEADLINE>
<SUBJECT> BC-RWANDA-FRANCE 2NDLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>GIKONGORO, Rwanda (Reuter) </DATELINE>
<P>  French troops in southwestern Rwanda received the order Monday to halt the Rwandan Patriotic Front in its advance west across the country -- disclosed as the RPF rebel troops seized the capital of Kigali as well as the government-held southern city of Butare. </P>
<P> The order marks a radical shift in France's policy toward the Rwandan Patriotic Front and puts ``Operation Turquoise'' on a new offensive footing. Until now Paris had insisted that it was leading a purely humanitarian mission and has avoided whenever possible any confrontation with the rebel movement. </P>
<P> Kigali fell to the rebels early Monday, according to independent witnesses and rebel commanders. A Reuter correspondent in Kigali said columns of jubilant rebel soldiers wound their way up the hillsides of the rambling capital toward key locations. </P>
<P> Tired and wet, they stood waving and smiling at groups of journalists. </P>
<P> The push on Kigali took place as other rebel forces seized control Monday of Butare after fierce battles with government forces, French officers said. </P>
<P> Colonel Didier Thibaut, based in Gikongoro in southwestern Rwanda -- less than 30 miles from Butare -- said Colonel Jacques Rosier, commander of ``Operation Turquoise'' southern command, had given him orders to stop the rebel movement from capturing the town or going beyond it. </P>
<P> The rebels are believed to have around 2,000 men in western Rwanda. Butare, the second-biggest Rwandan city, is 19 miles east of Gikongoro. </P>
<P> ``We have received orders to stay at Gikongoro,'' Thibaut told journalists. ``We will stop whoever tries to bother the population, whether it is the Rwandan Patriotic Front or the Rwandan army.'' </P>
<P> ``They have taken Butare. The information we have is it fell at noon local time (6 a.m. EDT),'' a French military officer told Reuters at the main French base in Goma, Zaire, for ''Operation Turquoise.'' </P>
<P> He confirmed earlier reports that Kigali fell early Monday and said the rebels had pushed on after capturing Butare, which rebel forces entered Sunday, and heavy fighting was raging in the south of the country. </P>
<P> Thibaut said the decision by the French government to halt the rebels from their western march came into effect immediately. He added that the French would be flying in reinforcements today to boost their 100-man presence in Gikongoro. </P>
<P> ``No one will go any further ... ours remains a humanitarian mission, in the security sense of the word,'' said Thibaut. </P>
<P> With Butare having fallen into rebel hands, a confrontation between the French and rebel forces has become a greater concern. The two sides clashed for the first time Sunday when the rebels fired from the hills on a French convoy evacuating around 300 Butare residents to Gikongoro. </P>
<P> No French troops were wounded but the rebels were believed to have suffered several casualties. </P>
<P> The Rwandan Patriotic Front has always accused the French of intervening in Rwanda to help government forces, who were trained and armed by the French in the past. </P>
<P> ``If the RPF comes here (Gikongoro) and threatens the population, we will shoot on them without any hesitation,'' Thibaut said. </P>
<P> ``We have the means and we will soon have more. It's true that there are 2,000 of them, but you saw yesterday afternoon how powerful our firepower was,'' he added, referring to Sunday's clash. </P>
</TEXT>
</DOC>
