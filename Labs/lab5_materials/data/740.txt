<DOC>
<DOCID>REU006-0293.940812</DOCID>
<TDTID>TDT001830</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/12/94 18:03</DATE>
<TITLE> ARGENTINE HOSPITALS ON RED ALERT FOR NEW ATTACK</TITLE>
<HEADLINE> ARGENTINE HOSPITALS ON RED ALERT FOR NEW ATTACK</HEADLINE>
<SUBJECT> BC-ARGENTINA-IRAN-HOSPITALS URGENT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BUENOS AIRES (Reuter) </DATELINE>
<P>  Argentina ordered emergency rooms at Buenos Aires hospitals on red alert Friday after being warned a new attack will follow an anti-Jewish blast that killed nearly 100 people last month. </P>
</TEXT>
</DOC>
