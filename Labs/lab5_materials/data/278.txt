<DOC>
<DOCID>REU009-0182.940718</DOCID>
<TDTID>TDT000688</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/18/94 13:06</DATE>
<TITLE> UN TO APPROVE INSPECTOR-GENERAL POST SHORTLY</TITLE>
<HEADLINE> UN TO APPROVE INSPECTOR-GENERAL POST SHORTLY</HEADLINE>
<SUBJECT> BC-UN-INSPECTOR </SUBJECT>
<AUTHOR>     By Evelyn Leopold </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>UNITED NATIONS (Reuter) </DATELINE>
<P>  A Clinton administration proposal to establish an inspector-general's office to investigate abuse and waste in the U.N. bureaucracy is expected to be adopted by the General Assembly this week. </P>
<P> U.S. Ambassador Madeleine Albright told reporters Monday that she was pleased with the result of negotiations in the assembly's administrative and budgetary committee. The panel has approved a draft informally and is expected to send it shortly to the 184-member General Assembly, where passage is virtually certain. </P>
<P> ``This should help give the American people the confidence that our money is spent wisely,'' she said. </P>
<P> ``Achieving this goal has been a central plank of the Clinton administration's effort to reinvent the United Nations -- not to tear it down but to make it work,'' she said. </P>
<P> The leading candidate for the post, to be established at the senior level of undersecretary-general, is German diplomat Karl Paschke, currently in charge of personnel in Bonn's foreign office, diplomats said. </P>
<P> Paschke, expected to be appointed before the end of the month for a five-year term, was a former deputy ambassador in the German Embassy in Washington and served as press spokesman for former Foreign Minister Hans-Dietrich Genscher.  The post will not be officially called an inspector-general but the office of internal oversight services, with the power to ``examine, review and appraise the use of financial resources.'' </P>
<P> It would also ``evaluate the efficiency and effectiveness of the implementation of the organization's programs and legislative mandates,'' according to the draft resolution. </P>
<P> The new inspector would be under the secretary-general but have ``operational independence.'' </P>
<P> Whistle-blowers would be protected in a provision that calls for ``direct confidential access of staff members'' and ''protection against repercussions.'' </P>
<P> With the United States in arrears in its dues to the United Nations for the past 10 years, Congress last year withheld more money unless an inspector-general was appointed. </P>
<P> It killed a proposed $175 million fund for unforeseen peacekeeping costs and withheld 10 percent of its regular contribution. Currently, the United States owes the United Nations over $1 billion for peacekeeping and regular dues. </P>
</TEXT>
</DOC>
