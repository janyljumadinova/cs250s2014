<DOC>
<DOCID>REU008-0283.940817</DOCID>
<TDTID>TDT002048</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 10:34</DATE>
<TITLE> SRI LANKA OPPOSITION WINS GENERAL ELECTION</TITLE>
<HEADLINE> SRI LANKA OPPOSITION WINS GENERAL ELECTION</HEADLINE>
<SUBJECT> BC-SRILANKA-ELECTION 1STLD </SUBJECT>
<AUTHOR>     By Rohan Gunasekera </AUTHOR>
<TEXT>
<NOTES> (Eds: adds official result, details of power-sharing talks)</NOTES>
<DATELINE>COLOMBO, Sri Lanka (Reuter) </DATELINE>
<P>  The opposition People's Alliance has won Sri Lanka's general election with 105 seats to the ruling United National Party's 94, the Elections Commissioner's office said Wednesday. </P>
<P> But party sources said the UNP was desperately seeking coalitions with minority Tamil parties and an ally of the People's Alliance to try to preserve its 17-year hold on power. </P>
<P> The People's Alliance ally the Sri Lanka Muslim Congress won seven seats, while an independent Tamil group standing in the north and allied to the ruling party got nine. </P>
<P> Two other Tamil parties, the Tamil United Liberation Front (TULF) and the Democratic People's Liberation Front, won five and three seats respectively. </P>
<P> An independent group standing in the central hills won one seat and a fringe leftist group took one seat in the southern Hambantota district. </P>
<P> The party sources said ruling party officials had begun wooing the Muslim Congress with the promise of three ministerial portfolios if it helped the UNP stay in power by forming a loose coalition. </P>
<P> The UNP was also trying to entice the TULF to support it, the sources said. </P>
<P> President Dingiri Banda Wijetunga, a UNP appointee, said earlier in a televised address he would announce the new government once the strengths of the parties were known. </P>
<P> ``I appeal to all my countrymen to remain calm,'' he said. ``You may rest assured that I will be acting in terms of the constitution.'' </P>
<P> Wijetunga said that under the constitution, he would appoint as prime minister the person who in his opinion could command the confidence of parliament. </P>
</TEXT>
</DOC>
