<DOC>
<DOCID>REU005-0228.940710</DOCID>
<TDTID>TDT000351</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/10/94 07:50</DATE>
<TITLE> TANZANIA REMOVES ACCUSED RWANDAN MASSACRE LEADER</TITLE>
<HEADLINE> TANZANIA REMOVES ACCUSED RWANDAN MASSACRE LEADER</HEADLINE>
<SUBJECT> BC-RWANDA-MASSACRES </SUBJECT>
<AUTHOR>     By Adam Lusekelo </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NGARA, Tanzania (Reuter) </DATELINE>
<P>  A former Rwandan mayor suspected of masterminding massacres in his homeland was expelled from a refugee camp in Tanzania Sunday. </P>
<P> Tanzanian officials said Jean-Baptiste Gatate, a former mayor of the town of Murambi, was removed from the Benaco Camp with his family and four bodyguards and taken by bus under police escort to the capital Dar es Salaam. </P>
<P> Government sources said Gatate, branded a butcher by the rebel Rwanda Patriotic Front (RPF), would seek refuge in Goma, eastern Zaire, just across the border from the self-declared Rwandan government's refuge in the northwest town of Gisenyi. </P>
<P> The Tanzania government was embarrassed by suggestions it was harboring a leader of the notorious Interahamwe militia since he arrived in Benaco as rebels closed in on southeast Rwanda. </P>
<P> Self-confessed Interahamwe members captured by rebels in May told journalists they slaughtered men, women and children from the Tutsi minority and Hutu government opponents on Gatate's orders. </P>
<P> Gatate, an activist in slain President Juvenal Habyarimana's National Republican Movement for Development and Democracy, escaped to Benaco Camp amid a flood of some 300,000 refugees, mosty of them Hutu. </P>
<P> Thousands of angry Hutus forced aid workers in Benaco to withdraw temporarily last month after U.N. officials objected to Gatate's presence at Benaco, the world's largest refugee camp. </P>
<P> Former Tanzanian President Julius Nyerere last week attacked the government over Rwanda's three months of civil war and massacres estimated by aid officials to have killed more than 500,000 people. </P>
<P> ``I fail to understand why, even the government of Tanzania has kept quiet on this. This was a planned genocide to kill Tutsis and enemies of the ruling party. You cannot be neutral on that.'' he said. </P>
<P> ``In the refugees from Rwanda we have those who are fleeing the war, and those plain killers who are in the camps here. Why are we protecting them? Why? They are known,'' he told reporters. </P>
</TEXT>
</DOC>
