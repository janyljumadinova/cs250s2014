<DOC>
<DOCID>REU010-0447.940721</DOCID>
<TDTID>TDT000875</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/21/94 17:39</DATE>
<TITLE> FLORIDA GROUP WILL NOT PRESS FOR ANTI-GAY AMENDMENT</TITLE>
<HEADLINE> FLORIDA GROUP WILL NOT PRESS FOR ANTI-GAY AMENDMENT</HEADLINE>
<SUBJECT> BC-USA-GAYS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TALLAHASSEE, Fla (Reuter) </DATELINE>
<P>  Anti-gay activists dropped plans Thursday to put before voters a proposed amendment to Florida's constitution that would have prohibited state gay rights legislation. </P>
<P> The Tampa-based American Family Association of Florida decided it would be too difficult to win support for the measure statewide. </P>
<P> Instead, the organization said it will try to pass such measures locally and is already backing a similar initiative in the city of Tampa. </P>
<P> ``We believe that a statewide constitutional amendment effort to stop gay rights is like killing an ant with a shotgun,'' said association president David Caton. </P>
<P> The group had planned to include the proposed constitutional amendment in the statewide ballot November 8. </P>
<P> The proposed amendment would have banned municipalities in Florida from including sexual preference as a characteristic protected by anti-discrimination laws. Only race, color, religion, sex, national origin, age, handicap, ethnic background, marital status and familial status can be used as protected criteria. </P>
<P> Caton said his group will now back Stop Turning Out Prisoners, another conservative activist group that is trying to place a petition on the ballot requiring prisoners to serve at least 85 percent of their sentences. </P>
</TEXT>
</DOC>
