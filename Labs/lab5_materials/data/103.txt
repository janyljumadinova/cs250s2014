<DOC>
<DOCID>REU004-0020.940707</DOCID>
<TDTID>TDT000238</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/07/94 08:41</DATE>
<TITLE> RESEARCHERS USE COMPUTER TO TRACK HEALTHY SPERM</TITLE>
<HEADLINE> RESEARCHERS USE COMPUTER TO TRACK HEALTHY SPERM</HEADLINE>
<SUBJECT> BC-HEALTH-SPERM </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  Researchers at a British university said Thursday they were using a sperm-tracking computer to help infertile couples. </P>
<P> Computer Image Sperm Screening was developed by scientists at the Nurture Fertility Clinic at Nottingham University to help doctors select fertile sperm. In its first clinical trial three women became pregnant through the technique, which doctors hope to launch worldwide by the end of the year. </P>
<P> ``We can program in certain parameters of movement of the tail (of the sperm), movement of the head, the percentage of straightness,'' Steve Green, a researcher at the clinic, told British Broadcasting Corporation television. </P>
<P> ``The computer will then analyze a sperm sample and a sperm that satisfies all of what we've asked it to look for, it will display as a different color (on the screen).'' </P>
<P> The technique has allowed the doctors to double their fertilization success rate, Green said. They hope that once the technique, which was used unsuccessfully on pandas at the London Zoo, is refined it will produce even better results. </P>
<P> ``It wouldn't surprise me that once we refine our techniques we will see an even greater increase that will certainly lead to the outcome of normal healthy children in a much higher incidence because if you improve fertilization rates you improve the chance of delivery,'' said Dr. Simon Fishel, the clinic's scientific director. </P>
<P> ``It is the development of technologies like these which will help men with fertility problems achieve their own child.'' </P>
</TEXT>
</DOC>
