<DOC>
<DOCID>REU002-0279.940704</DOCID>
<TDTID>TDT000121</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/04/94 13:13</DATE>
<TITLE> RWANDAN SOLDIER CLINGS VAINLY TO FRENCH FOR LIFE</TITLE>
<HEADLINE> RWANDAN SOLDIER CLINGS VAINLY TO FRENCH FOR LIFE</HEADLINE>
<SUBJECT> BC-RWANDA-ORPHANS </SUBJECT>
<AUTHOR>     By Patrick Muiruri </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BUTARE, Rwanda (Reuter) </DATELINE>
<P>  A Rwandan government soldier found hiding in a French evacuation convoy was gunned down by rebels after a life-and-death tug-of-war involving French troops. </P>
<P> The soldier clung desperately to the French troops, jumping from one jeep to another pleading for them to protect him. </P>
<P> But when it was obvious the French commandos were not going to help, with their two jeeps surrounded by Rwandan Patriotic Front (RPF) fighters with guns drawn, the frightened soldier made a run for it. </P>
<P> A rebel figher went after him and cut him down with a burst from his AK47 assault rifle. </P>
<P> The incident was witnessed by a Reuter Television cameraman Sunday just outside the besieged government-held town of Butare in southern Rwanda. </P>
<P> The convoy was part of the French evacuation of 600 orphans and 300 adult civilians from Butare under a truce agreed with the rebels, despite the rebels opposing the French humanitarian mission on the grounds that Paris had supported the Hutu-led govermment they are fighting to topple. </P>
<P> When the procession of 20 vehicles carrying evacuees, troops and journalists reached a rebel checkpoint about five miles from Butare, the government soldier was discovered hiding in the convoy. </P>
<P> After 20-25 minutes of arguing and shouting, the government soldier leapt onto a French military jeep and clung desperately to French troops. </P>
<P> There were three elite French commandos in the jeep. The RPF men surrounded it with their guns drawn. All other vehicles in the convoy drove on, leaving just two jeeps and a truck which were at the tail end of the convoy. </P>
<P> A tussle ensued, with the rebels trying to drag the man from the jeep. The French forces appeared reluctant to help. So the Rwandan soldier jumped out of the jeep and into the second one. </P>
<P> The scene was repeated. Again there was a struggle, with the French providing no help. </P>
<P> Finally, the Rwandan soldier -- realizing the French troops would not intervene -- jumped off the jeep and started running. One RPF soldier said ``maliza yeye'' (finish him). </P>
<P> An RPF fighter ran after the soldier, firing with his AK 47. The soldier tumbled into the nearby bush. </P>
<P> The orphans were to be taken to the neighboring Burundian capital Bujumbura while the adults were sent to the town of Cyangugu on the border with Zaire. The evacuees were from both the minority Tutsi and majority Hutu tribes. </P>
<P> The Tutsi-dominated RPF, while agreeing to a truce for the evacuation, remains deeply suspicious of France's intervention because Paris has previously armed and supported the Hutu-backed government. </P>
</TEXT>
</DOC>
