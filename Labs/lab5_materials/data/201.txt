<DOC>
<DOCID>REU006-0433.940713</DOCID>
<TDTID>TDT000486</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/13/94 08:49</DATE>
<TITLE> U.S.CONSUMER PRICES ROSE 0.3 PERCENT IN JUNE</TITLE>
<HEADLINE> U.S.CONSUMER PRICES ROSE 0.3 PERCENT IN JUNE</HEADLINE>
<SUBJECT> BC-USA-ECONOMY </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  U.S. consumer prices rose a moderate 0.3 percent in June, the government said Wednesday, in the second straight batch of inflation figures that should calm fears that the economy is in danger of overheating. </P>
<P> The report, coming on the heels of news that wholesale prices were steady last month, further reduces the likelihood the Federal Reserve will feel compelled to boost interest rates soon to quell inflation pressures. </P>
<P> Gasoline prices, which tumbled in May, rose last month, the Labor Department said, while fresh vegetable prices posted their largest rise in a year. The prices of medical care, clothing and airline tickets also rose. </P>
<P> The rise in the Consumer Price Index followed a 0.2 percent gain in May and a 0.1 percent April rise. </P>
<P> Excluding volatile food and energy prices, the ``core'' CPI rose 0.3 percent last month after rising 0.3 percent in May and 0.2 percent in April. </P>
<P> The rise in the CPI -- the most widely used measure of inflation -- as well as the core rate matched economists' expectations. </P>
<P> The central bank has boosted short-term interest rates four times since early February, and economists expect it will continue to do so in the months ahead as inflation gradually picks up. </P>
<P> Last Friday's unexpectedly strong June employment report had fueled speculation on Wall Street that the Fed would act again quickly to nip inflation in the bud. </P>
<P> During the first half of the year, the CPI rose at a 2.5 percent seasonally adjusted annual rate. That compares with a 2.7 percent increase in all of 1993. </P>
<P> The CPI report showed that energy prices rose 0.1 percent last month after dropping 1.0 percent in May. Gasoline prices prices rose 0.5 percent after tumbling 1.8 percent. </P>
<P> Food and drink prices rose 0.3 percent, matching the May advance. Prices of fresh vegetables rose 6.3 percent, the biggest jump since a 12.5 percent rise in May 1993. Beef, veal and pork prices fell. </P>
<P> Housing costs rose 0.1 percent after an 0.2 percent increase in May, while medical care costs advanced 0.4 percent, the same as in May. Apparel and upkeep costs jumped 0.6 percent versus 0.4 percent in May. </P>
<P> The June rise in the CPI means that a basket of goods that cost $100 on average between 1982 and 1984 cost $148.00 last month. </P>
</TEXT>
</DOC>
