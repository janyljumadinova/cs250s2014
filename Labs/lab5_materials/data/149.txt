<DOC>
<DOCID>REU005-0217.940710</DOCID>
<TDTID>TDT000349</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/10/94 06:16</DATE>
<TITLE> ISRAEL SAYS SUMMIT WITH JORDAN WOULD BOOST PEACE</TITLE>
<HEADLINE> ISRAEL SAYS SUMMIT WITH JORDAN WOULD BOOST PEACE</HEADLINE>
<SUBJECT> BC-MIDEAST-ISRAEL 1STLD </SUBJECT>
<AUTHOR>      By Howard Goller </AUTHOR>
<TEXT>
<NOTES>(Eds: adds comment from Rabin spokesman graf 3, edits)</NOTES>
<DATELINE>JERUSALEM (Reuter) </DATELINE>
<P>  Israeli said Sunday that Jordan's King Hussein would give a big boost to peace if he made good on his stated readiness to meet Prime Minister Yitzhak Rabin in public. </P>
<P> The king, who has met Israeli leaders secretly for decades, told members of Jordan's parliament on Saturday he would meet Rabin in order to collect on a U.S. promise to re-equip Jordan's army and waive $950 million in debts. </P>
<P> ``We said long ago that direct contacts between decision-makers on both sides of the conflict are the best ways and means to find solutions and we are welcoming such a meeting whenever it will be appropriate and well-arranged,'' Rabin's spokesman Oded Ben-Ami told Reuters. </P>
<P> The king said Washington promised that once a public meeting with Rabin was held, to take immediate steps to waive the debts and modernize Jordan's 100,000-strong army. He left the timing of the meeting open. </P>
<P> ``If the meeting between me and the Israeli Prime Minister is a price to change the picture of this country, I will not hesitate at all and consider it a service for my country,'' King Hussein said in the televised remarks. </P>
<P> Israel's Deputy Foreign Minister Yossi Beilin told Israel Radio he was ``very happy'' about the king's remarks. </P>
<P> ``I would be even happier if he would say that Jordan has an interest in making peace with Israel and not necessarily that it's an interest in the face of the Americans,'' Beilin said. </P>
<P> ``I was very happy to hear the things he told the members of parliament,'' Israeli Deputy Foreign Minister Yossi Beilin said. </P>
<P> ``I would be even happier if he would say that Jordan has an interest in making peace with Israel and not necessarily that it's an interest in the face of the Americans,'' Beilin told Israel Radio. </P>
<P> ``But whatever reason or excuse will bring about a meeting between King Hussein and the prime minister of Israel will be for the good if such a meeting will take place and I hope it will take place as soon as possible.'' </P>
<P> ``It would certainly be a very important step forward in the peace process,'' Beilin said. </P>
<P> The king said Washington promised, once a public meeting with Rabin was held, to take immediate steps to waive the debts and modernize Jordan's 100,000-strong army. He left the timing of the meeting open. </P>
<P> ``If the meeting between me and the Israeli Prime Minister is a price to change the picture of this country, I will not hesitate at all and consider it a service for my country,'' King Hussein said in the televised remarks. </P>
<P> Israeli officials will set foot on Jordanian soil for the first time publicly on July 18 when talks begin with Jordanian officials along their border on border demarcation, water and other issues blocking a peace treaty. </P>
<P> U.S.-brokered talks on economic cooperation will begin four days later in the Jordan Valley in the presence of the Israeli and Jordanian foreign ministers and possibly Secretary of State Warren Christopher. </P>
<P> Beilin spoke before leaving for a meeting in Tunisia of a steering committee for multi-national Middle East peace talks. </P>
<P> He said he expected an argument there over which of several short-term and far-reaching projects deserved priority. </P>
<P> ``We are approaching a moment of truth when we have to decide between different projects,'' Beilin said. </P>
</TEXT>
</DOC>
