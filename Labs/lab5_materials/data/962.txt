<DOC>
<DOCID>REU011-0135.940824</DOCID>
<TDTID>TDT002368</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/24/94 01:27</DATE>
<TITLE> NUDE THIEF SNATCHES WOMAN'S HANDBAG</TITLE>
<HEADLINE> NUDE THIEF SNATCHES WOMAN'S HANDBAG</HEADLINE>
<SUBJECT> BC-JAPAN-NUDE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TOKYO (Reuter) </DATELINE>
<P>  A nude thief startled a Japanese woman in an apartment building and robbed her of $250, police said Wednesday. </P>
<P> The woman, 46, was riding an elevator to visit a friend on the sixth floor of a building in Fukuoka, southern Japan, when it stopped unexpectedly on the second floor and the door jammed open. </P>
<P> As she got out of the lift to walk up the stairs, a naked man -- thought to be in his early 20s -- jumped out from a corner and snatched her handbag. </P>
<P> Police think the thief may have tampered with the elevator. They said it was the first time they had heard of a thief using nudity as a tactic. </P>
</TEXT>
</DOC>
