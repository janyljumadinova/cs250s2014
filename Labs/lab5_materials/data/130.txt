<DOC>
<DOCID>REU004-0413.940708</DOCID>
<TDTID>TDT000297</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 18:55</DATE>
<TITLE> BOAT PEOPLE STILL FLEEING HAITI AT RECORD PACE</TITLE>
<HEADLINE> BOAT PEOPLE STILL FLEEING HAITI AT RECORD PACE</HEADLINE>
<SUBJECT> BC-HAITI-REFUGEES 2NDLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: Updates figures throughout)</NOTES>
<DATELINE>MIAMI (Reuter) </DATELINE>
<P>  The U.S. Coast Guard picked up almost 10,000 Haitian boat people from Monday to Thursday, in what it said was the busiest four-day period in its history. </P>
<P> A Coast Guard spokeswoman said Friday the flow of Haitians was continuing unabated, as 971 had been taken from the waters around Haiti by late afternoon Friday. </P>
<P> The mass exodus of 9,697 boat people picked up from Monday through Thursday, and 12,571 so far in July, has strained the United States' capacity to process them. </P>
<P> Since June 16, when the Clinton administration began interviewing asylum-seekers in Jamaica rather than automatically sending them back to Haiti, the Coast Guard has picked up 18,392 boat people. </P>
<P> A U.S. official in Washington said 12,700 Haitian emigrants are at the U.S. military base at Guantanamo Bay, Cuba -- 200 more than its current capacity. The base is scrambling to expand facilities to accept up to 20,000 migrants. </P>
<P> Three thousand more Haitian migrants are on U.S. vessels in the Caribbean, the official said.  So far, 441 Haitians have opted to return to their homeland rather than staying at Guantanamo, the official said. </P>
<P> The Clinton administration earlier this week stopped giving Haitian boat people the opportunity to apply for U.S. resettlement. </P>
<P> Panama backed away Thursday from an agreement to provide a haven for 10,000 refugees. </P>
</TEXT>
</DOC>
