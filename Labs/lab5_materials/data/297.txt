<DOC>
<DOCID>REU009-0400.940719</DOCID>
<TDTID>TDT000743</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/19/94 13:54</DATE>
<TITLE> USAIR CRASH SURVIVOR SUES FOR $125 MILLION</TITLE>
<HEADLINE> USAIR CRASH SURVIVOR SUES FOR $125 MILLION</HEADLINE>
<SUBJECT> BC-USA-USAIR </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ATLANTA, (Reuter) </DATELINE>
<P>  A Texas man who was severely injured in the July 2 crash of a USAir plane in Charlotte, North Carolina, has filed a $125 million lawsuit over the incident. </P>
<P> In a state court lawsuit filed Monday in Atlanta, Dorian Doucette, 20, seeks $25 million in actual damages and $100 million in punitive damages from USAir, the pilot and co-pilot of the plane and the aircraft's manufacturer, McDonnell Douglas Corp. </P>
<P> The USAir jet crashed while trying to land in Charlotte, with 37 of the 57 people aboard killed. </P>
<P> Doucette, an Army private from Baytown, Texas, near Houston, suffered burns on more than 60 percent of his body and the loss of a leg, the lawsuit said. He is listed in critical condition at Brooke Army Medical Center in San Antonio, Texas. </P>
<P> Other plaintiffs in the suit are his parents, Floyd and Gracie Doucette and his son Dante Keon Doucette. </P>
<P> The suit alleges that pilot Michael Greenlee and co-pilot James Hayes were negligent in trying to land the plane at Charlotte airport amid warnings of potential windshears and despite having enough fuel to land elsewhere. </P>
<P> It also alleges that Arlington, Virginia-based USAir failed to train its pilots properly to protect passengers from windshears and pressured its pilots to land aircraft on time despite dangerous weather conditions. </P>
<P> USAir declined comment on the lawsuit as a matter of policy. A spokesman said the carrier is fully insured for all liabilities. </P>
<P> St Louis-based McDonnell Douglas also said it does not comment on pending litigation. </P>
</TEXT>
</DOC>
