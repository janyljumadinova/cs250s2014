<DOC>
<DOCID>REU005-0196.940710</DOCID>
<TDTID>TDT000342</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/10/94 01:35</DATE>
<TITLE> VIETNAM TO STOP USING RUSSIAN AIRLINERS</TITLE>
<HEADLINE> VIETNAM TO STOP USING RUSSIAN AIRLINERS</HEADLINE>
<SUBJECT> BC-VIETNAM-AIRLINE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>HANOI, Vietnam (Reuter) </DATELINE>
<P>  Vietnam Airlines will stop using Russian planes, in order to allay concerns about its safety record, the official Vietnam News reported Sunday. </P>
<P> ``In an effort to clear its safety reputation, Vietnam Airlines has decided to gradually replace all Russian-made planes,'' the newspaper said. </P>
<P> It quoted the state-run company's sales manager, Nguyen Thi Minh Yen, as saying its Russian-built planes would be phased out by the end of the year. </P>
<P> ``In fact the Russian jets have been reliable and safe but foreigners just don't like them and we have to accept this,'' she was quoted as saying. </P>
<P> The number of foreigners visiting Vietnam has soared in the past couple of years and officials want to ensure potential visitors are not put off by the country's dilapidated transportation system. </P>
<P> Apart from aging Russian-made Tupolev and Yak-40 airliners currently relegated to domestic routes, Vietnam Airlines has 11 aircraft at present, seven Airbus A-320s leased from Air France, two Boeing 767s leased from Ansett Airlines and two of its own French-made ATR-72s. </P>
<P> The company has said it plans to more than double its fleet in the next six years. </P>
</TEXT>
</DOC>
