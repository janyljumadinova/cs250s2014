<DOC>
<DOCID>REU008-0164.940816</DOCID>
<TDTID>TDT002014</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/16/94 19:28</DATE>
<TITLE> NO LEADS IN PANAMA BOMBING, CHIEF INVESTIGATOR SAYS</TITLE>
<HEADLINE> NO LEADS IN PANAMA BOMBING, CHIEF INVESTIGATOR SAYS</HEADLINE>
<SUBJECT> BC-PANAMA-BOMB </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PANAMA CITY, Panama, (Reuter) </DATELINE>
<P>  Nearly a month after the bombing of a commuter plane in Panama that killed 21 people, Panama's chief investigator in the case said Tuesday police have no leads as to who was behind the incident. </P>
<P> ``We have no evidence linking anyone to the bombing right now,'' Carlos Augusto Herrera, the government's chief investigator for the case, told Reuters. </P>
<P> Herrera denied reports in Panama's press that police had arrested four men in connection with the July 19 bombing. </P>
<P> The attack killed several prominent local Jewish businessmen and took place around the same time as a bomb blast destroyed the headquarters of several Jewish groups in Argentina. </P>
<P> Herrera said three Arabic-speaking men were in custody for carrying false passports and were being questioned by officials about the bombing. </P>
<P> ``Of course, we are keeping the possibility open that they had something to do with the crash, but we have no evidence in that direction,'' Herrera said. </P>
<P> Investigators have said a bomb caused the plane to explode shortly after take-off from the Atlantic port city of Colon. Herrera said suspicion rests on an unclaimed victim who officials believe is from Lebanon and may have acted as a suicide bomber. </P>
<P> ``We are focusing in that direction, but we cannot say anything conclusive until the investigation is complete,'' Herrera said. </P>
<P> Herrera also denied published reports that officials had found evidence of a local organization sympathetic to the pro- Iranian group Hizbollah, which is based in Lebanon. </P>
<P> Panama's Jews have said they believe the bombing was the work of anti-Jewish ``terrorists'' trying to derail the Middle East peace process. </P>
<P> Others here believe Colombian drug lords are responsible. </P>
<P> U.S. and Israeli intelligence officials are assisting in the investigation, according to embassies from both nations in Panama City. </P>
</TEXT>
</DOC>
