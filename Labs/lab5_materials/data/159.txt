<DOC>
<DOCID>REU005-0410.940711</DOCID>
<TDTID>TDT000368</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/11/94 03:58</DATE>
<TITLE> G7 SEEKS MORE INFORMALITY FROM ITS ANNUAL SUMMIT</TITLE>
<HEADLINE> G7 SEEKS MORE INFORMALITY FROM ITS ANNUAL SUMMIT</HEADLINE>
<SUBJECT> BC-GROUP 1STLD </SUBJECT>
<AUTHOR>     By Paul Holmes </AUTHOR>
<TEXT>
<NOTES>(Eds: New throughout)</NOTES>
<DATELINE>NAPLES, Italy (Reuter) </DATELINE>
<P>  Group of Seven leaders want to get back to an armchair atmosphere at their annual summits after a meeting at which issues of the day from Bosnia and North Korea to the dollar crowded in on their agenda. </P>
<P> The three-day gathering in Naples was the 20th get-together of leaders of the world's richest nations in a series going back to a meeting in 1975 at the Chateau of Rambouillet, France. </P>
<P> The ``fireside chat'' feel of the first summit, when French President Valery Giscard d'Estaing and five fellow leaders met to swap views on exchange rates, has long gone. </P>
<P> Glittering formal banquets and enormous motorcades in the hermetically-sealed center of Naples, made-for-TV walkabouts by some of the leaders and a packed program for wives showed how much summitry had changed since Rambouillet. </P>
<P> Two hundred journalists covered that first meeting compared with the 3,000 who invaded Naples to follow the leaders of Britain, Canada, France, Germany, Italy, Japan and the United States and, for Sunday's ``G8'' political talks, Russia. </P>
<P> Prime Minister Jean Chretien of Canada, whose country joined capitalism's top table in 1976 to make the G6 the G7, now wants to restore some of the original atmosphere when the venue moves to Halifax, Nova Scotia, next year. </P>
<P> ``There is a great desire to make it more informal than at the moment,'' said Chretien. ``Everyone has the same view. We have to come back to more businesslike operation.'' </P>
<P> The leaders agreed to focus the 1995 summit on the economic challenges of the 21st century and a review of multinational institutions whose adequacy has come under scrutiny in a rapidly changing, unstable post-Cold War world. </P>
<P> ``As we approach the threshold of the 21st century, we are conscious of our responsibility to renew and revitalize these institutions and to take on the challenge of integrating the newly emerging market democracies across the globe,'' the leaders said in their economic statement. </P>
<P> In Naples, the cornerstone themes of jobs and growth had to compete with the immediate problems of the war in Bosnia, the sudden death of North Korea's ``Great Leader'' Kim Il-sung and market expectations -- ultimately unfulfilled -- of action to rescue a weak dollar. </P>
<P> Next year will undoubtedly also see last minute changes to the best laid plans of the summiteers. </P>
<P> ``The obvious forecast (for summits of the 1990s) is that the leaders will discuss the issues that they will be forced to address,'' Italian international affairs analyst Guido Garavoglia wrote in a history of G7 summitry published for the Naples meet. </P>
<P> ``Given the nature of the G7, it is bound to face the major challenges of the day (and) the flexibility that characterises this semi-institution is a particularly valuable asset,'' Garavoglia wrote. </P>
<P> Apart from failure to meet market expectations on the dollar, the Naples summit also saw President Clinton drop a bid for early talks on free trade in telecommunications and aircraft in the face of French and German opposition. </P>
<P> But consensus appeared to have been reached on a host of political issues. </P>
<P> They include a pledge to keep up the pressure on Haiti's army rulers to restore democracy and a call on Algeria's army-backed government to negotiate with non-violent Islamist opposition forces to end civil strife dramatized by the eve-of-summit slaying of seven Italian seamen. </P>
<P> On North Korea, the presence around the same table of leaders of the world's eight most powerful nations including Russia allowed for a rapid united stance on policy towards Pyongyang following Kim's death. </P>
<P> In a tough statement, they said they would continue to urge North Korea to open its nuclear program to full international inspection and remove suspicions ``once and for all.'' </P>
<P> The summit also yielded a stern warning to Bosnia's factions to accept a ``make-or-break'' big power peace plan by July 19 or confront the risk of a wider war in the Balkans. </P>
<P> Russia, present at the political talks for the first time as a full participant, and backed by host Italy, pressed for a more institutionalized ``G8'' foreign policy role through meetings of foreign ministers between summits to respond to crises. </P>
<P> Italian Foreign Minister Antonio Martino said ministers of the G7 and Russia would meet in New York in September during the U.N. General Assembly. </P>
</TEXT>
</DOC>
