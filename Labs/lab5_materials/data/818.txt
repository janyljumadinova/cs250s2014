<DOC>
<DOCID>REU008-0183.940816</DOCID>
<TDTID>TDT002016</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/16/94 20:47</DATE>
<TITLE> BOY'S STEPFATHER ACCUSES JACKSON OF BREAKING UP FAMILY</TITLE>
<HEADLINE> BOY'S STEPFATHER ACCUSES JACKSON OF BREAKING UP FAMILY</HEADLINE>
<SUBJECT> BC-USA-JACKSON </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  The stepfather of a 14-year-old boy who accused Michael Jackson of sexually molesting him sued the pop superstar Tuesday, alleging the entertainer used his wealth and fame to break up their family. </P>
<P> In a lawsuit filed in Santa Monica Superior Court, the stepfather alleged that Jackson ``interposed himself'' into his family, showering his wife and children with gifts and ultimately ``sexually assaulting'' his stepson, according to the man's lawyer, Brian Freedman. </P>
<P> Jackson has denied the boy's claims, which first came to light in August 1993 and resulted in a civil settlement earlier this year in which Jackson agreed to pay millions of dollars. Jackson's lawyers could not be reached for comment on the latest lawsuit. </P>
</TEXT>
</DOC>
