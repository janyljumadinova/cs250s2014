<DOC>
<DOCID>REU008-0077.940715</DOCID>
<TDTID>TDT000591</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 11:21</DATE>
<TITLE></TITLE>
<HEADLINE></HEADLINE>
<SUBJECT> BC-RWANDA-USA-RELATIONS -2 WASHINGTON </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE></DATELINE>
<P> ``We're going to derecognize the Rwandan diplomatic establishment here,'' Christopher told a news conference at the White House. </P>
<P> ``We'll be taking some steps here today to, in effect, indicate that the embassy here in Washington is no longer representative and we're taking the appropriate diplomatic steps to end their representation here in Washington.'' </P>
<P> Christopher added that this did not mean the United States was recognizing a new government in Rwanda. </P>
<P> He spoke as waves of refugees fleeing Rwanda's civil war poured into Zaire for a second day along with the retreating army of the collapsed government. The refugees are mostly Hutus fleeing the rapid advance of the Tutsi-dominated Rwanda Patriotic Front. </P>
</TEXT>
</DOC>
