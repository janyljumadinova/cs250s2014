<DOC>
<DOCID>REU013-0221.940727</DOCID>
<TDTID>TDT001122</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/27/94 19:07</DATE>
<TITLE> CRIME SPREE BLAMED FOR TOURISM DECLINE</TITLE>
<HEADLINE> CRIME SPREE BLAMED FOR TOURISM DECLINE</HEADLINE>
<SUBJECT> BC-JAMAICA-TOURISM </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>KINGSTON, Jamaica (Reuter) </DATELINE>
<P>  Second-quarter tourist arrivals in Jamaica dropped 8.5 percent from last year, according to figures released Wednesday, and hoteliers blamed a recent spate of crimes against visitors. </P>
<P> Quarterly figures were released Wednesday by the Jamaica Tourist Board, which said arrivals during the first quarter of 1994 were up by the same 8.5 percent over last year. As a result, total arrivals for the first half of 1994 are up just 0.8 percent, to 830,383 visitors, killing off expectations for a strong year. </P>
<P> Analysts predict industry earnings will be flat because hotels have had to discount heavily to overcome a spate of bad publicity from the crime wave. </P>
<P> In June, a well-known community worker from Orlando, Florida, was murdered in the villa where he was staying on Jamaica's north coast. On July 3, an 18-year-old American visitor was shot and killed in central Jamaica. </P>
<P> Earlier this year, two Pennsylvania tourists were shot and robbed while rafting, and two young British girls were raped by security guards at a north coast hotel. </P>
</TEXT>
</DOC>
