<DOC>
<DOCID>REU002-0124.940803</DOCID>
<TDTID>TDT001406</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 13:57</DATE>
<TITLE> CHAPLIN, CHIMP COSTUMES IN MOVIE AUCTION</TITLE>
<HEADLINE> CHAPLIN, CHIMP COSTUMES IN MOVIE AUCTION</HEADLINE>
<SUBJECT> BC-BRITAIN-CHAPLIN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  The costume worn by Charlie Chaplin when he played a Hitler-like character in ``The Great Dictator'' and a chimp costume from a Tarzan movie are among the items to be offered for sale at an auction house next month. </P>
<P> Sotheby's said Wednesday the Chaplin costume was the highlight of its planned sale September 14 of movie memorabilia. </P>
<P> Chaplin wore the costume -- an off-white jacket with blue lapels, jodhpurs and a cap -- when portraying the character Adenoid Hynkel in the 1940 film. It is expected to fetch more than $38,400. </P>
<P> The chimp costume, from ``Greystoke: The Legend of Tarzan, Lord of the Apes,'' is expected to sell for $4,600. </P>
<P> The ski suit worn by Roger Moore's stunt double in the James Bond movie ``The Spy Who Loved Me'' is also being offered for sale. </P>
<P> Apart from the outfits from memorable and not so memorable movies, Sotheby's is exploiting publicity over the Steven Spielberg feature film ``The Flintstones'' to offer cartoon stills from the original Flintstones television series. </P>
<P> Depicting Fred Flintsone and Barney Rubble, the original stills are expected to sell for about $770 apiece. </P>
</TEXT>
</DOC>
