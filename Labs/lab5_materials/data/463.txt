<DOC>
<DOCID>REU013-0404.940728</DOCID>
<TDTID>TDT001168</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/28/94 17:43</DATE>
<TITLE> WARRING YEMENIS MEET, AGREE MORE TALKS-UN MEDIATOR</TITLE>
<HEADLINE> WARRING YEMENIS MEET, AGREE MORE TALKS-UN MEDIATOR</HEADLINE>
<SUBJECT> BC-YEMEN-TALKS 3RDLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Updates with confirmation of meeting, recasts)</NOTES>
<DATELINE>GENEVA (Reuter) </DATELINE>
<P>  Representatives of the Yemeni government in Sanaa and the defeated secessionists in the south held three hours of talks Thursday and agreed to pursue a ``political dialogue,'' U.N. mediator Lakhdar Brahimi said. </P>
<P> He told reporters the meeting around a table at the U.N. European headquarters had been held ``in a good atmosphere'' and that the two sides would maintain contact with him to fix a date and venue for a further encounter. </P>
<P> ``The important thing is that there is a recognition that there is a profound need for a political dialogue. That is very encouraging,'' the former Algerian foreign minister said. Both sides would now report back to their leaderships. </P>
<P> There was no comment from either delegation -- headed for Sanaa by planning minister Abdul Karim al-Iryani and for the secessionists by Haider Abu-Bakr al-Attas, prime minister of the short-lived southern government. </P>
<P> But Brahimi -- initially appointed to try to head off the civil war that broke out in May and ended July 7 with the defeat of the separatists -- said he had been authorized by both sides to speak on their behalf. </P>
<P> The talks, and their apparently positive outcome, was a clear triumph for Brahimi, who earlier in the day had appeared to face the meeting's collapse before it started. </P>
<P> Yemen's Foreign Minister Mohammed Salem Basendwa had told Reuters in Dubai by telephone from Sanaa that al-Iryani would not meet the southerners and would simply explain to the mediator his government's refusal to talk. </P>
<P> He said al-Atass and Abdullah al-Asnaj, the secessionists' foreign minister, did not represent the people of the south and had ``no capacity at all.'' </P>
<P> A clearly relieved but tired-looking Brahimi said the aim of the meeting, which he had agreed earlier with the two sides at a meeting in New York, was to work towards national reconciliation after the bloody two-month conflict. </P>
<P> But he told a news conference that the situation was still ``very complex'' and both sides realized the causes of the split in the four-year unity government of Yemen and the civil war remained. </P>
</TEXT>
</DOC>
