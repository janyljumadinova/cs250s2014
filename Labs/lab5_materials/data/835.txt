<DOC>
<DOCID>REU008-0306.940817</DOCID>
<TDTID>TDT002052</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 12:13</DATE>
<TITLE> VENEZUELAN PEASANTS END MEXICAN EMBASSY TAKEOVER</TITLE>
<HEADLINE> VENEZUELAN PEASANTS END MEXICAN EMBASSY TAKEOVER</HEADLINE>
<SUBJECT> BC-VENEZUELA-MEXICO </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CARACAS (Reuter) </DATELINE>
<P>  Thirty-seven Venezuelan peasants who took over the Mexican embassy in Caracas to demand land rights and show support for Mexico's Zapatista rebels ended their occupation early Wednesday, witnesses said. </P>
<P> The unarmed peasants from the central Venezuelan state of Aragua forced their way into the embassy Tuesday afternoon. </P>
<P> Police surrounded the diplomatic mission, and the protesters agreed to leave peacefully after lengthy negotiations with Venezuelan government officials. </P>
<P> A lawyer representing the peasants, Alberto Solana, told reporters they had achieved their aim of winning guarantees of land ownership. </P>
<P> Solano said the peasants were members of the left-wing Revolutionary Bolivariano Movement 200 founded by Lieutenant- Colonel Hugo Chavez Frias, the leader of a bloody coup attempt in Venezuela in February 1992. </P>
<P> Chavez was jailed after the failed attempt to topple then President Carlos Andres Perez but was released by President Rafael Caldera in March and instantly formed his grassroots political movement. </P>
<P> The takeover of the embassy was also intended to express solidarity with the Zapatista rebels who staged a bloody uprising in southern Mexico in January, Solano said. </P>
<P> The protesters were allowed to go free and have returned to Aragua, Foreign Ministry spokesman Luis Moreno Gomez told Reuters. </P>
</TEXT>
</DOC>
