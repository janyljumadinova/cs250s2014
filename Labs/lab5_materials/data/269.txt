<DOC>
<DOCID>REU009-0012.940717</DOCID>
<TDTID>TDT000664</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/17/94 15:57</DATE>
<TITLE> ASSAD SAID UNCONCERNED BY HUSSEIN-RABIN MEET</TITLE>
<HEADLINE> ASSAD SAID UNCONCERNED BY HUSSEIN-RABIN MEET</HEADLINE>
<SUBJECT> BC-MIDEAST-CHRISTOPHER </SUBJECT>
<AUTHOR>     By Patrick Worsnip </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SHANNON, Ireland (Reuter) </DATELINE>
<P>  Syrian President Hafez al-Assad has told the United States he is unconcerned by King Hussein of Jordan's forthcoming summit with Israeli Prime Minister Yitzhak Rabin, U.S. officials said Sunday. </P>
<P> ``He as much as said: 'Well, that's King Hussein's business','' said a senior official aboard a plane taking Secretary of State Warren Christopher to Israel at the start of a week-long Middle East shuttle. </P>
<P> ``It wasn't a problem.'' </P>
<P> President Clinton announced last Friday that Hussein and Rabin would hold a summit in Washington July 25. It will be the first public meeting between the two men although there have been reports of several secret talks. </P>
<P> Before announcing the summit, Clinton held a 35-minute telephone conversation with Assad in what officials said was an attempt to reassure him that Washington was also keen to see progress in Syrian-Israeli peace talks. </P>
<P> Some U.S. officials remain concerned that Assad will feel left behind in the peace process which began three years ago and has seen agreements between Israel and the Palestine Liberation Organization (PLO) and now progress with Jordan. </P>
<P> The state-controlled Syrian press has criticized both developments, but the official aboard Christopher's plane noted that it had remained silent on the Washington summit announcement. </P>
<P> ``Assad has said that as far as he is concerned, he is focused on what he is doing with us,'' said the official, briefing reporters on condition he not be identified. </P>
<P> The Syrian-Israeli talks center on the Golan Heights, seized by Israel from Syria in the 1967 Middle East war. There has been no agreement so far on the timing of an Israeli withdrawal and officials expect progress to be slow. </P>
<P> Pushing along the Israeli-Syrian talks is a major aim of this week's tour. Christopher is due to make at least one and possibly two trips to Damascus as well as visiting Israel and Jordan and meeting PLO chairman Yasser Arafat. </P>
<P> The Secretary of State arrives just hours after bloody clashes between Israelis and Palestinians at the Erez checkpoint between the Gaza Strip and Israel. </P>
<P> In a statement released on his plane, Christopher expressed ``shock and deep sadness'' over the casualties and called on both sides to intensify contacts through their security liaison system to avoid future such incidents. </P>
<P> In one highlight of the tour Christopher will attend a meeting between the Israeli and Jordanian foreign ministers, Shimon Peres and Abdul-Salam Majali, on the Jordanian side of the Dead Sea Wednesday. </P>
<P> The official accompanying Christopher said King Hussein had prepared the ground carefully with his subjects for the summit announcement, and that most Jordanians now felt that ``Jordan should not lose out by holding back.'' </P>
<P> Jordan has border and water issues to resolve with Israel. No peace treaty is expected to be signed at the summit. </P>
<P> On Washington's overall strategy, the official said: ``You make progress wherever you can make progress ... We think progress can be made -- it's just a question of how quickly.'' </P>
<P> The official also denied that there were any U.S. plans to set up an Aid for International Development office in Arab East Jerusalem to administer aid for Gaza and Jericho where the PLO has set up a self-rule authority. </P>
<P> Israel has strongly opposed such a move as lending weight to PLO claims to East Jerusalem. Israel says Jerusalem will remain its undivided capital. </P>
</TEXT>
</DOC>
