<DOC>
<DOCID>REU002-0073.940703</DOCID>
<TDTID>TDT000099</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/03/94 14:11</DATE>
<TITLE> AFGHAN PRESIDENT OFFERS TO HAND OVER TO NEUTRALS</TITLE>
<HEADLINE> AFGHAN PRESIDENT OFFERS TO HAND OVER TO NEUTRALS</HEADLINE>
<SUBJECT> BC-AFGHAN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PESHAWAR, Pakistan (Reuter) </DATELINE>
<P>  Afghan President Burhanuddin Rabbani has offered to hand over power to a neutral government to end current fighting, his spokesman said Sunday. </P>
<P> Rabbani's assurance not to run for another term and to hand over to a neutral administration was contained in a letter delivered to an Islamic peace envoy Sunday, spokesman Masud Khalili told Reuters. </P>
<P> The neutral administration is to be chosen by a council likely to meet next week in the western Afghan town of Herat. </P>
<P> Khalili, Rabbani's special envoy, said he delivered the letter to Organization of Islamic Conference (OIC) secretary-general Hamid al-Gabid in the Pakistani capital Islamabad Sunday. </P>
<P> Gabid arrived in Islamabad Saturday at the start of a mission to end fighting between forces loyal to Rabbani and his opponents led by Prime Minister Gulbuddin Hekmatyar and northern warlord General Abdul Rashid Dostum. </P>
<P> Doctors and officials in Kabul said at least 14 people were killed and about 90 wounded there Saturday in bombing and shelling by the rival factions, who began their latest round of battles for supremacy on January 1. </P>
<P> The Afghan capital was comparatively calm Sunday, the official Kabul Radio said. </P>
<P> But artillery and rocket attacks on the frontlines started in the morning and continued until the evening, it said. </P>
<P> Officials and hospitals say more than 100 people have been killed and 700 injured, many of them civilians, in the latest battles that erupted in Kabul last week. </P>
<P> On Thursday Rabbani said he was ready to step down to make way for a new government but demanded his foes stop bombing Kabul. </P>
</TEXT>
</DOC>
