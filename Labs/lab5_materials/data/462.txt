<DOC>
<DOCID>REU013-0392.940728</DOCID>
<TDTID>TDT001167</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/28/94 16:46</DATE>
<TITLE> SENATE PROBING FOSTER DEATH IN WHITEWATER SESSION</TITLE>
<HEADLINE> SENATE PROBING FOSTER DEATH IN WHITEWATER SESSION</HEADLINE>
<SUBJECT> BC-WHITEWATER-SENATE </SUBJECT>
<AUTHOR>     By William Scally </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  The Senate Banking Committee will open hearings on the Whitewater affair Friday with an inquiry into the suicide a year ago of White House Deputy Counsel Vincent Foster, the committee said Thursday. </P>
<P> The hearing, at which medical and police witnesses will testify, follows Whitewater hearings by the House of Representatives Banking Committee at which the Foster suicide was voted off-limits. </P>
<P> In an exhaustive report in his investigation of the Foster suicide, Whitewater Special Counsel Robert Fiske concluded that Foster did indeed kill himself in a Virginia park near Washington, where his body was found on July 20, 1993. </P>
<P> The House committee Thursday held its second day of hearings into contacts between White House aides and Treasury Department officials concerning an investigation into the failed Madison Guaranty savings and loan institution in Arkansas. </P>
<P> The head of the bank was a partner with the Clintons in a money-losing investment in the Whitewater vacation development in the Ozark Mountains. </P>
<P> Fiske has asked that the current hearings should not go beyond the contacts and the Foster suicide so as not to hamper his own inquiries into other areas. </P>
<P> In a party line vote Tuesday, the House committee decided not to go into the suicide after Chairman Henry Gonzalez, a Texas Democrat, said no one would dispute Fiske's findings and that further airing of the suicide would distress Foster's family. </P>
<P> Witnesses at the Senate hearing will include Charles Hirsch, chief medical examiner of New York City, who was a consultant for Fiske's inquiry, U.S. Park Police officers and an FBI agent. </P>
<P> Meanwhile, Representative Dan Burton, an Indiana Republican who has raised questions about Foster's suicide, said Thursday he had interviewed a witness known as ``CW,'' who discovered Foster's body. </P>
<P> ``CW,'' who was also interviewed by Fiske's investigators, said in a statement released by Burton that a police photo taken of the body differed from what he had seen, suggesting that the body had been moved. </P>
<P> Conservative conspiracy theorists have suggested that Foster might have been murdered or that his body was taken to the park after he died somewhere else. </P>
<P> Whitewater hearings are to continue next week in both the House and Senate banking committees. </P>
</TEXT>
</DOC>
