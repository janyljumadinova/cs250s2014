<DOC>
<DOCID>REU006-0439.940813</DOCID>
<TDTID>TDT001859</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/13/94 14:35</DATE>
<TITLE> EDINBURGH FESTIVAL ``BLASPHEMY'' ROW OVER JESUS PLAY</TITLE>
<HEADLINE> EDINBURGH FESTIVAL ``BLASPHEMY'' ROW OVER JESUS PLAY</HEADLINE>
<SUBJECT> BC-BRITAIN-FRINGE </SUBJECT>
<AUTHOR>     By Jenny Shields </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>EDINBURGH, Scotland (Reuter) </DATELINE>
<P>  A play featuring a modern-behaved Jesus Christ married to a prostitute has outraged some residents in the Edinburgh Festival's host city. </P>
<P> ``Passion,'' a controversial drama on Jesus claims he did not die on the cross, as Christians believe, but made love to Mary Magdalene, a whore-turned-disciple, and fathered two children. </P>
<P> Every year at least one theatrical act manages to grab the limelight of the festival with stories of lewd or dangerous performances, outraging councillors and swelling ticket sales. </P>
<P> It's part of the annual ritual surrounding the world's largest arts festival and this year's Fringe -- which gets properly under way at the weekend -- is no exception. </P>
<P> ``I don't need anyone updating what Jesus Christ means to me,'' said local councillor Moira Knox. ``It has offended a lot of people. It's really just not on,'' Knox told BBC radio. </P>
<P> In the play Herod's wife is portrayed as a sado-masochist, while Jesus and Mary make love on the Mount of Olives before escaping to France where they raise two children. </P>
<P> Knox, dubbed the city's ``moral guardian'' called the play a ``blasphemous and crass mockery of the life of Christ.'' </P>
<P> Claiming the festival organizers had reached a ``decadent low'' by allowing the play to be performed, Knox said she wanted to see a return of the city's bailies - functionaries last seen 20 years ago, to act as censors of Fringe acts. </P>
<P> ``Passion'' will run at the Church Hill Theatre from Sunday to August 24. </P>
<P> The Toyko Shock Boys, who are all Japanese, are described as ``anarchic kamikaze daredevils who perform unspeakable acts with samurai swords, scorpions, firecrackers and watermelons'' to a Japanese rock and roll soundtrack. </P>
<P> Council officials who have seen their act at the Assembly Rooms, say that while it looks real it is actually illusory. </P>
<P> Meanwhile Jim Rose, a Fringe circus performer who prides himself on outrageous stunts, has had hospital treatment after one trick went badly wrong. </P>
<P> Rose was hammering a spoon up his nostril during a live radio program in the Scottish capital when, according to a report in The Scotsman newspaper, he collapsed and was taken to hospital where casualty doctors discovered he had ruptured some nasal membranes. </P>
<P> A spokesman for the show on Festival FM, Mark Borkowski, was quoted as saying that Rose had not done the trick for some time. ``He had eaten a lightbulb on the show before and wanted to do something different. He usually manages it with a nail.'' </P>
<P> Staff at Edinburgh District Council, which provides the entertainment licences and many of the venues for the shows, have seen it all before. </P>
<P> A council spokesman said: ``Every year we have at least one act trying to win a bit of advance publicity by luring people into condemning it as shocking or indecent or whatever.'' </P>
<P> This year's Fringe will involve more than 9,000 performers putting on hundreds of plays, scores of musicals as well as circuses, comedy and dance routines in venues ranging from the grandeur of the Assembly Hall to a brewery and a hamburger diner. </P>
</TEXT>
</DOC>
