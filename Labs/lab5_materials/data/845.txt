<DOC>
<DOCID>REU008-0382.940817</DOCID>
<TDTID>TDT002075</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 21:42</DATE>
<TITLE> ISRAEL, PLO TACKLE THREATS TO SELF-RULE ACCORD</TITLE>
<HEADLINE> ISRAEL, PLO TACKLE THREATS TO SELF-RULE ACCORD</HEADLINE>
<SUBJECT> BC-MIDEAST-TALKS 2NDLD </SUBJECT>
<AUTHOR>     By Samia Nakhoul </AUTHOR>
<TEXT>
<NOTES>(Eds: Updates throughout)</NOTES>
<DATELINE>ALEXANDRIA, Egypt (Reuter) </DATELINE>
<P>  Israel and the PLO, seeking to overcome thorny issues of security and finance threatening their fragile peace agreement, gave a hopeful assessment after a lengthy day of talks. </P>
<P> ``By and large this has provided some solutions to important problems and we intend to continue negotiations until we overcome remaining problems,'' Israeli Foreign Minister Shimon Peres told reporters early Thursday. </P>
<P> Peres and PLO negotiator Nabil Shaath issued an 11-point statement covering security in the autonomous regions of Gaza and Jericho, a release of Israeli-held prisoners, international funding for Palestinian authorities and an expansion of their powers. </P>
<P> But it did not mention plans for elections in the self-rule areas, which Palestinians say they fear Israel is seeking to stall. </P>
<P> ``We agreed on taking the necessary measures to reduce terror,'' Peres said during a short break in the talks which lasted into the early hours of Thursday. </P>
<P> Later he declined to give details but he said Israel ``expects the Palestinian police ... to get hold of those who are terrorizing innocent people and send them to court.'' </P>
<P> He added: ``We never said that the Palestinian authorities would have 100 percent success but we want to see 100 percent effort.'' </P>
<P> Shaath said the self-rule accord signed by the two sides in Cairo three months ago ``includes our commitment on the maintenance of peace and security and doing everything possible to stop violence. We are committed to our pronouncements on the issue.'' </P>
<P> The statement said Israel and the Palestine Liberation Organization agreed to continue talks on early empowerment, or the expansion of Palestinian authority from Jericho to the rest of the West Bank, ``so as to conclude them by August 24.'' </P>
<P> ``In addition, the two sides agreed to jointly approach donor countries with a request to support the initial funding of the agrement with $30 million to $40 million which are needed to bridge the gap expected in the budget of the transferred spheres,'' it said. </P>
<P> It said 249 prisoners held by Israel would be released. Peres said they would go free within two days. </P>
<P> Israel would also consider opening or rebuilding houses of prisoners and deportees whose homes were sealed or destroyed by Israeli authorities during their detention. </P>
<P> Palestinian delegates told reporters they submitted papers to the Israelis earlier in the week on preparations for Palestinian elections. </P>
<P> PLO delegate Saab Erekat said they demanded elections by December 15, but Israel was preventing them from registering voters in the West Bank and had not released any population records. </P>
<P> ``We feel that the elections should not be delayed. They are an extension of democracy and the only real mandate that can be given (to the Palestinian authority),'' Shaath said. </P>
<P> Peres said Israel was not seeking to delay the election. ``In accordance with the accord, we have to negotiate,'' he said. </P>
<P> The statement also covered details of security at border crossings into the Palestinian self-rule areas and links between the two separate enclaves of Gaza and Jericho -- issues which have proved stumbling blocks in the past. </P>
<P> The two sides agreed on a twice-weekly bus service between Gaza and Jericho to carry uniformed Palestinian police and people denied entry into Israel. </P>
<P> They also agreed to hold discussions toward establishing an industrial zone in northern Gaza with cooperation from Israeli industrialists. </P>
</TEXT>
</DOC>
