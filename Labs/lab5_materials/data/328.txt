<DOC>
<DOCID>REU010-0283.940721</DOCID>
<TDTID>TDT000834</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/21/94 01:11</DATE>
<TITLE> BURMA SAID TO OFFER TO FREE DISSIDENT SUU KYI</TITLE>
<HEADLINE> BURMA SAID TO OFFER TO FREE DISSIDENT SUU KYI</HEADLINE>
<SUBJECT> BC-BURMA-DISSIDENT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TOKYO (Reuter) </DATELINE>
<P>  Burma has offered to free dissident leader Aung San Suu Kyi from house arrest if she agrees to leave the country for at least five years, a Japanese daily reported Thursday. </P>
<P></P>
<P> ``I would be happy to welcome her back if she returned home (after five years of exile) and worked for the sake of the nation... </P>
<P> ``When the time is ripe, I want to hold talks with her with no conditions... I do not consider her an enemy,'' he added. </P>
<P> She led the opposition National League for Democracy (NLD) to a landslide victory in 1990 elections but the military junta ignored the result and refused to transfer power to the elected representatives. </P>
<P> The State Law and Order Restoration Council, the political body of the Burmese military junta, formed in 1988 after the suppression of a bloody uprising, continues to rule the country. </P>
<P> ``There is no reason to impose Western values on Asian countries whose history and culture differ from those in Europe and the United States,'' the Yomiuri Shimbun quoted Khin Nyunt, Burma's most powerful general, as saying. </P>
<P> The Burmese government had originally said her house arrest could last as long as five years, but a junta official said in February that her first year of detention was actually an ``arrest period'' and she would not be released until 1995 at the earliest. </P>
<P> Some Burmese dissidents have said an apparent, recent softening of the junta's hard-line stance on Suu Kyi is merely a ploy in advance of this week's meeting of the Association of South East Asian Nations (ASEAN), which Burma's Foreign Minister Ohn Gyaw is due to attend for the first time. </P>
<P> ASEAN groups Indonesia, Singapore, Malaysia, Thailand, Brunei and the Philippines. </P>
</TEXT>
</DOC>
