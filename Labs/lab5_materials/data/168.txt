<DOC>
<DOCID>REU006-0076.940711</DOCID>
<TDTID>TDT000392</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/11/94 16:22</DATE>
<TITLE> ROCKETS GREET ISLAMIC PEACE MISSION TO KABUL</TITLE>
<HEADLINE> ROCKETS GREET ISLAMIC PEACE MISSION TO KABUL</HEADLINE>
<SUBJECT> BC-AFGHAN-FIGHTING 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: adds attack on talks' venue, fresh details)</NOTES>
<DATELINE>KABUL, Afghanistan (Reuter) </DATELINE>
<P>  Rockets hit the presidential palace in Kabul Monday when an Islamic peace envoy lunched there with Afghan President Burhanuddin Rabbani, officials said. </P>
<P> But they said nobody was hurt in the attack, in which one rocket hit the presidential palace in central Kabul and seven others landed close to it. </P>
<P> Organization of Islamic Conference (OIC) Secretary General Hamid al-Gabid will continue his mission to find ways to end the Afghan factional fighting, a spokesman for him said. </P>
<P> Rival factions bombed and rocketed each other's positions in and around Kabul Monday before Gabid arrived from neighboring Pakistan for a day's visit, during which he also met former defense minister Ahmed Shah Masood and Ittehad-i-Islami party leader Abdurrab Rasul Sayyaf. </P>
<P> Anti-Rabbani forces led by Prime Minister Gulbuddin Hekmatyar and northern warlord General Abdul Rashid Dostum fired 300 rockets on Kabul in the last 24 hours, the official Kabul radio said Monday night. </P>
<P> Gabid met Rabbani soon after arriving and later drove to Paghman, northwest of Kabul, to meet Sayyaf and Masood. </P>
<P> A meeting with Hekmatyar was set for Tuesday at his Charasyab base, south of Kabul. </P>
<P> Occasional rocket fire still boomed after Gabid arrived. He had twice postponed the trip for security reasons. </P>
<P> ``We have offered a formula for peace,'' Gabid told reporters after meeting Rabbani at the presidential palace, a frequent target of rockets. He declined to give details. </P>
<P> He said he was encouraged by his talks with Rabbani and added: ``We are working to achieve this (peace) process.'' </P>
<P> Rabbani said he supported peace. ``We want to fight for peace ...for a solution of the country's problems,'' he said. </P>
</TEXT>
</DOC>
