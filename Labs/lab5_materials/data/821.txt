<DOC>
<DOCID>REU008-0227.940817</DOCID>
<TDTID>TDT002029</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 03:50</DATE>
<TITLE> ``DAY OF JACKAL'' AUTHOR CALLS CARLOS COWARD, FAILURE</TITLE>
<HEADLINE> ``DAY OF JACKAL'' AUTHOR CALLS CARLOS COWARD, FAILURE</HEADLINE>
<SUBJECT> BC-CARLOS-FORSYTH </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  International guerrilla Carlos was a failure whose career was a litany of cowardly attacks, according to Frederick Forsyth, author of the thriller ``The Day of the Jackal.'' </P>
<P> ``At base he was a homicidal psychopath. He killed because he enjoyed it and the more helpless the victim the better,'' Forsyth said in an article in Wednesday's Daily Mail newspaper. </P>
<P> ``Given a choice, he preferred to shoot a man at point blank range between the eyes, three times,'' Forsyth added in an article headlined ``How the master terrorist myth grew around a common coward.'' </P>
<P> Carlos was tagged ``The Jackal'' after the main protagonist of Forsyth's novel, who narrowly fails to assassinate former French President Charles de Gaulle. </P>
<P> Carlos was ordered held in custody by a French court Tuesday after being extradited from Sudan. He had eluded capture for 20 years during which he carried out audacious guerrilla attacks. </P>
<P> Forsyth said the world at large had been dazzled by Carlos, but he was not fooled. ``In the furor surrounding his capture, I may appear to be one of the few singularly unimpressed by the terrorist nicknamed the Jackal,'' Forsyth said. </P>
<P> Carlos' reputation was built on a myth founded on the Venezuelan-born guerrilla's love of publicity, his reputation for enjoying the good life and his ability to move around like a ``human chameleon,'' he said. </P>
<P> The guerrilla was a political failure, Forsyth said. Carlos' bombings, extortions and murders were aimed against Israel, in support of radical Palestinians, and to support his extreme left-wing views. </P>
<P> ``If as a Marxist-Leninist, he fought for communism, the cause has failed miserably. It is virtually extinct as a philosophical and political creed,'' Forsyth said. </P>
<P> ``If he fought to destroy Israel, it stands today as strong as ever. And if he killed for the proletariat, he was utterly repudiated. </P>
<P> ``He stands a failure, rejected on every issue that counts.'' </P>
</TEXT>
</DOC>
