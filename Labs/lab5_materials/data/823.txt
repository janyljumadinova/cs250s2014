<DOC>
<DOCID>REU008-0242.940817</DOCID>
<TDTID>TDT002039</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 06:04</DATE>
<TITLE> PERES STARTS TALKS WITH MUBARAK</TITLE>
<HEADLINE> PERES STARTS TALKS WITH MUBARAK</HEADLINE>
<SUBJECT> BC-MIDEAST-TALKS-PERES </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ALEXANDRIA, Egypt (Reuter) </DATELINE>
<P>  Israeli Foreign Minister Shimon Peres, in Egypt for talks with senior PLO officials on expanding Palestinian self-rule, met Egyptian President Hosni Mubarak Wednesday. </P>
<P> Peres did not speak to reporters as he entered the presidential palace in the Mediterranean city of Alexandria, but an Israeli diplomat said their talks began immediately. </P>
<P> Peres was accompanied by Environment Minister Yossi Sarid, foreign ministry director-general Uri Savir and Deputy Defense Minister Mordechai Gur. </P>
</TEXT>
</DOC>
