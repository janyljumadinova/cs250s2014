<DOC>
<DOCID>REU007-0313.940815</DOCID>
<TDTID>TDT001919</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/15/94 11:59</DATE>
<TITLE> GERMAN JUDGE REGRETS WORDING OF HOLOCAUST VERDICT</TITLE>
<HEADLINE> GERMAN JUDGE REGRETS WORDING OF HOLOCAUST VERDICT</HEADLINE>
<SUBJECT> BC-GERMANY-RIGHTIST </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BONN, Germany (Reuter) </DATELINE>
<P>  German judge Wolfgang Mueller admitted Monday to ``unfortunate formulations'' in a controversial judgment that appeared to show understanding for a far-right leader who denied the Nazi Holocaust. </P>
<P> A three-judge panel chaired by Mueller provoked outrage last week by saying Guenter Deckert, head of the right-wing National Democratic Party, was an intelligent man of character for whom denying the Holocaust was a ``matter of the heart.'' </P>
<P> On this basis, it limited Deckert's punishment for inciting racial hatred to a one-year suspended sentence and a $6,300 fine instead of the two-year jail term demanded by prosecutors. </P>
<P> ``We obviously failed in places to make sufficiently clear that the opinions expressed were those of the defendant and not the court,'' Mueller said in a statement issued by a law firm. </P>
<P> ``Certainly, that was partly due to misleading or unfortunate formulations which are much regretted.'' </P>
<P> Mueller, described by colleagues as a liberal, said the court had tried to determine Deckert's motivations and had not wanted to condone his point of view. </P>
<P> The court's ruling sparked international protests, calls for a parliamentary debate and for the judges to be removed from their jobs. Chancellor Helmut Kohl called it a disgrace. </P>
<P> Court president Gunter Weber said last week he hoped the judges would not have to be diverted to other duties, but admitted it might be hard for them to stay in their posts. </P>
<P> Both prosecution and defense have appealed against the sentence to the Federal Court of Justice. </P>
</TEXT>
</DOC>
