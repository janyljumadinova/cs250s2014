<DOC>
<DOCID>REU006-0032.940811</DOCID>
<TDTID>TDT001762</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/11/94 18:40</DATE>
<TITLE> GOLF-AZINGER STRUGGLES IN WITH A 75</TITLE>
<HEADLINE> GOLF-AZINGER STRUGGLES IN WITH A 75</HEADLINE>
<SUBJECT> BC-PGA-AZINGER </SUBJECT>
<AUTHOR>     By Larry Fine </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TULSA, Okla.(Reuter) </DATELINE>
<P>  Paul Azinger satisfied one goal by being able to defend his title at the PGA Championship, but the gritty competitor who is winning his battle with cancer was agitated by his play Thursday. </P>
<P> ``Obviously, I'm not as ready as I thought I was,'' said Azinger, who was five over par for his first nine holes on the way to a 75 at the par-70 Southern Hills Country Club. </P>
<P> The 34-year-old Azinger, playing in only his second tournament since coming back from an eight-month layoff due to cancer treatment, sets high standards. </P>
<P> He had an entirely different idea of how he would fare. </P>
<P> ``I envisioned shooting 64 and being in the headlines of every paper in the world,'' he said. ``I tried to envision my first tournament back and me walking up the 18th fairway and winning. </P>
<P> ``I'm a believer that you can become what you see yourself becoming, whether it's good or bad.'' </P>
<P> Azinger said he succeeded in keeping his emotions under control but soon realized he was not prepared well enough to play at his accustomed level. </P>
<P> ``I wasn't as emotional on the first tee as last week,'' said Azinger, who received a stirring welcome back from crowds at the Buick Open in Michigan. </P>
<P> ``I made an effort not to be. I just felt like I couldn't walk around in a fog and expect to have any chance to play well here,'' added Azinger, who said he consciously avoided making eye contact with the Southern Hills crowd and quieted them by quickly taking his stance over the tee ball. </P>
<P> Emotions did not hold Azinger back. The rust in his game did. </P>
<P> Azinger admitted he did not feel comfortable or confident about hitting to specific distances. </P>
<P> ``I may have been a little remiss in preparing to come back by not hitting every single club in my bag and pacing off exactly how far they were going to go. </P>
<P> ``There were times when I got a yardage like 210 and I didn't have any idea. Was it a two iron, three iron, four iron or five iron?'' </P>
<P> Azinger said it was a good sign that he felt frustrated by his sub-standard round. </P>
<P> ``I'm not here for a walk in the park,'' he said. ``I'm here to compete.'' </P>
</TEXT>
</DOC>
