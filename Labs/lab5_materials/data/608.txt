<DOC>
<DOCID>REU002-0425.940804</DOCID>
<TDTID>TDT001482</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/04/94 17:14</DATE>
<TITLE> ISRAELI JETS HIT SOUTH LEBANON, KILL 10 CIVILIANS</TITLE>
<HEADLINE> ISRAELI JETS HIT SOUTH LEBANON, KILL 10 CIVILIANS</HEADLINE>
<SUBJECT> BC-LEBANON-RAID 3RDLD (PICTURE) </SUBJECT>
<AUTHOR>     By Kamel Jaber </AUTHOR>
<TEXT>
<NOTES> (Eds: Updates death toll to 10, adds Israeli apology)</NOTES>
<DATELINE>DEIR AZ-ZAHRANI, Lebanon (Reuter) </DATELINE>
<P>  Israeli warplanes bombed guerrilla posts and a building in south Lebanon Thursday, killing three civilian men, four children and three women and wounding 15 people, security sources said. </P>
<P> The Israeli army said the attack that wrecked the two story house was a mistake and apologized. </P>
<P> The civilian casualties threaten a surge of violence in the south as pro-Iranian Hizbollah leaders have in the past repeatedly vowed to rocket northern Israel if Lebanese civilians were attacked. </P>
<P> The security sources said the jets fired a rocket into the house of Mohammad Taraboulsi in the village of Deir az-Zahrani near the market town of Nabatiyeh. </P>
<P> The house and a nearby shop were flattened. The civilians were at the house and inside the shop when the planes attacked, the sources said. </P>
<P> Some of the victims' bodies were charred beyond recognition, witnesses said. </P>
<P> Relief workers were digging through the rubble, searching for survivors or more bodies, they added. </P>
<P> Before attacking Deir az-Zahrani, the planes fired three rockets at guerrilla posts in Ain Bouswar village in Iqlim al-Toufah, a ridge 25 miles south of Beirut used by Hizbollah guerrillas to launch attacks against Israel's occupation zone in the south. </P>
<P> The raids, the 27th and 28th into Lebanon this year, were the first since Israel threatened ``painful response'' last week following guerrilla attacks on Israeli troops in the south. </P>
<P> Fears of a massive Israeli strike into Lebanon subsided earlier this week after a flurry of contacts between Beirut and Washington. </P>
<P> The concern followed Israeli threats to avenge a series of deadly guerrilla attacks in south Lebanon and bombings of Israeli and Jewish targets in Buenos Aires and London last month. </P>
<P> An understanding by which Israel ended a seven-day blitz of south Lebanon last summer stipulated that it stop attacking civilian villages in the south in return for an end to Hizbollah's rocket attacks on northern Israel. </P>
<P> The understanding has been shaken but in general has held. </P>
<P> In a separate development, Hizbollah said in a statement one of its guerrillas was killed ``as he was carrying out his holy war duties.'' It gave no further details. </P>
</TEXT>
</DOC>
