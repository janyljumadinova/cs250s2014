<DOC>
<DOCID>REU002-0191.940803</DOCID>
<TDTID>TDT001435</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 18:49</DATE>
<TITLE> GUNMEN WOUND ISRAELI SOLDIER NEAR HEBRON</TITLE>
<HEADLINE> GUNMEN WOUND ISRAELI SOLDIER NEAR HEBRON</HEADLINE>
<SUBJECT> BC-ISRAEL-SHOOTING 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: adds incidents in Hebron and north West Bank)</NOTES>
<DATELINE>JERUSALEM (Reuter) </DATELINE>
<P>  Gunmen shot and slightly wounded an Israeli soldier traveling in an army vehicle near Hebron in the occupied West Bank on Wednesday, military sources said. </P>
<P> Jewish settler sources said the gunmen opened fire from a moving vehicle and fled in the direction of Hebron. The attack was similar to one near the same place Tuesday in which a soldier was slightly wounded. </P>
<P> Israel Radio said gunmen also shot at one of the fortified Jewish enclaves in Hebron but no one was hurt. </P>
<P> They said there was a similar attack in the northern West Bank and the attackers fled toward the Palestinian self-rule enclave of Jericho. </P>
<P> Settlers said after Tuesday's attack near Hebron that an army decision to thin out its forces in the Hebron area was a mistake. </P>
<P> Hebron, where about 400 Jews live in heavily fortified enclaves surrounded by 100,000 Palestinians, is a flashpoint of Jewish-Arab violence. It is the site of the Tomb of the Patriarchs, which is sacred to both Muslims and Jews. </P>
</TEXT>
</DOC>
