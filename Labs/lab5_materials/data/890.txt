<DOC>
<DOCID>REU009-0277.940819</DOCID>
<TDTID>TDT002190</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/19/94 13:20</DATE>
<TITLE> BRITISH SCRABBLE PLAYERS DISDAIN PC LANGUAGE</TITLE>
<HEADLINE> BRITISH SCRABBLE PLAYERS DISDAIN PC LANGUAGE</HEADLINE>
<SUBJECT> BC-BRITAIN-SCRABBLE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  British scrabble players should be free to use a wide range of terms in the English language, including expletives and slang terms with racist connotations, its official dictionary editor said Friday. </P>
<P> Britain has opted not to follow the U.S. producers of the world-famous word game, who edited out up to 100 terms that could be construed as racist, sexist or blasphemous -- or politically incorrect. </P>
<P> ``We feel quite strongly at Chambers (dictionaries) and Spears, which produce Scrabble, that all these words are part of the English language,'' said editor Catherine Schwarz. </P>
<P> She added that players could use their discretion as to whether they used the words. </P>
<P> Terms which were earmarked for censorship in the United States but tolerated in Britain include ``asshole,'' ``crap,'' ``dago'' (derogatory word for a foreigner, especially a Spaniard, Portuguese or Italian) and ``chink'' (slang term for a Chinese). </P>
</TEXT>
</DOC>
