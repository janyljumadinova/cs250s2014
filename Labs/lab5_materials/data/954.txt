<DOC>
<DOCID>REU011-0070.940823</DOCID>
<TDTID>TDT002349</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/23/94 16:51</DATE>
<TITLE> MARIJUANA PLANTS FOUND AT HEWLETT-PACKARD FACILITY</TITLE>
<HEADLINE> MARIJUANA PLANTS FOUND AT HEWLETT-PACKARD FACILITY</HEADLINE>
<SUBJECT> BC-DRUGS-MARIJUANA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SAN FRANCISCO (Reuter) </DATELINE>
<P>  Security guards found about 120 marijuana plants growing on the grounds of a northern California factory owned by electronics firm Hewlett-Packard Co, the company said Tuesday. </P>
<P> Jeff Weber, a spokesman for Hewlett-Packard's facility at Santa Rosa, 50 miles north of San Francisco, said a worker at the plant found a hose leading from the company's irrigation system near a baseball field to a heavily overgrown open space on plant grounds last Friday. </P>
<P> The worker alerted security guards who investigated and found a drip irrigation system and about 120 marijuana plants at the end of the hose, Weber said. </P>
<P> The security guards then called police who took away the plants, he said. </P>
<P> ``It's very surprising and we're quite unhappy about the situation,'' Weber said, adding that it was the first time that marijuana plants had been found growing on company property. </P>
<P> Some 2,200 employees work at the 195-acre facility in Santa Rosa where Hewlett-Packard manufactures electronic test and measurement instruments. </P>
<P> The plant is ringed by a perimeter fence and guards are posted at the main entrances. </P>
<P> Weber said the plants were found in a virtually inaccessible area. ``It took a chain saw to cut our way through the thick undergrowth to get to the area.'' </P>
<P> It was not known who was cultivating the plants and there were no leads, he said. It was possible that someone had entered company property illegally and cultivated the plants. </P>
<P> Weber said the company had not opened a formal internal investigation, but it was checking remaining open space at the Santa Rosa plant to make sure no other plants were growing there and was asking employees to contact the security department if they had any information about the plants. </P>
<P> Weber said the plants varied in size between three and eight feet tall and had apparently not been harvested. </P>
<P> Illegal growing of marijuana plants is quite common in several northern California counties. </P>
</TEXT>
</DOC>
