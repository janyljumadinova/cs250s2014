<DOC>
<DOCID>REU014-0330.940730</DOCID>
<TDTID>TDT001249</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/30/94 14:56</DATE>
<TITLE> AFGHAN RIVALS TRADE BLAME OVER DEATH OF REPORTER</TITLE>
<HEADLINE> AFGHAN RIVALS TRADE BLAME OVER DEATH OF REPORTER</HEADLINE>
<SUBJECT> BC-AFGHAN-REPORTER </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ISLAMABAD, Pakistan (Reuter) </DATELINE>
<P>  An Afghan reporter for the British Broadcasting Corporation (BBC) was found shot dead Saturday near the Afghan capital Kabul, where rival factions fighting for supremacy blamed each other for the killing. </P>
<P> Mir Wais Jalil, 25, was killed Friday night by gunmen who captured him when he was returning from interviewing Prime Minister Gulbuddin Hekmatyar at his Charasyab base south of Kabul, the reporter's relatives said in neighboring Pakistan. </P>
<P> The BBC in London condemned the killing and a spokesman described Jalil as ``a highly professional journalist who was widely respected for his accurate, impartial reporting of the events in Afghanistan.'' </P>
<P> Jalil had worked for the BBC's Pashtu service for two years. ``He was extremely good in news gathering,'' a colleague said, ``He was very energetic.'' </P>
<P> His family has settled temporarily in Peshawar, Pakistan, because of factional fighting in Kabul between forces loyal to President Burhanuddin Rabbani and his opponents led by Hekmatyar and northern warlord General Abdul Rashid Dostum. </P>
<P> The official Kabul Radio, controlled by Rabbani, said the reporter was killed by militia controlled by Hekmatyar and his blood-smeared body was found at Chilsitoon in southern Kabul. </P>
<P> But Hekmatyar's Hezb-i-Islami (HIA) party said Jalil was killed by members of the Khad former secret police who it said were fighting for Rabbani. </P>
<P> A statement issued in Peshawar said Jalil was returning to Kabul by taxi after interviewing Hekmatyar, accompanied by an Italian journalist, and had passed the last Hezb post. </P>
<P> ``However, his body was seen lying near an area close to the frontlines by a passer-by who reported to the security offices of the HIA,'' it said. </P>
<P> ``Security personnel of the HIA cordoned off the area and arrested a few suspects and interrogation is still continuing to pinpoint the culprits.'' </P>
<P> There was no immediate independent information on the identity of the killers. </P>
</TEXT>
</DOC>
