<DOC>
<DOCID>REU005-0072.940810</DOCID>
<TDTID>TDT001693</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/10/94 04:56</DATE>
<TITLE> BRITISH MOTHER FURIOUS OVER RAPIST, 10</TITLE>
<HEADLINE> BRITISH MOTHER FURIOUS OVER RAPIST, 10</HEADLINE>
<SUBJECT> BC-BRITAIN-RAPE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  The mother of a three-year-old girl raped by a boy of 10 has expressed outrage that British police chose only to caution the youngster instead of prosecuting him. </P>
<P> The boy admitted unlawful sexual intercourse with the child last October when she stayed overnight at his home, a newspaper said. His mother, a family friend, put the children to sleep in the same bed. </P>
<P> ``Every night, I get into bed and think about it. I get so angry I can't control myself,'' the girl's mother told the Daily Telegraph. </P>
<P> Police said the boy had not been charged by the Crown Prosecution Service because the girl was too young to give evidence. ``It was considered a caution was the way to deal with the matter,'' a police spokeswoman said. </P>
</TEXT>
</DOC>
