<DOC>
<DOCID>REU004-0146.940707</DOCID>
<TDTID>TDT000253</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/07/94 17:03</DATE>
<TITLE> STOLTENBERG CAUTIONS UN ON ANY NEW SERBIA SANCTIONS</TITLE>
<HEADLINE> STOLTENBERG CAUTIONS UN ON ANY NEW SERBIA SANCTIONS</HEADLINE>
<SUBJECT> BC-YUGOSLAVIA-UN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>UNITED NATIONS (Reuter) </DATELINE>
<P>  U.N. mediator Thorvald Stoltenberg Thursday cautioned the Security Council on tightening a trade embargo against Serbia because of its impact on neighboring Macedonia, diplomats said. </P>
<P> Stoltenberg did not give an opinion of whether Serbia should be punished with more sanctions but said the council needed to consider the impact on Macedonia. </P>
<P> He said that Macedonia, which is already under a boycott by Greece, was facing immense economic hardships as a result of the sanctions. </P>
<P> ``The council needs to think about the direct impact on Macedonia,'' one envoy at the meeting said. </P>
<P> Stoltenberg, a former Norwegian minister, briefed the council on the latest international peace map for Bosnia which gives Muslims and Croats 51 percent of the country and their Serb foes the remainder. </P>
<P> The map, obtained by Reuters, was handed to leaders of Bosnia's warring factions Tuesday. </P>
<P> The Bosnians are to give their formal replies on July 19. It is expected that if the Serbs reject the plan international pressure would mount for a tightening of the trade embargo on Serbian-dominated Yugoslavia. </P>
<P> Stoltenberg also said that if the latest plan was rejected the direct result would be an increase in fighting. </P>
<P> He told reporters the plan was the ``only realistic proposal,'' adding that ``the alternative is continued war with more hatred, more ethnic cleansing.'' </P>
<P> At the council meeting, Stoltenberg said negotiators were guided by the need to maintain the union of Bosnia. </P>
<P> Bosnian Serbs have already said that their approval hinged on the acceptance of a self-declared Serb state in Bosnia. </P>
<P> The new map gives the Serbs more territory than the one drawn up by former mediators Cyrus Vance and Lord Owen more than a year ago, which allocated about 43 percent to the Bosnian Serbs. The Clinton administration had criticized the Vance-Owen map but has backed the new compromise plan. </P>
</TEXT>
</DOC>
