<DOC>
<DOCID>REU008-0386.940717</DOCID>
<TDTID>TDT000656</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/17/94 01:26</DATE>
<TITLE> ISRAELI TROOPS KILL ONE ARAB AT GAZA CHECKPOINT</TITLE>
<HEADLINE> ISRAELI TROOPS KILL ONE ARAB AT GAZA CHECKPOINT</HEADLINE>
<SUBJECT> BC-MIDEAST-ISRAEL-GAZA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>GAZA (Reuter) </DATELINE>
<P>  Israeli soldiers shot and killed a Palestinian and wounded at least 30 at the crossing point from the Gaza Strip to Israel Sunday, Palestinian sources said. </P>
<P> Israeli military sources said Palestinians wrested a gun from a Palestinian policeman at a Palestinian checkpoint several hundred yards south of Israel's Erez checkpoint and fired shots in the air. </P>
<P> Palestinians denied anyone took a police gun. </P>
<P> They said when workers became restless while Israeli troops took an unusually long time to allow them through the Erez checkpoint. </P>
<P> ``Besides the delay, the Israeli border police used clubs to beat us,'' said a 22-year-old worker named Ahmed who helped bring wounded men to Gaza's Shifa Hospital. ``They provoked us and we started throwing stones at the soldiers.'' </P>
<P> Palestinian sources named the dead man as Riad Yassin, 26, of Khan Younis. They said he was shot in the head. </P>
</TEXT>
</DOC>
