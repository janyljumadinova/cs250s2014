<DOC>
<DOCID>REU014-0204.940729</DOCID>
<TDTID>TDT001211</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/29/94 17:27</DATE>
<TITLE> O.J. SIMPSON'S HEISMAN TROPHY STOLEN</TITLE>
<HEADLINE> O.J. SIMPSON'S HEISMAN TROPHY STOLEN</HEADLINE>
<SUBJECT> BC-USA-SIMPSON-TROPHY 1STLD </SUBJECT>
<AUTHOR>     By Matt Spetalnick </AUTHOR>
<TEXT>
<NOTES> (Eds: Combines takes, adds details)</NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  The Heisman Trophy that football legend O.J. Simpson won in 1968 and the jersey he wore were stolen from a display case at the University of Southern California, a university spokesman said Friday. </P>
<P> The trophy and team jersey were found to be missing from the lobby of the university's Heritage Hall just before 11 p.m. PDT Thursday (2 a.m. EDT Friday). </P>
<P> The theft was immediately reported to police. There were no suspects or leads and a police spokeswoman said the theft ``is not being treated as a special case.'' </P>
<P> Simpson, who is jailed awaiting trial on charges of murdering his ex-wife and a male friend, was awarded the coveted Heisman Trophy at the end of his senior year. </P>
<P> The Heisman is given annually to the nation's most outstanding college football player. Simpson, a star running back for the USC Trojans, later went on to a record-breaking professional career in the National Football League.  On June 17 -- the day Simpson led police on a bizarre flight from justice before finally surrendering -- university officials had taken the precaution of removing the trophy from its glass display case out of fear it might be vandalized. </P>
<P> The trophy was returned to public viewing three days later when officials deemed it safe. It had become something of a shrine for Simpson fans. </P>
<P> The theft, believed to have occurred Thursday night, was announced the same day Simpson appeared in court and heard a judge set his trial to begin September 20. He is charged in the June 12 stabbing deaths of Nicole Brown Simpson, 35, and Ronald Goldman, 25. </P>
<P> The stolen items were kept in a separate display case as were the trophies for the university's three other Heisman winners. The jersey taken in the theft bore the number 32 that Simpson also wore with the Buffalo Bills. </P>
<P> ``The removal of the trophy and jersey is believed to be the first incident of vandalism or theft against memorabilia located in the building, which houses the offices of the university's athletic department,'' university spokesman Rob Asghar said. </P>
<P> The year he won the Heisman, Simpson set a single-season rushing record with 1,709 yards. That mark was eclipsed in 1981 by USC's fourth Heisman winner, Marcus Allen. </P>
<P> Simpson equalled or surpassed 19 NCAA, conference and USC records during his years with the Trojans. He later became the first NFL running back to top the 2,000-yard rushing mark in a single season. </P>
<P> After retiring in 1979, Simpson glided smoothly into a second career as a sports announcer, advertising pitchman and actor. </P>
</TEXT>
</DOC>
