<DOC>
<DOCID>REU008-0182.940715</DOCID>
<TDTID>TDT000619</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 17:14</DATE>
<TITLE> LAST HAITIANS PROCESSED AT JAMAICA CENTER</TITLE>
<HEADLINE> LAST HAITIANS PROCESSED AT JAMAICA CENTER</HEADLINE>
<SUBJECT> BC-HAITI-REFUGEES </SUBJECT>
<AUTHOR>     By Michael Becker </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>KINGSTON, Jamaica (Reuter) </DATELINE>
<P>  U.S. officials said they were processing the last Haitian boat people brought to a processing center in Jamaica and were awaiting orders to shut down the operation and return to the United States Friday. </P>
<P> By Friday afternoon, 33 of the last 368 had been granted official U.S. refugee status, with 10 awaiting interviews. </P>
<P> Under a new U.S. policy, Haitian boat people not granted refugee status are allowed to decide whether they will be returned to their homeland or taken to temporary ``safe havens'' in third countries. </P>
<P> Officials said 72 of the last group volunteered to be returned to Haiti. The rest will be taken to the U.S. Naval Base at Guantanamo Bay in Cuba. </P>
<P> The boat people granted refugee status in Jamaica, or those determined to have fled their homeland for political and not economic reasons, will be the last granted the opportunity to reach the United States. </P>
<P> To discourage the flood of boat people, the White House announced July 5 that Haitians picked up at sea would no longer be eligible for resettlement in America. </P>
<P> Including 120 Haitians picked up on Friday, the U.S. Coast Guard said it has intercepted 23,845 Haitians at sea in 1994, and 15,483 in July. </P>
<P> The numbers have declined this week, but U.S. officials and refugee advocates said it is too soon to determine whether it is due to the new U.S. policy or bad weather off Haiti. </P>
<P> Once the last Haitians are processed, the 1,000 U.S. personnel associated with the processing center, aboard a Navy hospital ship anchored off Kingston Harbor, were to await permission to leave Kingston, a spokesman said. </P>
<P> A land-based refugee processing center in the Turks & Caicos Islands south of the Bahamas is ready to open. </P>
<P> U.S. officials said 1,977 Haitians were processed at the Jamaica center, with 560 given refugee status since it opened. </P>
<P> The center has proven a boon to local businesses, which estimated they have done just under US$200,000 of business over the past month supplying it with goods and services. </P>
</TEXT>
</DOC>
