<DOC>
<DOCID>REU001-0033.940801</DOCID>
<TDTID>TDT001300</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/01/94 06:07</DATE>
<TITLE> ARAFAT DEMANDS TALKS WITH ISRAEL ON JERUSALEM NOW</TITLE>
<HEADLINE> ARAFAT DEMANDS TALKS WITH ISRAEL ON JERUSALEM NOW</HEADLINE>
<SUBJECT> BC-MIDEAST-JERUSALEM </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>GAZA (Reuter) </DATELINE>
<P>  PLO leader Yasser Arafat said Monday that Palestinians wanted to accelerate negotiations on the future of Jerusalem now that Israel had recognized a special status for Jordan over the city's Muslim shrines. </P>
<P> ``For us Jerusalem is one issue, one cause, the political issue, sovereignty, holy sacred places, both Christian and Muslim,'' Arafat told a news conference in Gaza where he settled a month ago under the PLO's peace deal with Israel. </P>
<P> Palestinians view East Jerusalem as the capital of a future state. Israel captured East Jerusalem in 1967 and annexed it, declaring Jerusalem its eternal, undivided capital. </P>
<P> Palestinian officials were furious last week when an accord signed in Washington between Israel and Jordan said Israel would give high priority to Jordan as guardian of Jerusalem's Muslim shrines in negotiations on the final status of the holy city. </P>
<P> An official PLO statement accused Israel at the time of violating the declaration of principles signed with the Palestine Liberation Organization in Washington 10 months ago. </P>
<P> ``According to our declaration of principles...(negotiations on Jerusalem start) as soon as possible and not exceeding the beginning of the third year,'' Arafat said Monday. </P>
<P> ``OK, if they (the Israelis) have decided to start now, we are insisting to start now,'' he said. </P>
<P> ``Jerusalem from our point of view is the capital of our independent Palestinian state,'' Arafat said. </P>
</TEXT>
</DOC>
