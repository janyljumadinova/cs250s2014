<DOC>
<DOCID>REU004-0249.940809</DOCID>
<TDTID>TDT001653</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/09/94 01:59</DATE>
<TITLE> SOUTH SAYS NORTH ABDUCTED 438 OVER FOUR DECADES</TITLE>
<HEADLINE> SOUTH SAYS NORTH ABDUCTED 438 OVER FOUR DECADES</HEADLINE>
<SUBJECT> BC-KOREA-ABDUCTIONS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SEOUL, South Korea (Reuter) </DATELINE>
<P>  Communist North Korea has abducted 438 South Koreans over the past four decades, Seoul's Deputy Premier and Unification Minister Lee Hong-koo said Tuesday. </P>
<P> Lee told a parliamentary committee the number included 397 fishermen, 20 navy personnel and 21 passengers and crew from three hijacked aircraft. </P>
<P> Of the total, seven were known to have died and two had been sent back to the South as spies, Lee said. </P>
<P> A Unification ministry spokesman said the list of those abducted since the end of the 1950-1953 Korean War excluded South Koreans kidnapped by the North in third countries. </P>
<P> Lee said the South Korean government has demanded on more than 20 occasions since the early 1980s that the North repatriate the southerners but there has been no response. </P>
<P> North Korea has denied the South's accusations and said South Koreans in the North were voluntary defectors. </P>
<P> Lee also said North Korea has 12 detention centers across the country housing 200,000 political prisoners. He said the centers were guarded by heavily armed soldiers and surrounded by mine fields and high-voltage barbed wire. </P>
<P> He said about 15 political prisoners a year had been publicly executed for attempting to escape. </P>
<P> The human rights group Amnesty International last month named 49 political prisoners, including 11 South Koreans, whom it said were reportedly held in a North Korean detention center in 1990. </P>
</TEXT>
</DOC>
