<DOC>
<DOCID>REU002-0400.940705</DOCID>
<TDTID>TDT000152</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/05/94 03:59</DATE>
<TITLE> ZIMBABWE FARMERS SEEK TO ANNUL SEIZURE OF FARMS</TITLE>
<HEADLINE> ZIMBABWE FARMERS SEEK TO ANNUL SEIZURE OF FARMS</HEADLINE>
<SUBJECT> BC-ZIMBABWE </SUBJECT>
<AUTHOR>     By Francis Mdlongwa </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>HARARE, Zimbabwe (Reuter) </DATELINE>
<P>  Three white Zimbabwean farmers, defying government threats, went to the high court Tuesday seeking to overturn controversial legislation allowing the state to virtually seize farms to satisfy land hunger among blacks. </P>
<P> But most political analysts saw the landmark case as testing future black-white relations in the southern African country that gained independence from Britain in 1980 after a long racial war essentially over land. </P>
<P> The farmers want the court to declare 1992's Land and Acquisition Act -- President Robert Mugabe's centerpiece in plans to wrest control of the economy from whites to blacks -- as null and void, arguing that it goes against basic human rights. </P>
<P> ``The main argument will be that land designation, earmarking the farms to be bought by the government, is unconstitutional,'' their lawyer, Richard Wood, told Reuters. </P>
<P> Mugabe has used the act, praised by Zimbabwe's 10 million blacks but condemned by the 100,000 whites and Western states, to ``designate'' 70 farms across the country for the resettlement of hundreds of thousands of landless blacks. </P>
<P> The country's 4,500 white farmers, owners of 70 percent of all fertile land, say the law infringes on their rights because it does not allow them to challenge the government's decision to forcibly buy their land nor to contest the compensation offered. </P>
<P> They also argue that the resettled blacks will not have farming skills and this could lead to food shortages in the country, which normally is a food exporter to poorer neighbors. </P>
<P> Mugabe, a former guerrilla leader who fought a bitter seven-year war against white minority rule before independence, has vowed to punish whites challenging the law in court. </P>
<P> In a recent interview, he said the land had been seized from indigenous blacks by whites who settled in what was then Rhodesia in 1894 and all that his government was doing was returning it to them. </P>
<P> ``Let the people have their land,'' he said. </P>
<P> In other remarks, Mugabe suggested that some whites in the country were abusing his policy of national reconciliation, proclaimed at independence to try to heal the war wounds. </P>
<P> Analysts said that although relations between whites and blacks had remained largely amicable over the past 14 years, they could sour easily over the land question. </P>
<P> Other analysts noted that Mugabe, facing fresh general elections within 10 months, would want to show his black supporters especially that he was in charge. </P>
<P> ``Failure in this case by the government could be fatal. Its whole reputation among blacks would suffer irreparable damage,'' an African diplomat commented. </P>
<P> In recent months, Mugabe and his ministers have been at pains to tell blacks, disgruntled over lack of economic opportunities and troubled by biting Western-backed reforms, that the government was moving to ``indigenize'' the economy. </P>
<P> These plans included setting up funds to buy shares for blacks in some state-assisted companies that are being privatized as well as giving greater state financial aid to so-called emergent industrialists. </P>
</TEXT>
</DOC>
