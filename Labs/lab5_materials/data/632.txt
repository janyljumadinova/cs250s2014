<DOC>
<DOCID>REU003-0303.940806</DOCID>
<TDTID>TDT001562</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/06/94 15:18</DATE>
<TITLE> LATIN AMERICAN GROUP APPROVES RE-ENTRY OF PANAMA</TITLE>
<HEADLINE> LATIN AMERICAN GROUP APPROVES RE-ENTRY OF PANAMA</HEADLINE>
<SUBJECT> BC-LATAM-RIO </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BOGOTA, Colombia (Reuter) </DATELINE>
<P>  Latin American foreign ministers decided in principle at a meeting Saturday to allow Panama to rejoin their main discussion forum, the Rio Group, an official told reporters. </P>
<P> Colombian Foreign Minister Noemi Sanin de Rubio said the ministers were unanimous in their decision to readmit Panama as a full member of the Rio Group. </P>
<P> The ministers are in Bogota for Sunday's inauguration of President Ernesto Samper. </P>
<P> Ministers suspended Panama from the group several years ago to protest its regime, then led by General Manuel Noriega. </P>
<P> Noriega seized power but was deposed in January 1990 following a U.S. military invasion. He was deported to the United States, where he was sentenced to life imprisonment for drug trafficking. </P>
<P> The country held its first free presidential election since the invasion earlier this year. </P>
<P> ``All those present representing the Rio Group approved the return of Panama, which was a founder of the Rio Group, to the heart of this community,'' Sanin said. </P>
<P> The decision would be made official after Trinidad and Tobago's foreign minister arrived in Bogota to give his formal assent, Sanin added. </P>
</TEXT>
</DOC>
