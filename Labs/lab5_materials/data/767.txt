<DOC>
<DOCID>REU007-0157.940814</DOCID>
<TDTID>TDT001890</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/14/94 11:44</DATE>
<TITLE> BRITISH MINISTER IN U.S. HEALTH CARE ``SELL-OFF'' FIGHT</TITLE>
<HEADLINE> BRITISH MINISTER IN U.S. HEALTH CARE ``SELL-OFF'' FIGHT</HEADLINE>
<SUBJECT> BC-HEALTH-BRITAIN-USA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  The U.S. health care debate is starting to affect the rest of the world. </P>
<P> A British government minister confirmed Sunday he had held talks with a private U.S. health care firm that is seeking to expand. </P>
<P> The opposition Labor party accused the free-market Conservatives of plotting to sell off parts of Britain's National Health Service, a prized but overburdened institution that once served as a world model for open-access health care. </P>
<P> Health Minister Tom Sackville confirmed a report in the Sunday Times newspaper saying he had met officials from Salick Health Care, a Los Angeles-based firm, to discuss possible joint ventures in the health service. </P>
<P> Under a controversial five-year program of health reforms, the government is trying to foster internal competition in the British medical market and is encouraging hospitals to explore partnerships with the private sector. </P>
<P> The Sunday Times said U.S. firms seeking to expand into the British market were interested in buying prestige hospitals or specialist facilities. Some were seeking government permission to undercut NHS prices, it said. </P>
<P> Sackville said Salick, a cancer care specialist, was ``one of a number of American companies who are trying to gain a foothold in the U.K. </P>
<P> ``I explained the reformed NHS to them, and said that while we are encouraging the NHS to cooperate with the independent sector of health care, we as a department remained strictly neutral in the negotiating process,'' he said in a statement to Britain's Press Association news agency. </P>
<P> Labor pounced on the controversy as ammunition for its view that the government has put the health service next on its list of state services ripe for privatization. </P>
<P> ``Having sold off the family silver, the government seems now to be intent on selling the family legacy of good health and security as well,'' said a Labor spokesman. </P>
<P> Britain appeared to have become a ``soft touch'' for U.S. firms seeking refuge from President Clinton's health care reforms, he added. </P>
<P> Opinion polls have shown public fears of a privatized health service boost support for Labor. </P>
</TEXT>
</DOC>
