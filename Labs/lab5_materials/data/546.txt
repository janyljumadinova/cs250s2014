<DOC>
<DOCID>REU001-0292.940802</DOCID>
<TDTID>TDT001355</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/02/94 11:59</DATE>
<TITLE> DEFEATED RWANDAN ARMY LIVES ON FOOD AID IN ZAIRE</TITLE>
<HEADLINE> DEFEATED RWANDAN ARMY LIVES ON FOOD AID IN ZAIRE</HEADLINE>
<SUBJECT> BC-RWANDA-ARMY </SUBJECT>
<AUTHOR>     By Peter Smerdon </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MUGUNGA, Zaire (Reuter) </DATELINE>
<P>  An estimated 20,000 defeated Rwandan troops are living off international food aid in the bush of Zaire and are giving no sign of preparing for a counter-attack or guerrilla operations into their homeland. </P>
<P> Their ranks depleted by disease and desertion, soldiers in uniform mixed with civilians Tuesday at Mugunga camp, 15 miles west of the Zairean border town of Goma, collecting bags of U.N. food agency maize meal. </P>
<P> The U.N. High Commissioner for Refugees (UNHCR), which is coordinating the international relief effort for Rwanda, says it is not supplying the Rwandan troops with anything until they are out of uniform when they will be classified as civilians. </P>
<P> Thousands of troops live in Mugunga camp among an estimated 300,000 refugees and hundreds more soldiers daily carry food three miles to a makeshift army camp in the bush. </P>
<P> Perhaps eight percent of the food supplies delivered to the camp daily by aid agencies is siphoned off by the defeated army. </P>
<P> ``It is a major problem and has been going on far too long,'' said UNHCR spokesman Panos Moumtzis. ``They are harrassing the refugees, taking their food and intimidating them against going home. </P>
<P> ``They fear if the refugees go back they will be left here alone, so (we) want to keep them here until there is a settlement.'' </P>
<P> Former Rwandan chief of staff Major-General Augustin Bizimungu told the British Broadcasting Corporation Monday the soldiers had regrouped and had no intention of disbanding. </P>
<P> But he said they did not wish to renew the civil war and would seek talks with the new government in Kigali. The government has so far snubbed their overtures and left them to their exile. </P>
<P> The UNHCR says they should disband, get out of uniform and integrate with their families as civilians. But Zairean President Mobutu Sese Seko ruled they should be isolated in a separate site. </P>
<P> No Zairean troops patrol the road between Mugunga and the army camp deeper in the lush bush where soldiers lounge outside their tents with the Rwandan army vehicles they escaped in. </P>
<P> Zairean troops disarmed many when they flooded into Zaire to escape the victorious rebel forces but some units were allowed across the border with their arms and others hid their guns. </P>
<P> U.N. officials saw no sign that the troops were preparing for a new war in Rwanda. They do not parade or train and spend their time stealing food and waiting for a political solution. </P>
<P> ``We do not want to feed an army in exile,'' said Moumtzis. ``We are examining options to keep the soldiers out of the camp, possibly barbed wire and private security guards, but it is difficult.'' </P>
<P> The soldiers in Mugunga refuse to speak to reporters. They hold that the international media sided with Rwanda Patriotic Front (RPF) rebels in the three-month war their army lost. </P>
<P> ``They are soldiers but it is their right to eat,'' Faustin Nzabalinda, former prefect of the northwestern Rwandan town of Ruhengeri, told Reuters aboard a British distribution truck. </P>
<P> An army officer, seeing photographers taking pictures of the soldiers, told the men to disperse -- in an apparent attempt to stop their thieving being made public. </P>
<P> Before being driven out of Rwanda three weeks ago, local officials, soldiers and militiamen from the majority Hutu tribe helped organize and took part in the massacre of over half a million members of the Tutsi minority and Hutu government opponents. </P>
<P> The Tutsi-dominated RPF says the one million refugees in the Goma area are welcome to return home. The former government and defeated army tell them that if they go back they will be slaughtered by the RPF. </P>
<P> ``It is necessary for us to fight them. The Rwandan army will return and fight if it necessary,'' Nzabalinda said. ``We civilians will return after the RPF is driven all the way back to Uganda.'' </P>
</TEXT>
</DOC>
