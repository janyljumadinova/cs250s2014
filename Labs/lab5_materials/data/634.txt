<DOC>
<DOCID>REU003-0320.940806</DOCID>
<TDTID>TDT001569</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/06/94 18:41</DATE>
<TITLE> ARKANSAS GOP TO CANDIDATE -- CAMPAIGN BEGINS AT HOME</TITLE>
<HEADLINE> ARKANSAS GOP TO CANDIDATE -- CAMPAIGN BEGINS AT HOME</HEADLINE>
<SUBJECT> BC-USA-ARKANSAS-REPUBLICANS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LITTLE ROCK, Ark (Reuter) </DATELINE>
<P>  Arkansas's Republican Party, embarrassed by wife-beating charges pending against their attorney general nominee, urged the candidate Saturday to take care of his family problems but did not call for him to withdraw. </P>
<P> ``We didn't think it was our place to tell him to withdraw,'' state Republican Chairman Asa Hutchinson said following a closed meeting of the party's executive committee Saturday. ``Ultimately it's up to him to decide whether to withdraw. </P>
<P> Dan Ivy, 44, who was unavailable for comment, said Friday it would be ``a cold day in hell'' before he withdrew from the campaign in which he faces Democratic incumbent Winston Bryant. Bryant is favored to win the November election. </P>
<P> Ivy, a Fayetteville, Arkansas lawyer, pleaded innocent Friday to charges of battery. He was arrested July 24 after his wife, Sarah, 47, called police to say Ivy was threatening her with a baseball bat and pulling her hair. </P>
<P> His wife has since begun divorce proceedings and has filed a separate civil suit seeking $50,000 in damages. </P>
<P> ``We did encourage Mr Ivy to put family considerations above everything else. We think he needs to get his priorities in order,'' the party chairman said. </P>
<P> Ivy says his wife beat him and that she has a history of mental disorder. </P>
</TEXT>
</DOC>
