<DOC>
<DOCID>REU002-0222.940704</DOCID>
<TDTID>TDT000115</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/04/94 08:09</DATE>
<TITLE> AUSTRALIAN TENNIS GREAT LEW HOAD DIES AT 59</TITLE>
<HEADLINE> AUSTRALIAN TENNIS GREAT LEW HOAD DIES AT 59</HEADLINE>
<SUBJECT> BC-TENNIS-HOAD 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: updates with details.)</NOTES>
<DATELINE>MADRID (Reuter) </DATELINE>
<P>  Lew Hoad, the Australian tennis star and twice Wimbledon champion, died Sunday evening in Malaga, Spain, an Australian embassy spokeswoman in Madrid said Monday. </P>
<P> Hoad, 59, had recently been diagnosed as having leukemia. </P>
<P> In London, International Tennis Federation president Brian Tobin said in a statement the sport had lost ``one of its true legends.'' </P>
<P> ``Many of his contemporaries believe that, at his best, Lew was one of the greatest players ever to grace the game,'' Tobin said. </P>
<P> ``He was a spectacular player, a great personality, a good guy and a friend to all who new him.'' </P>
</TEXT>
</DOC>
