<DOC>
<DOCID>REU005-0069.940709</DOCID>
<TDTID>TDT000321</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/09/94 08:54</DATE>
<TITLE> YELTSIN SAYS RUSSIA EQUAL WITH G7, BEGGAR NO MORE</TITLE>
<HEADLINE> YELTSIN SAYS RUSSIA EQUAL WITH G7, BEGGAR NO MORE</HEADLINE>
<SUBJECT> BC-GROUP-CIS-YELTSIN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MOSCOW (Reuter) </DATELINE>
<P>  President Boris Yeltsin said Saturday that Russia was no longer a beggar but an equal partner of the world's Group of Seven rich nations and voiced hopes for a Group of 8 with Moscow as a new political member. </P>
<P> ``The political climate will be different this time,'' Yeltsin told reporters at Moscow's Vnukovo airport before his departure for Naples to attend this weekend's G7 summit. </P>
<P> ``In my earlier meetings with the G7, I assumed the role of asking (for aid). Now this is over. This time, I will insist that I am an equal partner with the biggest countries of the world, the G7.'' </P>
<P> Unlike previous G7 summits when he was an observer, the Russian leader will for the first time be a full participant in Sunday's political debate on major world issues. </P>
<P> Yeltsin said Russia was ready to join the G7 ``politically'' to form a new grouping of G8 -- or what some other Russian officials call a ``G7-plus-one.'' </P>
<P> But Russia was still weak economically, Yeltsin said. He made clear it would be some time before Russia's economy becomes fit enough to join the capitalist world's rich men's club -- France, Italy, Germany, Japan, Canada, Britain and the United States. </P>
<P> ``Russia is ready for the idea of a political G8,'' Yeltsin said. ``Already we are participating in political solutions to world conflicts. We are being consulted by G7 leaders on many world issues including nuclear safety. </P>
<P> ``But as far as the economy is concerned, we should be realistic. We still don't have full stabilization. We still don't feel strong enough economically,'' he said. </P>
<P> ``We've come a long way ... we just need several other moves on the chessboard. This may happen in the coming years. Then there'll be a complete balance and we will not feel humiliated.'' </P>
<P> The G7 meeting is not expected to produce any fresh economic aid for Russia. </P>
<P> Yeltsin said he would not be pushing for a specific aid package. But he said he would press for a broad cooperation accord with the G7 similar to one Russia signed with the European Union last month. </P>
<P> Yeltsin's June meeting with EU leaders in Corfu seems to have dealt with most immediate trade problems with Europe. But the Russians still have a bone to pick with the United States and say that some 260 discriminatory measures remain on U.S. statute books. </P>
</TEXT>
</DOC>
