<DOC>
<DOCID>REU003-0349.940706</DOCID>
<TDTID>TDT000208</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/06/94 15:25</DATE>
<TITLE> RIGHTS GROUP SAYS ALL PARTIES IN SUDAN WAR AT FAULT</TITLE>
<HEADLINE> RIGHTS GROUP SAYS ALL PARTIES IN SUDAN WAR AT FAULT</HEADLINE>
<SUBJECT> BC-RIGHTS-SUDAN (EMBARGOED) </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> Release at 6:30 p.m. EDT</NOTES>
<DATELINE>NEW YORK (Reuter) </DATELINE>
<P>  Both sides in the war in southern Sudan have caused the devastating loss of life and starvation among civilians, a human rights group said Wednesday. </P>
<P> The New York-based Human Rights Watch/Africa made the charge in a 279-page report that it said chronicled human rights abuses by the Sudanese government and the two factions of the Sudan People's Liberation Army in the 11-year long war. </P>
<P> ``All parties have waged war in total disregard of the welfare of the civilian population and in violation of almost every rule of war applicable in an internal armed conflict,'' the report said. </P>
<P> It said the government undertook a bombing campaign this year that displaced 100,000 civilians. In addition government forces have abducted women and children and restricted relief efforts by outside agencies. </P>
<P> The report also said the People's Liberation Army has attacked civilians, starved them out in some cases and abducted underage boys to press them into fighting. </P>
<P> It said the U.N. Security Council should institute an arms embargo on both sides and the United States and other nations must keep pressure on Sudan to respect human rights and permit relief operations. </P>
</TEXT>
</DOC>
