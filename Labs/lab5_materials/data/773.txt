<DOC>
<DOCID>REU007-0211.940814</DOCID>
<TDTID>TDT001904</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/14/94 20:48</DATE>
<TITLE> CUBANS HIJACK BOAT TO JAMAICA</TITLE>
<HEADLINE> CUBANS HIJACK BOAT TO JAMAICA</HEADLINE>
<SUBJECT> BC-CUBA-JAMAICA 1STLD </SUBJECT>
<AUTHOR>     By Michael Becker </AUTHOR>
<TEXT>
<NOTES>(Eds: adding Sunday Cuban count for Florida)</NOTES>
<DATELINE>KINGSTON, Jamaica (Reuter) </DATELINE>
<P>  Cubans armed with knives and a gun hijacked a fishing boat and ordered it to sail to the United States, but the vessel instead went to Jamaica, police said Sunday. </P>
<P> The boat, which arrived on the north coast of Jamaica near Montego Bay Friday night, carried 23 people. Nineteen asked to seek political asylum in the United States and were to be turned over to U.S. embassy officials Monday in Kingston. The other four asked to return to Cuba. </P>
<P> Police reports said three of the Cubans were preparing for a regular fishing trip when they were hijacked by a group of 20. Three of the hijackers were reportedly armed, one with a gun and two with knives. </P>
<P> It was unclear whether the fishermen tricked the hijackers by going to Jamaica, or whether they had gone to Jamaica because it was closer. </P>
<P> It was the second group of Cubans fleeing their country to arrive in the past month. On July 12, nine Cubans were picked up at sea by a Belize-registered vessel after their small craft capsized. They applied for political asylum in Jamaica and are now in Kingston being processed by representatives from the United Nations High Commissioner for Refugees. </P>
<P> In Florida, at least three other boats have been detained since Thursday on suspicion of smuggling Cubans into the United States. The three-member crew of one of those vessels said they also had been hijacked and were allowed to return to Cuba Saturday. The five men they accused of carrying out the hijacking with a gun, knives and a speargun remained jailed in Key West. </P>
<P> The U.S. Coast Guard also rescued 156 Cubans in 26 groups in the Florida Straits Saturday, and 94 Cubans in 11 groups Sunday. </P>
<P> The number of Cubans fleeing their country in boats and rafts has increased steadily in the past two weeks, since Cuban president Fidel Castro threatened to open Cuba's ports to anyone who wants to leave. Castro made the threat to protest U.S.immigration policy, which he claims has led to hijackings and unrest in his country. </P>
</TEXT>
</DOC>
