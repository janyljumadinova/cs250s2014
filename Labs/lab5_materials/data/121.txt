<DOC>
<DOCID>REU004-0255.940708</DOCID>
<TDTID>TDT000278</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 06:51</DATE>
<TITLE> CLINTON GIVES EAST EUROPE CAUTIOUS HOPE OVER NATO</TITLE>
<HEADLINE> CLINTON GIVES EAST EUROPE CAUTIOUS HOPE OVER NATO</HEADLINE>
<SUBJECT> BC-POLAND-CLINTON </SUBJECT>
<AUTHOR>     By Timothy Heritage </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WARSAW (Reuter) </DATELINE>
<P>  President Clinton did not fulfill all Poland's hopes during his brief visit to Warsaw this week, but he gave a clear signal to eastern Europe that full NATO membership is a realistic dream. </P>
<P> Clinton told the Polish parliament during his 26-hour stay that it was only a matter of time before NATO let the former communist-ruled countries of eastern and central Europe join. </P>
<P> Some Polish politicians were disappointed he did not go further by setting a timetable for entry. But others were cautiously optimistic if only because he showed his concern for Poland's security and other needs just by visiting Warsaw. </P>
<P> ``I think that today we can feel a little closer to NATO,'' Foreign Minister Andrzej Olechowski said at the end of Clinton's visit Thursday evening. </P>
<P> ``I think things could move faster. Western institutions are reluctant to change ... But overall we think things are progressing,'' he said. </P>
<P> Membership of the North Atlantic Treaty Organization is a key foreign policy goal for Poland and other countries in eastern Europe because of lingering fears over Russia. </P>
<P> President Lech Walesa presented Poland's case in an unusually forthright dinner toast Wednesday, saying that a leap forward was badly needed to improve the security of eastern Europe. </P>
<P> Clinton provided no firm security guarantees or set any date for when Walesa's fears might be appeased. </P>
<P> But he was careful to underline the importance of the Partnership for Peace scheme under which countries of the region have established closer links with NATO. He called it the vehicle carrying these countries towards NATO membership. </P>
<P> Clinton also said countries such as Poland would not be abandoned in a ``gray zone'' between Moscow and the West and made clear he would not let Russia block NATO membership for others. </P>
<P> ``All in all, I think one should welcome what Clinton said here,'' Longon Pastusiak, a veteran left-wing politician, said. </P>
<P> ``But caution would be advisable when gauging the stimulus this will give to Polish-American relations and to Poland's sense of security,'' he said. </P>
<P> Clinton promised Poland a military and economic aid package worth more than $200 million, although much of the sum will be provided in cooperation with international institutions. </P>
<P> Pastusiak described the assistance as a ``drop in the bucket compared to our needs.'' </P>
</TEXT>
</DOC>
