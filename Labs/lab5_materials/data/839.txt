<DOC>
<DOCID>REU008-0340.940817</DOCID>
<TDTID>TDT002062</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 16:36</DATE>
<TITLE> STATE OF MINNESOTA AND INSURER SUE TOBACCO FIRMS</TITLE>
<HEADLINE> STATE OF MINNESOTA AND INSURER SUE TOBACCO FIRMS</HEADLINE>
<SUBJECT> BC-USA-TOBACCO-LAWSUIT 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Recasts, adds details)</NOTES>
<DATELINE>ST. PAUL, Minn (Reuter) </DATELINE>
<P>  Minnesota's attorney general and the state's largest health insurer filed suit against the tobacco industry Wednesday ``This case will be vigorously defended in the courts and our record there speaks for itself,'' she added. </P>
<P> Ciresi said information gathered during congressional hearings earlier this year in Washington about supposed collusion among the tobacco giants would be among the evidence presented by Minnesota in its case. </P>
<P> The state also planned to present data compiled from media reports on the increasingly potent issue of smoking, including stories about how one mans,'' Ciresi said. </P>
<P> Ciresi said the conspiracy began in 1954, when initial health studies condemning smoking prompted tobacco company executives to meet -- despite the antitrust implications of such a meeting -- and organize jointly-funded public relations and research arms. </P>
<P> ``It is time to make the tobacco profiteers pay for the devastation they have caused,'' he said. </P>
</TEXT>
</DOC>
