<DOC>
<DOCID>REU004-0265.940809</DOCID>
<TDTID>TDT001654</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/09/94 03:14</DATE>
<TITLE> BRITISH CAMERAMAN DEPORTED FROM CYPRUS</TITLE>
<HEADLINE> BRITISH CAMERAMAN DEPORTED FROM CYPRUS</HEADLINE>
<SUBJECT> BC-CYPRUS-BRITISH </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NICOSIA, Cyprus (Reuter) </DATELINE>
<P>  A British free-lance cameraman was deported from Cyprus Tuesday after the authorities of this divided Mediteranean island labelled him a security risk. </P>
<P> ``He is on the plane taking off for Amsterdam ... that's all we can say,'' an immigration officer at Larnaca international airport told Reuters by phone. </P>
<P> Peter Williams, 33, an ex-British soldier and father of four children from his marriage to a Greek Cypriot woman from whom he is now separated, was refused entry Sunday when he arrived at Larnaca airport from San Francisco via Amsterdam. </P>
<P> Williams strongly protested the accusation he had been spying for the breakaway state in Turkish-occupied north Cyprus, recognized only by Turkey. </P>
<P> Cyprus has been divided since 1984 following a Turkish invasion sparked by a short-lived coup in Nicosia engineered by the military junta then ruling Greece. </P>
<P> ``It's all lies. I've never spied for anyone. I will fight these lies at the European court ... The authorities harassed me ever since I had a very brief affair with a Turkish Cypriot girl,'' Williams told Reuters Monday evening. </P>
</TEXT>
</DOC>
