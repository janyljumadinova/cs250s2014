<DOC>
<DOCID>REU010-0164.940720</DOCID>
<TDTID>TDT000790</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/20/94 13:06</DATE>
<TITLE> CLINTON'S ACCUSERS RETURNS TO ARKANSAS</TITLE>
<HEADLINE> CLINTON'S ACCUSERS RETURNS TO ARKANSAS</HEADLINE>
<SUBJECT> BC-USA-FLOWERS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LITTLE ROCK, Ark (Reuter) </DATELINE>
<P>  Gennifer Flowers, the former nightclub singer whose allegations of a love affair with Bill Clinton almost sank his 1992 presidential bid, returned to Arkansas Wednesday to campaign against him. </P>
<P> ``I don't want Bill to be re-elected. I think the country can do better,'' Flowers said. ``The character issue, the moral issue, is important in the office.'' </P>
<P> Flowers, 44, who now lives in Dallas, appeared on a two-hour radio talk show on KBIS in Little Rock to advertise copies of her tape-recorded conversations with Clinton. She is selling them through a toll-free telephone number for $19.95. </P>
<P> ``This isn't about money. I'll do well to recover my expenses. This is about vindication,'' Flowers said. </P>
<P> Flowers said her nude pictorial in the December 1992 issue of Penthouse ``was worth $10 million. You'd take your clothes off for that, wouldn't you?'' she told a caller. </P>
<P> A Penthouse spokeswoman said the magazine does not comment on contracts with anyone who poses nude but added: ``that seems like a mighty high sum to me.'' </P>
<P> Clinton, Flowers said, ``was rewarded for his indiscretions by becoming president of the United States and I'm expected to go up to the hills of Arkansas and keep my mouth shut.'' </P>
<P> She said she was writing a book about her relationship with Clinton and was developing a stage act. </P>
<P> ``Nobody ever mentions my entertainment career,'' she complained. </P>
</TEXT>
</DOC>
