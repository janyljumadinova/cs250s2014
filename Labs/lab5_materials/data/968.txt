<DOC>
<DOCID>REU011-0184.940824</DOCID>
<TDTID>TDT002384</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/24/94 06:38</DATE>
<TITLE> KNOWLES, CAMPBELL TO VIE IN ALASKA GOVERNOR RACE</TITLE>
<HEADLINE> KNOWLES, CAMPBELL TO VIE IN ALASKA GOVERNOR RACE</HEADLINE>
<SUBJECT> BC-USA-POLITICS-ALASKA </SUBJECT>
<AUTHOR>     By Yereth Rosen </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ANCHORAGE, Alaska (Reuter) </DATELINE>
<P>  Former Anchorage Mayor Tony Knowles and Anchorage businessman Jim Campbell will face each other in November after they won their parties' primary elections Tuesday. </P>
<P> With 384 of 468 statewide precincts reporting early Wednesday, Democrat Knowles had 38.2 percent of the vote in a largely three-way race, while Republican Campbell had 49.6 percent, just 779 votes ahead of his rival, former Anchorage Mayor Tom Fink. </P>
<P> A third Republican candidate took 1.8 percent of the vote. </P>
<P> In Alaska primaries, voters must choose from two separate ballots -- one listing Republicans and the other listing Democratic, Green Party, Alaskan Independence Party and any other candidates. </P>
<P> Knowles and Campbell were the primary favorites. </P>
<P> Knowles, considered a liberal and environmentalist, was a comfortable 10.7 percentage points ahead of his nearest rival, former Lieutenant Governor Stephen McAlpine, considered more pro-development and friendly to the oil industry. </P>
<P> In third place in the Democratic race, with 21.7 percent, was former Alaska House Speaker Sam Cotten, who had distinguished himself by taking the campaign's toughest stance against the oil industry. </P>
<P> The moderate Campbell barely beat the conservative Fink, despite outspending his Republican rival by a 6-to-1 margin. </P>
<P> In a speech to supporters at the central election headquarters here, Campbell said the November race offers voters vastly different candidates -- ``one who says he created jobs and says he has done all these things, and another one, Jim Campbell, who has done all those things.'' </P>
<P> Knowles, a restaurateur who was also the Democratic gubernatorial nominee four years ago, said he expects more such shots as he and Campbell vie to replace outgoing Independent Governor Walter Hickel. </P>
<P> ``It will be a hard-fought race, I'm sure,'' Knowles told Reuters. ``I hope I address the questions that Alaskans are asking.'' </P>
<P> In the primary race for Alaska's sole U.S. House seat, incumbent Don Young, a Republican, was unopposed and received 100 percent of the party's vote. </P>
<P> Democrat Tony Smith, a former state commerce commissioner, faced only token opposition for his party's nomination and won 73 percent of the vote on Alaska's so-called ``open'' ballot. </P>
<P> Smith said he is unafraid of predictions that Democrats will take a beating at the polls this November because of President Clinton's unpopularity. </P>
<P> ``I don't think it's going to be a bad year for Democrats in Alaska,'' Smith told Reuters. ``I think it's going to be a bad year for incumbents.'' </P>
<P> Jonni Whitmore, representing the Green Party of Alaska, took 15.2 percent of the open-ballot vote, an impressive showing for the young environmentalist party. </P>
<P> She and Jim Sykes, the Greens' gubernatorial candidate and winner of 3.8 percent of the open-ballot vote, pointed out that they are the only candidates in their races who oppose opening Alaska's Arctic National Wildlife Refuge to oil drilling. </P>
<P> Lieutenant Governor Jack Coghill, who has been feuding with former running-mate Hickel, captured the Alaskan Independence Party nomination with 6.7 percent of the open-ballot vote. </P>
<P> The party, used by Hickel and Coghill in their 1990 victory, rejects most environmental regulations and envisons Alaska as an independent nation. </P>
</TEXT>
</DOC>
