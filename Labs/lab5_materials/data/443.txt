<DOC>
<DOCID>REU013-0241.940727</DOCID>
<TDTID>TDT001126</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/27/94 20:56</DATE>
<TITLE> FOCUS SHIFTS TO MOTIVES BEHIND PANAMA CRASH</TITLE>
<HEADLINE> FOCUS SHIFTS TO MOTIVES BEHIND PANAMA CRASH</HEADLINE>
<SUBJECT> BC-PANAMA-CRASH </SUBJECT>
<AUTHOR>     By David Luhnow </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PANAMA CITY (Reuter) </DATELINE>
<P>  Investigators said Wednesday they are not ruling out any motives for last week's deadly bombing of a Panamanian commuter plane that killed 21 people, most of them Jewish. </P>
<P> ``We are keeping all possibilities open (in the investigation),'' Manuel Coloma, spokesman for Panama's Judicial Police, told Reuters. </P>
<P> Israeli officials visiting Panama said it was ``unclear'' if the bombing of the plane that killed 12 Jews was linked to a wave of suspected Islamic extremist attacks in Argentina and London. </P>
<P> The Panama bombing came a day after the bombing of a Jewish community center in Argentina that may have killed as many as 100 people. In the past two days, Jewish targets in London have been bombed. </P>
<P> Israeli officials have expressed outrage at the attacks, but said they hoped they would not derail the Middle East peace process. </P>
<P> ``Israel will not be deterred by criminal acts on the part of the enemies of peace ... we will redouble our commitment to a just and lasting coexistence in the Middle East,'' Ovadia Eli, vice president of Israel's parliament, told reporters in Panama. </P>
<P> Panamanian police previously concluded a bomb inside the plane caused the July 19 crash and have asked Israel and the United States to help investigate the source. </P>
<P> In the week following the bombing, speculation has raged in Panama that it was the work of either anti-Jewish Islamic groups or Colombian drug traffickers. Callers identifying themselves as belonging to both groups have reportedly claimed responsibility for the crash. </P>
<P> Members of Panama's Jewish community said the subsequent attacks against Jewish targets in London strengthen their suspicions that the Panama crash was caused by extremists. </P>
<P> ``We are more firmly convinced every day that (the Panama crash) was part of a worldwide effort by suicide commandos to target Jewish communities,'' said Claudio Avruj, the Carribean area director for the Jewish organization B'nai B'rith. </P>
<P> Others, including Panama's President-elect Ernesto Perez Balladares, have said the crash was more likely a result of shady business dealings in the Colon Free Zone, a duty-free district where most of the plane's passengers worked. </P>
</TEXT>
</DOC>
