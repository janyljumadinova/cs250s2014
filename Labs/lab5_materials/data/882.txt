<DOC>
<DOCID>REU009-0201.940819</DOCID>
<TDTID>TDT002181</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/19/94 05:58</DATE>
<TITLE> FRANCE INTERNS MOROCCAN WITH SUSPECTED MILITANTS</TITLE>
<HEADLINE> FRANCE INTERNS MOROCCAN WITH SUSPECTED MILITANTS</HEADLINE>
<SUBJECT> BC-ALGERIA-FRANCE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PARIS (Reuter) </DATELINE>
<P>  France has interned a Moroccan with 25 suspected Algerian Muslim fundamentalists seized in a crackdown after five French government employees were killed in Algeria, the interior ministry said Friday. </P>
<P> The man was arrested in the southern town of Avignon on Thursday and taken to an abandoned army barracks at Folembray in northeastern France, a ministry spokeswoman said. </P>
<P> The spokeswoman would not give the name of the interned man or say why he had been arrested. French media said he was a senior figure at a mosque in Avignon. </P>
<P> The alleged Muslim extremists, believed by police to be members or sympathizers of Algeria's outlawed Islamic Salvation Front (FIS), are being held in administrative detention without trial. </P>
<P> The FIS was outlawed in Algeria after the army-backed authorities canceled a general election in January 1992 that the party had been on the verge of winning. Since then, more than 4,000 people have died in political violence. </P>
<P> A Muslim fundamentalist group claimed responsibility for the killing of the five French government workers. </P>
</TEXT>
</DOC>
