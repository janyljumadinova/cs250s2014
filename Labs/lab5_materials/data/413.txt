<DOC>
<DOCID>REU012-0433.940726</DOCID>
<TDTID>TDT001062</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/26/94 15:21</DATE>
<TITLE> RUSSIA, ESTONIA REACH TROOP PULLOUT DEAL</TITLE>
<HEADLINE> RUSSIA, ESTONIA REACH TROOP PULLOUT DEAL</HEADLINE>
<SUBJECT> BC-CIS-ESTONIA-RUSSIA 1STLD </SUBJECT>
<AUTHOR>     By Nigel Stephenson </AUTHOR>
<TEXT>
<NOTES>(Eds: Updates, adds details)</NOTES>
<DATELINE>MOSCOW (Reuter) </DATELINE>
<P>  Russia agreed Tuesday to withdraw its troops from Estonia by August 31, ending half a century of military presence in the Baltic states. </P>
<P> After nearly five hours of talks, Estonian President Lennart Meri said he and President Boris Yeltsin also signed an agreement resolving the issue of the status of Russian military pensioners living in Estonia. </P>
<P> ``Leaving Estonia on the last day of August will mean that the last consequences of World War Two are eliminated in the Republic of Estonia,'' Meri told a news conference delayed for nearly three hours by the extended talks. </P>
<P> ``This day is a turning point in relations between Estonia and, in more general terms, between the Baltic states and the Russian Federation.'' </P>
<P> Moscow has already withdrawn from Lithuania troops left behind by the collapse of the Soviet Union and has agreed to leave Latvia by the end of August. </P>
<P> But an accord to pull its estimated 2,000 troops out of Estonia, which like the other Baltic states regained its independence from the Soviet Union in August 1991, has been stalled by disagreement over the status of ethnic Russians there, particularly some 9,000 retired military officers. </P>
<P> Some of the pensioners are still in their 30s and served with the KGB security police or GRU military intelligence and are seen by some Estonians as a potential fifth column. </P>
<P> Estonia has insisted the issues of the troop pullout and the pensioners are not linked. </P>
<P> In the end, both sides were able to claim success. </P>
<P> The Itar-Tass news agency quoted Yeltsin as saying after the talks: ``The presidents of Russia and Estonia signed an agreement today that the rights of the Russian military pensioners would be respected equally to the rights of Estonian citizens.'' He said they also signed an agreement on the troop withdrawal. </P>
<P> Asked if he agreed with Yeltsin, Meri said, ``Quite sure. We can guarantee to all the people living in Estonia equal rights.'' </P>
<P> He said the Estonian citizens' only privilege was the right to vote in parliamentary elections, adding that the agreement on the pensioners had involved no changes in Estonian legislation. </P>
<P> Estonian Foreign Minister Juri Luik told the news conference the pensioners and their families would be able to apply for and obtain residence permits ``with the exception of people to whom, within the decision of the Estonian government, those persons will be denied on the grounds of the security of Estonia.'' </P>
<P> He said the commission examining applications would include a representative of the Conference on Security and Cooperation in Europe ``to guarantee fair play.'' </P>
</TEXT>
</DOC>
