<DOC>
<DOCID>REU010-0139.940821</DOCID>
<TDTID>TDT002274</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/21/94 10:49</DATE>
<TITLE> FATAH LINKS PLO CHARTER CHANGES TO RECOGNITION</TITLE>
<HEADLINE> FATAH LINKS PLO CHARTER CHANGES TO RECOGNITION</HEADLINE>
<SUBJECT> BC-MIDEAST-ARAFAT-FATAH </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TUNIS, Tunisia (Reuter) </DATELINE>
<P>  Yasser Arafat's Fatah group has decided that Israel must recognize a Palestinian state if it wants the PLO to change its charter denying the Jewish state's right to exist, a leading Fatah official said Sunday. </P>
<P> Abu Nizar, a member of Fatah's central committee, told Reuters the decision was taken Saturday night at a meeting in Tunis chaired by the PLO leader, who returned from the self-rule areas to discuss the issue with Palestinians still in exile. </P>
<P> ``The charter cannot be amended without Israel's recognition of a Palestinian state. We cannot recognize Israel's right to exist without reciprocation, which is Israel's recognition of the existence of a Palestinian state,'' Abu Nizar said. </P>
<P> ``Our decision was that Israeli forces should first redeploy in the West Bank, be replaced by Palestinian police, then there would be elections, then the Palestine National Council would meet and discuss both the charter's amendment and the proclamation of the Palestinian's state independence,'' he said. </P>
<P> ``If Israel does not recognize the establishment of a Palestinian state with East Jerusalem as a capital, then there would not be any decision to cancel the clauses of the charter calling for the liberation of Palestine.'' </P>
<P> Israeli Prime Minister Yitshak Rabin insisted last week after a meeting with Arafat in Gaza that the PLO should set a firm date for convening the PNC to amend sections of the charter Israel sees as calling for its destruction. Arafat, in a letter to Rabin at the time of last year's self-rule deal, undertook to amend those articles that deny Israel's right to exist. </P>
</TEXT>
</DOC>
