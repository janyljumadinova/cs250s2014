<DOC>
<DOCID>REU002-0269.940804</DOCID>
<TDTID>TDT001454</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/04/94 03:19</DATE>
<TITLE> VISIT SIGNALS GRADUAL THAW IN US-NZ RELATIONS</TITLE>
<HEADLINE> VISIT SIGNALS GRADUAL THAW IN US-NZ RELATIONS</HEADLINE>
<SUBJECT> BC-NEWZEALAND-USA </SUBJECT>
<AUTHOR>     By Simon Louisson </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WELLINGTON, New Zealand (Reuter) </DATELINE>
<P>  Assistant Secretary of State Winston Lord met New Zealand leaders Thursday in the highest-level U.S. visit for three years, but the two sides indicated only slow progress in improving relations. </P>
<P> President Clinton's administration decided in February to upgrade political contacts, frozen in 1987 after a left-wing government banned nuclear weapons and nuclear-powered ships from New Zealand waters. </P>
<P> Security and military cooperation remains suspended. </P>
<P> ``There's a feeling that we have an opportunity with the recent change in policy to strengthen our relationship and to work on many areas of convergence,'' Lord told reporters after meeting Foreign Minister Don McKinnon. </P>
<P> ``We still have some residual problems,'' said Lord, who also held talks with Prime Minister Jim Bolger. </P>
<P> New Zealand was part of the tripartite ANZUS alliance with the United States and Australia but was suspended in the late 1980s because of its anti-nuclear legislation. </P>
<P> Despite the election of the present conservative administration in 1990, the government has been unable to repeal the anti-nuclear legislation because of its popularity. </P>
<P> ``The same things have been said that have been said for the last three or four years,'' McKinnon said after a 45-minute meeting with Lord. </P>
<P> The pair discussed security in the Asian region, New Zealand's role in APEC (Asia-Pacific Economic Cooperation forum), U.N. Security Council issues and ASEAN (Association of Southeast Asian Nations) issues. </P>
<P> ``We reiterated the fact that we are very pleased with the new relationship that was established by the Clinton administration that was put in place earlier this year,'' McKinnon said. </P>
<P> Lord said before his visit that it was part of a new policy of greater dialogue on political and defense matters. </P>
<P> This has included a recent visit by outgoing U.S. Pacific fleet commander Admiral Charles Larsen, and meetings in the United States between McKinnon and Secretary of State Warren Chistopher. </P>
<P> Lord said on Wednesday his visit did not ``in any way reflect a softening of the United States position on the basic nuclear question.'' </P>
<P> He did not envisage a full return to the alliance relationship unless Wellington repealed its anti-nuclear laws. </P>
<P> An indication that there remained sticking points is the U.S. reluctance to allow Bolger to visit the White House. </P>
<P> Lord said he was aware of Bolger's interest in visiting. He said Clinton was looking forward to seeing Bolger at the APEC meeting in Indonesia in November but the president's schedule was ``extremely tight.'' </P>
</TEXT>
</DOC>
