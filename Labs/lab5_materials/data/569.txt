<DOC>
<DOCID>REU002-0064.940803</DOCID>
<TDTID>TDT001393</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 09:51</DATE>
<TITLE> AMTRAK TRAIN DERAILS IN N.Y., NEARLY 100 INJURED</TITLE>
<HEADLINE> AMTRAK TRAIN DERAILS IN N.Y., NEARLY 100 INJURED</HEADLINE>
<SUBJECT> BC-USA-TRAIN 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: Updates with later figures, new throughout)</NOTES>
<DATELINE>BATAVIA, NY (Reuter) </DATELINE>
<P>  Fourteen cars of an Amtrak train heading to Chicago derailed early Wednesday in western New York state, injuring at least 96 people, an Amtrak spokesman and local officials said. </P>
<P> Officials said some of the injuries were serious but none was life-threatening. Amtrak spokeswoman Sue Martin said 96 passengers had been treated for injuries, with six admitted to hospitals. </P>
<P> All of the passengers were out of the train cars by mid-morning, local officials at the scene said. </P>
<P> The train, the Lake Shore Limited, was carrying approximately 340 passengers, Martin said. </P>
<P> Officials said the train cars slid down a 50-foot embankment in a heavily wooded area at 3:45 a.m. EDT . The two locomotives and remaining two cars of the 16-car train stayed on the tracks. </P>
<P> The cause of the derailment was not immediately known. Martin said Amtrak investigators and officials from the National Transportation Safety Board were on their way to the scene. </P>
<P> She said she knew of no similar problems along that area of track in recent years. </P>
<P> While the train is operated by Amtrak, the national rail passenger company, the tracks are owned by the freight railroad Conrail. </P>
<P> Martin said the train normally operates at 79 mph along the section of track where the derailment took place. </P>
<P> The Lake Shore Limited service actually begins separately in New York City and Boston with the two sections meeting up in Albany, New York, for the trip to Chicago through northern New York state, Ohio, Indiana and Illinois. </P>
</TEXT>
</DOC>
