<DOC>
<DOCID>REU005-0094.940810</DOCID>
<TDTID>TDT001700</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/10/94 07:40</DATE>
<TITLE> SOUTH AFRICA MAY SEEK LEGAL TRADE IN RHINO HORN</TITLE>
<HEADLINE> SOUTH AFRICA MAY SEEK LEGAL TRADE IN RHINO HORN</HEADLINE>
<SUBJECT> BC-SAFRICA-RHINOS </SUBJECT>
<AUTHOR>     By Anton Ferreira </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>JOHANNESBURG (Reuter) </DATELINE>
<P>  South Africa might seek the lifting of an international ban on trade in the horns of endangered rhinoceroses, a conservation official said Wednesday. </P>
<P> ``It's possible, though not absolutely certain,'' Martin Brooks, a rhino expert at the Natal Parks Board (NPB), told Reuters. ``It's being debated at the moment.'' </P>
<P> He said an application for the easing of curbs on rhino horn trade could be made at a meeting of the Convention on International Trade in Endangered Species (CITES) in the United States in November. </P>
<P> Brooks said that if trade were to be allowed, a technique for tracing the source of horn developed at the University of Cape Town would be an important safeguard. </P>
<P> ``This could prevent the laundering of illegal horn as legal horn,'' said Brooks, who is chairman of the World Conservation Union's African rhino specialist group. </P>
<P> Researchers at the university have developed a technique of analyzing the chemical make-up of elephant tusks and rhino horns which pinpoints their origin to within a small geographical area. </P>
<P> One of the researchers, Julia Lee-Thorp, said they had analyzed horn from Kenya, Tanzania, Namibia, Zambia, Zimbabwe and South Africa. </P>
<P> ``Wherever we've done it, it seems to work very well,'' she said. ``It is an absolutely unequivocal test.'' </P>
<P> The technique relies on the fact that chemical elements in the horn, a dense hair-like material, can be traced back to the grass or leaves an animal has eaten and then back to the rocks in the area where the plants grew. </P>
<P> It can also be used to tell if a horn came from a white rhino, which has a relatively stable population, or a black rhino, which is more rare. </P>
<P> Clive Walker, chairman of South Africa's Rhino and Elephant Foundation, said the number of black rhino in Africa had declined from 15,000 a decade ago to less than 2,500 now. </P>
<P> ``The international ban on trade in rhino horn has been in effect for 18 years. It has probably failed to save a single rhino,'' he said. </P>
<P> Poachers have decimated rhino populations to obtain the horns, which are used as dagger handles in Yemen and are ground to powder for traditional medicines in Asia. </P>
</TEXT>
</DOC>
