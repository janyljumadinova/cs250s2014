<DOC>
<DOCID>REU014-0212.940729</DOCID>
<TDTID>TDT001212</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/29/94 17:46</DATE>
<TITLE> BRITISH WOMEN PLEAD NOT GUILTY TO MURDER CONSPIRACY</TITLE>
<HEADLINE> BRITISH WOMEN PLEAD NOT GUILTY TO MURDER CONSPIRACY</HEADLINE>
<SUBJECT> BC-USA-GURU 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Adds details)</NOTES>
<DATELINE>PORTLAND, Ore (Reuter) </DATELINE>
<P>  Two British women pleaded not guilty Friday to charges that they conspired to murder a U.S. attorney who deported Bhagwan Shree Rajneesh, an Indian guru who preached free love. </P>
<P> Sally Croft, 44, of Totnes, England, and Susan Hagan, 47, of Bedmond, England, appeared in the Portland courtroom of U.S. Magistrate Donald Ashmanskas to be formally charged with conspiracy to murder. </P>
<P> The two women, wearing casual clothes, were handcuffed. They appeared upbeat, smiling and talking to their lawyers. </P>
<P> They said little in court, except to plead ``not guilty'' to the charge of conspiracy to murder. </P>
<P> Croft and Hagan were extradited to Oregon from Britain this week, accused of plotting with others to assassinate former U.S. Attorney Charles Turner in 1985 when Turner was conducting an investigation of illegal activities at a religious commune set up by Rajneesh in central Oregon. </P>
<P> If found guilty, they face a maximum sentence of life imprisonment. </P>
<P> Ashmanskas set a trial date of September 27 before U.S. District Judge Malcolm Marsh. The trial is expected to last at least three weeks. </P>
<P> Ashmanskas ordered the women released without bail pending trial, but he set a number of strict conditions. The women must remain in Oregon, surrender their passports and wear electronic bracelets on their wrists so authorities can track their movements. </P>
<P> If they did flee, they would waive the right to an extradition hearing and could be tried in their absence. </P>
<P> The two women have made arrangements to stay with friends in the Portland area. </P>
<P> Croft hired an attorney while a court-appointed attorney, Colleen Scissors, was named to represent Hagan. </P>
<P> Scissors told reporters she did not believe that either woman could get a fair trial in Oregon because of the extensive pre-trial publicity and because of a general feeling in Oregon against followers of Rajneesh. </P>
<P> Several other charges have been filed in Oregon against Croft and Hagan, including wire-tapping and a federal weapons charge. But they may only be tried on conspiracy to murder, the charge on which they were extradited to the United States. </P>
<P> Rajneesh, who had a collection of 90 Rolls Royces, moved from India to Oregon in 1980. He and his orange-clad followers bought a ranch where they built the town of Rajneeshpuram. </P>
<P> The commune fell apart in 1985 after Rajneesh was convicted of fraud by arranging sham marriages so his followers could enter the United States. </P>
<P> He paid a $400,000 fine and agreed to leave the country. He died in India in 1990. </P>
<P> Croft, an accountant, was the chief financial officer of the commune, according to the indictment filed in court. </P>
<P> She went by the name of Ma Prem Savita, while Hagan was known as Ma Anand Su. </P>
<P> The indictment alleges the two participated in a plot to assassinate Turner and other people on a commune ``enemies list''. </P>
<P> Turner was targeted because he was heading an investigation of immigration fraud ``that threatened the existence of the commune,'' according to the indictment. </P>
<P> The conspirators sent members of the commune to other states to buy pistols and silencers, authorities allege. But they never carried out the attack. </P>
</TEXT>
</DOC>
