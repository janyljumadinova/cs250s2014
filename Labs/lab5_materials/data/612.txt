<DOC>
<DOCID>REU003-0041.940805</DOCID>
<TDTID>TDT001497</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/05/94 03:14</DATE>
<TITLE> CAMBODIA GIVES $10,000 TO RWANDA</TITLE>
<HEADLINE> CAMBODIA GIVES $10,000 TO RWANDA</HEADLINE>
<SUBJECT> BC-RWANDA-CAMBODIA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PHNOM PENH, Cambodia (Reuter) </DATELINE>
<P>  Impoverished Cambodia has given $10,000 for emergency relief aid to Rwanda as a gesture of solidarity, a government official said Friday. </P>
<P> ``We cannot be unmoved by the suffering of the Rwandan people and refugees,'' the official said, citing a letter from Foreign Minister Prince Norodom Sirivudh to the United Nations, which will distribute the Cambodian aid. </P>
<P> Cambodia, emerging from decades of war, revolution, and genocide, was all too aware of the problems facing Rwanda and the importance of international generosity, the official said. </P>
</TEXT>
</DOC>
