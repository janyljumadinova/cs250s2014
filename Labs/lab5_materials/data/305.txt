<DOC>
<DOCID>REU010-0068.940720</DOCID>
<TDTID>TDT000773</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/20/94 02:54</DATE>
<TITLE> ENVOY CONDEMNS ATTACK ON PEACEKEEPERS IN SOMALIA</TITLE>
<HEADLINE> ENVOY CONDEMNS ATTACK ON PEACEKEEPERS IN SOMALIA</HEADLINE>
<SUBJECT> BC-SOMALIA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MOGADISHU, Somalia (Reuter) </DATELINE>
<P>  The United Nations' special representative in Somalia condemned an attack on U.N. peacekeepers in the capital and said such action would badly affect world help to the country. </P>
<P> ``It is important that the humanitarian nature of UNOSOM (United Nations Operation in Somalia) is understood by the Somali people,'' said Ghanaian U.N. Ambassador Victor Gbeho in a statement released late Tuesday. </P>
<P> ``Such attacks will adversely affect the assistance of the international community to Somalia,'' Gbeho added. </P>
<P> Somali gunmen killed two Malaysian U.N. peacekeepers Monday and captured 11 other U.N. troops in a Mogadishu ambush, but released their captives within hours. </P>
<P> Gbeho slammed what he called a ``dastardly attack'' and reiterated the urgent need to create a stable environment that would encourage reconciliation and reconstruction. </P>
<P> It was the worst attack on U.N. peacekeepers in Somalia in two months and U.N. Secretary-General Boutros-Boutros Ghali said it underlined the need for a critical review of U.N. operations. </P>
<P> The U.N. Security Council, bruised by its failure to bring peace to Somalia despite brokering peace talks, is due to decide at the end of September whether to extend its mandate there. </P>
<P> U.N. officials say that continued violence will prompt the 19,000-strong U.N. force to pull out and the international community, weighed down with crises elsewhere, to turn its back on the Horn of Africa country. </P>
</TEXT>
</DOC>
