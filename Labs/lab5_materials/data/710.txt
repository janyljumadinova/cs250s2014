<DOC>
<DOCID>REU005-0363.940811</DOCID>
<TDTID>TDT001744</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/11/94 10:57</DATE>
<TITLE> AT LEAST 21 KILLED IN RUSSIAN RAIL ACCIDENT</TITLE>
<HEADLINE> AT LEAST 21 KILLED IN RUSSIAN RAIL ACCIDENT</HEADLINE>
<SUBJECT> BC-CIS-RUSSIA-RAIL </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MOSCOW (Reuter) </DATELINE>
<P>  At least 21 people were killed and 50 were taken to hospital with injuries after a commuter train slammed into freight wagons that had been derailed in southwest Russia Thursday, emergency officials said. </P>
<P> A spokeswoman for the National Emergencies Ministry said the accident occurred after five container wagons broke loose from a freight train at Dvuluchnoye, 70 miles southeast of the town of Belgorod. </P>
<P> A commuter train carrying local passengers crashed into the freight wagons and its rear two carriages leapt the rails and overturned. </P>
<P> The freight train had just crossed into Russia from Ukraine, where it had been undergoing repairs, when the container wagons uncoupled, said the spokeswoman, who was speaking by telephone. </P>
</TEXT>
</DOC>
