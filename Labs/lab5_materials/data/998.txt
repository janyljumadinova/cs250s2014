<DOC>
<DOCID>REU012-0116.940825</DOCID>
<TDTID>TDT002475</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/25/94 23:43</DATE>
<TITLE> PAPUA NEW GUINEA TO REPATRIATE 5,000 INDONESIANS</TITLE>
<HEADLINE> PAPUA NEW GUINEA TO REPATRIATE 5,000 INDONESIANS</HEADLINE>
<SUBJECT> BC-INDONESIA-PAPUA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>JAKARTA, Indonesia (Reuter) </DATELINE>
<P>  Papua New Guinea has agreed to repatriate to villages in neighboring Irian Jaya about 5,000 Indonesians who have fled the province in the past decade, Indonesia's Antara news agency said Friday. </P>
<P> Anthony Mahulette, Indonesia's consul in Vanimo town, near the border, told Antara that Papua New Guinea made the decision known in recent bilateral meetings but approval was needed from both governments. </P>
<P> About 3,000 of the 5,000 border-crossers live in camps supervised by the U.N. High Commissioner for Refugees. The rest are scattered in towns including Vanimo, Lae, Wewak, Daru, Madeng and Port Moresby, Antara said. </P>
<P> Relations have improved in the past year, and both sides agreed in March to develop a cross-border road after allowing troops to work in each other's territories. </P>
</TEXT>
</DOC>
