<DOC>
<DOCID>REU010-0298.940721</DOCID>
<TDTID>TDT000836</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/21/94 03:18</DATE>
<TITLE> A YEAR AFTER PEACE TREATY, LIBERIANS FIGHT ON</TITLE>
<HEADLINE> A YEAR AFTER PEACE TREATY, LIBERIANS FIGHT ON</HEADLINE>
<SUBJECT> BC-LIBERIA-WAR (NEWS ANALYSIS) </SUBJECT>
<AUTHOR>     By Steve Weizman </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ABIDJAN, Ivory Coast (Reuter) </DATELINE>
<P>  On July 25, 1993, Liberia's civil war protagonists signed a long-awaited peace accord, pledging to start disarming and install a transitional government within 30 days. </P>
<P> A year later, fewer than 10 percent of an estimated 30,000 fighters have laid down their guns, the government can do little outside the capital Monrovia, and the shaky truce between the main players is threatened by a proliferation of warlords. </P>
<P> ``It's getting more and more like Somalia every day and a lot of people believe it will become worse than Somalia,'' a United Nations official told Reuters in neighboring Ivory Coast. ``There are flareups everywhere. Even the main highways, although they are open, cannot be called safe,'' she said. </P>
<P> The leaders of the biggest militias, Charles Taylor and Alhaji Kromah, could probably agree on most issues and find ways to work together, judging by their publicly stated positions. </P>
<P> But both men are kept off-balance by attacks from the Krahn tribe, which analysts believe aims to regain by force the ascendancy it enjoyed under slain president Samuel Doe. </P>
<P> Taylor's National Patriotic Front of Liberia (NPFL) is battling Krahn fighters in the east and centre of the country and now faces the additional challenge of an internal schism. </P>
<P> NPFL ministers in the power-sharing government are turning against the man who put them there and some diplomats believe they have made a secret pact with Krahn gunmen edging toward Taylor's inland capital at Gbarnga. </P>
<P> ``The NPFL is not the problem, but rather its leader, Mr Charles Taylor, who has led the organization astray and continues to dwell on armed struggle ... because he knows that he cannot win elections,'' Labour Minister Tom Woewiyu told a Monrovia news conference Tuesday. </P>
<P> Taylor, who launched the civil war in December 1989, says he is for elections and analysts say he could well draw more votes than any other candidate now on the scene. </P>
<P> Kromah's ULIMO militia also has a Krahn problem. Last May Krahn renegades under General Roosevelt Johnson seized ULIMO headquarters northwest of Monrovia and hundreds of militiamen have died in fighting, which is still raging. </P>
<P> Elections, originally scheduled for last February, then September, now appear to be a distant dream. Large parts of the country are still battlegrounds and half Liberia's two million citizens are refugees in neighbouring states. </P>
<P> Even the United Nations, which clung to the September schedule after others said it was unworkable, now says polling depends on the success of a disarmament drive that has yet seriously to begin. </P>
<P> ``Until the disarmament process is substantially accomplished, the holding of free and fair elections will not be possible,'' a Security Council statement said last week. </P>
<P> The statement told the warring factions to meet by the end of this month and set a target date for disarmament. </P>
<P> Foreign electoral advisers say the talks should include the Liberia Peace Council (LPC), a Krahn militia that surfaced after last July's accord and demands government portfolios. </P>
<P> Taylor opposes participation of the LPC, the group harrying him in the south and east, saying its aggression should not be rewarded. A U.N. official, however, saw no alternative. </P>
<P> ``They will have to include them if it is to be successful, even though there will be a lot of resentment,'' she said. </P>
<P> Ivorian officials, alarmed by border incursions and the presence of some 300,000 Liberian refugees on their soil, hope fresh initiatives to resolve the conflict can be agreed at a West African summit due to be held in Nigeria next week. </P>
<P> Liberian leader David Kpomakpor held pre-summit talks with the presidents of neighbouring Guinea and Sierra Leone on Wednesday but announced no breakthrough. </P>
</TEXT>
</DOC>
