<DOC>
<DOCID>REU008-0345.940716</DOCID>
<TDTID>TDT000643</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/16/94 17:26</DATE>
<TITLE> U.S. STILL PUSHING PANAMA TO ACCEPT HAITIANS</TITLE>
<HEADLINE> U.S. STILL PUSHING PANAMA TO ACCEPT HAITIANS</HEADLINE>
<SUBJECT> BC-PANAMA-HAITI </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PANAMA CITY (Reuter) </DATELINE>
<P>  The United States is still trying to get Panama to accept Haitian refugees and has come up with a new plan to locate them in an area along the Panama Canal that does not have U.S. military bases, officials said. </P>
<P> President Guillermo Endara told reporters of the proposal late Friday, but said he would support it only if President-elect Ernesto Perez Balladares could persuade Panamanians to accept the plan. </P>
<P> ``My successor, Ernesto Perez Balladares, who has insisted on bringing in the Haitians, must first convince Panamanians and then I would be disposed to consider the issue,'' Endara said. </P>
<P> Endara agreed earlier this month to accept as many as 10,000 refugees, but later changed his mind. The U.S. proposal to locate them in U.S. military bases, he said, violated the 1977 Panama Canal treaties signed by the two countries. </P>
<P> His switch was prompted by strong public opposition, which has shown no signs of abating. </P>
<P> In an editorial Saturday, the influential daily La Prensa called the United States' continuing campaign to get Panama to accept the Haitians ``absurd'' and said the country ``cannot and should not'' take in the refugees. </P>
<P> Endara's reversal put a huge dent in President Clinton's plan of trying to find third countries to take in the thousands of Haitians who have been seized in recent months trying to make their way to the United States. </P>
<P> Perez Balladares, who takes office September 1 following his victory in May 8 national elections, has said that his government would consider harboring some Haitians for as long as six months. </P>
<P> Endara said the new U.S. proposal calls for placing Haitians in a site within the Panama Canal zone that is under the control of the Panama Canal Commission and not the U.S. military. </P>
<P> The commission runs the canal, which for decades was under U.S. control but is being turned over to Panama under the 1977 treaties signed by then-President Jimmy Carter. </P>
<P> The current U.S. plan calls for the United Nations to construct and run camps for the refugees, Endara said. </P>
<P> Perez Balladares was scheduled to meet with Clinton next week in Washington, where the Haitian refugee problem was expected to be a top item of discussion. </P>
</TEXT>
</DOC>
