<DOC>
<DOCID>REU008-0441.940818</DOCID>
<TDTID>TDT002098</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/18/94 07:20</DATE>
<TITLE> NOBEL AUTHOR ELIAS CANETTI DIES</TITLE>
<HEADLINE> NOBEL AUTHOR ELIAS CANETTI DIES</HEADLINE>
<SUBJECT> BC-SWISS-CANETTI </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ZURICH, Switzerland (Reuter) </DATELINE>
<P>  Bulgarian-born Nobel-prize winning writer Elias Canetti, who died at the weekend, was buried Wednesday in a grave next to James Joyce, his German publishers said Thursday. </P>
<P> He died in Zurich at the age of 89, publishers Carl Hanser Verlag said. </P>
<P> A Zurich city council spokesman said Canetti passed away peacefully. He was buried next to Joyce, the famous Irish author who died in Zurich in 1941. </P>
<P> Canetti was born in 1905 the son of Spanish-Jewish parents in Bulgaria. His family moved in 1911 to Manchester where he learned English and two years later to Vienna where he learned German, which became his language for writing. He studied in Zurich and Frankfurt. </P>
<P> His first big novel ``Auto Da Fe'' appeared in 1935. After Nazi Germany occupied Austria in 1938, he and his wife Veza left Vienna for Paris and then moved to London, where he obtained a British passport that he kept for the rest of his life. </P>
<P> Considered one of the most important authors writing in German, he won further acclaim in the 1970s and 1980s with his autobiographical works ``The Tongue Set Free,'' ``The Torch in my Ear,'' and ``The Play of the Eyes.'' </P>
<P> Lars Gyllensten, secretary of the committee that awarded the Nobel prize to Canetti in 1981, said the writer was a pivotal representative of central European culture and literature. </P>
<P> Shortly before his death Canetti had completed a further manuscript of his memoires, which will be published shortly. </P>
<P> Apart from the Nobel literature prize, he won the German critics prize in 1966, the Georg Buechner prize in 1972, and the Gottfried Keller prize in 1977. </P>
<P> Canetti's first wife, also a writer, died in 1963. Eight years later he married Hera Buschor, an art historian, and spent most of his remaining years in Zurich. </P>
</TEXT>
</DOC>
