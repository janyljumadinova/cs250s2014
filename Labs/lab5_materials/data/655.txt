<DOC>
<DOCID>REU004-0094.940808</DOCID>
<TDTID>TDT001616</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/08/94 08:27</DATE>
<TITLE> ROSE CALLS FOR SARAJEVO DMZ</TITLE>
<HEADLINE> ROSE CALLS FOR SARAJEVO DMZ</HEADLINE>
<SUBJECT> BC-YUGOSLAVIA-ZONE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SARAJEVO, Bosnia-Herzegovina (Reuter) </DATELINE>
<P>  The commander of U.N. troops in Bosnia called Monday for a demilitarized zone around Sarajevo, in a security measure after last Friday's NATO strike against rebel Serbs. </P>
<P> Lieutenant-General Sir Michael Rose sought a meeting with the commander of the Bosnian Serb forces, General Ratko Mladic, to discuss the issue, a U.N. spokeswoman said. </P>
<P> The plan calls for a withdrawal of all armed and uniformed soldiers and would allow both Serb and Muslim forces to take their weapons out of the zone for use elsewhere. </P>
<P> Weapons collection points, set up when a 12-mile heavy weapons exclusion zone was established around Sarajevo last February, would be scrapped. </P>
<P> U.N. spokeswoman Claire Grimes said of the DMZ plan: ``General Rose feels such a measure would enhance the security of Sarajevo.'' </P>
<P> Tension rose in the city after NATO launched an air strike against the Serbs last week to punish them for seizing back some of their heavy weapons from under U.N. guard in Sarajevo. </P>
</TEXT>
</DOC>
