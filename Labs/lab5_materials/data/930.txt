<DOC>
<DOCID>REU010-0200.940821</DOCID>
<TDTID>TDT002288</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/21/94 18:55</DATE>
<TITLE> UNSER JR COMES HOME WITH SEVENTH WIN OF SEASON</TITLE>
<HEADLINE> UNSER JR COMES HOME WITH SEVENTH WIN OF SEASON</HEADLINE>
<SUBJECT> BC-MOTOR RACING INDYCAR </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOUDON, New Hampshire, (Reuter) </DATELINE>
<P>  Al Unser Jr. winning for the seventh time this season, led a sweep of the top three spots for the Penske Ilmor team at the New Hampshire International Speedway on Sunday. </P>
<P> Unser Jr. took the lead with five laps to go in the 200-mile race when Emerson Fittipaldi sputtered into the pits out of gas. </P>
<P> The pole sitter, Fittipaldi had led for the first 46 laps and had regained the lead on lap 152. The Brazilian was cruising along with a 20 second lead before his fuel problems. </P>
<P> Fittipaldi still finished third with Paul Tracy of Canada second for the second consecutive top three sweep by the Penske Ilmor team. It was also the fourth time this season the Penske Ilmor team had taken the top three spots. </P>
<P> Unser, who averaged 122.635 mph for the 200 lap race, now had a commanding lead in the the series standings with 173 points to Fittipaldi's 133 with four races remaining. </P>
<P> Defending champion Nigell Mansell of Britain was forced out after a collision with his Newman-Haas Racing teammate Mario Andretti. </P>
<P> The mishap came on lap 109 with Andretti running fifth and Mansell second. With Mansell attempting to pass on the low side of the first turn the veterans touched wheels with Andretti's Lolo bouncing up into the outside wall. Andretti suffered only a slight bruise to his right thigh. </P>
<P> ``I got a good jump down the main straight, and I was alongside maybe a half a car in front going into turn one,'' Mansell said. ``I don't whether Mario didn't see me, but he came across. I tried to give him some room and almost came over into the pit lane. But unfortunately we touched.'' </P>
<P> Although Mansell was able to continue, he retired on lap 129 when his Lola had handling problems. </P>
</TEXT>
</DOC>
