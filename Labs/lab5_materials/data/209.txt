<DOC>
<DOCID>REU007-0100.940713</DOCID>
<TDTID>TDT000500</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/13/94 15:49</DATE>
<TITLE> PERRY: COPTER DOWNING ``NEVER SHOULD HAVE HAPPENED''</TITLE>
<HEADLINE> PERRY: COPTER DOWNING ``NEVER SHOULD HAVE HAPPENED''</HEADLINE>
<SUBJECT> BC-IRAQ-HELICOPTERS-REPORT 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: joins takes, expands with extra background)</NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  The Pentagon said Wednesday the ''friendly fire'' shootdown of two U.S. helicopters over Iraq was caused by human error and Defense Secretary William Perry called it ``a tragedy that never should have happened.'' </P>
<P> Perry presented the results of the two-month probe which concluded the pilots of two F-15C jets who shot down the helicopters down and the crew of an AWACS (Airborne Warning and Control System) plane monitoring flights in the area made several mistakes that led to the tragedy. </P>
<P> ``This report tells in great detail the root causes of this tragedy. It's a tragedy that never should have happened,'' he told a news conference at the Pentagon. </P>
<P> Perry said he was handing the report to U.S. commanders responsible for U.S. operations in the area and they would establish whether and what disciplinary action was warranted. </P>
<P> The chief investigating officer, Air Force Major General James Andrus, told the same news conference: ``There were multiple causes of the shootdown, any one of which, had it not existed, may have prevented the accident.'' </P>
<P> The report made clear human errors, in particular failures in communication, were primarily responsible for the April 14 tragedy, in which the two F-15C jets fired missiles at a pair of U.S. Army UH-60 Blackhawk helicopters. </P>
<P> They mistook them for Iraqi helicopters flying over a ''no-fly'' zone established to protect the minority Kurds. All 26 people on the helicopters died, including 15 Americans, most of them military personnel, five Kurds and three officers from Turkey, two from Britain and one from France. </P>
<P> ``The accident was caused by a breakdown in command guidance and supervision and the misidentification of the Blackhawks,'' Andrus said. </P>
<P> He cited the fact that the F-15 pilots were unaware the Blackhawks were operating in the area and listed a series of mistakes by the crew of an AWACS plane supervising flights in the area. Among those mistakes was the failure to inform the F-15 pilots of the Blackhawks' presence. </P>
<P> The commander of the AWACS crew, he said additionally, was ''not mission ready.'' </P>
<P> ``He had flown only one sortie in the previous three months and did not meet command standards for mission-ready status,'' Andrus said. </P>
<P> The two helicopters were assigned to United Nations duty in the mountainous northern Iraqi zone, where an international effort was underway to protect the minority Kurd people threatened by the Iraqi army after the end of the Gulf War. </P>
<P> The F-15 pilots, who were helping enforce a ``no fly'' zone to protect minority Kurds in northern Iraq from attack, were assigned to the U.S. Air Forces Command in Europe. </P>
<P> All American AWACS planes are under the U.S. Air Combat Command, based at Tinker Air Force Base in Oklahoma. </P>
</TEXT>
</DOC>
