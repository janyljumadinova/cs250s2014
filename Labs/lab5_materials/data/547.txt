<DOC>
<DOCID>REU001-0299.940802</DOCID>
<TDTID>TDT001358</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/02/94 12:31</DATE>
<TITLE> QUITTING SMOKING INCREASES LIFESPAN MORE THAN DIET</TITLE>
<HEADLINE> QUITTING SMOKING INCREASES LIFESPAN MORE THAN DIET</HEADLINE>
<SUBJECT> BC-HEALTH-SMOKING (EMBARGOED) </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> Release at 4 p.m.</NOTES>
<DATELINE>CHICAGO (Reuter) </DATELINE>
<P>  Stopping smoking has a greater impact on lengthening a person's lifespan than lowering cholesterol through dietary change, a study said Tuesday. </P>
<P> Researchers at Montreal General Hospital found that quitting smoking increases life expectancy by approximately two to four years for men and about two to three years for women. </P>
<P> In the study in the American Medical Association's publication Archives of Internal Medicine, researchers considered risk factor modifications in men and women aged 30 to 74 who were free of coronary heart disease. </P>
<P> Using a computer model to estimate the results, they study found that ``younger men, aged 30 to 59 years, might live slightly longer after dietary change, but among women and older men the average benefits would be neligible,'' author Steven Grover wrote. </P>
<P> Although study results indicated that dietary modifications help prevent coronary heart disease, ``the benefits of smoking cessation are more uniform across age and sex and are substantially greater than those predicted for dietary change.'' </P>
</TEXT>
</DOC>
