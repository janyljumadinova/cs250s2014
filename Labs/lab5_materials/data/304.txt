<DOC>
<DOCID>REU010-0040.940719</DOCID>
<TDTID>TDT000764</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/19/94 20:22</DATE>
<TITLE> YAMAHA CRITICIZES USE OF ITS VEHICLE IN ``LASSIE''</TITLE>
<HEADLINE> YAMAHA CRITICIZES USE OF ITS VEHICLE IN ``LASSIE''</HEADLINE>
<SUBJECT> BC-USA-LASSIE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CYPRESS, Calif.(Reuter) </DATELINE>
<P>  Yamaha Motor Corp USA said Tuesday it asked a film maker to stop movie commercials of a young actor doing a dangerous maneuver on a Yamaha all-terrain vehicle The company said it asked Paramount Pictures to halt promotion of the film ``Lassie'' with a scene from the movie where a cast member does a ``wheelie'' on a Yamaha ATV. </P>
<P> The scene has been used in television commercials promoting the movie, which opens Friday. </P>
<P> A Yamaha ATV is a four-wheel, all-terrain vehicle. A wheelie involves making the vehicle balance only on its two rear wheels, thrusting the front wheels high off the ground. </P>
<P> Yamaha Motor Corp USA said it formally requested that Paramount stop the promotion because it does not want viewers to be encouraged to try such stunt riding. </P>
<P> ``Yamaha expressly warns all ATV operators not to performn wheelies or engage in other stunt riding on ATVs,'' Russell Jura, Yamaha's general counsel, said in the request to Paramount. ``Such activity places an ATV operator at risk of serious bodily injury. </P>
<P> ``Yamaha is especially concerned that young people not engage in such unsafe behavior,'' Jura said. ``ATVs are not toys; they are serious machines that must be operated safely.'' </P>
<P> The company said the Badger ATVs used in ``Lassie'' are recommended only for children 12 and over and always with adult supervision. </P>
<P> Yamaha said it also asked Paramount to include disclaimers in the film that ATV operators should not perform any stunt or trick operations. </P>
<P> A Paramount spokesman said the movie studio is looking into the issue and could not comment further at this time. </P>
<P> Roy Watson, general manager of product validation for Yamaha, said the company was aware that the vehicle was being used in the film, but it was unaware that the use involved wheelies. </P>
<P> ``We don't know what else is in the film,'' Watson said. He said Yamaha did not receive any payment from Paramount for the appearance of the vehicle in the film. </P>
<P> Payments for so-called product placements are common by Hollywood studios. </P>
<P> The Walt Disney Co last year edited out a scene of two men lying down in the middle of a highway in the film ``The Program'' after a copycat incident by viewers of the film resulted in a death. </P>
</TEXT>
</DOC>
