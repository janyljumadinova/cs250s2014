<DOC>
<DOCID>REU013-0308.940728</DOCID>
<TDTID>TDT001154</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/28/94 10:34</DATE>
<TITLE> HOUSE-SENATE CONFERENCE COMPLETES CRIME BILL</TITLE>
<HEADLINE> HOUSE-SENATE CONFERENCE COMPLETES CRIME BILL</HEADLINE>
<SUBJECT> BC-USA-CRIME-BILL </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  A joint committee of senators and representatives Thursday approved a sweeping $30 billion bill to fight crime. </P>
<P> The bill, which includes a ban on 19 types of semiautomatic weapons, is a compromise between crime bills approved by the Senate and the House. The final bill must now be passed by both chambers. </P>
</TEXT>
</DOC>
