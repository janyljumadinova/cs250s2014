<DOC>
<DOCID>REU002-0158.940803</DOCID>
<TDTID>TDT001409</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 16:21</DATE>
<TITLE> ARGENTINA INVESTIGATING IRANIAN ROLE IN BLAST</TITLE>
<HEADLINE> ARGENTINA INVESTIGATING IRANIAN ROLE IN BLAST</HEADLINE>
<SUBJECT> BC-ARGENTINA-IRAN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BUENOS AIRES (Reuter) </DATELINE>
<P>  Argentina said Wednesday it was still investigating possible links between Iranian diplomats and the July 18 bomb blast that killed nearly 100 people at a Jewish community center. </P>
<P> ``If it is proved that Iranian diplomats were involved in terrorist activities, then we would have to rethink our relations with Iran,'' Foreign Minister Guido Di Tella told a news conference. </P>
<P> News media reported that investigators believed a member of Iran's mission in Buenos Aires had been visiting dealers in recent months inquiring about prices of vans similar to the one that was laden with explosives in the bombing. </P>
<P> Iran has denied any role in the attack and an Iranian newspaper said Wednesday that Argentina should officially apologize to Iran over earlier suggestions that Tehran was involved. Argentine President Carlos Menem said Monday that investigators believed the Iranian embassy could have played a part in the attack and Argentina was considering breaking diplomatic relations. </P>
<P> The bomb that ripped through the offices of Argentina's main Jewish organizations killed at least 96 people. </P>
</TEXT>
</DOC>
