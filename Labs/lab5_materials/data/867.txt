<DOC>
<DOCID>REU009-0043.940818</DOCID>
<TDTID>TDT002116</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/18/94 12:30</DATE>
<TITLE> FORMER COMMUNIST SUPERSPY GIVES UP TOP INTELLIGENCE JOB</TITLE>
<HEADLINE> FORMER COMMUNIST SUPERSPY GIVES UP TOP INTELLIGENCE JOB</HEADLINE>
<SUBJECT> BC-POLAND-SPY 1STLD </SUBJECT>
<AUTHOR>     By Marcin Grajewski </AUTHOR>
<TEXT>
<NOTES> (Eds: Adds quotes, background, edits throughout)</NOTES>
<DATELINE>WARSAW, Poland (Reuter) </DATELINE>
<P>  A former communist superspy, who was put in charge of Poland's civil intelligence three days ago, resigned Thursday after an outcry that his nomination would threaten the country's relations with the West. </P>
<P> Marian Zacharski, sentenced to life imprisonment by a Los Angeles court in 1981 over espionage charges and later released in a spy swap, said he was quiting because he did not want to aggravate the dispute. </P>
<P> ``I do not want to be a source of conflicts among Poles at a time when Poland most needs cooperation and agreement,'' he said in a statement. </P>
<P> Zacharski is believed to have taken secret plans of Hawk and Phoenix missiles and those of the anti-missile system Patriot during his espionage operations in the United States between 1975 and 1981. </P>
<P> In 1985, he returned to Poland after being exchanged in Berlin for Western agents caught in the then Soviet bloc. He held no official posts since then. </P>
<P> President Lech Walesa Wednesday asked the left-wing government of Prime Minister Waldemar Pawlak to revoke Zacharski's nomination, saying it would pose a threat to Poland's relations with the West. </P>
<P> Walesa's request followed harsh criticism of the nomination by opposition parties many of which said it was a sign the ruling left-wing parties, rooted in the Communist era, were bringing back the old system. </P>
<P> In his statement Zacharski said he did not ask for the job and his chief motivation was to serve his country. </P>
<P> ``In all my life I have always been directed by the well-being of Poland,'' he said. ``In the name of that loyalty, I spent four years in an American prison.'' </P>
<P> He said however he would be always prepared to come back to the service. </P>
<P> The State Protection Office (UOP), Poland's secret services where Zacharski was employed, had earlier defended him, saying he was ``a talented, dynamic professional.'' </P>
</TEXT>
</DOC>
