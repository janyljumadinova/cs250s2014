<DOC>
<DOCID>REU004-0206.940808</DOCID>
<TDTID>TDT001644</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/08/94 18:41</DATE>
<TITLE> HONDURAS PRESIDENT BOWS TO ARMY, ALLOWS DRAFT</TITLE>
<HEADLINE> HONDURAS PRESIDENT BOWS TO ARMY, ALLOWS DRAFT</HEADLINE>
<SUBJECT> BC-HONDURAS-ARMY </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>TEGUCIGALPA, Honduras (Reuter) </DATELINE>
<P>  Bowing to pressure from Honduras's powerful armed forces, President Carlos Roberto Reina has agreed to draft 7,000 men into the army, a senior government official said Monday. </P>
<P> ``The president will call men aged 18 to 30 for the army just as the officers asked,'' the official, who asked to remain anonymous, told Reuters. </P>
<P> Honduras's highest-ranking officer, General Luis Discua, last week revealed that Reina and armed forces chiefs had a tense meeting during which officers asked Reina if he was trying to destroy the army. </P>
<P> Some officers also said they had a shortfall of 7,000 men in an apparent challenge to a new law drafted by Reina and approved in May by the legislature to have a volunteer army. </P>
<P> The draft does not officially end until 1995, but Reina and armed forces chiefs were reported to have reached an informal agreement that would end the draft this year. </P>
<P> The army is also chafing at proposed budget cuts, the removal of the national police from its control and other moves by Reina, a former human rights attorney who took office in January, to roll back the military's influence in politics. </P>
<P> The army ruled Honduras for nearly 20 years until 1982, when it turned over power to civilians, but it continues to exert a great deal of power. </P>
</TEXT>
</DOC>
