<DOC>
<DOCID>REU007-0339.940815</DOCID>
<TDTID>TDT001928</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/15/94 14:26</DATE>
<TITLE> PERU FIRST LADY DENOUNCES CAMPAIGN TO MAKE HER LOOK INSANE</TITLE>
<HEADLINE> PERU FIRST LADY DENOUNCES CAMPAIGN TO MAKE HER LOOK INSANE</HEADLINE>
<SUBJECT> BC-PERU-SUSANA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LIMA (Reuter) </DATELINE>
<P>  Peruvian President Alberto Fujimori's estranged wife Susana Higuchi said in a published interview Monday there was a high-level campaign to make it appear she was depressed or going insane. </P>
<P> In the latest of a series of interviews in the past week, Higuchi told El Comercio newspaper she had been barred temporarily from entering Government Palace early Sunday. After she told the press, the guards let her in. </P>
<P> Higuchi, in a telephone conversation from the palace residence, said she had returned home around 2:30 a.m. Sunday morning after spending the day at the home of her former personal assistant Renee Odria. </P>
<P> Higuchi took refuge August 4 in Odria's home to avoid what she called ``personal pressures'' after she said publicly that a law banning her from seeking office in next year's elections was unconstitutional. She returned to Palace last Thursday. </P>
<P> ``They want to accuse me of being in a severe depression or that I am near insanity,'' Higuchi said. ``There is a campaign to make it look like I am crazy.'' </P>
<P> Asked who was orchestrating the campaign, she said: ``The campaign comes from a very high level.'' </P>
<P> She has said she was considering running for president in order to challenge the constitutionality of the law, adding that her estrangement from her husband is over a ``difference of ideas'' and not ``marital discord.'' </P>
<P> Fujimori has denied any order was given to prevent Higuchi from entering the Palace and said the delay Sunday was due to ''security measures.'' </P>
<P> Higuchi said she was alone in the Palace Residence and that Fujimori and three of their four children were apparently sleeping in the offices of the National Intelligence Service. </P>
</TEXT>
</DOC>
