<DOC>
<DOCID>REU008-0102.940816</DOCID>
<TDTID>TDT001986</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/16/94 14:17</DATE>
<TITLE> MAN ARRESTED IN RAPE OF SEVEN-YEAR-OLD IN HOSPITAL</TITLE>
<HEADLINE> MAN ARRESTED IN RAPE OF SEVEN-YEAR-OLD IN HOSPITAL</HEADLINE>
<SUBJECT> BC-USA-RAPE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CHICAGO (Reuter) </DATELINE>
<P>  A seven-year-old girl raped in her hospital bed a week ago has identified her attacker, police said Tuesday. </P>
<P> James Spratt, 20, was arrested by police Monday and identified in a lineup by the girl and hospital personnel. </P>
<P> The girl, who was in Wyler Children's Hospital receiving treatment for a kidney infection August 10, told police Spratt entered her room after a nurse departed. He allegedly told the girl he was her uncle, then covered her mouth as he raped her. </P>
<P> The girl was released from the hospital the next day and began receiving counseling. The attack, the second reported in the past month inside Chicago hospitals, has triggered concerns about security in public facilities. </P>
<P> ``We have to be open to those who need us and still keep out bad people. It's not easy,'' said Wyler Children's Hospital spokesman Susan Phillips. </P>
</TEXT>
</DOC>
