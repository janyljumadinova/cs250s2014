<DOC>
<DOCID>REU010-0395.940822</DOCID>
<TDTID>TDT002322</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/22/94 20:33</DATE>
<TITLE> MEXICO RULING PARTY LOOKS SET FOR CONGRESS MAJORITY</TITLE>
<HEADLINE> MEXICO RULING PARTY LOOKS SET FOR CONGRESS MAJORITY</HEADLINE>
<SUBJECT> BC-MEXICO-ELECTIONS-CONGRESS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MEXICO CITY (Reuter) </DATELINE>
<P>  Mexico's long-ruling Institutional Revolutionary Party (PRI) looks set to keep its majority in both chambers of Congress as well as winning the presidency, according to preliminary results issued Monday. </P>
<P> With about 36 percent of the ballots counted, the PRI was leading the race for the lower house Chamber of Deputies in the capital Mexico City and in 29 of the country's 31 states, the Federal Electoral Institute said. </P>
<P> The conservative National Action Party (PAN) was leading in the other two states. </P>
<P> Under Mexico's complex voting system for the legislature, 300 of the 500 lower house seats are chosen in those races. A further 200 are chosen by proportional representation from a national list, on which the PRI had 48 percent of the vote. </P>
<P> ``If they are in the upper 40s (in vote percentage), the PRI will keep its majority in the lower house,'' John Bailey, a Mexico analyst based at Georgetown University, told Reuters. </P>
<P> The PRI had 322 seats in the lower house in the outgoing legislature. Some analysts had said before the election that the ruling party could lose its majority in the lower house. </P>
<P> In the upper house Senate, the PRI was also ahead of the opposition in Mexico City and in 30 of the 31 states. Under new rules, the Senate is being expanded from 64 seats, of which the PRI had 61 in the outgoing legislature, to 128. </P>
<P> Under the new rules, the second-place party in Senate races Sunday will receive one seat for every two won by the first-placed party, a move aimed at increasing opposition representation in the upper house. </P>
<P> PRI candidate Ernesto Zedillo has already claimed victory in the presidential race. Preliminary results gave him about 47 percent of the vote. </P>
</TEXT>
</DOC>
