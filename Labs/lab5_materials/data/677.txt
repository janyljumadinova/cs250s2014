<DOC>
<DOCID>REU004-0379.940809</DOCID>
<TDTID>TDT001671</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/09/94 15:51</DATE>
<TITLE> FEATURE: SCULPTOR'S IDYLLIC HOME PRESERVED IN NEW HAMPSHIRE</TITLE>
<HEADLINE> FEATURE: SCULPTOR'S IDYLLIC HOME PRESERVED IN NEW HAMPSHIRE</HEADLINE>
<SUBJECT> BC-ART-SAITGAUDENS </SUBJECT>
<AUTHOR>     By Christine Gardner </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CORNISH, NH (Reuter) </DATELINE>
<P>  The landscape of the Connecticut River Valley on the New Hampshire-Vermont border proved the ideal setting for the art of Augustus Saint-Gaudens, who lived and worked here from 1885 to his death in 1907. </P>
<P> Its dramatic views of the ancient and once powerful volcano Mount Ascutney provided the artist with a refuge from the frenzy of New York. It was here that he created some of America's most spectacular monumental sculptures. </P>
<P> Those works include the memorial to Civil War General William Sherman at Fifth Avenue near New York's Central Park and the first important portrayal of Abraham Lincoln, which was set up in Lincoln Park, Chicago, in 1887. </P>
<P> Saint-Gaudens' inspiration for the statue's poignant, downcast look is believed to have been the men he encountered in New Hampshire. A friend, Charles Beaman, who sold him his house in Cornish, promised him he would see many ``Lincoln-shaped'' men in the northern New England state. </P>
<P> Today, Saint-Gaudens' home and studios are meticulously preserved and cared for by the National Park Service. The site, open from May to October and visited annually by over 40,000 people, is the only artist's residence with original works in the nation to be cared for under federal mandate. </P>
<P> ``It is our country's most beautiful coin,'' personally commissioned by President Theodore Roosevelt, said John Dryfhout, curator of the Cornish site. Thirteen of 11,200 coins struck, in very high relief, are worth over $1 million each, said Dryfhout. One was once owned by Egypt's King Farouk. </P>
<P> Saint-Gaudens' home and studios offer a rare glimpse into how an artist of high achievement lived and worked. And revamping the house and grounds itself was an artistic achievement. </P>
<P> When the Dublin-born artist first came to ``Huggin's Folly,'' as the brick house was originally called, he was appalled. It was March, New Hampshire's bleakest month, and the rundown hilltop home looked forbidding to the artist, then 37 years old. </P>
<P> But he soon he warmed to the house's legends, among them, that it had been a tavern where wayward women enticed lonely travelers. He renamed it ``Aspet'' and redesigned the exterior in neoclassical style, filling it with tapestries, a satin Empire couch, other exquisite furnishings and paintings by his wife and other members of the ``Cornish Colony'' of artists. </P>
<P> Members included illustrator Maxfield Parrish, who lived in nearby Plainfield. But most came up from New York and Boston, seeking, like Saint-Gaudens, a refuge from the city. </P>
<P> He turned the barn into a studio and cleared away brush, creating sweeping lawns where he and friends played volleyball and croquet. </P>
<P> Examples of Saint-Gaudens' work are tucked here and there -- a cast of the mysterious Adams Memorial, a cloaked woman in meditation, sits in a grotto; a copy of the gilded Amor Caritas angel hovers over a sunken fountain in a temple-like courtyard. The Luxembourg Museum in Paris houses the original. </P>
<P> Last month a new bronze cast of the statue of Admiral David ``Damn the Torpedoes'' Farragut was unveiled at the site. The original, the artist's first major public monument, was placed in New York's Madison Square Garden in 1881. </P>
<P> The cast sits on the original sandstone base, designed by noted architect Stanford White and decorated with bas relief dolphins and goddesses, said to be the first use of Art Nouveau in a public monument. </P>
<P> But the cast is at risk of deterioration and monitors wired to the back of the base keep track of humidity and movement. ''Stone isn't static,'' said Dryfhout. ``It could be a disaster if it breaks off.'' </P>
<P> Even more at risk is a cast of Saint-Gaudens' Shaw Memorial depicting Civil War Colonel Robert Gould Shaw leading his regiment of black men into battle. The original, in Boston Common, was finished in 1897 after 14 years of work in the Cornish studio. </P>
<P> The cast at Cornish, with its complex interior of wood, steel and textile stresses, has been outside, subjected to the elements, and is showing cracks and wear. </P>
<P> The statue needs to be housed, a $1 million project, said Dryfhout. ``We need to do something more now. It needs to be put in an enclosed, museum environment. This is a national treasure, a unique cast that doesn't exist anywhere else in the world.'' </P>
</TEXT>
</DOC>
