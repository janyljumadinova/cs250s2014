<DOC>
<DOCID>REU014-0252.940729</DOCID>
<TDTID>TDT001229</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/29/94 23:07</DATE>
<TITLE> U.S. MARINES TO JOIN FIREFIGHTERS IN THE NORTHWEST</TITLE>
<HEADLINE> U.S. MARINES TO JOIN FIREFIGHTERS IN THE NORTHWEST</HEADLINE>
<SUBJECT> BC-USA-WILDFIRES 1STLD </SUBJECT>
<AUTHOR>     By Michael McCarthy </AUTHOR>
<TEXT>
<NOTES>(Eds: Updates with two Washington towns threatened, details)</NOTES>
<DATELINE>SEATTLE (Reuter) </DATELINE>
<P>  U.S. Marines from Camp Pendleton were ordered to battle raging fires in the Northwest that have already consumed nearly 100 homes and other structures and threatened residents in two small towns, officials said Friday. </P>
<P> Dry, parched lands, high temperatures and heavy winds fanned fires throughout other Western states, with blazes reported in Utah, Nevada, Montana, Idaho and California. </P>
<P> Nearly 5,000 residents in two small Washington towns, Chelan and Leavenworth, were instructed by officials to be ready to evacuate if the fires continued their approach. </P>
<P> ``Winds were picking up and everyone was getting a little nervous,'' said Alice Lasher, spokeswoman for the Multiagency Coordinary Center. </P>
<P> According to the National Fire Center in Boise, Idaho, 11,500 firefighters, 600 fire engines, 100 helicopters and 19 air tankers are now deployed to fight fires in the West. </P>
<P> President Clinton told reporters that, as of Friday morning, 320 fires were burning in the seven states and that the federal government had mobilized more than 330 fire crews and more than 200 fire engines, helicopters and air tankers.  Marine reinforcements will add 1,000 firefighters to nearly 8,000 men and women now battling to contain 30 major fire complexes burning in Washington and Oregon. </P>
<P> ``Two battalions of Marines have begun training today and will be deployed to fight the fires as soon as possible,'' Clinton said. ``Our hearts go out to all those who have been displaced, or who have lost property in these fires.'' </P>
<P> The fires, which were sparked by lightning strikes last weekend, have blackened more than 130,000 acres of tinder-dry timber and grassland. </P>
<P> To date, 15 homes and 79 barns and other structures have been burned and more than 700 people have been evacuated, many of them visitors who were vacationing at the resorts around Lake Chelan in the north central part of Washington. </P>
<P> Weather forecasts offered little cause for hope. On Friday, the National Weather Service forecast below normal rainfall in the Northwest and above normal temperatures for August. </P>
<P> The region's largest fire, called the Tyee Creek fire, has nearly tripled in size over the past three days and is estimated to have destroyed more than 70,000 acres of forest in the mountains of central Washington, fire officials said. </P>
<P> Firefighters suspect the Tyee fire has scorched more land, but thick clouds of smoke obscured the rugged terrain, blinding fire spotters.  Chris Strebig, also of the Multiagency Coordinating Center, said fire officials cannot predict when they may be able to contain the Tyee Creek blaze. </P>
<P> In north-central Oregon, firefighters successfully used back fires to halt the spread of a 50,000-acre fire on the Warm Springs Indian Reservation. </P>
<P> In Idaho, fire fighters have been working to contain three major fires. Driven by shifting winds, the 4,000-acre Elevator Canyon fire in southeastern Idaho has changed direction several times, said Ray Mitchell of the U.S. Bureau of Land Management. </P>
<P> ``The weather has been really squirrely with these thunder storm cells moving through the area. It's hard to make a plan that works,'' Mitchell told Reuters. </P>
<P> Two aircraft and 30 people from Colorado's Peterson Air Force Base are fighting fires in Idaho. </P>
<P> With so many fires going on at once, fire fighting resources are stretched to the limit, Mitchell said, ``There's a real shortage of people and equipment throughout the area.'' </P>
</TEXT>
</DOC>
