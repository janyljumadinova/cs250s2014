<DOC>
<DOCID>REU004-0442.940708</DOCID>
<TDTID>TDT000306</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 22:27</DATE>
<TITLE> DENNY CASE DEFENDANT SENTENCED TO PROBATION</TITLE>
<HEADLINE> DENNY CASE DEFENDANT SENTENCED TO PROBATION</HEADLINE>
<SUBJECT> BC-USA-DENNY </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LOS ANGELES (Reuter) </DATELINE>
<P>  The last of a group of black men tried in the attack on white truck driver Reginald Denny was sentenced Friday to three years probation for shooting a gun at Denny's truck at the onset of the 1992 L.A. riots. </P>
<P> Lance Jerome Parker was acquitted on the most serious charges he faced in connection with the attack on Denny, who was dragged from his truck and severely beaten at the flashpoint of rioting that followed the acquittal of four white police officers in the beating of black motorist Rodney King. </P>
<P> But he was convicted May 2 of shooting at an unoccupied vehicle for firing a shotgun at Denny's gravel truck as the driver lay bloodied and semiconscious next to the rig at the an intersection on April 29, 1992. </P>
<P> The assault on Denny, broadcast across the nation by a television crew hovering overhead in a helicopter, was seen as a parallel to the beating of King. </P>
<P> The acquittal of the four Los Angeles police officers on state charges related to the King beating sparked days of rioting in Los Angeles, the worst U.S. civil unrest this century. </P>
<P> Parker, a former semi-pro football player, maintained his innocence throughout his trial, claiming he was not at the intersection when Denny was attacked. </P>
<P> He was given credit for 135 days spent in the county jail following his arrest. </P>
<P> Deputy District Attorney Kevin McCormick had urged the court to give Parker a stiffer sentence, saying he had ``aggravated an already horrific situation'' by bringing a shotgun to the flashpoint. </P>
<P> Of about a half dozen defendants charged in the attack on Denny, only one, Damian Williams, was convicted on a serious charge. </P>
<P> Williams, who was seen throwing a brick that hit Denny in the head, was convicted of felony mayhem and was sentenced last December to the maximum term of 10 years in prison. </P>
</TEXT>
</DOC>
