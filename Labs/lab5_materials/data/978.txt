<DOC>
<DOCID>REU011-0313.940824</DOCID>
<TDTID>TDT002408</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/24/94 18:06</DATE>
<TITLE> CLINTON URGES END TO ARAB BOYCOTT OF ISRAEL</TITLE>
<HEADLINE> CLINTON URGES END TO ARAB BOYCOTT OF ISRAEL</HEADLINE>
<SUBJECT> BC-MIDEAST-CLINTON </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  President Clinton Wednesday urged an end to the Arab economic boycott of Israel, saying it now harms Palestinians as well as Israelis. </P>
<P> ``The boycott must be ended,'' Clinton said by satellite hookup to a Chicago convention of the Jewish service organization, B'nai B'rith. </P>
<P> Clinton said that the Palestinian accord with Israel means that the Palestinians have entered into a new economic relationship with Israel. </P>
<P> ``Continuing the boycott harms not only Israel but the Palestinians as well,'' Clinton said. </P>
<P> Clinton also said ending the decades-old boycott was justified given the progress was being made on the Syrian negotiating track in efforts to reach a comprehensive peace in the Middle East. </P>
<P> ``With serious progress being made on the Syrian negotiating track, retaining this relic of a bygone era cannot possibly be justified,'' he said. </P>
<P> Clinton, who has witnessed Israel and the Palestine Liberation Organization and Israel and Jordan sign peace accords, said the United States has a right to expect all participants in the peace process to live up to the commitments they have made. </P>
<P> ``In this regard, it's heartening to hear from many Palestinians their genuine desire for democratic elections, representative government and transparent and accountable institutions -- these things they need and they deserve nothing less,'' he said. </P>
<P> Clinton also said that just as Washington has acted to sustain and enhance Israel's qualitative military edge, ``so too it will help to compensate for any strategic advantages Israel may choose to give up for peace.'' </P>
</TEXT>
</DOC>
