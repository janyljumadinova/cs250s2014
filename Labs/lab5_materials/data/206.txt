<DOC>
<DOCID>REU007-0085.940713</DOCID>
<TDTID>TDT000497</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/13/94 15:00</DATE>
<TITLE> CLINTON SETS AID PACKAGE FOR FLOOD-RAVAGED STATES</TITLE>
<HEADLINE> CLINTON SETS AID PACKAGE FOR FLOOD-RAVAGED STATES</HEADLINE>
<SUBJECT> BA-USA-FLOOD-AID </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ALBANY, Ga (Reuter) </DATELINE>
<P>  President Clinton Wednesday announced a package of more than $60 million in federal relief for flood-ravaged Georgia, Alabama and Florida. </P>
<P> Most of the assistance announced by Clinton after he viewed flood-swamped areas by air on a Marine helicopter was for Georgia, hardest hit of the three states from torrential rain and swollen rivers. </P>
</TEXT>
</DOC>
