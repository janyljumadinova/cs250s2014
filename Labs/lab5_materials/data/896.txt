<DOC>
<DOCID>REU009-0349.940819</DOCID>
<TDTID>TDT002212</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/19/94 18:07</DATE>
<TITLE></TITLE>
<HEADLINE></HEADLINE>
<SUBJECT> BC-HEALTH-CONGRESS-PLAN -2 WASHINGTON </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE></DATELINE>
<P> Chafee said the health proposals, which the moderates hope will form the basis of a compromise that can break the logjam in the Senate on health care, will cover 92 to 93 percent of the population by 1999. </P>
<P> The group separately briefed both Majority Leader George Mitchell, a Maine Democrat, and Minority Leader Bob Dole of Kansas. </P>
<P> Both praised it as a serious effort, although neither made a committment pending further review. Mitchell cancelled a Saturday Senate session to give members time to study the Chafee group's so-called ``mainstream'' proposal. </P>
<P> Although moderates of both parties have pinned hopes on the mainstream group, members acknowledge that they are still waiting for some budget data which will shape both their deficit and coverage goals. </P>
<P> President Clinton wants universal coverage. Mitchell's plan aims to cover 95 percent of Americans by 2000. </P>
<P> ``Our final figures are not in and we have some figures still to come in -- particularly dealing with the amount of subsidies we can give,'' said Chafee, adding, ``This is a bill that can pass this year. </P>
</TEXT>
</DOC>
