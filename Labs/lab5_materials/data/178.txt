<DOC>
<DOCID>REU006-0130.940711</DOCID>
<TDTID>TDT000410</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/11/94 20:29</DATE>
<TITLE> BRITAIN INCREASES PENALTY FOR MALE RAPE</TITLE>
<HEADLINE> BRITAIN INCREASES PENALTY FOR MALE RAPE</HEADLINE>
<SUBJECT> BC-BRITAIN-RAPE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  Britain's upper house of parliament Monday night brought the penalty for raping a man into line with that for raping a woman. </P>
<P> The House of Lords voted to create a new crime of male rape carrying a maximum penalty of life imprisonment, the same as that for raping a woman. </P>
<P> Previously British law did not recognize the term male rape. Offenders were charged with sexual assault or non-consensual buggery, which carried a maximum sentence of 10 years in jail. </P>
<P> The vote came after the conservative government agreed to back an amendment to the law proposed by opposition Labor Party peer Lord Ponsonby. Campaigners argued that a change to the law would help victims seek help. </P>
<P> It follows a vote earlier this year by parliament to lower the age of consent for homosexuals from 21 to 18. </P>
<P> The change, which will give male rape victims the same protection as female victims such as anonymity in court proceedings, brings Britain into line with several European countries and most U.S. states. </P>
</TEXT>
</DOC>
