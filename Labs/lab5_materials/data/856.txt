<DOC>
<DOCID>REU008-0449.940818</DOCID>
<TDTID>TDT002100</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/18/94 08:08</DATE>
<TITLE> ISRAEL ARMY SAYS HOLD GOLAN EVEN IF PEACE COMES</TITLE>
<HEADLINE> ISRAEL ARMY SAYS HOLD GOLAN EVEN IF PEACE COMES</HEADLINE>
<SUBJECT> BC-MIDEAST-ISRAEL-SYRIA </SUBJECT>
<AUTHOR>     By Bradley Burston </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>JERUSALEM (Reuter) </DATELINE>
<P>  Israel's army chief Ehud Barak said in remarks broadcast Thursday the Jewish state should remain on the Golan Heights even under an eventual peace with Syria. </P>
<P> But an Israeli cabinet minister who helped forge a self-rule deal with the PLO said Israel would have to make a ``very deep and very painful'' withdrawal from the strategic plateau to make peace with Damascus. </P>
<P> Barak, speaking to Israeli reporters in Washington Wednesday after discussing with senior U.S. defense officials the security implications of a future pact with Syria, said: </P>
<P> ``From a professional military standpoint, as long as there is no peace we need every meter (yard) of the Golan Heights, and also at a time of peace we had better stay on the Golan Heights.'' </P>
<P> Wrangling over the strategic plateau, which Israel captured from Syria in the 1967 Middle East war, has stymied U.S.-brokered peace talks between the sides since negotiations began almost three years ago. </P>
<P> Israel has offered an as yet unspecified withdrawal from the Golan, but demands Syrian assurances that peace will include open borders, trade, and exchange of embassies. </P>
<P> Responding to Barak's remarks, Sarid said in exchange for peace with Syria that the government would have to approve a pullout. ``The withdrawal will have to be very deep and very painful,'' Sarid told army radio. </P>
<P> Washington, which has mediated between the sides in a series of shuttles by ecretary of State Warren Christopher, views an Israeli-Syrian accord as the key to an overall settlement in the region. </P>
<P> Prime Minister Yitzhak Rabin declined to comment directly on Barak's statement. ``I gave him the possibility of presenting his position,'' Rabin told Israel Radio. </P>
<P> Rabin reiterated his promise to submit any withdrawal to Israeli voters in a referendum or in early elections. </P>
<P> Barak said Israel had not asked Washington to deploy troops on the Golan under a future settlement. </P>
</TEXT>
</DOC>
