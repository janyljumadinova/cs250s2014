<DOC>
<DOCID>REU002-0328.940704</DOCID>
<TDTID>TDT000129</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/04/94 15:44</DATE>
<TITLE> NORTHERN YEMEN SAYS ITS TROOPS CAPTURE MUKALLA</TITLE>
<HEADLINE> NORTHERN YEMEN SAYS ITS TROOPS CAPTURE MUKALLA</HEADLINE>
<SUBJECT> BC-YEMEN-CITY 1STLD </SUBJECT>
<AUTHOR>     By James Anderson </AUTHOR>
<TEXT>
<NOTES>(Eds: adds northern TV announcement, detail)</NOTES>
<DATELINE>SANAA, Yemen (Reuter) </DATELINE>
<P>  Northern Yemen announced said Monday its forces had captured the eastern port of Mukalla, a key base for their southern civil war enemies. </P>
<P> In Aden, southern officials contacted by Reuters from Dubai denied the northern claim. </P>
<P> ``The forces of unity have captured Mukalla and are pursuing the separatist gang in (the eastern province of) Hadramawt,'' said a statement read on the main television news broadcast in the northern capital Sanaa. </P>
<P> ``There was no fighting in the city itself,'' a senior government official told Reuters. </P>
<P> ``The big fighting was in Broum, west of Mukalla, which began on Sunday and lasted 14 to 16 hours. Then our forces entered the city itself without further fighting.'' </P>
<P> Mukalla, in Hadramawt, is 390 miles by road east of southern Yemen's main city, Aden. </P>
<P> Northern and southern Yemen, partners in a united state since 1990, have been fighting each other since May 4. Northern forces are besieging Aden, capital of a self-declared southern state trying to break away from the union. </P>
<P> The south's president, Ali Salem al-Baidh, has been using Mukalla as a base. The port is also a key entry point for arms and supplies needed by the south's outnumbered forces and a major southern air base is close to the city. </P>
<P> A southern official in Aden, contacted by Reuters from Dubai, said: ``This report is not true. They are still far from Mukalla.'' </P>
<P> It was not immediately possible to confirm either report independently. </P>
<P> Southern Yemen's biggest oil region, the Masila block, lies beyond Mukalla. Masila crude is exported from the Ash-Shihir tanker terminal, just 20 miles east of the port. </P>
<P> Masila, operated by Canadian Occidental Petroleum Ltd, produces just under 160,000 barrels per day (bpd) of oil -- just under half the combined output of northern and southern Yemen. </P>
<P> Northern Yemen's Marib field, which produced about 190,000 bpd, has been shut down since a southern air raid last Thursday and oil industry sources estimate it may take a week or more before the damage can be repaired. </P>
</TEXT>
</DOC>
