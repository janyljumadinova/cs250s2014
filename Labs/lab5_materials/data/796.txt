<DOC>
<DOCID>REU008-0011.940816</DOCID>
<TDTID>TDT001967</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/16/94 05:07</DATE>
<TITLE> HEATWAVE FORCES JAPAN TO IMPORT INDUSTRIAL WATER</TITLE>
<HEADLINE> HEATWAVE FORCES JAPAN TO IMPORT INDUSTRIAL WATER</HEADLINE>
<SUBJECT> BC-JAPAN-WATER 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: Updates with Tokyo supply cuts, more details)</NOTES>
<DATELINE>TOKYO (Reuter) </DATELINE>
<P>  Japan's blistering hot summer forced one Japanese firm to announce emergency imports of water Tuesday while water supply to the Tokyo region has been cut by nearly a third amid a worsening drought. </P>
<P> Japan Energy Corp is to import a total of 63,000 metric tons of water for industrial use from four Asian countries for drought-hit refineries in western and central Japan, a company spokesman said. </P>
<P> Water authorities are also cutting supplies to Tokyo and surrounding areas by 30 percent starting late Tuesday because water levels in the Tone River reservoir system supplying the region are continuing to fall. </P>
<P> The move, which will reduce water pressure, is aimed at saving enough water to avoid cutting off homes and businesses, according to the Construction Ministry. </P>
<P> It could mean poor water quality and difficulty drawing tap water at higher elevations. By Monday, reserves in eight reservoirs along the river had fallen to 100 million tons, or 29 percent of capacity. </P>
<P> Low rainfall in the month-long rainy season to mid-July has been followed by Japan's hottest summer on record, triggering severe water shortages in many parts of Japan, especially the central and western regions. </P>
<P> People in Aichi prefecture in central Japan have running water only five hours a day, a local government official said. </P>
<P> In neighboring Mie prefecture supplies were cut by 65 percent last Friday. They may be cut by 80 or 90 percent unless there is substantial rainfall in the area, another official said. </P>
<P> The drought is threatening to shut down many oil refineries, which depend on water for their cooling units and in-house power. </P>
<P> The Japan Energy spokesman said it has contracted with a South Korean shipping firm for three cargoes of 6,000 tons of water each from the southern South Korean port of Pusan. The first cargo will arrive at its 200,200 barrel-per-day Mizushima refinery in western Japan on August 18. </P>
<P> Water will also be imported from Hong Kong, China and Vietnam. Three shipments of 5,000 tons of water each will arrive from Hong Kong for Mizushima starting on August 22 and one shipment of 10,000 tons from Shanghai on August 25. </P>
<P> Japan Energy's 95,000 barrels-per-day Chita refinery in Aichi prefecture will receive 20,000 tons of industrial water from Vietnam, he said. It is believed to be the first time Japan has been forced to import water. </P>
<P> In Tokyo, the Tone reservoirs will dry up by the end of August if the weather continues as predicted by the Meteorological Agency. </P>
<P> ``The agency says it will be dry for the rest of August but that we should get regular rainfall at the beginning of September,'' said a Construction Ministry official. ``If that happens, we won't have to cut off water supplies.'' </P>
<P> Japan is a humid country, buffeted by typhoons and heavy rain for much of the year, and the drought has caught authorities unawares. But now they are trying to prepare for future emergencies. </P>
<P> The Health Ministry is considering giving more help to regional governments next year for them to build desalination plants to convert seawater to fresh water, which is cheaper than building new dams. </P>
<P> Japan's first large plant, capable of producing 40,000 tons or 40 million liters of water a day, is already being built in the southern islands of Okinawa. The average household uses 400 liters a day. </P>
</TEXT>
</DOC>
