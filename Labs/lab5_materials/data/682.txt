<DOC>
<DOCID>REU004-0418.940809</DOCID>
<TDTID>TDT001684</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/09/94 18:58</DATE>
<TITLE> COAST GUARD INTERCEPTS CUBA BOAT REPORTED HIJACKED</TITLE>
<HEADLINE> COAST GUARD INTERCEPTS CUBA BOAT REPORTED HIJACKED</HEADLINE>
<SUBJECT> BC-CUBA-BOAT 2NDLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES>(Eds: Updates that boat intercepted by Coast Guard)</NOTES>
<DATELINE>MIAMI (Reuter) </DATELINE>
<P>  A U.S. Coast Guard cutter Tuesday afternoon retrieved 27 people from a Cuban government boat that Havana said had been hijacked, Coast Guard officials said. </P>
<P> Cuba contacted the Coast Guard to report that a naval vessel had been hijacked and one of its crew members killed near the port of Mariel at about 5 p.m. EDT Monday. </P>
</TEXT>
</DOC>
