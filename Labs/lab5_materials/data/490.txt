<DOC>
<DOCID>REU014-0257.940730</DOCID>
<TDTID>TDT001232</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/30/94 00:36</DATE>
<TITLE> JAPAN RESUMES TRADE TALKS WITH U.S. AS SANCTIONS LOOM</TITLE>
<HEADLINE> JAPAN RESUMES TRADE TALKS WITH U.S. AS SANCTIONS LOOM</HEADLINE>
<SUBJECT> BC-USA-JAPAN-TRADE 1STLD </SUBJECT>
<AUTHOR>     By Rich Miller </AUTHOR>
<TEXT>
<NOTES>(Eds: Updates with Japanese proposals)</NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  Japan proposed a new plan in trade talks with the United States in a bid to achieve a breakthrough ahead of the threatened start of sanctions proceedings against Tokyo, Japanese officials said Friday. </P>
<P> But it was unclear whether the proposals would break the deadlock in talks that were due to resume here on Saturday. </P>
<P> The talks are focused on Japanese government procurement practices for telecommunications and medical equipment that the United States says discriminate against foreign imports. </P>
<P> The dollar, which bounded over 100 yen Thursday for the first time in a month on hopes that a trade accord might be struck, stalled on Friday as the markets awaited results of the talks. </P>
<P> The two nations remain at odds over how to measure improved market access as well as U.S. demands for changes to Japanese government procurement procedures. </P>
<P> Japanese officials said they had proposed changes in Japan's government procurement practices in response to the U.S. demand for such changes. But they said Tokyo remained opposed to U.S. requests for yardsticks to measure progress toward more open trade out of fear that such indicators might end up being mandatory targets. </P>
<P> U.S. officials said Washington stands ready to launch sanctions proceedings unless Tokyo makes major concessions in the negotiations -- something they do not expect to happen. </P>
<P> The two officials leading the talks are Japanese Deputy Foreign Minister Sadayuki Hayashi and his U.S. counterpart, Deputy Trade Representative Charlene Barshefsky. The two met informally Thursday night but full-scale negotiations did not begin until Friday. </P>
<P> Washington has set a July 31 deadline for deciding whether Tokyo's procedures for government purchases of telecommunications and medical equipment are discriminatory, the first step toward possible sanctions. </P>
<P> Sanctions would be implemented only if the two sides fail to reach agreement during a 60-day consultation period. </P>
<P> Washington said it had no plan to delay the July 31 deadline, although it has done so twice already. </P>
<P> Japanese officials, who had earlier warned that they might break off the talks if Washington set the clock ticking on sanctions, seemed to back away from that threat on Friday. </P>
<P> ``I'm hoping the talks will be concluded, but even if there were no accord (by July 31), we would continue to seek one,'' Japanese Finance Minister Masayoshi Takemura told reporters in Tokyo. </P>
<P> U.S. officials were also playing down the importance of the July 31 deadline, noting that the two sides would still have two months in which to sort out their differences even if sanctions proceedings are launched. </P>
<P> Government procurement, along with insurance and cars and car parts, is a priority sector in the so-called economic framework negotiations between the United States and Japan. </P>
</TEXT>
</DOC>
