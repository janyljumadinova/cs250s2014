<DOC>
<DOCID>REU004-0070.940808</DOCID>
<TDTID>TDT001612</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/08/94 05:45</DATE>
<TITLE> HIZBOLLAH ATTACKS ISRAEL'S OCCUPATION ZONE</TITLE>
<HEADLINE> HIZBOLLAH ATTACKS ISRAEL'S OCCUPATION ZONE</HEADLINE>
<SUBJECT> BC-MIDEAST-LEBANON </SUBJECT>
<AUTHOR>     By Haitham Haddadin </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BEIRUT, Lebanon (Reuter) </DATELINE>
<P>  Lebanon's pro-Iranian Hizbollah group, their leaders vowing to fight on against Israel, attacked the Jewish state's occupation zone in south Lebanon Monday with anti-tank rockets and machine guns, security sources said. </P>
<P> Lebanon, meanwhile, showed signs of irritation with Secretary of State Warren Christopher, with a government minister accusing him of trying to pressure it into concessions by not visiting it during his latest Middle East peace shuttle. </P>
<P> Christopher ``does not visit Lebanon during his tours of the region to put pressure on it to accept the Israeli conditions and to take part in the multilateral peace talks,'' Defense Minister Mohsen Dalloul said in remarks published by newspapers Monday. </P>
<P> Christopher has only paid two visits to Lebanon, which with Syria has been boycotting formal peace talks, during several peace missions to the Middle East. His predecessor James Baker visited Lebanon once since the launch of the process in 1991. </P>
<P> With Christopher in Damascus on Sunday trying to bring Syria and Israel closer together, Hizbollah chief Sheik Hassan Nasrallah vowed to press on with the fight against Israel. </P>
<P> ``We will continue the road of resistance no matter how high the price, as without it we will be threatened and our sanctity will be usurped,'' he said in remarks published by Beirut newspapers Monday. </P>
<P> ``It is not only our duty but our dignity and pride,'' added Nasrallah, who was speaking at the funeral of a guerrilla killed in confrontations with Israeli forces or allied militiamen in south Lebanon. </P>
<P> Israel says Syria, the main powerbroker in Lebanon, could curb attacks by Hizbollah. Washington argues that Syria does not control Hizbollah but can be influential with it. </P>
<P> Christopher, in talks with Syrian President Hafez al-Assad in Damascus on Sunday, raised the issue in detail. </P>
<P> A U.S. official said after the discussions that Assad ``listened carefully (but) I wouldn't say that there were any specific set of responses.'' </P>
<P> Security sources in the south said guerrillas attacked a post of Israel's South Lebanon Army (SLA) militia allies in Rashaf just inside the occupation zone Monday. </P>
<P> There was no report of casualties in the attack which Hizbollah said it carried out. </P>
<P> Hizbollah guerrillas fired Katyusha rockets across the Israel-Lebanon border wounding three civilians last week after Israel killed at least six civilians in an air raid on the south Lebanon village of Deir al-Zahrani. Guerrillas killed two Israeli soldiers in the occupation zone on Saturday. </P>
<P> ``If they hurt us in Deir al-Zahrani they feel the pain in settlements in northern Palestine and in the fields of confrontation,'' Nasrallah said. </P>
<P> Reiterating the Beirut government's stand on Hizbollah, Lebanese Labor Minister Abdullah al-Amin was quoted by papers as saying: ``We, the army and the resistance are all in one trench to defend our land and people and to liberate the land.'' </P>
<P> Beirut argues that resisting Israeli forces is a legitimate right of all Lebanese as long as their land is occupied. </P>
<P> ``Neither us nor sisterly Syria will sign any peace treaty...until the completion of the Israeli withdrawal from our land,'' Amin added. </P>
</TEXT>
</DOC>
