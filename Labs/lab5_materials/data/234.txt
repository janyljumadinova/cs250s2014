<DOC>
<DOCID>REU008-0012.940715</DOCID>
<TDTID>TDT000579</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 03:29</DATE>
<TITLE> EU EXPECTED TO PICK SANTER FOR COMMISSION JOB</TITLE>
<HEADLINE> EU EXPECTED TO PICK SANTER FOR COMMISSION JOB</HEADLINE>
<SUBJECT> BC-EUROPE </SUBJECT>
<AUTHOR>     By Jeremy Gaunt </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BRUSSELS (Reuter) </DATELINE>
<P>  The European Union is expected Friday to pick Luxembourg Prime Minister Jacques Santer to head the European Commission, its main engine of regulation and enforcement. </P>
<P> Barring last-minute surprises, EU leaders gathering here for an emergency summit were considered likely to appoint Santer, 57, to replace Jacques Delors, the charismatic French Socialist who is to leave the EU executive board in January after a decade as president. </P>
<P> ``If ... there was a consensus on my name, then, in the higher interests of my own country, I could not refuse this candidature,'' Santer said in a radio interview Thursday. </P>
<P> The Luxembourger is a compromise candidate, emerging after a search that began in June when Britain vetoed the appointment of Belgian Prime Minister Jean-Luc Dehaene, seen as too much of an enthusiast for European integration. </P>
<P> Other candidates remain in the fray. Dehaene himself, supported last month by 11 of the 12 EU states, has never withdrawn and may yet be proposed again by Belgium. </P>
<P> Denmark has also said it would propose Poul Schlueter, its former prime minister. </P>
<P> But Santer's appointment was considered by EU officials to be almost certain, despite the widepread criticism that he would be a weak president, subject to behind-the-scenes manipulation, and representing the lowest common denominator. </P>
<P> The absence of any open objection to Santer by EU states was seen as proof that they could reach a consensus around him. </P>
<P> Britain, for example, was said to have far fewer problems with Santer than Dehaene, despite their shared belief in federalism, given the Luxembourger's stated commitment to decentralization and free trade. </P>
<P> And Santer meets many of the requirements for the job that EU leaders have informally set. </P>
<P> He is a Christian Democrat from a small state, balancing the tenure of French Socialist Delors, and, as a prime minister, comes from among the leaders themselves. </P>
<P> Santer has many years experience in EU politics, having chaired negotiations leading to both the Maastricht treaty on European union and to the Single European Act, creating Europe's single market. </P>
<P> He is also a fluent French speaker, meeting an absolute criterion set down as recently as Thursday by French President Francois Mitterrand. </P>
<P> France fears for the future of its language in the Union against encroaching English, which Santer also speaks, along with German. </P>
<P> Nonetheless, Santer has hardly been embraced as the man Europe needs. </P>
<P> ``He is not the candidate of our dreams,'' one Belgian Christian Democrat member of the European Parliament told Belgian television. </P>
<P> EU leaders, meanwhile, planned to take advantage of the special summit to tackle other matters. </P>
<P> Also on the agenda was a discussion of the latest Bosnia peace plan and a briefing on last weekend's Group of Seven industrial nations summit from its host, Italian Prime Minister Silvio Berlusconi. </P>
</TEXT>
</DOC>
