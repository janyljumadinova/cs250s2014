<DOC>
<DOCID>REU004-0025.940807</DOCID>
<TDTID>TDT001593</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/07/94 20:54</DATE>
<TITLE> MUSLIM REBELS FREE CATHOLIC PRIEST IN PHILIPPINES</TITLE>
<HEADLINE> MUSLIM REBELS FREE CATHOLIC PRIEST IN PHILIPPINES</HEADLINE>
<SUBJECT> BC-PHILIPPINES-PRIEST </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MANILA, Philippines (Reuter) </DATELINE>
<P>  Muslim extremists have freed a Roman Catholic priest held hostage for two months in the southern Philippines, a senior government official said Monday. </P>
<P> The priest, Cirilo Nacorda, was freed in Basilan Sunday night, Interior and Local Governments Secretary Rafael Alunan told reporters. </P>
<P> The priest, captured by fundamentalist Abu Sayyaf rebels June 8, will be presented to President Fidel Ramos when he arrives in Zamboanga city after a state visit to Brunei. </P>
<P> Vice-President Joseph Estrada said in a radio interview from Zamboanga that Nacorda was in the hands of the military after being released in Basilan island off the south-western corner of Mindanao. </P>
<P> The rebels had demanded a $115,000 ransom for Nacorda's freedom, but Estrada said no money was paid. Nacorda, 36, was among 74 people abducted on a highway in Basilan exactly two months ago. </P>
<P> Fifteen of his companions were killed shortly after they were seized but 57 were later freed. One man escaped. </P>
</TEXT>
</DOC>
