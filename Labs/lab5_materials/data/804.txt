<DOC>
<DOCID>REU008-0076.940816</DOCID>
<TDTID>TDT001984</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/16/94 12:44</DATE>
<TITLE> CANADIAN SPY AGENCY PROBED FOR NEO-NAZI INFORMANT</TITLE>
<HEADLINE> CANADIAN SPY AGENCY PROBED FOR NEO-NAZI INFORMANT</HEADLINE>
<SUBJECT> BC-CANADA-NEONAZI </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>OTTAWA (Reuter) </DATELINE>
<P>  Canada's domestic spy agency will be investigated for links with a white-supremacist group following reports that a paid informant helped set up and run the neo-Nazi organization, officials said Tuesday. </P>
<P> The Toronto Sun newspaper reported Sunday that Grant Bristow, a founder and leading member of the Heritage Front, the country's largest neo-Nazi group, was on the payroll of the Canadian Security Intelligence Service. </P>
<P> Solicitor General Herb Gray told reporters Tuesday the government had asked an independent external review body to investigate the agency's connection with the neo-Nazi group. </P>
<P> ``In order to clear the air we want the Security Intelligence Review Committee to look into the matter and make a report,'' Gray said. </P>
<P> A spokesman for CSIS would not say whether Bristow worked for the spy agency but he said CSIS had not broken any laws. He said the agency never commented on its operations. </P>
<P> The Toronto Sun said Bristow was recruited by CSIS in 1989, six months before the Heritage Front was set up. Members of the group told the newspaper Bristow had provided money for travel and other expenses. </P>
<P> Jewish organizations welcomed the investigation and said they were upset that taxpayers dollars may have been used to create an anti-Semitic group. </P>
</TEXT>
</DOC>
