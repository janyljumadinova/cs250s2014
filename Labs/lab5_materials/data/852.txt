<DOC>
<DOCID>REU008-0429.940818</DOCID>
<TDTID>TDT002096</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/18/94 06:04</DATE>
<TITLE> HEAD OF ARMENIAN CHURCH, VAZGEN I, DIES AT 85</TITLE>
<HEADLINE> HEAD OF ARMENIAN CHURCH, VAZGEN I, DIES AT 85</HEADLINE>
<SUBJECT> BC-CIS-ARMENIA-CHURCH </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MOSCOW (Reuter) </DATELINE>
<P>  Vazgen I, head of the Armenian church for nearly 40 years, died Thursday at the age of 85, Russian news agencies said. </P>
<P> He studied in Bucharest and held church posts in Athens before his appointment as catholicos (the Patriarch of Armenia) in 1955. </P>
<P> Itar-Tass news agency said he had worked until the end for a halt to the Armenian-Azeri bloodshed over the disputed territory of Nagorno-Karabakh. </P>
<P> He was the first person to receive the title of National Hero of Armenia after the Caucasus nation won its independence from Moscow in 1991. </P>
<P> The Armenian church, distinctive for the conical roofs of its churches, says it is the oldest in the Christian world. </P>
</TEXT>
</DOC>
