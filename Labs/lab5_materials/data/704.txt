<DOC>
<DOCID>REU005-0282.940810</DOCID>
<TDTID>TDT001728</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/10/94 23:22</DATE>
<TITLE> RESIDENTS EVACUATED IN PATH OF FIRE IN CALIFORNIA</TITLE>
<HEADLINE> RESIDENTS EVACUATED IN PATH OF FIRE IN CALIFORNIA</HEADLINE>
<SUBJECT> BC-USA-WILDFIRES-EVACUATION </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SAN FRANCISCO, (Reuter) </DATELINE>
<P>  About 1,200 people were evacuated from the path of a wildfire Wednesday that swept within one mile of homes near the picturesque town of Columbia in northern California, officials said. </P>
<P> All residents from the Cedar Ridge area were evacuated by the Tuolumne County sheriff's department, said Paula Schnarr of the Fire Information Center in northern California. </P>
<P> Flames scorched 800 acres before the fire could be 30 percent contained and the cause of the blaze was under investigation, she said. </P>
<P> About 550 summer camp children evacuated early Wednesday from two nearby camps were also being sent home, said Schnarr. </P>
<P> The fires in northern California were among 33 major fires ablaze over more than 321,000 acres in nine western states. </P>
<P> Hot, dry conditions were making it difficult for firefighters to get a handle on the numerous forest and range fires, officials said. </P>
</TEXT>
</DOC>
