<DOC>
<DOCID>REU009-0006.940818</DOCID>
<TDTID>TDT002101</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/18/94 09:59</DATE>
<TITLE> NEW POLISH INTELLIGENCE CHIEF QUITS AFTER OUTCRY</TITLE>
<HEADLINE> NEW POLISH INTELLIGENCE CHIEF QUITS AFTER OUTCRY</HEADLINE>
<SUBJECT> BC-POLAND-SPY </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WARSAW, Poland (Reuter) </DATELINE>
<P>  The newly appointed head of Polish intelligence, who was once jailed for spying in the United States, resigned Thursday after just three days in office. </P>
<P> Marian Zacharski, who was sentenced to life in prison by a Los Angeles court in 1981 and later released in a spy swap, said in a statement he was quitting because he did not want to aggravate a public row which followed his nomination Monday.  ``I don't want to be a source of conflicts among Poles at a time when Poland most needs cooperation and agreement,'' Zacharski said. </P>
<P> President Lech Walesa asked the government Wednesday to revoke the appointment of Zacharski, who had been a spy for former Communist Poland, saying it would harm the country's relations with the West. </P>
<P> Zacharski is believed to have intercepted secret plans of Hawk and Phoenix missiles and those of the anti-missile system Patriot during his espionage operations in the United States between 1975 and 1981. </P>
</TEXT>
</DOC>
