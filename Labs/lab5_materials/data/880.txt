<DOC>
<DOCID>REU009-0194.940819</DOCID>
<TDTID>TDT002170</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/19/94 05:08</DATE>
<TITLE> SEVEN RUSSIAN GUARDS KILLED IN TAJIK REBEL ATTACK</TITLE>
<HEADLINE> SEVEN RUSSIAN GUARDS KILLED IN TAJIK REBEL ATTACK</HEADLINE>
<SUBJECT> BC-CIS-TAJIKISTAN-BORDER 1STLD </SUBJECT>
<AUTHOR>     By Yuri Kushko </AUTHOR>
<TEXT>
<NOTES> (Eds: raises casualty figures, adds details)</NOTES>
<DATELINE>MOSCOW (Reuter) </DATELINE>
<P>  Seven Russian border guards were killed and 13 wounded in Tajikistan when rebels launched an overnight raid on the frontier with Afghanistan, a border guards spokesman said Friday. </P>
<P> It was the most serious blow in more than a year for the Russian force policing Tajik borders under an agreement with the ex-communist government in Dushanbe. </P>
<P> The Kremlin protested formally to Afghanistan. </P>
<P> The spokesman, contacted in the Tajik capital by telephone, said the fighting started Thursday night and subsided by Friday morning. </P>
<P> Itar-Tass news agency quoted the Russian border guards commander in Dushanbe as saying the attack was preceded by two days of shelling both from Tajik territory and from Afghanistan, where the rebels found refuge after losing a civil war in 1992. </P>
<P> Russia agreed to keep its troops patroling the border last year, declaring that it considered the Tajik frontier with Afghanistan as effectively Russia's own. </P>
<P> Colonel Vladimir Novikov said around 500 rebels were involved in the overnight attack. He said the rebels were stepping up their attacks in an attempt to seize a strip of Tajik territory to announce the creation of a rebel government. </P>
<P> ``This is not just Tajikistan's business,'' he said. ``This is a challenge to Russia as well.'' </P>
<P> In July 1993, more than 20 Russian border guards were killed in a rebel attack on the same border posts, prompting Russian promises to destroy any rebel installation on Afghan territory which they deemed threatening to the guards. </P>
<P> The 25,000-man Russian army has been active in the Central Asian state since the civil war that killed thousands, drove hundreds of thousands from their homes and ruined the economy. </P>
</TEXT>
</DOC>
