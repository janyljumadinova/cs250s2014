<DOC>
<DOCID>REU003-0202.940805</DOCID>
<TDTID>TDT001538</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/05/94 20:16</DATE>
<TITLE> GOLF-AZINGER EMOTIONAL ON RETURN FOLLOWING CANCER TREATMENT</TITLE>
<HEADLINE> GOLF-AZINGER EMOTIONAL ON RETURN FOLLOWING CANCER TREATMENT</HEADLINE>
<SUBJECT> BC-GOLF-AZINGER </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>GRAND BLANC, Mich.(Reuter) </DATELINE>
<P>  An overwhelmed Paul Azinger was choked with emotion on the first tee Friday as he made his long-awaited comeback to professional golf after beating cancer. </P>
<P> Azinger, playing his first event since he was diagnosed with lymphoma in his left shoulder eight months ago, received a huge ovation from the overflowing gallery when he arrived at the first tee for the Buick Open at Warwick Hills. </P>
<P> ``It was pretty emotional on the first tee, I have to tell you,'' Azinger said after shooting four-over-par 76. </P>
<P> ``I didn't think about getting choked up, but I did. I was nervous when I got there and the ovation continued, continued and continued. </P>
<P> ``I had to take a lot of deep breaths, suck some air and gather myself.'' </P>
<P> Azinger hit a nice opening drive, but not many other good shots as he struggled to judge his approach shots accurately. </P>
<P> ``I didn't play the way I wanted and at times I was aggravated about it, but it beats the alternative,'' said Azinger, who completed radiation and chemotherapy treatments for the lymphoma two months ago. </P>
<P> ``Victory for me is just being out there. It's over,'' he said of the cancer. ``I've beaten it and it's time to move on. This is my element, where I love to be.'' </P>
<P> Azinger admitted he was drained by the time he finished 18 holes. </P>
<P> ``I feel physically tired and emotionally tapped out. Tomorrow will probably be easier.'' </P>
<P> Azinger next week defends his title at the PGA Championship, which he won last year shortly before discovering the lymphoma of the right shoulder, a type of cancer from which he says there is a 90 percent recovery rate. </P>
<P> ``I never lived with the fear of death once I found out the cancer hadn't spread,'' he said. </P>
</TEXT>
</DOC>
