<DOC>
<DOCID>REU002-0030.940703</DOCID>
<TDTID>TDT000093</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/03/94 10:46</DATE>
<TITLE> RULING EXPECTED IN CITADEL DISCRIMINATION CASE</TITLE>
<HEADLINE> RULING EXPECTED IN CITADEL DISCRIMINATION CASE</HEADLINE>
<SUBJECT> BC-USA-CITADEL </SUBJECT>
<AUTHOR>     By Lorrie Grant </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CHARLOTTE, NC (Reuter) </DATELINE>
<P>  After a year of arguments and evidence, the bitter sex discrimination case against The Citadel may finally be resolved this month. </P>
<P> U.S. District Judge C. Weston Houck is reviewing both sides' proposals for how the all-male state-funded military college might admit women. </P>
<P> He has promised to issue a decision this month on whether and how women might be admitted to the 152-year-old Charleston, South Carolina, military school. But however the judge rules, attorneys in the case say that an appeal is virtually certain. </P>
<P> The issue arose in March 1993, when Shannon Faulkner, then 18, filed suit against the school for rescinding her offer of admission when it discovered that she was a woman. </P>
<P> Faulkner, who dreamed of joining The Citadel's prestigious cadet corps, had her gender deleted from her high school transcripts in hope of winning admission to the school. </P>
<P> The suit was appealed all the way to the Supreme Court, with the Citadel arguing that its cadet corps should remain all male, and Faulkner's legal team maintaining that a state-funded institution should not exclude women. </P>
<P> In January, U.S. Supreme Court Chief Justice William Rehnquist ruled Faulkner could attend The Citadel as a civilian until a lower court decided on her full admission based on plans set forth by both parties. </P>
<P> Faulkner's attorneys urged that she be admitted to the Corps of Cadets this fall. </P>
<P> But The Citadel, which has since admitted Faulkner as a civilian day student, has argued that Faulkner be admitted to an all-female program of some sort at another school. </P>
<P> ``Our remedial plan was to develop a parallel program of some sort,'' said Dawes Cooke, a Citadel attorney. The Citadel suggested contracting with a college outside the state or enhancing the level of state grants to women attending private women's colleges to meet the sex equity requirements. </P>
<P> ``The judge ... may take bits and pieces out of each of them,'' said Suzanne Coe, a member of the Faulkner legal team. </P>
<P> Another Citadel suggestion was that it develop a program like one devised by Virginia Military Institute -- the nation's only other all-male, state-funded military school. </P>
<P> After legal challenges, Lexington, Virginia-based VMI created a military program for women at Mary Baldwin College, a private women's school 30 miles north in Staunton. </P>
<P> If Judge Houck rules Faulkner must be admitted as a cadet, the Citadel has suggested she must not only wear the uniform, but also trim her hair into a traditional ``knob,'' or crewcut. </P>
<P> Faulkner's attorney called that requirement punitive. </P>
<P> ``The people that want to have her shave her head are doing that out of punitive reasons. They want to tar and feather her, and then laugh,'' Coe said. </P>
<P> The contingency plan submitted by the Citadel also banned makeup and jewelry, and stipulated that women cadets adhere to U.S. Army physical fitness standards that include timed runs, situps and pushups. </P>
<P> Faulkner's attorneys also said they were concerned that the Citadel prevent harassment if Faulkner wins her case. </P>
<P> ``We requested the court to require The Citadel to do active recruiting of other women,'' Coe said. </P>
<P> Rick Mill, a spokesman for The Citadel, said that only one other woman had applied for admission, but was rejected. ``Only one woman applied and she did not meet requirements.'' </P>
<P> Both attorneys have said appeals are likely. </P>
</TEXT>
</DOC>
