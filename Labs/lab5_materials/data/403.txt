<DOC>
<DOCID>REU012-0338.940726</DOCID>
<TDTID>TDT001047</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/26/94 07:24</DATE>
<TITLE> BOMB EXPLODES NEAR ISRAELI CONSULATE IN LONDON</TITLE>
<HEADLINE> BOMB EXPLODES NEAR ISRAELI CONSULATE IN LONDON</HEADLINE>
<SUBJECT> BC-BRITAIN-BOMB URGENT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  A bomb exploded near the Israeli consulate in west London Tuesday, witnesses said. </P>
<P> ``It's just gone off. It's blown out windows,'' said a shop assistant in Kensington High Street, near the consulate. </P>
</TEXT>
</DOC>
