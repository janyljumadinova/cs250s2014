<DOC>
<DOCID>REU004-0240.940708</DOCID>
<TDTID>TDT000266</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 04:06</DATE>
<TITLE> SETTLERS ATTACK ARAB HOUSES AFTER KILLING</TITLE>
<HEADLINE> SETTLERS ATTACK ARAB HOUSES AFTER KILLING</HEADLINE>
<SUBJECT> BC-MIDEAST-ISRAEL-UNREST </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>HEBRON, West Bank (Reuter) </DATELINE>
<P>  Jewish settlers, angry over the killings of two Israelis in the occupied West Bank, attacked Palestinian houses and burned an Arab's car in Hebron overnight, Palestinian sources said Friday. </P>
<P> The settlers blamed the attacks on Prime Minister Yitzhak Rabin's peace moves with the Palestine Liberation Organization. </P>
<P> The Israeli army imposed a tight curfew on Hebron's 100,000 Arab residents after gunmen sprayed bullets into a settler's car in Hebron killing Sarit Prigal, a 17-year-old Jewish girl from Kiryat Arba settlement and wounding her brother and father. </P>
<P> A few hours earlier, the body of a 20-year-old Israeli soldier was found in an abandoned house near Jerusalem. The body bore stab wounds and his weapon was missing. </P>
<P> The soldier, identified as Arye Frankental, had been missing since he left an army training center Wednesday. He was apparently kidnapped and killed by Palestinians, Israeli security sources said. </P>
<P> Palestinians said Kiryat Arba settlers spilled into Hebron -- a flashpoint of Arab-Jewish tension -- blocking the Jerusalem-Hebron highway with burning tires and throwing rocks at Arab houses. </P>
<P> The settlers smashed windows in several houses and torched an Arab-owned car, they said. No one was injured in the attacks. </P>
<P> The latest killings darkened the air of optimism and relative calm that surrounded Yasser Arafat's triumphant return to the Gaza Strip and the West Bank and talks with Rabin in Paris this week. </P>
<P> Israeli media said Rabin learned of the killings after his return Thursday from Paris where he met Arafat and shared a U.N. peace prize with the PLO leader, who formally established Palestinian self-rule during his landmark visit. </P>
<P> In February, a settler shot dead some 30 Arab worshippers in a Hebron mosque before he was beaten to death by survivors. </P>
<P> The massacre brought a monthlong halt to Middle East peace talks and led to the deployment of an international observer force in the city. </P>
</TEXT>
</DOC>
