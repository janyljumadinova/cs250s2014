<DOC>
<DOCID>REU013-0063.940727</DOCID>
<TDTID>TDT001082</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/27/94 04:07</DATE>
<TITLE> N.KOREA HAS FIVE NUCLEAR WARHEADS - DEFECTOR</TITLE>
<HEADLINE> N.KOREA HAS FIVE NUCLEAR WARHEADS - DEFECTOR</HEADLINE>
<SUBJECT> BC-KOREA-DEFECTOR URGENT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SEOUL (Reuter) </DATELINE>
<P>  North Korea has developed five nuclear warheads and is now concentrating on developing missiles to carry them, a North Korean defector who is a son-in-law of Pyongyang's Prime Minister Kang Song-san said Wednesday. </P>
<P> The defector, Kang Myong-do, 36, told a news conference in Seoul that North Korea was not simply using suspicions about its nuclear program as a negotiating tactic to obtain concessions from the West. </P>
</TEXT>
</DOC>
