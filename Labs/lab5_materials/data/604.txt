<DOC>
<DOCID>REU002-0375.940804</DOCID>
<TDTID>TDT001478</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/04/94 13:32</DATE>
<TITLE> TEN INFANT DEATHS LINKED TO BATTERY-OPERATED CRADLE</TITLE>
<HEADLINE> TEN INFANT DEATHS LINKED TO BATTERY-OPERATED CRADLE</HEADLINE>
<SUBJECT> BC-HEALTH-CRADLE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BOSTON (Reuter) </DATELINE>
<P>  At least 10 infants have died in a battery-powered rocking cradle that was recalled from the market more than two years ago, but the deaths were not fully investigated, the Boston Globe reported Thursday. </P>
<P> The newspaper said pathologists' reports listed the cause of 10 deaths as sudden infant death syndrome (SIDs), but parents of several babies had blamed the deaths on the Graco Converta-Cradles. </P>
<P> Graco, a Pennsylvania-based company, voluntarily recalled the cradles in February 1992 after two infants died in them and two others experienced breathing difficulties. </P>
<P> The Globe quoted a lawyer for Graco as saying that all the deaths linked to the cradle were due to SIDs and therefore the company was not liable. </P>
<P> But, according to the Globe report, several parents believe that the cradle's head-to-toe rocking motion caused their children to slide into positions that cut off their air supply.  In addition, some of the parents told the Globe that the deaths of their children had not been fully investigated before being attributed to SIDs. </P>
<P> The newspaper quoted two influential SIDs researchers, Dr James Kemp and Dr Bradley Thach of Washington University School of Medicine in St. Louis, as saying that SIDS is too often used as a diagnosis in the absence of a thorough examination into an infant's death. </P>
<P> ``Infants are at risk for both SIDs and accidental suffocation,'' the doctors wrote in the New England Journal of Medicine in 1991. ``On postmortem examination, however, it is difficult to distinguish one from the other without information from the scene of death.'' </P>
</TEXT>
</DOC>
