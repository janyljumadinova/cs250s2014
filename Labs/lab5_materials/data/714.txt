<DOC>
<DOCID>REU005-0423.940811</DOCID>
<TDTID>TDT001750</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/11/94 15:22</DATE>
<TITLE> RWANDAN ENEMIES REPORTED TO BE MEETING IN ZAIRE</TITLE>
<HEADLINE> RWANDAN ENEMIES REPORTED TO BE MEETING IN ZAIRE</HEADLINE>
<SUBJECT> BC-RWANDA-TALKS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>GOMA, Zaire (Reuter) </DATELINE>
<P>  The Tutsi-dominated rebels who took power last month in Kigali, and the defeated Rwandan army are apparently holding talks in order to rebuild their war-ravaged country, a senior French army officer said Thursday. </P>
<P> ``Talks between the RPF (Rwanda Patriotic Front) and the FAR (former government army) are apparently taking place in Zaire,'' Colonel Alain Rambeau told reporters. </P>
<P> The French army spokesman refused to elaborate, saying: ``I can't tell you more at this stage.'' </P>
<P> The issue of how to lure about two million refugees back has been a priority of the new Rwandan government. Prime Minister Faustin Twagiramungu said this week Rwanda would have no real economy, political life or stability without them. </P>
<P> The ousted Hutu government and its Hutu army, with an estimated 20,000 soldiers in camps around the Zairean border towns of Goma and Bukavu, want the refugees to stay in exile even at the cost of dying in disease-ridden camps. </P>
<P> They are telling the predominantly Hutu refugees that the RPF is slaughtering anyone who returns. </P>
<P> In the Rwandan capital Kigali the U.N. High Commissioner for Refugees (UNHCR) said Thursday five clergymen from the Goma camps were visiting the city. </P>
<P> ``They've been here for three days, talking to people and officials in border areas and Kigali,'' said UNHCR spokesman Chris Janowsky, adding they would soon return to Goma. </P>
<P> The new Rwandan government installed by the RPF also wants to avoid a new refugee exodus to Zaire after French troops pull out from the southwest in 10 days at the end of their two-month U.N. mandate. </P>
</TEXT>
</DOC>
