<DOC>
<DOCID>REU002-0112.940803</DOCID>
<TDTID>TDT001405</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 12:54</DATE>
<TITLE> HOWARD STERN PLANS ANNOUNCEMENT ON RUN FOR GOVERNOR</TITLE>
<HEADLINE> HOWARD STERN PLANS ANNOUNCEMENT ON RUN FOR GOVERNOR</HEADLINE>
<SUBJECT> BC-USA-STERN </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NEW YORK (Reuter) </DATELINE>
<P>  Talk show host Howard Stern, who promised to make one of the more interesting candidates of the year in his quest to become New York's governor, said Wednesday he would soon make a major announcement about the race. </P>
<P> A spokesman for Stern would not disclose the content of the announcement, planned for Thursday morning. </P>
<P> Stern told listeners of his highly rated and frequently fined talk show that he would talk about his lost court battle over financial disclosure. </P>
<P> He has said that he did not want to tell people how much he made from his syndicated morning radio show and his best-selling book ``Private Parts.'' </P>
<P> Stern is known for his harsh language and lewd behavior that have earned his employer numerous fines from the Federal Communications Commission but made him a cult hero. </P>
<P> He argued in a court filing that the law violated his constitutional rights to privacy, freedom of speech and freedom of association. </P>
<P> But the court in Albany refused Tuesday to grant Stern a temporary injunction that would allow him to not file the information while he continued to argue the issue. </P>
<P> In the ruling, the state judge noted that 40 states and the federal government have disclosure rules for candidates. It said that Stern appeared unlikely to succeed in overturning New York's law. </P>
<P> Stern is running as the candidate of the state's Libertarian Party, although one party official said Wednesday that ``he doesn't talk to us very much.'' </P>
<P> When Stern announced his candidacy in March, he said he only wanted to do three things: restore the death penalty in New York, add more toll takers on the highways and have road construction crews do all their work at night. </P>
<P> While few gave Stern a chance of victory, some political analysts said his presence on the ballot could draw some votes among his core following of conservative, young males. </P>
<P> They said that could help Democrat incumbent Mario Cuomo, who is facing a tough battle in his bid for a fourth term. </P>
</TEXT>
</DOC>
