<DOC>
<DOCID>REU008-0183.940715</DOCID>
<TDTID>TDT000620</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 17:19</DATE>
<TITLE> GREENSPAN SAYS DEFICIT COULD HURT ``IDEAL'' TIMES</TITLE>
<HEADLINE> GREENSPAN SAYS DEFICIT COULD HURT ``IDEAL'' TIMES</HEADLINE>
<SUBJECT> BC-USA-ECONOMY-GREENSPAN 1STLD </SUBJECT>
<AUTHOR>     By Alver Carlson </AUTHOR>
<TEXT>
<NOTES> (Eds: updates throughout)</NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  Federal Reserve Chairman Alan Greenspan Friday said the United States is experiencing ''ideal'' economic times but warned that future prosperity was jeopardizing the country's still huge budget deficit. </P>
<P> ``The U.S. economy has recently been experiencing the ideal combination of rising activity, falling unemployment and slowing inflation,'' Greenspan told a bipartisan commission on government spending and tax reform. </P>
<P> But he made clear that the country must not let good economic times create an atmosphere in which policy makers take the view that the deficit, despite some recent improvement, is no longer a critical problem. </P>
<P> ``We cannot let this good behavior lull us into neglecting the underlying problems of our economy,'' Greenspan told lawmakers. </P>
<P> The central banker said the impact of the deficit is pervasive and continues to keep long-term rates higher than they would be, perversely making it more expensive to finance the red ink itself. </P>
<P> ``Long-term interest rates are higher now because markets are anticipating rising deficits in the next century,'' Greenspan said. </P>
<P> At the same, he said he detected a dramatic shift in the public's attitude toward the deficit and a willingness to make the hard choices necessary in bringing the economy more into balance. </P>
<P> ``I hope and really do believe (that progress will be made),'' Greenspan told the commission, adding that improvement will show up in the levels of interest rates. </P>
<P> But Greenspan made it clear than the United States really has no choice but to deal with the deficits since eventually they will produce economic instability and very hard times. </P>
<P> ``There is no alternative to scaling back growth in federal spending if we are to avoid growing deficits as we move into the next century,'' he said. </P>
<P> Greenspan dodged questions about tax policy and specific revenue cuts, saying as he has in the past that such choices are political in nature and he would rather not add his voice to the debate. </P>
<P> At the same time, he said the deficit in the years ahead is mostly the creature of health care costs, expected to balloon as American society increasingly ages. </P>
<P> ``Entitlements are programmed to grow at a rate that will surely exceed growth in the tax base, threatening a destabilizing escalation of deficits,'' he said. </P>
</TEXT>
</DOC>
