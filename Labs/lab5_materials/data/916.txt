<DOC>
<DOCID>REU010-0049.940820</DOCID>
<TDTID>TDT002249</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/20/94 15:35</DATE>
<TITLE> CLINTON TAKES STEPS TO PRESSURE CASTRO GOVERNMENT</TITLE>
<HEADLINE> CLINTON TAKES STEPS TO PRESSURE CASTRO GOVERNMENT</HEADLINE>
<SUBJECT> BC-CUBA-USA 1STLD </SUBJECT>
<AUTHOR>     By Steve Holland </AUTHOR>
<TEXT>
<NOTES> (Eds: updates throughout, adds byline)</NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  President Clinton Saturday cut off the cash flow to Cuba and trimmed charter flights as part of get-tough measures to punish President Fidel Castro for provoking an exodus of Cuban boat people to America. </P>
<P> Clinton stepped up the pressure on Castro a day after reversing a 28-year policy of allowing automatic political asylum to Cubans who make it to the American shore in a bid to staunch a repeat of the 1980 Mariel boatlift when 125,000 Cubans flooded the coast. </P>
<P> ``Over the past two weeks, the government of Cuba has taken actions to provoke a mass exodus to the United States ... The solution to Cuba's many problems is not an uncontrolled exodus, it is freedom and democracy for Cuba,'' Clinton said Saturday. </P>
<P> The president ordered a stop to cash remittances from the United States to Cuba, effectively stopping Cuban-Americans from sending cash to families in Cuba. Up to now $300 could be sent every three months. </P>
<P> U.S. officials said the order could deprive the Cuban economy of as much as $500 million a year. </P>
<P> Family gift packages will be limited to medicine, food and strictly humanitarian items. Transfer of funds for humanitarian purposes will require specific authorization of the Treasury Department. </P>
<P> U.S. officials said one way the Cuban government profits from the cash remittances is by getting the hard currency when U.S. dollars are converted into the Cuban currency. </P>
<P> The administration had debated cutting the cash payments in half but was persuaded to cut them out altogether because it would have a greater impact on Castro. </P>
<P> ``We now know that an awful lot of the cash payments were actually going to prop up Castro,'' said Florida Governor Lawton Chiles, who welcomed the measures. </P>
<P> Secondly, Clinton said the only charter flights permitted between Miami and Havana will be those clearly designed to accommodate legal immigrants. Commercial travel is already banned under the U.S. trade embargo against Cuba. </P>
<P> Up to now Cuban-Americans have been able to charter flights to find relatives in Cuba or check on property. A White House official said journalists and academics would still be able to charter flights there under the new rule. </P>
<P> Third, the United States ``will use all appropriate means'' to increase and amplify its international broadcasts to Cuba. </P>
<P> U.S.-operated Radio Marti and TV Marti are to broadcast messages urging Cubans not to take to the sea and tell them they would not be allowed entry into the United States if they make it to the U.S. shore, U.S. officials said. </P>
<P> To overcome Cuban jamming of the broadcasts they are to be beamed directly from circling C-130 aircraft, officials said. </P>
<P> Fourth, the United States will seek U.N. resolutions and support through other international organizations to condemn alleged human rights violations by the Castro government. </P>
<P> In his statement, Clinton said his intention was to ``limit the ability of the Cuban government to accumulate foreign exchange and to enable us to expand the flow of information to the Cuban people.'' </P>
<P> Clinton was under pressure from some members of Congress to take tougher measures against the Castro government. Some Cuban-Americans in Congress complained it was wrong to punish Cubans for seeking refuge from the Castro government. </P>
<P> ``Certainly we want to make it clear that the objective of our policy is not to punish those who are fleeing repression,'' said White House spokeswoman Dee Dee Myers. </P>
<P> The Cuban boat people plucked from the sea are being taken to the U.S. naval base at Guantanamo Bay on the southeast tip of Cuba. </P>
<P> U.S. officials say they have had positive indications from some countries in the region that they will provide safe havens to Cubans and that they want to move them out of Guantanamo as quickly as possible. </P>
<P> Those countries who have been contacted or will be contacted include Grenada, Antigua, Surinam and the Turks and Caicos and Panama, officials said. </P>
<P> In Havana, a Foreign Ministry statement Saturday accused Washington of planning to turn the Guantanamo Bay base, on Cuba's southeast tip, into a ``concentration camp.'' </P>
<P> Clinton accuses Castro of encouraging Cubans to make the risky passage to Florida. But the Cuban statement said people had been spurred to leave the island by ``sordid and hostile'' U.S. policy toward Cuba over the last 35 years, and in particular by Washington's longstanding economic embargo against the island. </P>
<P> There was no official comment from Havana on the measures Clinton announced Saturday, but state radio said at mid-day the moves were aimed ``to reinforce the harassing of Cuba.'' Chiles and Jorge Mas Carnosa, head of the Cuban American National Foundation, and other Cuban-American leaders, were briefed on the measures at the White House. They welcomed them wholeheartedly. </P>
</TEXT>
</DOC>
