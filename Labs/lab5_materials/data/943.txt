<DOC>
<DOCID>REU010-0377.940822</DOCID>
<TDTID>TDT002318</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/22/94 18:29</DATE>
<TITLE> WIFE OF MISSING LIBYAN EX-MINISTER SEES U.N. CHIEF</TITLE>
<HEADLINE> WIFE OF MISSING LIBYAN EX-MINISTER SEES U.N. CHIEF</HEADLINE>
<SUBJECT> BC-UN-KIKHIA </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>UNITED NATIONS (Reuter) </DATELINE>
<P>  Secretary-General Boutros Boutros-Ghali Monday received the wife of missing Libyan dissident and former Foreign Minister Mansour Kikhia, who was apparently abducted in Cairo last December. </P>
<P> ``The secretary-general hopes he will reappear without further delay,'' U.N. spokesman Ahmad Fawzi told reporters. </P>
<P> Kikhia, who once served as Libya's U.N. envoy and later went into exile, was apparently abducted after attending a meeting in the Egyptian capital of the Arab Organization for Human Rights. </P>
<P> Boutros-Ghali, a former Egyptian deputy prime minister, urged the Egyptian authorities at the time to pursue their investigation, but there has been no word on Kikhia's fate. </P>
</TEXT>
</DOC>
