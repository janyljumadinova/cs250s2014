<DOC>
<DOCID>REU009-0021.940717</DOCID>
<TDTID>TDT000665</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/17/94 16:38</DATE>
<TITLE> KIM JONG-IL MOURNS BEFORE FATHER'S DELAYED FUNERAL</TITLE>
<HEADLINE> KIM JONG-IL MOURNS BEFORE FATHER'S DELAYED FUNERAL</HEADLINE>
<SUBJECT> BC-KOREA-MOURNING </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  North Korea's heir-apparent Kim Jong-il mourned his father Sunday alongside foreign visitors, the North Korean news agency KCNA reported on the day the country's long-time ruler Kim Il-sung's funeral was due to have taken place. </P>
<P> North Korea said Saturday it was delaying the funeral of Kim Il-sung, who died on July 8, from Sunday until Tuesday because of the increasing numbers of mourners wishing to pay their respects. </P>
<P> KCNA, monitored in London, said Kim Jong-il, 52, Kim Il-sung's son, gathered with foreign delegations and senior party and state officials to: ``express deep condolences before the bier of the respected leader President Kim Il-sung and stood by the bier as a guard of honor.'' </P>
<P> The agency described the foreign party, mainly Koreans resident abroad, as joining the man expected to take over the helm in Pyongyang in laying wreaths and a moment's silent tribute to ``the greatest man among the great men who devoted his whole life to the sovereignty and development of the country.'' </P>
<P> In neighboring South Korea officials have said the delay in Kim Il-sung's funeral could be a sign of possible problems in his son's succession or the North using the mourning period to consolidate Kim Jong-il's power base. </P>
</TEXT>
</DOC>
