<DOC>
<DOCID>REU004-0180.940808</DOCID>
<TDTID>TDT001633</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/08/94 16:54</DATE>
<TITLE> THREE RUNNING IN COLORADO G.O.P. GUBERNATORIAL RACE</TITLE>
<HEADLINE> THREE RUNNING IN COLORADO G.O.P. GUBERNATORIAL RACE</HEADLINE>
<SUBJECT> BC-USA-POLITICS-COLORADO </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>DENVER (Reuter) </DATELINE>
<P>  Colorado Republicans will choose Tuesday which of three candidates will face Democratic Governor Roy Romer in November, but millionaire oilman Bruce Benson appears to have the clear lead. </P>
<P> Romer, a former chairman of the National Governors' Association who is often mentioned as a possible cabinet officer, is unopposed in the primary. </P>
<P> Benson's opponents, state senator Mike Bird and Dick Sargeant, a two-time candidate for state treasurer, have made much of the money Benson has spent and the fact that he by-passed the state party caucuses to get on the ballot. </P>
<P> Benson, who has reported his wealth at about $24 million, has spent about $1 million of his own money on the campaign. </P>
<P> Recent polls shows Benson ahead of his two opponents, who have tried to paint him as a candidate who once supported the much delayed Denver International Airport. </P>
<P> Candidates can't move far enough to distance themselves from the new airport, the opening of which has been delayed four times. </P>
<P> But crime has become the big issue in this year's election after last summer's escalation in violent crimes. </P>
<P> Even Sargent's vow to slash state taxes has not seemed to give him much of a push. </P>
<P> In Denver's 1st congressional district, Pat Schroeder faces an opponent in a primary for the first time in 22 years. She is facing opposition from a Lyndon Larouche supporter, Tom Simpson. </P>
<P> In the 2nd congressional district four Republicans are vying for the chance to do battle in November with Democrat David Skaggs, who is seeking his fifth term. </P>
</TEXT>
</DOC>
