<DOC>
<DOCID>REU001-0271.940802</DOCID>
<TDTID>TDT001353</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/02/94 10:54</DATE>
<TITLE> DETAINED IRAN BOMB SUSPECT DIES OF BULLET WOUNDS</TITLE>
<HEADLINE> DETAINED IRAN BOMB SUSPECT DIES OF BULLET WOUNDS</HEADLINE>
<SUBJECT> BC-IRAN-BOMBING 2NDLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Updates with suspect's death)</NOTES>
<DATELINE>NICOSIA, Cyprus (Reuter) </DATELINE>
<P>  The main suspect in a June bomb attack which killed 26 people at Iran's holiest shrine died in a Tehran hospital Tuesday from bullet wounds he suffered during his arrest, Iran's IRNA news agency said. </P>
<P> Mahdi Nahvi, with bullet wounds in the abdomen and the spleen and under the collarbone, died a day after security agents caught up with him in Tehran and captured him following a shootout, IRNA said. </P>
<P> Iran's Intelligence Minister Ali Fallahiyan said Monday Nahvi had confessed that he belonged to the Iraq-based Mujahideen Khalq organization. </P>
<P> IRNA said Iranian television showed Nahvi on his hospital bed Monday night. When asked why he planted the bomb at the shrine in the northeastern city of Mashhad June 20, he said: ``I did it because Mujahideen Khalq .. .ordered me to do so.'' </P>
<P> The Mujahideen denied Monday it had any link with Nahvi. </P>
</TEXT>
</DOC>
