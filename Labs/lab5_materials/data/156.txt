<DOC>
<DOCID>REU005-0303.940710</DOCID>
<TDTID>TDT000356</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/10/94 14:54</DATE>
<TITLE> BRITAIN'S PRINCE EDWARD OFFERED THRONE OF ESTONIA</TITLE>
<HEADLINE> BRITAIN'S PRINCE EDWARD OFFERED THRONE OF ESTONIA</HEADLINE>
<SUBJECT> BC-BRITAIN-EDWARD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  Britain's Prince Edward was offered the throne Sunday -- of Estonia. </P>
<P> The Royalist Party of the former Soviet terrority sent a letter to Queen Elizabeth's youngest son asking him to be their king. </P>
<P> ``We would be most honored if you would accept this rare request. Your background as an actor and television producer would be ideal to create the majesty a new king would require to combine ancient culture with modern political reality,'' the letter, quoted in the Sunday Telegraph, said. </P>
<P> Estonia won its independence in 1991 along with Latvia and Lithuania. The Baltic republic of 1.5 million inhabitants has not announced any plans to change its system of government or to establish a monarchy. </P>
<P> Buckingham Palace confirmed that the letter from the Estonia Royalist Party, which has about 10 percent of the seats in parliament, had arrived but said the prince had not seen it. </P>
<P> When asked about the possibilty of Edward becoming Estonia's king, he said: ``It sounds a charming idea but I don't think it is in the realm of reality.'' </P>
</TEXT>
</DOC>
