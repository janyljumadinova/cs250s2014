<DOC>
<DOCID>REU014-0167.940729</DOCID>
<TDTID>TDT001204</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/29/94 15:07</DATE>
<TITLE> LOCAL CURRENCY HITS NEW LOW AGAINST DOLLAR</TITLE>
<HEADLINE> LOCAL CURRENCY HITS NEW LOW AGAINST DOLLAR</HEADLINE>
<SUBJECT> BC-HAITI-ECONOMY </SUBJECT>
<AUTHOR>     By Andrew Downie </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>PORT-AU-PRINCE (Reuter) </DATELINE>
<P>  Economic sanctions and desperate financial decisions helped push the value of Haiti's currency down to a new low against the U.S. dollar, capping a disastrous week for the economy of the Western Hemisphere's poorest nation, economists and diplomats said Friday. </P>
<P> After a week of decline, the Haitian gourde was trading Friday among street changers at around 16.5-17.5 gourdes for one U.S. dollar, compared with last week's rate of 16 gourdes. </P>
<P> The current range is the gourde's weakest level ever against the U.S. currency since it was pegged at five to one dollar during the American occupation early this century. </P>
<P> Haitian economists said the steady erosion in value was due to the scarcity of dollars brought on by U.N.- and U.S.-imposed economic sanctions, and a chronic lack of confidence in the banking system. </P>
<P> That situation was exacerbated two weeks ago by the military-backed government's decision to eliminate a ceiling on the amount of gourdes in circulation. </P>
<P> ``It's really crazy what is going on right now,'' Haitian Central Bank economist Kesner Pharel said. </P>
<P> The blanket trade ban imposed in May to try to unseat the military leaders who seized power in a bloody coup d'etat nearly three years ago dealt a devastating blow to Haiti's already feeble economy, economists said. </P>
<P> Last month's decision by President Clinton's to severely restrict money transfers between Haiti and the United States -- believed to total up to $300 million a year -- deprived the tiny Caribbean nation of its largest source of foreign income after international aid. </P>
<P> But economists and diplomats said it was the decision by the military regime to issue more money that sparked this week's dash for dollars. </P>
<P> ``There was a pent-up demand for dollars,'' said a U.S. diplomat. ``Once they released the notes (last week) there were people who took them to buy dollars.'' </P>
<P> Officials in the military-backed regime said they voted to eliminate the monetary ceiling to alleviate the paucity of hard cash, but skeptical analysts said the regime had awarded themselves a license to print money. </P>
<P> The Haitian central bank said Friday it would release some 100 million gourdes as a result of eliminating the ceiling. </P>
<P> The U.S. diplomat also said if the government goes ahead with its ambitious and costly plans to revamp public works programs and hold elections, deficit spending will soar and feed fears of further inflation. </P>
<P> A lack of confidence in the country's banking and political systems has added to the problem. </P>
<P> Although Haiti is still a long way from reaching the hyper-inflation seen in other Latin American countries in the 1980's, both Pharel and the U.S. official said they had noticed the beginning of the ``dollarization'' of the economy -- the increasing preference for dollars rather than gourdes as the means of exchange. </P>
<P> The Haitian economy has crumbled since the 1991 coup that toppled Jean-Bertrand Aristide and the decline has quickened in the weeks following the tightened international sanctions. </P>
<P> Tens of thousands of jobs have been lost since the sanctions were imposed in May and many thousands more are expected to go in the coming months, economists said. </P>
<P> Unemployment is estimated at around 75 percent and inflation, now at about 60 percent, is on a gradual but inexorable march towards three digits, Haitian financial experts said. </P>
<P> Although accurate figures are impossible to obtain, the most recent Haitian Central Bank figures show economic output plummetted by a massive 19 percent between 1991 and 1993. Per capita Gross National Product was last recorded at $252 but is widely thought to have fallen even further. </P>
<P> ``We are looking at economic catastrophe,'' said the U.S. official. </P>
</TEXT>
</DOC>
