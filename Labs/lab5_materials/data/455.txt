<DOC>
<DOCID>REU013-0315.940728</DOCID>
<TDTID>TDT001157</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/28/94 11:16</DATE>
<TITLE> BASKETBALL-USA BEATS RUSSIA TO TAKE GOODWILL BRONZE</TITLE>
<HEADLINE> BASKETBALL-USA BEATS RUSSIA TO TAKE GOODWILL BRONZE</HEADLINE>
<SUBJECT> BC-GOODWILL-BASKETBALL </SUBJECT>
<AUTHOR>     By David Ljunggren </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>ST PETERSBURG (Reuter) </DATELINE>
<P>  The United States basketball team captured the Goodwill Games bronze medal Thursday, withstanding a comeback by Russia to win 80-71 in a game marked by a long delay to repair a broken basket. </P>
<P> Play was interrupted for 45 minutes early in the second half after American Michael Finley slammed the ball into the basket with such force that he snapped a safety mounting. </P>
<P> Finlay's basket gave the U.S. team a 51-41 lead but the Russians, cheered on by a vociferous home crowd, fought back after play resumed and snatched a 60-59 lead with 6:17 left on the clock after Sergei Bazarevich sank two free shots. </P>
<P> But the Americans scored three quick three-pointers, two from Shawn Respert, to regain the advantage and they held off a late Russian charge to win with some ease. </P>
<P> The Russians looked as if they were still feeling the effects of their disastrous semifinal defeat to Puerto Rico Wednesday, when they lost 69-65 after leading 41-28 at halftime. </P>
<P> For the United States, who were beaten 81-72 by Italy in the other semifinal, the win was sweet revenge for a 79-77 defeat to Russia in a first-round match. </P>
<P> ``It's a difficult challenge. The coach has to lift the players from the depths of despair and challenge them to play as well as possible,'' said U.S. coach George Raveling. </P>
<P> The young U.S. team, comprised of college players, is a mere shadow of the one expected to romp away with the world championship in Toronto next week. </P>
<P> Russia often looked sluggish and missed 10 of their 30 free throws. Coach Sergei Belov criticized several of his leading players afterwards for performing poorly. </P>
<P> ``Every coach knows how difficult it is to play after a defeat like the one we had last night,'' he said. </P>
</TEXT>
</DOC>
