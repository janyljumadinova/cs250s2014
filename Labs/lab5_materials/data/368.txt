<DOC>
<DOCID>REU011-0354.940723</DOCID>
<TDTID>TDT000962</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/23/94 23:06</DATE>
<TITLE> TYPHOON WALT INCHING TOWARD SOUTHERN JAPAN</TITLE>
<HEADLINE> TYPHOON WALT INCHING TOWARD SOUTHERN JAPAN</HEADLINE>
<SUBJECT> BC-JAPAN-TYPHOON 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: updates with details.)</NOTES>
<DATELINE>TOKYO (Reuter) </DATELINE>
<P>  Typhoon Walt Sunday brought welcome rain to some parts of Japan hit by a drought as it headed toward the southern main island of Kyushu, weathermen said. </P>
<P> Walt, with peak winds at its center of more than 63 mph, was still off the Pacific coast of Kyushu Sunday morning, inching northwest. </P>
<P> Weather officials issued storm and flood warnings across southern Japan and said the southern tip of Kyushu could come within Walt's storm damage zone by Sunday evening. </P>
<P> The typhoon has already triggered torrential rains of up to 10.6 inches in Nara prefecture in western Japan and 4.4 inches at Nichinan city on Kyushu in 24 hours, the Meteorological Agency said. </P>
<P> But Walt failed to fill a reservoir on Shikoku, the island worst hit by the country's July heatwave. </P>
<P> The reservoir, which supplies the city of Takamatsu, is down to five percent of normal levels and city residents were limited to five hours of tap water a day. </P>
<P> City officials had started rationing bottled water. </P>
<P> Neighboring Kochi prefecture is expecting crop damage in the region of $8 million from the drought. </P>
<P> In Tokyo, residents were asked to cut their water use by five percent Friday. Businesses that use more than 3,500 cubic feet of water a day, such as hotels and department stores, were asked to reduce usage by 10 percent. </P>
</TEXT>
</DOC>
