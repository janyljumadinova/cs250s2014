<DOC>
<DOCID>REU008-0175.940715</DOCID>
<TDTID>TDT000610</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 16:57</DATE>
<TITLE> HEALTH REFORM GROUP BITES PIZZA HUT, MCDONALD'S</TITLE>
<HEADLINE> HEALTH REFORM GROUP BITES PIZZA HUT, MCDONALD'S</HEADLINE>
<SUBJECT> BC-HEALTH-FASTFOOD 1STLD </SUBJECT>
<AUTHOR>     By Joanne Kenen </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>WASHINGTON (Reuter) </DATELINE>
<P>  Senator Edward Kennedy and a health reform group took a bite out of Pizza Hut and McDonald's Friday for insuring workers in foreign countries but fighting any requirement that they do the same here. </P>
<P> The activists, known as the Health Care Reform Project, said Pizza Hut's lawyers had written threatening letters to Washington-area television stations where the group hoped to air commercials about the fast food companies' benefits policies. </P>
<P> The commercials will not air, and the group is now trying to place print advertisements in major Sunday newspapers. </P>
<P> Officials at McDonald's were not immediately available to comment. Pizza Hut said the group's report was ``false and plain dirty politics.'' </P>
<P> An incensed Kennedy, a Massachusetts Democrat who was one of several pro-health reform lawmakers joining the group's news conference, ``invited'' top Pizza Hut executives to ``come right in here to explain what is so wrong about these ads'' before his Senate Labor and Human Resources Committee next week. </P>
<P> One of the most controversial issues in the health care debate is whether to have an employer mandate -- a requirement that businesses pay most of their workers' insurance costs. </P>
<P> Small businesses have bitterly fought that requirement. Among larger companies, the restaurant and retail industries, which have many low-wage hourly workers, have also resisted, asserting that a mandate will be so expensive that they will have to slash jobs and maybe even shut businesses. </P>
<P> McDonald's and PepsiCo, which owns Pizza Hut, are both active in the Health Care Equity Action League (HEAL) a business group that strongly opposes mandates. </P>
<P> But the reform project said that McDonald's and Pizza Hut are flourishing in four foreign countries which require employers to chip in for health insurance, Germany, Japan, the Netherlands and Belgium. </P>
<P> ``They're selling lots of pizza and burgers and paying for health insurance for hourly workers at the same time, overseas,'' said Robert Chlopak, a consultant on the Health Reform's three month study. </P>
<P> ``If Pizza Hut and McDonald's can pay for health insurance for restaurant workers in Germany and Japan and still turn an honest good old American profit, why can't they do the same thing in America,'' he added. </P>
<P> The letter from Pizza Hut's attorneys to the Washington television stations, which was made public, held out the possibility of legal action if they broadcast ads saying Pizza Hut does not offer health care coverage to U.S. employees. </P>
<P> The health reform project said Pizza Hut ``offered'' insurance to hourly workers, and only paid a small portion to workers who first paid for 100 percent of the coverage for six months. </P>
<P> It said McDonald's contributed to management and corporate employees insurance, but not to hourly employees. </P>
</TEXT>
</DOC>
