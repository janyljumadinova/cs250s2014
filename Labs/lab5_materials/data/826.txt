<DOC>
<DOCID>REU008-0259.940817</DOCID>
<TDTID>TDT002042</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/17/94 07:36</DATE>
<TITLE> SERB-MUSLIM FIGHTING FLARES AGAIN IN SARAJEVO</TITLE>
<HEADLINE> SERB-MUSLIM FIGHTING FLARES AGAIN IN SARAJEVO</HEADLINE>
<SUBJECT> BC-YUGOSLAVIA-ZONE </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>SARAJEVO, Bosnia-Herzegovina (Reuter) </DATELINE>
<P>  Artillery battles between Bosnian government and rebel Serb forces flared again inside the U.N. heavy-weapons exclusion zone around Sarajevo, a U.N. peacekeeping spokesman said Wednesday. </P>
<P> He reported ``intense artillery and mortar activity'' Tuesday near Breza in the northern end of the 12-mile radius around Sarajevo a week after fighting in the same area almost provoked NATO air strikes against both factions. </P>
</TEXT>
</DOC>
