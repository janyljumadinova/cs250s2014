<DOC>
<DOCID>REU007-0063.940813</DOCID>
<TDTID>TDT001878</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/13/94 23:25</DATE>
<TITLE> U.S. FREES CUBANS SUSPECTED OF SMUGGLING</TITLE>
<HEADLINE> U.S. FREES CUBANS SUSPECTED OF SMUGGLING</HEADLINE>
<SUBJECT> BC-CUBA-VESSEL </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>MIAMI (Reuter) </DATELINE>
<P>  U.S. officials decided Saturday to free the crew of a Cuban fishing boat detained a day earlier on suspicion of smuggling refugees to the United States, a U.S. Coast Guard spokesman said. </P>
<P> The Coast Guard escorted the boat into international waters off Florida Saturday morning, the spokesman said. </P>
<P> The Coast Guard said it picked up 136 Cubans from rafts and boats in waters between Cuba and Florida on Saturday. </P>
<P> The service has rescued more than 5,500 Cubans this year, the highest level since the 1980 Mariel boatlift, in which 125,000 Cubans reached Florida over several months. </P>
<P> The number of Cubans picked up in the Straits of Florida has been creeping up over the past two weeks. ``The number is a little high relative to what we had been used to,'' said a Coast Guard spokesman. </P>
<P> The Coast Guard said the three fishermen were picked up Friday south of Key West, Florida, with five refugees in their 50-foot fishing boat. All eight Cubans were taken ashore for questioning by the Immigration and Naturalization Service and the FBI. </P>
<P> The three crewmembers, who said they were licensed Cuban fishermen whose vessel had been hijacked by the refugees, asked to be allowed to return to their homeland. </P>
<P> The fishing boat was detained a day after U.S. Attorney General Janet Reno announced that law officers would enforce a series of tough measures intended to prevent smuggling of refugees from the Communist-governed nation. </P>
<P> Angered with U.S. policy that he claims has led to hijackings and unrest in his country, Cuban President Fidel Castro has threatened to unleash a flood of refugees. </P>
<P> In 1980, U.S. officials did not enforce immigration law that would have prevented Cuban-Americans from taking to the seas to bring friends and relatives from Cuba. </P>
<P> At least two other boats have been detained since Thursday on suspicion of smuggling Cubans. </P>
<P> Havana is about 90 miles from Key West, Florida. Customs officials have long suspected Cuban fishermen of accepting bribes to carry refugees to the Florida Keys. </P>
</TEXT>
</DOC>
