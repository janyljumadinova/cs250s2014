<DOC>
<DOCID>REU005-0212.940810</DOCID>
<TDTID>TDT001714</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/10/94 16:58</DATE>
<TITLE> BOY GNAWED TO DEATH BY PET; PARENTS GET SIX YEARS</TITLE>
<HEADLINE> BOY GNAWED TO DEATH BY PET; PARENTS GET SIX YEARS</HEADLINE>
<SUBJECT> BC-USA-RAT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>FULLERTON, Calif (Reuter) </DATELINE>
<P>  A homeless couple whose infant son was gnawed to death by the family's pet rat were each sentenced Wednesday to six years in prison after being found guilty of child neglect. </P>
<P> Prosecutor James Tanizaki said during the parents' trial the rat, named ``Homer,'' had bitten the four-month-old boy more than 100 times as he lay in the trash-strewn car where he lived with his mother, father and four-year-old sister. </P>
<P> The boy bled to death, Tanizaki said. </P>
<P> Orange County Superior Court Judge Kazuharu Makino said she was sentencing the child's parents, Steven and Kathleen Giguere, to the maximum term because they had sacrificed their children's welfare for drugs. </P>
<P> ``These two children were solely dependent on their mother and father for their welfare. These people were given public money for the welfare of their children. </P>
<P> ``Instead, that money went for drugs. They used that money for their own self interest so much that they couldn't pay rent, they couldn't buy food, they had to live in their car,'' Makino said. </P>
<P> Defense attorneys had argued unsuccessfully that the boy died of possible sudden death syndrome before the rat bit him him last August. </P>
</TEXT>
</DOC>
