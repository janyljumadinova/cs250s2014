<DOC>
<DOCID>REU014-0316.940730</DOCID>
<TDTID>TDT001247</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/30/94 13:28</DATE>
<TITLE> EUROPEAN MEETING SAYS DECRIMINALIZE PROSTITUTION</TITLE>
<HEADLINE> EUROPEAN MEETING SAYS DECRIMINALIZE PROSTITUTION</HEADLINE>
<SUBJECT> BC-EUROPE-SEX </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>EDINBURGH, Scotland (Reuter) </DATELINE>
<P>  The first Europe-wide conference of prostitutes, health workers and policy makers will call on the European Commission and the World Health Organization to decriminalize prostitution, delegates said. </P>
<P> For the last three days 130 delegates from more than 30 countries have been debating health and the sex industry behind closed doors at Heriot Watt University near Edinburgh. </P>
<P> ``Decriminalizing prostitution will not solve all the problems, but it would go a long way to help,'' said Dr David Bell, a public health consultant from Dundee, Scotland, who took part in the conference sessions Saturday. </P>
<P> ``No one can look after their own health properly unless they are in charge of themselves -- and under current legislation, prostitutes seldom are.'' </P>
<P> A report containing final recomendations from the conference will be presented to WHO and the European Commission in October, delegates said. </P>
<P> Members of the world's oldest profession want to work on a ``proper business footing like anyone else, to be free of the stigma attached to prostitution and to have the same access to health care as other members of society,'' said Ruth Morgan Thomas, one of the conference organizers. </P>
<P> Contrary to public assumptions, prostitutes are not ``riddled with HIV and AIDS,'' the university graduate and former prostitute added. </P>
<P> ``Working girls have been fighting sexually transmitted diseases for a long time. Syphilis was a killer and so is AIDS. And whatever the general public might think, you are actually far more likely to pick up the HIV virus through casual, unprotected, non-commercial sex than you are from going to a prostitute,'' Morgan Thomas said. </P>
<P> Another concern expressed at the conference was that many eastern European countries and former Soviet satellites may criminalize prostitution. </P>
<P> ``In Estonia, for example, prostitution is not illegal because under the Communist system, as it didn't officially exist, there was no need to have a law against it. Our fear is that it may now be criminalized,'' Morgan Thomas said. </P>
</TEXT>
</DOC>
