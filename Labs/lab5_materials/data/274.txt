<DOC>
<DOCID>REU009-0095.940718</DOCID>
<TDTID>TDT000674</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/18/94 01:53</DATE>
<TITLE> RWANDAN REBELS NAME PRESIDENT</TITLE>
<HEADLINE> RWANDAN REBELS NAME PRESIDENT</HEADLINE>
<SUBJECT> BC-RWANDA-PRESIDENT </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>NICOSIA, Cyprus (Reuter) </DATELINE>
<P>  The rebel Rwandan Patriotic Front (RPF) has named its representative in Brussels, Pasteur Bizimungu, as the new president of Rwanda, Radio Rwanda reported Monday. </P>
<P> The radio was monitored by the British Broadcasting Corporation. </P>
</TEXT>
</DOC>
