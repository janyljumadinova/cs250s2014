<DOC>
<DOCID>REU011-0394.940724</DOCID>
<TDTID>TDT000971</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/24/94 08:17</DATE>
<TITLE> POPE SLAMS CONTRACEPTION AHEAD OF U.N. CONFERENCE</TITLE>
<HEADLINE> POPE SLAMS CONTRACEPTION AHEAD OF U.N. CONFERENCE</HEADLINE>
<SUBJECT> BC-POPE-CONTRACEPTION </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>CASTELGANDOLFO, Italy (Reuter) </DATELINE>
<P>  Pope John Paul II, keeping up his stiff opposition to contraception ahead of a U.N. conference on population, Sunday slammed the use of artificial methods of birth control. </P>
<P> In an address to the faithful from his summer retreat at Castelgandolfo, north of Rome, the pope admitted that the growth of the world's population was worrying. </P>
<P> But, in remarks aimed at the United Nations conference on population in September, he added that not enough was being done to encourage natural methods of family planning. </P>
<P> The Catholic church rejects abortion and other artificial means of contraception and only accepts natural ones such as the rhythm method, based on when a woman is infertile. </P>
<P> ``How can one not be worried in front of the willingness to spend large sums of money to distribute contraceptive means which are ethically unacceptable?'' he said. </P>
<P> Referring to the September 5-13 conference in Cairo, the pope said the Church understood the problems posed by a rising world population. But he said it was up to individual couples and not public authorities to control demographic growth. </P>
<P> The pope's call followed an outspoken attack on the conference last week by a senior Vatican official who said it would cause ``the most disastrous massacre in history'' if it sanctioned abortion for family planning. </P>
</TEXT>
</DOC>
