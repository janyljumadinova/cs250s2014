<DOC>
<DOCID>REU008-0116.940715</DOCID>
<TDTID>TDT000599</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/15/94 13:55</DATE>
<TITLE> BASKETBALL STAR DERRICK COLEMAN DENIES ASSAULT</TITLE>
<HEADLINE> BASKETBALL STAR DERRICK COLEMAN DENIES ASSAULT</HEADLINE>
<SUBJECT> BC-USA-ASSAULT 1STLD </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Adds Coleman's statement, release from questioning)</NOTES>
<DATELINE>DETROIT (Reuter) </DATELINE>
<P>  New Jersey Nets basketball star Derrick Coleman denied a woman's allegations that he sexually assaulted her and was allowed to leave a downtown Detroit hotel room where police questioned him Friday. </P>
<P> No charges have been brought against Coleman, who left the Westin Hotel Renaissance Center early Friday afternoon after several hours of questioning by police investigators. </P>
<P> Coleman, 26, told reporters that he ``had no involvement whatsoever'' with the 22-year-old woman. </P>
<P> ``False allegations have been made against me and I gave the police my statement,'' he said. ``I'm just waiting to see what happens. In the end, I mean, the truth will come out.'' </P>
<P> A police spokesman said the woman filed a complaint stating that she met Coleman in a Detroit bar Thursday night and later went with him to a room in the hotel, where he assaulted her. </P>
<P> ``A woman alleges that Mr Coleman sexually assaulted her in a room at the Westin Hotel about 2:30 a.m.,'' Officer John Leavens told Reuters. ``The allegations that she's made are being investigated. No one has been charged.'' </P>
<P> Coleman, a Detroit native who has played with the Nets for four years, said he was on his way to a family reunion party Friday. The 6-foot-10-inch, 235-pound forward, who attended Syracuse Univeristy, is considered one of the best players in the National Basketball Association. </P>
<P> A spokesman for the New Jersey Nets declined to comment Friday on the woman's allegations. </P>
</TEXT>
</DOC>
