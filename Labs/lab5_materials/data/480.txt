<DOC>
<DOCID>REU014-0166.940729</DOCID>
<TDTID>TDT001203</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/29/94 14:51</DATE>
<TITLE> HIZBOLLAH WARNS OF U.S.-ISRAELI ATTACK</TITLE>
<HEADLINE> HIZBOLLAH WARNS OF U.S.-ISRAELI ATTACK</HEADLINE>
<SUBJECT> BC-LEBANON-USA-HIZBOLLAH </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>BEIRUT, Lebanon (Reuter) </DATELINE>
<P>  Lebanon's pro-Iranian Hizbollah (Party of God) warned Friday of an imminent U.S.-Israeli attack and said any such assault was doomed to fail. </P>
<P> ``We stress that we will be faithful to the resistance, thus any American-Zionist aggression that might take place will not be a picnic and will not pass without a response of an appropriate scale,'' Hizbollah said. </P>
<P> ``The resistance has proven its capabilities and thus it warns the enemy and its backers from getting involved in any adventure,'' said the statement, read by the group's chief, Sheik Hassan Nasrallah. </P>
<P> Hizbollah issued the statement after a meeting of its leadership in light of what it called Israeli threats and U.S. calls for its defeat after a string of bombings against Israeli and Jewish targets in Argentina, London and Panama. </P>
<P> Hizbollah denounced the United States and said statements by Secretary of State Warren Christopher provided ``the complete cover for the Zionist entity to resume a large scale aggression against Lebanon, its people and resistance.'' </P>
<P> Christopher said Thursday that ``groups like Hizbollah that wreak havoc and bloodshed must be defeated and Hizbollah's patron, Iran, must be contained.'' </P>
<P> Hizbollah, which is funded, trained and equipped by Tehran, urged the Argentine and British governments to seek accuracy before issuing accusations. Both countries charged Islamic fundamentalists linked to Iran with involvement in the blasts. </P>
</TEXT>
</DOC>
