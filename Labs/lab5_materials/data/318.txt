<DOC>
<DOCID>REU010-0191.940720</DOCID>
<TDTID>TDT000796</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/20/94 14:53</DATE>
<TITLE> PROSECUTOR ARRESTED IN DRAG ATTACK AT GAY CLUB</TITLE>
<HEADLINE> PROSECUTOR ARRESTED IN DRAG ATTACK AT GAY CLUB</HEADLINE>
<SUBJECT> BC-USA-GAYS </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES> (Eds: Note language in 5th para)</NOTES>
<DATELINE>BOCA RATON, Fla (Reuter) </DATELINE>
<P>  A state prosecutor and three rugby teammates were arrested after allegedly entering a gay bar dressed in women's clothes, screaming insults at the patrons and vandalizing the place, police said Wednesday. </P>
<P> The suspects were arrested Monday night a few blocks away from the Paradise Club, which attracts a homosexual clientele. The Palm Beach County sheriff's office charged them with disorderly intoxication after club patrons identified them. </P>
<P> One suspect wore a black slip dress with spaghetti straps, one wore a yellow dress and the other two were clad in yellow and blue miniskirts and T-shirts. </P>
<P> ``It didn't take too much to identify them,'' said Paradise employee Steve Pace. ``It was like a scary day from the drag shows.'' </P>
<P> Witnesses said the four were among 10 men who entered the bar clad in dresses, stockings and wigs and paraded around the bar dancing while yelling, ``Homos, faggots, dykes.'' The men ripped plants and pictures from the walls, and some began to strip while demanding free beer, the police report said. </P>
<P> The men left after scaring away half the patrons, Pace said. Six escaped, but a policeman spotted the other four in a parking lot, still wearing dresses and carrying a mannequin's torso. </P>
<P> Those arrested included Mark McHugh, 32, an assistant state attorney who manages Broward County's felony crime unit in Fort Lauderdale. McHugh was suspended from his supervisory duties and could face suspension from his job if formal charges are filed, chief assistant state attorney Ralph Ray said. </P>
<P> McHugh's attorney, David Bogenshutz, called it a case of mistaken identity and said McHugh was never in the Paradise Club. </P>
<P> The suspects were members of a rugby club and had spent the day golfing in women's clothes in a tournament spoofing the Ladies Professional Golf Association. </P>
</TEXT>
</DOC>
