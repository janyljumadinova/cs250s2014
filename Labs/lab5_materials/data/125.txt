<DOC>
<DOCID>REU004-0309.940708</DOCID>
<TDTID>TDT000287</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 12:24</DATE>
<TITLE> TURKEY-IRAQ PIPELINE DEAL OFF FOR ABOUT TWO WEEKS</TITLE>
<HEADLINE> TURKEY-IRAQ PIPELINE DEAL OFF FOR ABOUT TWO WEEKS</HEADLINE>
<SUBJECT> BC-TURKEY-IRAQ-UN </SUBJECT>
<AUTHOR>     By Evelyn Leopold </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>UNITED NATIONS (Reuter) </DATELINE>
<P>  Security Council approval of a deal to allow Turkey to repair and flush out its oil pipeline with Iraq will be delayed at least two weeks, diplomatic sources said Friday. </P>
<P> Turkey had hoped the resolution would be before the council by the end of this week and adopted next week but neither the United States nor Iraq have yet approved the fine points of the deal. </P>
<P> The council conducts its regular review of sanctions against Iraq about July 18 and Baghdad's Deputy Prime Minister Tareq Aziz is scheduled to be in New York next week to lobby council members to lift the oil embargo. </P>
<P> Envoys said he would probably be involved in pipeline talks also and some delegations thought it best the Turkey-Iraq deal not go before the council until after the sanctions review. </P>
<P> A 616-mile pipeline between Turkey and Iraq has been idle and rusting since sanctions were imposed on Baghdad after its invasion of Kuwait in August 1990. Turkey wants to repair it and needs Iraq's consent to empty out about 27 million barrels in several flushings of the line. </P>
<P> Turkey says Iraq owes it 3.8 million barrels. Iraq would be reimbursed for the remainder of the estimated $300 million by receiving humanitarian goods that are not barred under sanctions. </P>
<P> Baghdad had consented to several major conditions for the deal -- paying 30 percent of the value of the oil into a U.N. compensation fund for Gulf War victims and putting all proceeds into a U.N. escrow account. </P>
<P> But it still objects to U.S.-initiated restrictions on buying and distributing the relief supplies. </P>
<P> The food and medical supplies are to be ``distributed equitably'' among all Iraqis, including the Kurds in the north, not under Baghdad's control. Iraq objects to such wording in any council resolution as an illegal infringement of its sovereignty, diplomats said. </P>
<P> Turkish envoys said there was still a question about how much food will be going to Kurds in the north and who will supervise the distribution of supplies in the rest of the country. </P>
<P> Washington is eager to accomodate Turkey but at the same time wants to make sure sanctions stay in place against Iraq. </P>
</TEXT>
</DOC>
