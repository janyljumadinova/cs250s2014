<DOC>
<DOCID>REU001-0395.940802</DOCID>
<TDTID>TDT001366</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/02/94 18:56</DATE>
<TITLE> ARKANSAS PREPARES FOR TRIPLE EXECUTIONS</TITLE>
<HEADLINE> ARKANSAS PREPARES FOR TRIPLE EXECUTIONS</HEADLINE>
<SUBJECT> BC-USA-EXECUTION </SUBJECT>
<AUTHOR>     By Steve Barnes </AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LITTLE ROCK, Ark (Reuter) </DATELINE>
<P>  Arkansas is preparing to execute three convicted killers Wednesday, the first triple execution in the United States in 34 years. </P>
<P> Prison officials said Tuesday that the first execution will begin at 7 p.m. CDT (8 p.m. EDT) Wednesday and that the other two would follow ``as soon as is practicable.'' </P>
<P> Any hopes for a last-minute stay of execution seemed to evaporate Monday when the Eighth U.S. Circuit Court of Appeals refused to block the execution. </P>
<P> The U.S. Supreme Court has refused to hear further appeals and Gov. Jim Guy Tucker has refused the men a commutation to life imprisonment. </P>
<P> Darryl Richley, 43, Hoyt Clines, 37, and James Holmes, 37, were convicted of capital murder for the 1981 killing of a prosperous northwest Arkansas businessman, Don Lehman. </P>
<P> Testimony during the trial indicated that four men forced their way into the Lehman home, tied Lehman, beat him with a bicycle chain and shot him to death, forcing his wife and daughter to watch. </P>
<P> The fourth defendant, Michael Orndorff, was also condemned to death, but a procedural error resulted in his sentence being reduced to life imprisonment. </P>
<P> The three condemned men, to be executed by lethal injection, have been moved from Death Row to isolation cells near the death chamber. </P>
<P> Arkansas prison regulations allow condemned inmates visits with family on the day prior to an execution, with only attorneys and spiritual advisors permitted access on the day the sentence is to be carried out. </P>
<P> ``The warden informs me that all is quiet and that the necessary steps are being taken to make ready the execution process,'' prison spokesman Alan Ables said Tuesday. </P>
<P> ``Other than the obvious fact that the sentences of three men are to be carried out tomorrow night, there's nothing out of the ordinary to report,'' he said. </P>
</TEXT>
</DOC>
