<DOC>
<DOCID>REU012-0445.940726</DOCID>
<TDTID>TDT001065</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/26/94 16:35</DATE>
<TITLE></TITLE>
<HEADLINE></HEADLINE>
<SUBJECT> BC-MIDEAST-SUMMIT-CLINTON -2 WASHINGTON </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE></DATELINE>
<P> The three leaders appeared at a joint news conference only hours after King Hussein and Rabin addressed Congress and a day after Jordan and Israel signed a declaration ending the 46-year state of war between their two nations. </P>
<P> ``Today, I have reaffirmed to Prime Minister Rabin that as Israel moves forward in the peace process, the constant responsibility of the United States will be to help ensure its security,'' Clinton said. </P>
<P> ``I've also reaffirmed to King Hussein my determination to assist Jordan in dealing with its burden of debt and its defense requirements. I am working with Congress to achieve rapid action on both these matters,'' he said, referring to Amman's nearly $1 billion U.S. debt. </P>
<P> Clinton said ``the enemies of peace have not been silent'' even as the peace process unfolds, pointing out the bombing in London and one at a Jewish center in Buenos Aires last week. ''We will not, we must not allow them to disrupt this peace process,'' he said. </P>
</TEXT>
</DOC>
