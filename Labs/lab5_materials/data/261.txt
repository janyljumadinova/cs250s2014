<DOC>
<DOCID>REU008-0356.940716</DOCID>
<TDTID>TDT000649</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/16/94 18:46</DATE>
<TITLE> KING DISAGREED WITH CHURCHILL OVER WAR - BOOK</TITLE>
<HEADLINE> KING DISAGREED WITH CHURCHILL OVER WAR - BOOK</HEADLINE>
<SUBJECT> BC-BRITAIN-CHURCHILL </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LONDON (Reuter) </DATELINE>
<P>  Britain's royal family wanted to appease Hitler and Mussolini to avoid war in Europe and did not want Winston Churchill to become prime minister and lead the country after World War Two started, a new book alleges. </P>
<P> Historian Andrew Roberts says in his book ``Eminent Churchillians'' that King George VI, father of the present Queen Elizabeth II, resisted appointing Churchill prime minister. </P>
<P> ``It was not until Christmas 1940, seven months after Churchill became prime minister, that King George VI and his queen (now the queen mother) came round to him. Indeed, until then relations between them were characterized by mistrust and antagonism,'' says the book, serialized in the Sunday Times. </P>
<P> The book also claims the royal family had prepared to flee to Canada if Britain was invaded and, despite making a huge show of supporting their subjects while the country was being bombed by Germany and enduring terrible deprivation, dined luxuriously on fine food and wine. </P>
<P> Roberts says the royal family enthusiastically supported Churchill's predecessor, Neville Chamberlain, and agreed with him on the policy of trying to soothe the leaders of Germany and Italy, rather than challenging them. </P>
<P> ``In the run-up to the Second World War. the royal family was for appeasement of the dictators Hitler and Mussolini,'' he says. </P>
<P> ``They never expressed a word of sympathy for the Nazis; they probably despised them. But neither were they noticeably concerned about the plight of the Nazis' victims, generally considering a country's internal political arrangements to be its own affair.'' </P>
<P> But the royal family was very much involved in Britain's politics, Roberts writes. </P>
<P> ``The palace did what it could to offset the political crisis of May 1940 which brought Chamberlain down and Churchill to power,'' he says. </P>
<P> ``In the appointment of Churchill in 1940, one of the very few moments this century when the royal prerogative came decisively into play, with the political establishment split and the decision really resting with the crown, the king would have preferred any halfway suitable compromise candidate to Churchill to lead Britain in the Second World War,'' Roberts wrote. </P>
<P> He quotes King George's explanation of his appointment of Churchill as prime minister. ``There was only one person I could send for,'' the king said. </P>
<P> Roberts writes: ``Whenever that phrase appears, emphasis has been put on the ``one person' as though Churchill was the obvious choice. In fact the emphasis ought to be placed on ``only,' because there was no viable alternative.'' </P>
<P> He says the king was bitterly disappointed when then-Foreign Secretary Lord Halifax backed out as a candidate. </P>
</TEXT>
</DOC>
