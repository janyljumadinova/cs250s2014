<DOC>
<DOCID>REU004-0340.940708</DOCID>
<TDTID>TDT000290</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>07/08/94 14:19</DATE>
<TITLE> FLOODING DEATH TOLL RISES TO 20 IN SOUTHEASTERN U.S.</TITLE>
<HEADLINE> FLOODING DEATH TOLL RISES TO 20 IN SOUTHEASTERN U.S.</HEADLINE>
<SUBJECT> BC-USA-WEATHER 1STLD </SUBJECT>
<AUTHOR>     By Stan Darden </AUTHOR>
<TEXT>
<NOTES>(Eds: Updates death toll, adds report two infants killed)</NOTES>
<DATELINE>ATLANTA (Reuter) </DATELINE>
<P>  The death toll from widespread flooding in the southeastern United States rose to 20 Friday as skies over Georgia, Alabama and northern Florida began to clear for the first time in five days. </P>
<P> Ken Davis, spokesman for the Georgia Emergency Management Agency, said his office had received reports that two toddlers were swept to their deaths on the rampaging Flint River in Albany. </P>
<P> The deaths of the toddlers brought to 19 the number of deaths attributed to the flood in Georgia, with one death reported in Alabama. </P>
<P> The two Albany deaths could not be independently confirmed because of the inability to reach the Dougherty County Emergency Management Center by telephone. </P>
<P> As National Weather Service radar screens showed rains from the remnants of Tropical Storm Alberto diminishing, emergency crews continued to battle rising flood waters in many areas. </P>
<P> Nearly 500,000 people remained without drinking water across Georgia and tens of thousands were still homeless after being evacuated from paralyzed towns and farmlands across the region. </P>
<P> Georgia, where estimated damages already top $250 million, was declared a national disaster area by President Clinton Thursday. Alabama and Florida officials are also expected to be declared disaster areas. </P>
<P> More than a quarter million acres of crops have been flooded, leading a southeastern farm trade group to predict that weather damage would reduce the U.S. peanut crop by 10 percent this year. </P>
<P> The devastation also disrupted rail service through the southeast, shutting down Amtrak passenger travel along the Florida panhandle corridor from Miami to Los Angeles and causing delays of up to three days for freight between Florida and northern cities including Chicago. </P>
<P> ``I don't think any of us who have been on this trip today will ever forget what we have seen,'' Georgia Governor Zell Miller told reporters after returning from a helicopter tour of devastated areas. </P>
<P> ``This state has suffered a lot of disasters over its long history. But I don't think there has ever been one, certainly in recent times, that has been more far-reaching and more comprehensive and more costly.'' </P>
<P> Meanwhile, Alabama reported its first death, a 31-year-old man who drowned Thursday near the city of Geneva, 100 miles southeast of Montgomery. Geneva is one of two Alabama cities evacuated because of rising waters along the Pea River. </P>
<P> All other fatalities have occurred in Georgia, nine in the rural southwestern Sumter County area, which was virtually cut off by washed-out roads and bridges earlier in the week after more than 21 inches of rain fell in 24 hours. </P>
<P> Emergency management officials said Friday that the biggest problems lay 200 miles south of Atlanta in the city of Albany, where 11,000 people were evacuated Thursday as the Flint River overflowed banks and levees and emergency crews braced for a wave of flood waters from far upstream. </P>
<P> The river, which stretches from northern Georgia to Lake Seminole on the Florida border, was expected to crest Saturday at an unprecedented 24 feet above flood stage. Local officials in the Albany area requested helicopters equipped with night vision and rescue capabilities to search for the missing and crack down on looters. </P>
</TEXT>
</DOC>
