<DOC>
<DOCID>REU002-0164.940803</DOCID>
<TDTID>TDT001416</TDTID>
<SOURCE>REUTERS</SOURCE>
<DATE>08/03/94 16:56</DATE>
<TITLE> PERU OPPOSED TO USE OF FORCE IN HAITI</TITLE>
<HEADLINE> PERU OPPOSED TO USE OF FORCE IN HAITI</HEADLINE>
<SUBJECT> BC-HAITI-PERU </SUBJECT>
<AUTHOR></AUTHOR>
<TEXT>
<NOTES></NOTES>
<DATELINE>LIMA (Reuter) </DATELINE>
<P>  Peru said Wednesday it opposed a United Nation Security Council resolution authorizing the use of force in Haiti and would support diplomatic initiatives to restore democracy in the Caribbean nation. </P>
<P> ``Peru considers that the use of force would heighten the suffering of the Haitian people,'' the Foreign Ministry said in a statement. </P>
<P> Use of force would be ``incompatible with the juridical traditions of the Hemisphere of respect for non-intervention in the internal affairs of its states,'' it added. </P>
<P> The U.N. Security Council over the weekend approved a resolution allowing a United States-led invasion to restore democracy in Haiti. </P>
<P> The statement said Peru reiterated its firm belief that ''the legitimate government and state of law should be restored in Haiti,'' adding it condemned ``the Haitian regime's intransigence in finding a peaceful settlement to the crisis.'' </P>
<P> Once the legitimate government was restored, Peru would evaluate its participation in non-military actions if asked by international organizations, the statement concluded. </P>
<P> Foreign Minister Efrian Goldenberg, meanwhile, said Peru supported a Venezuelan initiative to form a high-level mission to negotiate the exit of Haiti's de facto regime and restore the elected government of ousted President Jean-Bertrand Aristide. </P>
</TEXT>
</DOC>
