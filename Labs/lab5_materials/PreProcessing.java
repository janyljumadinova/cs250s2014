import java.io.*;
import java.util.*;
/* Program to do basic preprocessing steps for the information
 * retrieval system:
 * - read files, clean out the documents
 * - find keywords
 * - remove stop words
 * - perform stemming
 * - print the summary of the results  
 */

public class PreProcessing {

	public static void main(String[] args) {
		
		HashMap<String,Integer> dict = new HashMap<String, Integer>();
				
		try {
			File dir = new File("data");
			System.out.println("Start up directory " + System.getProperty("user.dir"));
			
		    String[] children = dir.list();
		    if (children == null) {
		        // Either directory does not exist or is not a directory
		    } else {
		        for (int i=0; i<children.length; i++) {
		            // Get filename of file or directory
		            String filename = children[i];
		            String strRaw ="";
		            //System.out.print("Checking file: " + filename);
		            strRaw = readFileAsString(dir + File.separator + filename);
					
		            String strippedSGML = strRaw.replaceAll("<(.|\n)*?>", ""); // remove all SGML tags 
					strippedSGML = strippedSGML.toLowerCase(); // make lower case
					//System.out.println(strippedSGML);
					
					// take out all punctuation & new lines
					String[] strTokens = strippedSGML.split("[ `,'.()-;?#]|\r\n|\r|\n");
					for (int j=0; j< strTokens.length; j++){
						if (strTokens[j].length()> 0)
							if (dict.containsKey(strTokens[j])) {
								// if the dictionary contains our word, just increment the value
								dict.put(strTokens[j], dict.get(strTokens[j])+1);
							}
							else {
								// otherwise add it to our dictionary, first time we've seen it
								dict.put(strTokens[j], 1);
							}
							//System.out.println(strTokenized[j]);						
					}
		        }
		    }
		    
		    // limit output to first 10 and last 10 keywords
		    printSummary(dict);
		    
		    // remove stop words, expects "stopwords.txt" file in same directory
		    removeStopWords(dict);
		    printSummary(dict);
		    
		    //integrate Porters Stemmer algorithm
		    stemKeywords(dict);
		    printSummary(dict);
		    			
		} catch (Exception e) {
            // If something unexpected happened print exception information and quit 
            e.printStackTrace();
            System.exit(1);
        }
		
	}
	private static void printSummary(HashMap<String,Integer> d){
	    System.out.println("Total # of Keywords: " + d.size());
	    printFirstAndLast10(d);
	}
	@SuppressWarnings("unused")
	private static void printAllSorted(HashMap<String,Integer> d){
	    // This code sorts outputs HashMap sorting it by values
	    
        // First we're getting values array 
        ArrayList<Integer> values = new ArrayList<Integer>();
        values.addAll(d.values());
        // and sorting it (in reverse order)
        Collections.sort(values, Collections.reverseOrder());
 
        int last_i = -1;
        // Now, for each value 
        for (Integer i : values) {
            if (last_i == i) // without duplicates 
                continue;
            last_i = i;
            // we print all hash keys 
            for (String s : d.keySet()) {
                if (d.get(s) == i) // which have this value 
                    System.out.println(i + " : " + s);
            }
            // pretty inefficient, but works 
        }
		
	}
	private static void printFirstAndLast10(HashMap<String,Integer> d){
		System.out.println("First 10 Keywords:");
		printNSorted(d, 10, Collections.reverseOrder());
		System.out.println("Last 10 Keywords:");
		printNSorted(d, 10, null); // null gives natural sort order
	}
	private static void printNSorted(HashMap<String,Integer> d, int num, Comparator <? super Integer> order){
	    // This code sorts outputs HashMap sorting it by values
	    
        // First we're getting values array 
        ArrayList<Integer> values = new ArrayList<Integer>();
        values.addAll(d.values());
        // and sorting it (in reverse order)
        Collections.sort(values, order);
 
        int last_i = -1;
        int count = 0;
        // Now, for each value 
        for (Integer i : values) {
            if (last_i == i) // without duplicates 
                continue;
            last_i = i;
            // we print all hash keys 
            for (String s : d.keySet()) {
                if (d.get(s) == i){ // which have this value
                	count++;
                    System.out.println(i + " : " + s);
	                if (count > num) {
	                	break; // exit for after printing all requested values
	                }
                }
            }
            if (count > num) {
            	break; // no really, exit for after printing all requested values
            }
        }
	}
	private static void removeStopWords(HashMap<String,Integer> d){
		//take out our stop words from the dictionary
		System.out.println("\nRemoving Stop Words...");
		
		try {
			String strStopWords = readFileAsString("stopwords.txt");
			String strTokens[] = strStopWords.split("[ `,'.()-]|\r\n|\r|\n");
			for (int j=0; j< strTokens.length; j++){
				if (strTokens[j].length()> 0)
					if (d.containsKey(strTokens[j])) {
						// if the dictionary contains our remove it
						d.remove(strTokens[j]);
					}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	private static void stemKeywords(HashMap<String,Integer> d){
		// perform the porters stemming algorithm on the keywords
		// poorly -> poor, jumps -> jump, etc etc
		System.out.println("\nPerforming Stemming and combining keys...");
		
		// take each key, find it's stem, if it does not match it's stem, 
		// remove the key, and add it's value in to the using the stem as key
		Stemmer Stem = new Stemmer();

		//Set<String> keys = ; // create a copy of d.keySet so we can still modify d
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(d.keySet());
        
		for (String s: keys){
			
			Stem.add(s.toCharArray(), s.length());
			Stem.stem();
			String strStem = Stem.toString();
			
			//System.out.println("Original: '" + s + "' \tStem: '" + strStem + "'");
			
			if (s.compareTo(strStem) == 0) {
				//key matches it's stem, do nothing extra
				//System.out.println("Stem matches string");
			}
			else {
				//key does not match it's stem, remove it from the 
				//dictionary, add it's value to it's stem's value
				//System.out.println("Stem does not match string");
				
				//get the original key's value
				int key_val = d.get(s);
				
				// add it back to the dictionary adding it to the value for the stem
				if (d.containsKey(strStem)){
					// if we already have the stem, add it to the value there
					d.put(strStem, d.get(strStem)+key_val);
				}
				else {
					// if we don't already have the stem, create it here
					d.put(strStem, key_val); // 
				}
				
				// remove the original key since we just added it to it's stem
				d.remove(s);
			}
			
		}
		
	}
    private static String readFileAsString(String filePath)
    throws java.io.IOException{
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }

}

